# Third Party Components

The Digital Earth Viewer uses a set of third party components to achieve its functionality. These third party components are licensed under a set of diverse, but compatible open source licenses and listed below by area of interest.

## Client side libraries
These libraries are used for the client-side application
Library | Author | License | Repository
--------|--------|---------|-----------
Vue | Evan You & Contributors | [MIT](https://github.com/vuejs/vue/blob/dev/LICENSE) | https://github.com/vuejs/vue
lru_map | Rasmus Andersson | [MIT](https://github.com/rsms/js-lru/blob/master/LICENSE.txt) | https://github.com/rsms/js-lru
marked | Tony Brix & Contributors | [MIT](https://github.com/markedjs/marked/blob/master/LICENSE.md) | https://github.com/markedjs/marked
core-js | Denis Pushkarev & Contributors | [MIT](https://github.com/zloirock/core-js/blob/master/LICENSE) | https://github.com/zloirock/core-js

## Server Base Libraries
These libaries power the webserver, plugin loader and scheduler
Library | Author | License | Repository
--------|--------|---------|-----------
alcro | Srinivasa Mahesh | [MIT](https://choosealicense.com/licenses/mit) or [Apache](https://choosealicense.com/licenses/apache-2.0) | https://github.com/Srinivasa314/alcro
tokio | Carl Lerche & Contributors | [MIT](https://github.com/tokio-rs/tokio/blob/master/LICENSE) | https://github.com/tokio-rs/tokio
futures | Alex Crichton & Contributors | [MIT](https://github.com/rust-lang/futures-rs/blob/master/LICENSE-MIT) or [Apache](https://github.com/rust-lang/futures-rs/blob/master/LICENSE-APACHE) | https://github.com/rust-lang/futures-rs
warp | Sean McArthur & Contributors | [MIT](https://github.com/seanmonstar/warp/blob/master/LICENSE) | https://github.com/seanmonstar/warp
percent-encoding | Simon Sapin & Contributors | [MIT](https://github.com/servo/rust-url/blob/master/LICENSE-MIT) or [Apache](https://github.com/servo/rust-url/blob/master/LICENSE-APACHE) | https://github.com/servo/rust-url/
bytes | Carl Lerche & Contributors | [MIT](https://github.com/tokio-rs/bytes/blob/master/LICENSE) | https://github.com/tokio-rs/bytes
log | Steven Fackler & Contributors | [MIT](https://github.com/rust-lang/log/blob/master/LICENSE-MIT) or [Apache](https://github.com/rust-lang/log/blob/master/LICENSE-APACHE) | https://github.com/rust-lang/log
simplelog | Victor Brekenfeld | [MIT](https://github.com/Drakulix/simplelog.rs/blob/master/LICENSE.MIT) or [Apache2](https://github.com/Drakulix/simplelog.rs/blob/master/LICENSE.APACHE2) | https://github.com/drakulix/simplelog.rs
serde, serde_json | David Tolnay | [MIT](https://github.com/serde-rs/serde/blob/master/LICENSE-MIT) or [Apache](https://github.com/serde-rs/serde/blob/master/LICENSE-APACHE) | https://github.com/serde-rs/serde
chrono | Brandon W. Maister & Contributors | [MIT and Apache2.0](https://github.com/chronotope/chrono/blob/main/LICENSE.txt) | https://github.com/chronotope/chrono
git-version | Mara Bos & Contributors | [BSD 2-Clause-License](https://github.com/fusion-engineering/rust-git-version/blob/master/COPYING) | https://github.com/fusion-engineering/rust-git-version 
sha2 | Artyom Pavlov & Contributors | [Apache 2](http://www.apache.org/licenses/LICENSE-2.0) or [MIT](http://opensource.org/licenses/MIT) | https://github.com/RustCrypto/hashes
lazy-static | Marvin Löbel | [Apache 2](https://github.com/rust-lang-nursery/lazy-static.rs/blob/master/LICENSE-APACHE) | https://github.com/rust-lang-nursery/lazy-static.rs
lru | Jerome Froelich | [MIT](https://github.com/jeromefroe/lru-rs) | https://github.com/jeromefroe/lru-rs
rand | Diggory Hardy & Contributors | [MIT or Apache 2](https://github.com/rust-random/rand/blob/master/COPYRIGHT) | https://github.com/rust-random/rand
flate2 | Alex Crichton & Contributors | [MIT](https://github.com/rust-lang/flate2-rs/blob/master/LICENSE-MIT) or [Apache 2](https://github.com/rust-lang/flate2-rs/blob/master/LICENSE-APACHE) | https://github.com/rust-lang/flate2-rs
regex | Andrew Gallant & Contributors | [Apache](https://github.com/rust-lang/regex/blob/master/LICENSE-APACHE) or [MIT](https://github.com/rust-lang/regex/blob/master/LICENSE-MIT) | https://github.com/rust-lang/regex
toml-rs | Alex Crichton & Contributors | [MIT](https://github.com/alexcrichton/toml-rs/blob/master/LICENSE-MIT) or [Apache](https://github.com/alexcrichton/toml-rs/blob/master/LICENSE-APACHE) | https://github.com/alexcrichton/toml-rs
num_cpus | Sean McArthur & Contributors | [Apache 2](https://github.com/seanmonstar/num_cpus/blob/master/LICENSE-APACHE) or [MIT](https://github.com/seanmonstar/num_cpus/blob/master/LICENSE-MIT) | https://github.com/seanmonstar/num_cpus
dashmap | Joel Wejdenstal & Contributors | [MIT](https://github.com/xacrimon/dashmap/blob/master/LICENSE) | https://github.com/xacrimon/dashmap
crc32fast | Sam Rijs & Contributors | [MIT](https://github.com/srijs/rust-crc32fast/blob/master/LICENSE-MIT) or [Apache](https://github.com/srijs/rust-crc32fast/blob/master/LICENSE-APACHE) | https://github.com/srijs/rust-crc32fast
rusqlite | John Gallagher & Contributors | [MIT](https://github.com/rusqlite/rusqlite/blob/master/LICENSE) | https://github.com/rusqlite/rusqlite
mime-guess | Austin Bonander & Contributors | [MIT](https://github.com/abonander/mime_guess/blob/master/LICENSE) | https://github.com/abonander/mime_guess
stopwatch | Chucky Ellison & Contributors | [MIT](https://github.com/ellisonch/rust-stopwatch/blob/master/LICENSE.txt) | https://github.com/ellisonch/rust-stopwatch
rayon | Josh Stone & Contributors | [MIT](https://github.com/rayon-rs/rayon/blob/master/LICENSE-MIT) or [Apache](https://github.com/rayon-rs/rayon/blob/master/LICENSE-APACHE) | https://github.com/rayon-rs/rayon
async-trait | David Tolnay & Contributors | [MIT](https://github.com/dtolnay/async-trait/blob/master/LICENSE-MIT) or [Apache](https://github.com/dtolnay/async-trait/blob/master/LICENSE-APACHE) | https://github.com/dtolnay/async-trait
env-logger | Ashley Mannix & Contributors | [MIT](https://github.com/env-logger-rs/env_logger/blob/main/LICENSE-MIT) or [Apache](https://github.com/env-logger-rs/env_logger/blob/main/LICENSE-APACHE) | https://github.com/env-logger-rs/env_logger
smallvec | Matt Brubeck & Contributors | [MIT](https://github.com/servo/rust-smallvec/blob/master/LICENSE-MIT) or [Apache](https://github.com/servo/rust-smallvec/blob/master/LICENSE-APACHE) | https://github.com/servo/rust-smallvec
once_cell | Aleksey Kladov & Contributors | [MIT](https://github.com/matklad/once_cell/blob/master/LICENSE-MIT) or [Apache](https://github.com/matklad/once_cell/blob/master/LICENSE-APACHE) | https://github.com/matklad/once_cell

## Server Plugin Libraries
These libaries power the plugins that are included with the Digital Earth Viewer by default. Only libraries not included in the base server application are listed here.
Library | Author | License | Repository
--------|--------|---------|-----------
image | Andreas Molzer & Contributors | [MIT](https://github.com/image-rs/image/blob/master/LICENSE) | https://github.com/image-rs/image
rstar | Stephan Hügel & Contributors | [Apache 2](https://github.com/georust/rstar/blob/master/LICENSE-APACHE) | https://github.com/georust/rstar
gpx | Corey Farwell & Contributors | [MIT](https://github.com/georust/gpx/blob/master/LICENSE) | https://github.com/georust/gpx
reqwest | Sean McArthur & Contributors | [Apache 2](https://github.com/seanmonstar/reqwest/blob/master/LICENSE-APACHE) | https://github.com/seanmonstar/reqwest
bincode | Ty Overby & Contributors | [MIT](https://github.com/bincode-org/bincode/blob/trunk/LICENSE.md) | https://github.com/bincode-org/bincode
netcdf | Magnus Ulimoen & Contributors | [MIT](https://github.com/georust/netcdf/blob/master/LICENSE-MIT) or [Apache 2](https://github.com/georust/netcdf/blob/master/LICENSE-APACHE) | https://github.com/georust/netcdf
shapefile-rs | Roger Filmyer & Contributors | [MIT](https://github.com/tmontaigu/shapefile-rs/blob/master/LICENSE.md) | https://github.com/tmontaigu/shapefile-rs
geo | Stephan Hügel & Contributors | [MIT](https://github.com/georust/geo/blob/master/LICENSE-MIT) or [Apache](https://github.com/georust/geo/blob/master/LICENSE-APACHE) | https://github.com/georust/geo
rusttype | Alex Butler & Contributors | [MIT](https://gitlab.redox-os.org/redox-os/rusttype/-/blob/master/LICENSE-MIT) or [Apache 2](https://gitlab.redox-os.org/redox-os/rusttype/-/blob/master/LICENSE-APACHE) | https://gitlab.redox-os.org/redox-os/rusttype
palette | Erik Hedvall & Contributors | [MIT](https://github.com/Ogeon/palette/blob/master/LICENSE-MIT) or [Apache 2](https://github.com/Ogeon/palette/blob/master/LICENSE-APACHE) | https://github.com/Ogeon/palette
imageproc | Sven Nilsen & Contributors | [MIT](https://github.com/image-rs/imageproc/blob/master/LICENSE) | https://github.com/image-rs/imageproc
quick-xml | Johann Tuffe & Contributors | [MIT](https://github.com/tafia/quick-xml/blob/master/LICENSE-MIT.md) | https://github.com/tafia/quick-xml
