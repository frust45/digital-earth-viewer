FROM centos:8
MAINTAINER Valentin Buck

# Setup basics
RUN dnf update -y
RUN dnf install -y gcc curl clang make wget git m4 openssl-devel gtk3-devel cmake
#RUN apk add tzdata git coreutils curl clang make wget git m4 cmake openssl-dev gcc autoconf g++ util-linux automake libexecinfo-dev gtk4.0-dev gtk+3.0-dev gtk+2.0-dev sshpass mingw-w64-gcc

#Setup nodejs
RUN dnf module install -y nodejs:14

# Setup rust
RUN curl https://sh.rustup.rs -sSf | sh -s -- --default-toolchain nightly -y
RUN sh -c "source $HOME/.cargo/env"
ENV PATH="/root/.cargo/bin:${PATH}"
RUN sh -c "export PATH=\"$HOME/.cargo/bin:$PATH\""
