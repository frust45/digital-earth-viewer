//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

#![feature(array_windows)]
mod shapefile_source;
pub use shapefile_source::ShapeFileSource;