//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use geo::prelude::BoundingRect;
use geo::prelude::Contains;
use geo::MultiPolygon;
use shapefile;
use tileserver_model::*;

pub struct ShapeFileSource {
    _points: Vec<GeoPoint<Scalar>>,
    _lines: Vec<GeoLine<Scalar>>,
    polygons: Vec<MultiPolygon<f64>>,
    epsg_code: u32,
    bounds_polygons: Option<UECArea>,
}

#[async_trait]
impl SourceInit for ShapeFileSource {
    async fn initialize(settings: SourceConfig, mut source_info: SourceInfo) -> SourceInitResult {
        if settings.input_files.is_empty() {
            return Err("No shape file given".into());
        }

        /*let filename = settings.input_files[0]
            .file_name()
            .map(|f| f.to_string_lossy().to_string())
            .unwrap_or_else(|| "Shape File".to_string());*/

        let reader = shapefile::Reader::from_path(&settings.input_files[0])?;

        let epsg_code = settings
            .settings
            .get("epsg_code")
            .map(|l| l.parse().ok())
            .flatten()
            .unwrap_or(4326);

        let default_height = settings
            .settings
            .get("default_height")
            .map(|l| l.parse().ok())
            .flatten()
            .unwrap_or(0f32);
        let default_value = settings
            .settings
            .get("default_value")
            .map(|l| l.parse().ok())
            .flatten()
            .unwrap_or(0f32);

        let converter: Box<dyn CoordTransform> =
            tileserver_model::projections::get_coord_transform(epsg_code)
                .ok_or_else(|| format!("No transformation available for code {}", epsg_code))?;

        let mut points = Vec::new();
        let mut lines = Vec::new();
        let mut polygons = Vec::new();

        let tuple_to_point = |x, y| {
            //trace!("Point: x={}, y={}", x, y);
            let pos_deg = converter.to_deg(x, y);
            //trace!("Pos_deg: x={}, y={}", pos_deg.0, pos_deg.1);
            let pos = Position::new_coord(pos_deg.1, pos_deg.0);
            pos
        };

        reader
            .iter_shapes()
            .enumerate()
            .for_each(|(_, s)| match s {
                Ok(s) => match s {
                    shapefile::Shape::NullShape => {}
                    shapefile::Shape::Point(p) => {
                        points.push(GeoPoint::new_from_position(
                            tuple_to_point(p.x, p.y),
                            default_height,
                            0,
                            default_value,
                        ));
                    }
                    shapefile::Shape::PointM(p) => {
                        points.push(GeoPoint::new_from_position(
                            tuple_to_point(p.x, p.y),
                            default_height,
                            0,
                            p.m as f32,
                        ));
                    }
                    shapefile::Shape::PointZ(p) => {
                        points.push(GeoPoint::new_from_position(
                            tuple_to_point(p.x, p.y),
                            p.z as f32,
                            0,
                            default_value,
                        ));
                    }
                    shapefile::Shape::Polyline(l) => {
                        for part in l.parts() {
                            for [p1, p2] in part.array_windows() {
                                lines.push(GeoLine::new_from_position(
                                    tuple_to_point(p1.x, p1.y),
                                    default_height,
                                    tuple_to_point(p2.x, p2.y),
                                    default_height,
                                    0,
                                    0,
                                    default_value,
                                    default_value,
                                ));
                            }
                        }
                    }
                    shapefile::Shape::PolylineM(l) => {
                        for part in l.parts() {
                            for [p1, p2] in part.array_windows() {
                                lines.push(GeoLine::new_from_position(
                                    tuple_to_point(p1.x, p1.y),
                                    default_height,
                                    tuple_to_point(p2.x, p2.y),
                                    default_height,
                                    0,
                                    0,
                                    p1.m as f32,
                                    p2.m as f32,
                                ));
                            }
                        }
                    }
                    shapefile::Shape::PolylineZ(l) => {
                        for part in l.parts() {
                            for [p1, p2] in part.array_windows() {
                                lines.push(GeoLine::new_from_position(
                                    tuple_to_point(p1.x, p1.y),
                                    p1.z as f32,
                                    tuple_to_point(p2.x, p2.y),
                                    p2.z as f32,
                                    0,
                                    0,
                                    default_value,
                                    default_value,
                                ));
                            }
                        }
                    }
                    shapefile::Shape::Polygon(p) => {
                        let poly: geo::MultiPolygon<f64> = p.into();
                        polygons.push(poly);
                    }
                    shapefile::Shape::PolygonM(p) => {
                        let poly: geo::MultiPolygon<f64> = p.into();
                        polygons.push(poly);
                    }
                    shapefile::Shape::PolygonZ(p) => {
                        let poly: geo::MultiPolygon<f64> = p.into();
                        polygons.push(poly);
                    }
                    shapefile::Shape::Multipoint(_) => {
                        warn!("Unimplemented Shapefile feature: Multipoint");
                    }
                    shapefile::Shape::MultipointM(_) => {
                        warn!("Unimplemented Shapefile feature: MultipointM");
                    }
                    shapefile::Shape::MultipointZ(_) => {
                        warn!("Unimplemented Shapefile feature: MultipointZ");
                    }
                    shapefile::Shape::Multipatch(_) => {
                        warn!("Unimplemented Shapefile feature: Multipatch");
                    }
                },
                Err(e) => {
                    error!("Error reading shape: {:?}", e)
                }
            });

        let mut points_layer = LayerInfo::new("Points", GeoDataType::ScalarPoints);
        let extent = UECArea::area_from_points_iter(points.iter().map(|p| p.pos.to_uec()));
        if let Some(extent) = extent {
            points_layer.set_extent(extent);
        }
        points_layer.set_datarange_from_value_iterator(points.iter().map(|p| p.value));
        source_info.add_layer(points_layer);

        let mut lines_layer = LayerInfo::new("Lines", GeoDataType::ScalarLines);
        let extent = UECArea::area_from_points_iter(
            lines
                .iter()
                .map(|l| [l.pos.0.to_uec(), l.pos.1.to_uec()])
                .flatten(),
        );
        if let Some(extent) = extent {
            lines_layer.set_extent(extent);
        }
        lines_layer.set_datarange_from_value_iterator(
            lines.iter().map(|l| [l.value.0, l.value.1]).flatten(),
        );
        source_info.add_layer(lines_layer);

        let mut polygon_layer = LayerInfo::new("Polygons", GeoDataType::ScalarTiles);

        let poly_extent = polygons.iter().fold(None, |prev, next| {
            let bounds = next.bounding_rect()?;
            let min_x = bounds.min().x;
            let min_y = bounds.min().y;
            let max_x = bounds.max().x;
            let max_y = bounds.max().y;

            let pmin = tuple_to_point(min_x, min_y);
            let pmax = tuple_to_point(max_x, max_y);
            //trace!("Pmin: {:?}, Pmax: {:?}", pmin.to_coord(), pmax.to_coord());

            let extent = UECArea::area_from_two_points(pmin.to_uec(), pmax.to_uec());

            if let Some(prev_extent) = prev {
                Some(UECArea::extend_area_with_area(&prev_extent, extent))
            } else {
                Some(extent)
            }
        });

        if let Some(extent) = poly_extent {
            polygon_layer.set_extent(extent);
            let max_zoom_level = Tile::max_zoom_level_from_area_and_pixelsize(extent, (4096, 4096));
            polygon_layer.set_max_zoom_level(max_zoom_level);
            source_info.add_layer(polygon_layer);
        } else {
            polygon_layer.set_max_zoom_level(1);
        }

        Ok((
            source_info,
            Box::new(ShapeFileSource {
                _points: points,
                _lines: lines,
                polygons,
                epsg_code,
                bounds_polygons: poly_extent,
            }),
        ))
    }

    fn accepts_file(file: &std::path::Path) -> bool {
        file.extension().map(|ext| ext == "shp").unwrap_or(false)
    }
}

#[async_trait]
impl Source for ShapeFileSource {
    async fn sample(&self, query: &SampleQuery) -> SourceResult {
        if query.layername == "Polygons" {
            let converter: Box<dyn CoordTransform> =
                tileserver_model::projections::get_coord_transform(self.epsg_code).ok_or_else(
                    || format!("No transformation available for code {}", self.epsg_code),
                )?;
            let tile =
                TileData::new_from_coord_closure_parallel(query.tile.clone(), |pos: Coord| {
                    if let Some(poly_extent) = self.bounds_polygons {
                        if !Position::Coord(pos).to_uec().in_area(poly_extent) {
                            return std::f32::NAN;
                        }
                    }

                    let pos_converted = converter.from_deg(pos.lon, pos.lat);
                    let coordinate = geo::Coordinate {
                        x: pos_converted.0,
                        y: pos_converted.1,
                    };
                    self.polygons
                        .iter()
                        .enumerate()
                        .find(|(_index, poly)| poly.contains(&coordinate))
                        .map(|(i, _p)| i as f32)
                        .unwrap_or(std::f32::NAN)
                });
            Ok(Some(GeoData::ScalarTiles(tile)))
        } else {
            Err("Unimplemented!".to_string().into())
        }
    }
}
