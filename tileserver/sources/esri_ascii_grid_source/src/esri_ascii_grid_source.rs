//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use tileserver_model::*;
use crate::libesri;
use std::error::Error;
use tileserver_model::Coord;
use tileserver_model::rayon::prelude::*;
use std::path::Path;
use tileserver_model::async_trait;


pub struct EsriAsciiGridSource{
    esrifile: libesri::EsriFile,
    esri_array: Array2D<Scalar>,
    extent: UECArea,
}

#[async_trait]
impl SourceInit for EsriAsciiGridSource{
    async fn initialize(settings: SourceConfig, mut source_info: SourceInfo) -> SourceInitResult{
        if settings.input_files.is_empty() {
            return Err("No directory with blue marble files given".into());
        }
        let esri = libesri::load_file(&settings.input_files[0]).map_err(|_error|"Cannot read ASCII file".to_string())?;
        let name = &settings.input_files[0];
        let mut array: Array2D<Scalar> = Array2D::new(Point2D::xy(esri.header.ncols as f64, esri.header.nrows as f64));

        array.data.par_iter_mut().enumerate().for_each(|(i,v)| {
            let x = i % esri.header.ncols as usize;
            let y = i / esri.header.ncols as usize;
            *v = esri.data_lines[y][x] as f32;
        });

        let lonsize = esri.header.ncols as f64 * esri.header.cellsize;
        let latsize = esri.header.nrows as f64 * esri.header.cellsize;

        let mut max_zoom_level = 1;
        let pixel_size = constants::CIRCUMFERENCE_EQUATOR_METRES / (360f64 / esri.header.cellsize);

        while Tile::pixel_size_in_meters_from_path_length(max_zoom_level) > pixel_size {
            max_zoom_level += 1;
        }

        let extent_position_lon = esri.header.xllcorner;
        let extent_position_lat = esri.header.yllcorner + latsize;

        source_info.instance_name = Path::new(&name)
                    .file_name()
                    .map(|x| x.to_string_lossy().into_owned())
                    .unwrap_or_else(|| "Unidentified ESRI ASCII Grid File".to_string());

        let mut layer = LayerInfo::new("AreaData", GeoDataType::ScalarTiles);

        layer.set_datarange(esri.header.min_value as f32,  esri.header.max_value as f32)
        .set_unit("m")
        .set_max_zoom_level(max_zoom_level)
        .set_extent(UECArea::new(Coord::new(extent_position_lat, extent_position_lon).into(), UEC::xy(lonsize / 360.0, latsize / 180.0)));

        source_info.add_layer(layer);

        Ok((source_info,
            Box::new(EsriAsciiGridSource{
            esrifile: esri,
            esri_array: array,
            extent:UECArea::new(Coord::new(extent_position_lat, extent_position_lon).into(), UEC::xy(lonsize / 360.0, latsize / 180.0)),
        })))
    }

    fn accepts_file(file: &Path) -> bool {
        file.extension().map(|ext| ext.to_string_lossy().ends_with(".txt")).unwrap_or(false) //txt files have to end with .txt

    }
}

#[async_trait]
impl Source for EsriAsciiGridSource{
    async fn sample(&self, query: &tileserver_model::SampleQuery) -> Result<Option<GeoData>, Box<dyn Error + Send + Sync>> {
        let tile_extents = query.tile.to_uec_area();

        if UECArea::areas_overlap(self.extent, tile_extents){
            let uec_size = UEC::xy(
                self.esrifile.header.cellsize / 360.0,
                self.esrifile.header.cellsize / 180.0
            );
            let offset = (tile_extents.position - self.extent.position).to_point_2d().scale_xy( 1.0 / uec_size.x, 1.0 / uec_size.y);
            let subset_size = (tile_extents.extent).to_point_2d().scale_xy(1.0 / uec_size.x, 1.0 / uec_size.y);        
            let target_size = Point2D::xy(constants::TILESIZE.0 as f64, constants::TILESIZE.1 as f64);
            let tiledata = TileData::new_from_tile_and_array_2d(
                query.tile.clone(),
                self.esri_array.resample_subset_to(offset, subset_size, target_size)
            );
            if tiledata.is_empty() {
                Ok(None)
            } else {
            Ok(Some(GeoData::ScalarTiles(tiledata)))
            }
        }else{
            Ok(None)
        }
    }

}