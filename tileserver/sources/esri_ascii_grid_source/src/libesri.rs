//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use std::io::{BufRead, BufReader};
use std::fs::File;
use std::error::Error;

#[derive(Debug, Clone, Default)]
pub struct EsriHeader{
    pub ncols: u32,
    pub nrows: u32,
    pub xllcorner: f64,
    pub yllcorner: f64,
    pub xllcenter: f64,
    pub yllcenter: f64,
    pub cellsize: f64,
    pub nodata_value: f64,
    pub min_value: f64,
    pub max_value: f64
}

#[derive(Debug, Clone, Default)]
pub struct EsriFile{
    pub header: EsriHeader,
    pub data_lines: Vec<Vec<f64>>,
    pub valid_points: Vec<(u32,u32)>
}

impl EsriHeader{
    pub fn validate_position(&mut self){
        if self.xllcorner.is_nan(){
            self.xllcorner = self.xllcenter - (self.cellsize * (self.ncols as f64 / 2.0))
        }else if self.xllcenter.is_nan(){
            self.xllcenter = self.xllcorner + (self.cellsize * (self.ncols as f64 / 2.0))
        }
        if self.yllcorner.is_nan(){
            self.yllcorner = self.yllcenter - (self.cellsize * (self.nrows as f64 / 2.0))
        }else if self.yllcenter.is_nan(){
            self.yllcenter = self.yllcorner + (self.cellsize * (self.nrows as f64 / 2.0))
        }
    }
}

pub fn load_file(file: &std::path::Path) -> Result<EsriFile,Box<dyn Error>>{
    let reader = BufReader::new(File::open(file)?);

    let mut header = EsriHeader::default();

    header.xllcorner = std::f64::NAN;
    header.xllcenter = std::f64::NAN;
    header.yllcorner = std::f64::NAN;
    header.yllcenter = std::f64::NAN;

    let mut lines: Vec<Vec<f64>> = Vec::new();

    for line in reader.lines(){
        let line = line?;
        let c0 = line.trim().chars().next().unwrap_or('_');
        if c0.is_numeric() || c0  == '-' || c0 == '+'{
            lines.push(
                line
                    .trim()
                    .split_whitespace()
                    .map(|p| p.parse().unwrap_or(std::f64::NAN))
                    .map(|v| if v == header.nodata_value {std::f64::NAN} else {v})
                    .collect()
            );
        }else{
            if line.to_lowercase().starts_with("ncols"){
                header.ncols = line[6..].trim().parse()?;
            }else if line.to_lowercase().starts_with("nrows"){
                header.nrows = line[6..].trim().parse()?
            }else if line.to_lowercase().starts_with("xllcenter"){
                header.xllcenter = line[10..].trim().parse()?
            }else if line.to_lowercase().starts_with("xllcorner"){
                header.xllcorner = line[10..].trim().parse()?
            }else if line.to_lowercase().starts_with("yllcenter"){
                header.yllcenter = line[10..].trim().parse()?
            }else if line.to_lowercase().starts_with("yllcorner"){
                header.yllcorner = line[10..].trim().parse()?
            }else if line.to_lowercase().starts_with("cellsize"){
                header.cellsize = line[9..].trim().parse()?
            }else if line.to_lowercase().starts_with("dx"){ //custom attribute found in auv_bathy.asc
                header.cellsize = line[3..].trim().parse()?
            }else if line.to_lowercase().starts_with("nodata_value"){
                header.nodata_value = line[13..].trim().parse()?
            }
        }
    }
    
    header.validate_position();

    //eprintln!("Header: {:?}", header);
    //eprintln!("Line 0: {:?}", lines[0]);

    header.min_value = std::f64::INFINITY;
    header.max_value = std::f64::NEG_INFINITY;

    let mut valid_points: Vec<(u32,u32)> = Vec::new();

    for (y,l) in lines.iter().enumerate(){
        for (x, &v) in l.iter().enumerate(){
            if !v.is_nan(){
                header.min_value = header.min_value.min(v);
                header.max_value = header.max_value.max(v);
                valid_points.push((x as u32,y as u32));
            }
        }
    }

    //eprintln!("Found {} valid points", valid_points.len());

    Ok(EsriFile{
        header,
        data_lines: lines,
        valid_points
    })
}


#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let pb = std::path::PathBuf::from("../auv-bathy.asc".to_string());
        crate::libesri::load_file(&pb).unwrap();
    }

    #[test]
    fn gebco_works() {
        let pb = std::path::PathBuf::from("G:\\gebco_2019_n70.83984375000001_s43.945312500000014_w-10.72265625_e18.28125.asc".to_string());
        crate::libesri::load_file(&pb).unwrap();
    }
}
