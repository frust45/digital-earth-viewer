//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use tileserver_model::*;
use tileserver_model::tokio::sync::Mutex;
use std::error::Error;
use tileserver_model::async_trait;
use rusqlite::{Connection, Result};

#[allow(clippy::upper_case_acronyms)]
pub struct SQLiteSource{
    layers: Vec<LayerInfo>,
    timepages: Vec<Timestamp>,
    view_name: String,
    connection: Mutex<Connection>
}

#[async_trait]
impl SourceInit for SQLiteSource {
    async fn initialize(settings: SourceConfig, mut source_info: SourceInfo) -> SourceInitResult {
        let view_name = settings.settings.get("view_name").map(|l| l.as_str()).ok_or("No view to source data from given")?;

        if settings.input_files.is_empty(){
            return Err("No input file given".into());
        }
        let filename = settings.input_files[0].file_name().map(|f| f.to_string_lossy().to_string()).unwrap_or_else(|| "Unidentified CSV file".to_string());


        /*
        Convert your SQLite-DB to use unix timestamps from ISO 8601 (stored in a column named isotime)
        ALTER TABLE data ADD COLUMN unixtime integer;
        UPDATE data SET unixtime = strftime("%s", datetime(isotime));
        Then create indices
        CREATE INDEX time_index on data(unixtime)
        and so on for time, lat, lon, height
        And then create a view to retrieve the data
        CREATE VIEW view_plume AS SELECT unixtime as time, X as longitude, Y as latitude, depth * -1.0 as height, Class1, Class2, Class3 from plume_corrected_zero_filter


        If your data has multiple columns that are all used to represent time, use string concatenation and printf-interpolation to get an iso timestring and then unixify that
        Here's an example with a SELECT (the LIMIT 1 is a helpful trick to avoid returning the whole database each time)
        SELECT strftime("%s", 
            datetime(
                PRINTF("%04d", yr) || "-" || PRINTF("%02d", mon) || "-" || PRINTF("%02d", day) 
                || " " 
                || PRINTF("%02d", hh) || ":" || PRINTF("%02d", mm) || ":" || PRINTF("%02.f", ss)
            )
        ) as unixtime from SOCAT_without_header LIMIT 1;

        This yields this update-command:
        UPDATE SOCAT_without_header SET unixtime = strftime("%s", 
            datetime(
                PRINTF("%04d", yr) || "-" || PRINTF("%02d", mon) || "-" || PRINTF("%02d", day) 
                || " " 
                || PRINTF("%02d", hh) || ":" || PRINTF("%02d", mm) || ":" || PRINTF("%02.f", ss)
            )
        );
        */

        let conn = Connection::open(&settings.input_files[0]).map_err(|e| format!("Could not open database file {}: {:?}", filename, e))?;

        //Look for column names
        info!("Gathering column names");

        let column_names: Vec<String> = conn.query_row(
            &format!("SELECT * from {} LIMIT 1", view_name), 
            [], 
            |r| {
                let col_names = r.column_names().iter().map(|s| s.to_string()).collect();
                Ok(col_names)
            }).map_err(|e| format!("Error retrieving column names: {:?}", e))?;

        if column_names.iter().find(|&col| col == "latitude").is_none(){
            return Err("View contains no column named 'latitude'".to_string().into());
        }
        if column_names.iter().find(|&col| col == "longitude").is_none(){
            return Err("View contains no column named 'longitude'".to_string().into());
        }
        if column_names.iter().find(|&col| col == "height").is_none(){
            return Err("View contains no column named 'height'".to_string().into());
        }
        if column_names.iter().find(|&col| col == "time").is_none(){
            return Err("View contains no column named 'time'".to_string().into());
        }

        info!("Gathering limits");

        let limits: Vec<(String, (f64,f64))>= conn.query_row(
            &format!("SELECT {} from {}",
                column_names.iter().map(|colname| format!("min({}), max({})", colname, colname)).collect::<Vec<_>>().join(", "),
                view_name
            ),
            [],
            |row| {
                column_names.iter()
                        .enumerate()
                        .map(|(i, col)| Ok((col.clone(), (row.get(i * 2).unwrap_or(0.0), row.get(i * 2 + 1).unwrap_or(0.0)))))
                        //The unwrapping here is important in case one of the values ends up as NaN. This happens from time to time unfortunately.
                        .collect()
            })?;
        
        info!("Gathering timesteps");

        let mut timesteps_statement = conn.prepare(&format!("SELECT time, COUNT(*) FROM {} GROUP BY time ORDER BY time ASC", view_name))?;
        let timesteps_and_counts = timesteps_statement.query_map([], |row| {
            let (timestamp, count): (Timestamp, i64) = (row.get(0)?, row.get(1)?);
            Ok((timestamp * 1000, count))
        })?.collect::<Result<Vec<_>>>()?;
        drop(timesteps_statement);

        let mut timestamps = Vec::new();
        let mut last_time_added = 0;
        let mut time_count_added = 0;
        for (timestep, count) in timesteps_and_counts{
            if last_time_added == 0 || time_count_added as usize > constants::RECOMMENDED_PAGE_SIZE{
                timestamps.push(timestep);
                time_count_added = count;
                last_time_added = timestep;
            }else{
                time_count_added += count;
            }
        }
        
        info!("Creating layers");

        let lat_bounds = limits.iter().find(|(name, _range)| name == "latitude").map(|(_name, range)| *range).ok_or_else(|| "No limits for latitude found".to_string())?;
        let lon_bounds = limits.iter().find(|(name, _range)| name == "longitude").map(|(_name, range)| *range).ok_or_else(|| "No limits for longitude found".to_string())?;
        let time_bounds = limits.iter().find(|(name, _range)| name == "time").map(|(_name, range)| (range.0 as i64 * 1000i64, range.1 as i64 * 1000i64)).ok_or_else(|| "No limits for time found".to_string())?;
        let height_bounds = limits.iter().find(|(name, _range)| name == "height").map(|(_name, range)| *range).ok_or_else(||"No limits for height found".to_string())?;

        let position_extent = UECArea::area_from_two_points(Position::new_coord(lat_bounds.0, lon_bounds.0).to_uec(), Position::new_coord(lat_bounds.1, lon_bounds.1).to_uec());

        let layers: Vec<LayerInfo> = column_names.iter().filter(|&colname| colname != "latitude" && colname != "longitude" && colname != "time" && colname != "height").map(|colname| {
            let mut li = LayerInfo::new(colname, GeoDataType::ScalarPoints);
            li.set_extent(position_extent);
            li.zrange = Some((height_bounds.0 as f32, height_bounds.1 as f32));
            li.set_timesteps(&timestamps);
            li.timerange = Some((time_bounds.0 as Timestamp, time_bounds.1 as Timestamp));
            let datarange = limits.iter().find(|(name, _range)| name == colname).map(|(_name, range)| *range).ok_or_else(|| format!("No limits for {} found", colname))?;
            li.set_datarange(datarange.0 as f32, datarange.1 as f32);
            Ok(li)
        }).collect::<Result<Vec<LayerInfo>, Box<dyn Error + Send + Sync>>>()?;

        for layer in layers.iter().cloned(){
            source_info.add_layer(layer);
        }

        Ok((source_info, Box::new(
            SQLiteSource{
                layers,
                timepages: timestamps,
                view_name: view_name.to_string(),
                connection: Mutex::new(conn)
            }
        )))
    }

    fn accepts_file(_file: &std::path::Path) -> bool {
        true
    }
}

#[async_trait]
impl Source for SQLiteSource{
    async fn sample(&self, query: &SampleQuery) -> SourceResult {
        //Look for layer
        for layer in self.layers.iter(){
            if layer.name == query.layername{//Layer found
                //Check timesteps
                //trace!("Found layer");
                match (self.timepages.get(query.tpage as usize), self.timepages.get(query.tpage as usize + 1)){
                    (None, _) => { //The first timepage was not found, so we can't actually return a result
                        return Err(format!("Timestep {} requested but source only has {} timesteps", query.tpage, self.timepages.len()).into());
                    },
                    (Some(time_range_start), time_range_end) => {
                        //trace!("Found timepage");
                        let query = format!("SELECT longitude, latitude, height, time, {} as value from {} WHERE time >= ? AND time < ?",
                            layer.name,
                            self.view_name);
                        //trace!("Built querystring");
                        let conn = self.connection.lock().await;
                        //trace!("Got connection!");
                        let mut statement = conn.prepare_cached(&query)?;
                        //trace!("Statement created");
                        let time_start =  time_range_start / 1000i64;
                        let time_end = match time_range_end{
                            Some(time_range_end) => *time_range_end,
                            None => layer.timerange
                                        .map(|r| r.1)
                                        .unwrap_or(std::i64::MAX)
                        } / 1000i64;
                        let points = statement.query_map([
                           time_start,
                           time_end                            
                        ], 
                        |row|{
                            let latitude: f64 = row.get("latitude")?;
                            let longitude: f64 = row.get("longitude")?;
                            let height: f64 = row.get("height")?;
                            let time: Timestamp = row.get("time")?;
                            let value: f64 = row.get("value").unwrap_or(std::f64::NAN);
                            let point: GeoPoint<Scalar> = GeoPoint::new_from_coord(
                               Coord::new(latitude, longitude),
                               height as f32,
                               time * 1000i64,
                               value as f32
                            );
                            Ok(point)
                        })?.collect::<Result<Vec<_>,_>>()?;
                        //trace!("Query returned {} points", points.len());
                        if points.is_empty(){
                            return Ok(None);
                        }else{
                            return Ok(
                                Some(
                                    GeoData::ScalarPoints(
                                        PointsData::new(
                                            points
                                        )
                                    )
                                )
                            );
                        }
                    }
                }
            }
        }
        return Err(format!("No layer called {} in this source", query.layername).into());
    }
}