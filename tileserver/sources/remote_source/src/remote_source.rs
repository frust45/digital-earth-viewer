//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use tileserver_model::*;

use reqwest::{self, Client};

use tileserver_model::async_trait;

pub struct RemoteSource{
    remote_url: String,
    source_info: SourceInfo,
    client: Client
}

const SOURCELIST_URL: &str = "sourcelist";
const TILES_URL: &str = "tiles";
const ARRAYS_URL: &str = "arrays";

#[async_trait]
impl SourceInit for RemoteSource {
    async fn initialize(settings: SourceConfig, mut _source_info: SourceInfo) -> SourceInitResult {
        let mut url = settings.settings.get("url").map(|l| l.as_str()).ok_or("No remote host url given")?.to_string();
        let instance = settings.settings.get("source_name").map(|l| l.as_str()).ok_or("No source instance name given")?;

        if url.ends_with('/') {
            url = url.trim_end_matches('/').to_string();
        }

        let sourcelist_url = format!("{}/{}", url, SOURCELIST_URL);
        let source_infos: Vec<SourceInfo> = serde_json::from_str(
            &std::thread::spawn(move || {
                reqwest::blocking::get(&sourcelist_url)?.text()
            })
            .join()
            .map_err(|e| format!("Error retrieving source info: {:?}", e))?
            .map_err(|e| format!("Error reading response from remote server: {:?}", e))?
        ).map_err(|e| format!("Error parsing SourceInfos from remote server response: {:?}", e))?;


        let instance_source_info = source_infos
            .iter()
            .find(|si| si.instance_name == instance)
            .ok_or_else(|| format!("Could not find source info for source with instance name {}", instance))?.clone();

        
        let source_info = instance_source_info.clone();


        Ok((source_info, Box::new(
            RemoteSource{
                remote_url: url,
                client: reqwest::Client::new(),
                source_info: instance_source_info
            }
        )))
    }

    fn accepts_file(_file: &std::path::Path) -> bool {
        true
    }
}

#[async_trait]
impl Source for RemoteSource{
    async fn sample(&self, query: &SampleQuery) -> SourceResult {
        if let Some(layer_info) = self.source_info.layers.iter().find(|li| li.name == query.layername){
            let data = if layer_info.layer_type.is_point_type(){
                self.client.get(
                    &format!("{}/{}/{}/{}/{}",
                            self.remote_url,
                            ARRAYS_URL,
                            query.sourcename,
                            query.layername,
                            query.tpage)
                ).send().await?
                .bytes().await?.to_vec()
            }else{
                self.client.get(
                    &format!("{}/{}/{}/{}/{}/{}/{}", 
                            self.remote_url, 
                            TILES_URL, 
                            query.sourcename, 
                            query.layername, 
                            query.tile.path, 
                            query.tpage, 
                            query.zpage)
                ).send().await?
                .bytes().await?.to_vec()
            };

            if data.is_empty(){
                Ok(None)
            }else{
                Ok(
                    Some(
                        layer_info.layer_type.rehydrate(&data)
                    )
                )
            }
        }else{
            return Err(format!("No layer called {} in this source", query.layername).into());
        }
    }
}