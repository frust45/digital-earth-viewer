//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

mod remote_source;
pub use remote_source::RemoteSource;