use tileserver_model::chrono::Duration;
use tileserver_model::*;
use tileserver_model::rayon::prelude::*;
pub struct TimeMergingSource{
    layers: Vec<LayerInfo>,
    maximum_time_distance: Duration,
    minimum_fill_pixels: usize,
    instance_name: String
}

const PIXEL_COUNT: usize = constants::TILESIZE.0 * constants::TILESIZE.1;


#[async_trait]
impl SourceInit for TimeMergingSource{
    async fn initialize(settings: SourceConfig, mut source_info: SourceInfo) -> SourceInitResult {
        let instance_name = settings.settings.get("instance").map(|s| s.as_str()).ok_or_else(|| "No instance given for derivative source")?;
        let maximum_time_str = settings.settings.get("maximum_time").map(|s| s.as_str()).unwrap_or("1d");
        let minimum_fill_pixels: usize = 
            ((settings.settings.get("minimum_fill_percent")
                .and_then(|s| s.parse::<f64>().ok())
                .unwrap_or(90f64).max(0.0) * PIXEL_COUNT as f64 / 100f64)
            as usize).min(PIXEL_COUNT);
        
        let maximum_time_distance = chrono::Duration::milliseconds(TimeParser::duration_string_to_milliseconds(&maximum_time_str)?);
        
        //Get sourceinfo

        let r_source_info = DATAPROVIDER.get().ok_or_else(|| "Could not access dataprovider")?
            .wait_for_source_info(instance_name.to_string()).await?;

        //Find layerinfo

        let layers = r_source_info.layers.clone();

        for layer in layers.iter(){
            source_info.add_layer(layer.clone());
        }

        Ok((source_info.clone(), Box::new(
            TimeMergingSource{
                layers, 
                maximum_time_distance, 
                minimum_fill_pixels,
                instance_name: instance_name.to_string()
            }
        )))
    }

    fn accepts_file(_file: &std::path::Path) -> bool {
        false
    }
}

#[async_trait]
impl Source for TimeMergingSource{
    async fn sample(&self, query: &SampleQuery) -> SourceResult {

        if let Some(layer) = self.layers.iter().find(|layer| layer.name == query.layername){
            if (query.tpage as usize) < layer.timesteps.as_ref().map(|tsteps| tsteps.len()).unwrap_or(1usize){
                //Assemble tpages that fit
                let request_tpages = layer.timesteps.as_ref().map(|tsteps| {
                    let maxdate = TimeParser::timestamp_to_naivedatetime(*tsteps.get(query.tpage as usize).unwrap());
                    let mindate = maxdate - self.maximum_time_distance;
                    tsteps.iter().enumerate().rev().filter(|(_i,t)| {
                        let ndt = TimeParser::timestamp_to_naivedatetime(**t);
                        ndt >= mindate && ndt <= maxdate
                    }).map(|(i,_t)| i).collect::<Vec<_>>()  
                }).unwrap_or(vec![0]);

                //Request data
                let provider = DATAPROVIDER.get().ok_or_else(|| "Could not access dataprovider")?;
                let tile = query.tile.clone();

                let mut color_array: Option<TileData<Color>> = None;
                let mut scalar_array: Option<TileData<Scalar>> = None;

                let datatype = layer.layer_type;

                let mut more_data_needed = true;

                for (index, tpage) in request_tpages.iter().enumerate(){
                    trace!("Requesting tile {} ({} of {})", tpage, index, request_tpages.len());
                    let geodata = provider.get_geodata(
                        SampleQuery::new(
                            self.instance_name.clone(),
                            layer.name.clone(),
                            tile.clone(),
                            *tpage as u32,
                            query.zpage
                        ), datatype).await;

                    match geodata{
                        Err(e) => return Err(format!("Error sampling dependent source: {:?}", e).into()),
                        Ok(None) => continue,
                        Ok(Some(data)) => {
                            match data{
                                GeoData::ScalarTiles(scalartile) => {
                                    if let Some(array)  = scalar_array.as_mut(){
                                        array.data.par_iter_mut().zip(scalartile.data.par_iter()).for_each(|(original_value, next_value)| {
                                            if original_value.is_nodata(){
                                                *original_value = *next_value;
                                            }
                                        });
                                    }else{
                                        scalar_array = Some(scalartile);
                                    }

                                    if let Some(array) = scalar_array.as_ref(){
                                        more_data_needed = array.data.par_iter().filter(|v| !v.is_nodata()).count() < self.minimum_fill_pixels;
                                    }
                                },
                                GeoData::ColorTiles(colortile) => {
                                    if let Some(array)  = color_array.as_mut(){
                                        array.data.par_iter_mut().zip(colortile.data.par_iter()).for_each(|(original_value, next_value)| {
                                            if original_value.is_nodata(){
                                                *original_value = *next_value;
                                            }
                                        });
                                    }else{
                                        color_array = Some(colortile);
                                    }

                                    if let Some(array) = color_array.as_ref(){
                                        more_data_needed = array.data.par_iter().filter(|v| !v.is_nodata()).count() < self.minimum_fill_pixels;
                                    }
                                },
                                other=>  return Err(format!("Unimplemented merging for datatype {:?}", GeoDataType::from(other)).into())
                            }
                        }
                    }

                    if !more_data_needed {
                        break;
                    }
                }

                match layer.layer_type{
                    GeoDataType::ScalarTiles => {return Ok(scalar_array.map(|a| GeoData::ScalarTiles(a)))},
                    GeoDataType::ColorTiles => {return Ok(color_array.map(|a| GeoData::ColorTiles(a)))},
                    other=>  return Err(format!("Unimplemented merging for datatype {:?}", GeoDataType::from(other)).into())
                }

            }else{
                Err(format!("Tpage {} requested but only {} tpages in source", query.tpage, layer.timesteps.as_ref().map(|tsteps| tsteps.len()).unwrap_or(0usize)).into())
            }
        }else{
            Err(format!("Layer {} not found in source", query.layername).into())
        }
    }
}