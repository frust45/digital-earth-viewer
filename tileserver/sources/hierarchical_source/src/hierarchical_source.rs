//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer


use tileserver_model::*;
use lru::LruCache;
pub struct HierarchicalSource{
    layers: Vec<(SourceInfo, LayerInfo)>,
    layer_info: LayerInfo,
}

#[async_trait]
impl SourceInit for HierarchicalSource{
    async fn initialize(settings: SourceConfig, mut source_info: SourceInfo) -> SourceInitResult {
        let mut layer_keys: Vec<&str> = settings.settings.keys().filter(|skey| skey.starts_with("layer")).map(|s| s.as_str()).collect();
        let number_from_layer_key = |key: &str| -> i32 {key[key.find('_').map(|i| i+1).unwrap_or(0) ..].parse::<i32>().unwrap_or(0)};
        layer_keys.sort_unstable_by_key(|k| number_from_layer_key(k));
        
        let layer_tuples = layer_keys.iter().filter_map(|lkey| settings.settings.get(*lkey)).map(|s| {
            let mut parts = s.split('/');
            (parts.next().unwrap_or(""), parts.next().unwrap_or(""))}
        ).collect::<Vec<_>>();

        let mut layers = Vec::new();

        let mut best_resolution: u8 = 1;

        let mut layertype = None;

        let mut extent: Option<UECArea> = None;

        let mut timestamps: Vec<Timestamp> = Vec::new();

        let mut datarange: Option<(f32,f32)> = None;

        for (source, layer) in layer_tuples.iter(){
            info!("Hierarchical source: Loading source {} layer {}", source, layer);
            let source_info = DATAPROVIDER.get().unwrap().wait_for_source_info(source.to_string()).await?;
            let layerinfo = source_info.layers.iter().find(|l| &l.name == layer);
            if let Some(layer_info) = layerinfo{
                // Merge layer type
                if let Some(layer_type) = layertype{
                    if layer_type != layer_info.layer_type{
                        warn!("Layer {}/{} has type {:?}, but merging type is {:?}, skipping this layer", source, layer, layer_info.layer_type, layer_type);
                        continue;
                    }
                }else{
                    layertype = Some(layer_info.layer_type);
                }
                // Merge extent
                if let Some(extent) = extent{
                    if let Some(layer_extent) = layer_info.extent{
                        extent.extend_area_with_area(layer_extent);
                    }
                }else{ 
                    extent = layer_info.extent;
                }
                // Merge timesteps
                if let Some(layer_timestamps) = &layer_info.timesteps{
                    for t in layer_timestamps{
                        if !timestamps.contains(t){
                            timestamps.push(*t);
                        }
                    }
                }
                // Merge zoom level
                if layer_info.max_zoom_level > best_resolution {
                    best_resolution = layer_info.max_zoom_level;
                }
                //Merge datarange
                if let Some(datarange) = &mut datarange{
                    if let Some(layer_range) = &layer_info.datarange{
                        if(layer_range.0 < datarange.0){
                            datarange.0 = layer_range.0;
                        }
                        if(layer_range.1 > datarange.1){
                            datarange.1 = layer_range.1;
                        }
                    }
                }else{
                   datarange = layer_info.datarange; 
                }

                // Add layer
                layers.push((source_info.clone(), layer_info.clone()))
            }
        }

        timestamps.sort_unstable();

        let layertype = layertype.unwrap_or(GeoDataType::ScalarTiles);

        let mut merged_layer = LayerInfo::new("Merged", layertype);
        if let Some(extent) = extent{
            merged_layer.set_extent(extent);
        }
        if timestamps.len() > 0{
            merged_layer.set_timesteps(&timestamps);
        }
        merged_layer.set_max_zoom_level(best_resolution);

        if let Some((min, max)) = datarange{
            merged_layer.set_datarange(min, max);
        }

        source_info.add_layer(merged_layer.clone());

        Ok((source_info.clone(), Box::new(HierarchicalSource{
            layers, 
            layer_info: merged_layer,
        })))

    }
}

#[async_trait]
impl Source for HierarchicalSource{
    async fn sample(&self, query: &SampleQuery) -> SourceResult {
        let mut cache: LruCache<usize, Option<GeoData>> = LruCache::new(128);
        let mut result_tile: TileData<Scalar> = TileData::new(query.tile.clone());
        let query_time: Timestamp = *self.layer_info.timesteps.as_ref().map(|ts| ts.get(query.tpage as usize)).flatten().unwrap_or(&0);
        let closest_tpages_for_sources = self.layers.iter().map(|(_source_info, layer_info)| {
            if let Some(timestamps) = &layer_info.timesteps{
                let mut best_dist = Timestamp::MAX;
                let mut best_index = 0;
                for (i,t) in timestamps.iter().enumerate(){
                    let dist = (query_time - t).abs();
                    if dist < best_dist{
                        best_index = i;
                        best_dist = dist;
                    }
                }
                return best_index;
            }else{
                return 0;
            }
        }).collect::<Vec<_>>();
        debug!("Collected closest tpages");
        if self.layer_info.layer_type == GeoDataType::ScalarTiles{
            for index in 0 .. result_tile.data.len(){
                for (source_index,(source_info, layer_info)) in self.layers.iter().enumerate(){
                    if let Some(data) = cache.get(&source_index){
                        if let Some(GeoData::ScalarTiles(tiledata)) = data{
                            let data = tiledata.data[index];
                            if !data.is_nodata(){
                                result_tile.data[index] = data;
                                break;
                            }else{
                                continue;
                            }
                        }else{
                            continue;
                        }
                    }else{
                        let tpage = *closest_tpages_for_sources.get(index).unwrap_or(&0);
                        let layer_max_zoom_level = layer_info.max_zoom_level as usize;
                        let tile = Tile::from_tilepath(&query.tile.path[0..query.tile.path.len().min(layer_max_zoom_level)]).unwrap_or(query.tile.clone());
                        let request = SampleQuery::new(source_info.instance_name.clone(), layer_info.name.clone(), tile,tpage as u32, query.zpage);
                        debug!("Requesting tile {} for source {}", &query.tile.path[0..query.tile.path.len().min(layer_max_zoom_level)], source_info.instance_name);
                        let data = DATAPROVIDER.get().unwrap().get_geodata(request, GeoDataType::ScalarTiles).await?;
                        debug!("Received data");
                        if let Some(GeoData::ScalarTiles(tiledata)) = &data{
                            let data = tiledata.data[index];
                            if !data.is_nodata(){
                                result_tile.data[index] = data;
                            }
                        }
                        debug!("Storing tile {} for source {}", &query.tile.path[0..query.tile.path.len().min(layer_max_zoom_level)], source_info.instance_name);
                        cache.put(source_index, data);
                    }                 
                }
            }

            if result_tile.is_empty(){
                return Ok(None);
            }else{
                return Ok(Some(GeoData::ScalarTiles(result_tile)));
            }
            
        }else if self.layer_info.layer_type == GeoDataType::ColorTiles{
            for index in 0 .. result_tile.data.len(){
                for (source_index,(source_info, layer_info)) in self.layers.iter().enumerate(){
                    if let Some(data) = cache.get(&source_index){
                        if let Some(GeoData::ScalarTiles(tiledata)) = data{
                            let data = tiledata.data[index];
                            if !data.is_nodata(){
                                result_tile.data[index] = data;
                                break;
                            }else{
                                continue;
                            }
                        }else{
                            continue;
                        }
                    }else{
                        let tpage = *closest_tpages_for_sources.get(index).unwrap_or(&0);
                        let layer_max_zoom_level = layer_info.max_zoom_level as usize;
                        let tile = Tile::from_tilepath(&query.tile.path[0..query.tile.path.len().min(layer_max_zoom_level)]).unwrap_or(query.tile.clone());
                        let request = SampleQuery::new(source_info.instance_name.clone(), layer_info.name.clone(), tile,tpage as u32, query.zpage);
                        debug!("Requesting tile {} for source {}", &query.tile.path[0..query.tile.path.len().min(layer_max_zoom_level)], source_info.instance_name);
                        let data = DATAPROVIDER.get().unwrap().get_geodata(request, GeoDataType::ScalarTiles).await?;
                        debug!("Received data");
                        if let Some(GeoData::ScalarTiles(tiledata)) = &data{
                            let data = tiledata.data[index];
                            if !data.is_nodata(){
                                result_tile.data[index] = data;
                            }
                        }
                        debug!("Storing tile {} for source {}", &query.tile.path[0..query.tile.path.len().min(layer_max_zoom_level)], source_info.instance_name);
                        cache.put(source_index, data);
                    }                 
                }
            }
            if result_tile.is_empty(){
                return Ok(None);
            }else{
                return Ok(Some(GeoData::ScalarTiles(result_tile)));
            }
        }else{
            return Err(format!("Unsupported format: {:?}", self.layer_info.layer_type).into());
        }
    }
}