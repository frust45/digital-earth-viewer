//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use tileserver_model::*;

use netcdf::*;
use tileserver_model::Coord;

use std::{cmp::Ordering, error::Error};

use lazy_static::lazy_static;
use regex::Regex;
use std::convert::TryInto;
use std::path::Path;
use std::str::FromStr;
use tileserver_model::chrono::NaiveDate;

use tileserver_model::async_trait;

struct VariableIterator {
    indices: Vec<(usize, usize)>,
}
impl VariableIterator {
    #![allow(unused)]
    fn new(d: &[Dimension]) -> Self {
        VariableIterator {
            indices: d.iter().map(|dim| (0usize, dim.len() as usize)).collect(),
        }
    }
}
impl Iterator for VariableIterator {
    type Item = Vec<usize>;
    fn next(&mut self) -> Option<Vec<usize>> {
        let mut overflow = true;
        self.indices.iter_mut().for_each(|(i, l)| {
            if overflow {
                *i += 1;
                overflow = false;
            }
            if *i >= *l {
                *i = 0;
                overflow = true;
            }
        });
        match overflow {
            true => None,
            _ => Some(self.indices.iter().map(|(i, _l)| *i).collect()),
        }
    }
}

/// Source generating tiles from NetCDF raster files.
pub struct NetCDFSource {
    netcdf: netcdf::file::File,
    layers: Vec<LayerInfo>,
    timesteps: Vec<Timestamp>,
    heightsteps: Vec<f32>,
    lon_name_internal: String,
    lat_cell_bounds: Vec<LatCellBound>,
    lat_reversed: bool,
    lon_reversed: bool,
    lon_cell_bounds: Vec<LonCellBound>,
    lat_name_internal: String,
    time_name_internal: String,
    z_name_internal: String,
}

/// Parser/Converter trait, similar to `TryInto`
trait ToNumOption<T> {
    fn val(self) -> Option<T>;
}

impl ToNumOption<f64> for netcdf::attribute::AttrValue {
    fn val(self) -> Option<f64> {
        match self {
            netcdf::attribute::AttrValue::Uchar(x) => Some(x as f64),
            netcdf::attribute::AttrValue::Schar(x) => Some(x as f64),
            netcdf::attribute::AttrValue::Ushort(x) => Some(x as f64),
            netcdf::attribute::AttrValue::Short(x) => Some(x as f64),
            netcdf::attribute::AttrValue::Uint(x) => Some(x as f64),
            netcdf::attribute::AttrValue::Int(x) => Some(x as f64),
            netcdf::attribute::AttrValue::Ulonglong(x) => Some(x as f64),
            netcdf::attribute::AttrValue::Longlong(x) => Some(x as f64),
            netcdf::attribute::AttrValue::Float(x) => Some(x as f64),
            netcdf::attribute::AttrValue::Double(x) => Some(x),
            _ => None,
        }
    }
}

impl ToNumOption<f32> for netcdf::attribute::AttrValue {
    fn val(self) -> Option<f32> {
        match self {
            netcdf::attribute::AttrValue::Uchar(x) => Some(x as f32),
            netcdf::attribute::AttrValue::Schar(x) => Some(x as f32),
            netcdf::attribute::AttrValue::Ushort(x) => Some(x as f32),
            netcdf::attribute::AttrValue::Short(x) => Some(x as f32),
            netcdf::attribute::AttrValue::Uint(x) => Some(x as f32),
            netcdf::attribute::AttrValue::Int(x) => Some(x as f32),
            netcdf::attribute::AttrValue::Ulonglong(x) => Some(x as f32),
            netcdf::attribute::AttrValue::Longlong(x) => Some(x as f32),
            netcdf::attribute::AttrValue::Float(x) => Some(x),
            netcdf::attribute::AttrValue::Double(x) => Some(x as f32),
            _ => None,
        }
    }
}

impl ToNumOption<Vec<f64>> for netcdf::attribute::AttrValue {
    fn val(self) -> Option<Vec<f64>> {
        match self {
            netcdf::attribute::AttrValue::Uchars(x) => Some(x.iter().map(|f| *f as f64).collect()),
            netcdf::attribute::AttrValue::Schars(x) => Some(x.iter().map(|f| *f as f64).collect()),
            netcdf::attribute::AttrValue::Ushorts(x) => Some(x.iter().map(|f| *f as f64).collect()),
            netcdf::attribute::AttrValue::Shorts(x) => Some(x.iter().map(|f| *f as f64).collect()),
            netcdf::attribute::AttrValue::Uints(x) => Some(x.iter().map(|f| *f as f64).collect()),
            netcdf::attribute::AttrValue::Ints(x) => Some(x.iter().map(|f| *f as f64).collect()),
            netcdf::attribute::AttrValue::Ulonglongs(x) => {
                Some(x.iter().map(|f| *f as f64).collect())
            }
            netcdf::attribute::AttrValue::Longlongs(x) => {
                Some(x.iter().map(|f| *f as f64).collect())
            }
            netcdf::attribute::AttrValue::Floats(x) => Some(x.iter().map(|f| *f as f64).collect()),
            netcdf::attribute::AttrValue::Doubles(x) => Some(x),
            _ => None,
        }
    }
}

lazy_static! {
    static ref RE_SINCE: Regex =
        Regex::new(r"([a-z]+) since (\d{4})-(\d+)-(\d+) (\d{2}):(\d{2}):(\d{2})(.(\d))?").unwrap();
    static ref RE_DAY_AS: Regex = Regex::new(r"day as %Y%m%d\.%f").unwrap();
    static ref RE_SINCE_DATE: Regex = Regex::new(r"([a-z]+) since (\d{4})-(\d+)-(\d+)").unwrap();
}

/// This function converts date formats on the fly. Probably not the best for perfomance.
///
/// # Panics
/// This function panics if captures are missing, if the regex doesn't compile etc. If any of these fail,
/// the server is faulty anyways, so this needs to be tested rigorously before deployment.
fn time_from_parseable_scheme(time: f64, format: &str) -> Result<Timestamp, Box<dyn Error>> {
    if let Some(cs) = RE_SINCE.captures(format) {
        let dt = NaiveDate::from_ymd(
            i32::from_str(cs.get(2).unwrap().as_str()).unwrap(),
            u32::from_str(cs.get(3).unwrap().as_str()).unwrap(),
            u32::from_str(cs.get(4).unwrap().as_str()).unwrap(),
        )
        .and_hms_milli(
            u32::from_str(cs.get(5).unwrap().as_str()).unwrap(),
            u32::from_str(cs.get(6).unwrap().as_str()).unwrap(),
            u32::from_str(cs.get(7).unwrap().as_str()).unwrap(),
            cs.get(8)
                .map(|_| u32::from_str(cs.get(9).unwrap().as_str()).unwrap() * 100)
                .unwrap_or(0),
        );
        match cs.get(1).unwrap().as_str() {
            "days" => Ok(dt.timestamp_millis() + (time * 24.0 * 60.0 * 60.0 * 1000.0) as i64),
            "hours" => Ok(dt.timestamp_millis() + (time * 60.0 * 60.0 * 1000.0) as i64),
            "minutes" => Ok(dt.timestamp_millis() + (time * 60.0 * 1000.0) as i64),
            "seconds" => Ok(dt.timestamp_millis() + (time * 1000.0) as i64),
            _ => Err("Failed parsing \"Since\"-style date.".into()),
        }
    } else if let Some(_m) = RE_DAY_AS.find(format) {
        let intraday = (time.fract() * 24.0 * 60.0 * 60.0 * 1000.0) as i64;
        let day = time.trunc() as i64;
        let d = NaiveDate::from_ymd(
            ((day / 100 / 100) % (std::i32::MAX as i64 + 1))
                .try_into()
                .unwrap(),
            ((day / 100) % 100).try_into().unwrap(),
            (day % 100).try_into().unwrap(),
        )
        .and_hms(0, 0, 0);
        Ok(d.timestamp_millis() + intraday)
    } else if let Some(cs) = RE_SINCE_DATE.captures(format) {
        let dt = NaiveDate::from_ymd(
            i32::from_str(cs.get(2).unwrap().as_str()).unwrap(),
            u32::from_str(cs.get(3).unwrap().as_str()).unwrap(),
            u32::from_str(cs.get(4).unwrap().as_str()).unwrap(),
        )
        .and_hms(0, 0, 0);
        match cs.get(1).unwrap().as_str() {
            "days" => Ok(dt.timestamp_millis() + (time * 24.0 * 60.0 * 60.0 * 1000.0) as i64),
            "hours" => Ok(dt.timestamp_millis() + (time * 60.0 * 60.0 * 1000.0) as i64),
            "minutes" => Ok(dt.timestamp_millis() + (time * 60.0 * 1000.0) as i64),
            "seconds" => Ok(dt.timestamp_millis() + (time * 1000.0) as i64),
            _ => Err("Failed parsing \"Since\"-style date.".into()),
        }
    } else {
        error!(
            "Time format not supported, format: {}, time: {}",
            format, time
        );
        Err("Time format not supported.".into())
    }
}

const MAX_SAMPLEPOINTS: usize = 1024 * 1024;

#[derive(Debug)]
struct OneDimensionalSamplingParameters {
    start: usize,
    stride: isize,
    count: usize,
}
impl OneDimensionalSamplingParameters {
    fn new(start: usize, stride: isize, count: usize) -> Self {
        OneDimensionalSamplingParameters {
            start,
            stride,
            count,
        }
    }
}
#[derive(Debug)]
struct LatCellBound {
    start: f64,
    end: f64,
}
impl LatCellBound {
    fn from_coord(start: f64, end: f64) -> Self {
        //Reversed since UEC is reversed on the y axis compared to easting/northing
        let start_internal = start.max(end);
        let end_internal = start.min(end);
        Self {
            start: UEC::from(Coord::new(start_internal, 0.0)).y,
            end: UEC::from(Coord::new(end_internal, 0.0)).y,
        }
    }
    fn overlaps(&self, tile: &Tile) -> bool {
        self.end > tile.position.y && self.start < tile.position.y + tile.size.y
    }
    fn len(&self) -> f64 {
        let mut end_internal = self.end;
        while end_internal < self.start {
            end_internal += 1.0;
        }
        end_internal - self.start
    }
}

struct LonCellBound {
    start: f64,
    end: f64,
}
impl LonCellBound {
    fn from_coord(start: f64, end: f64) -> Self {
        let start_internal = start.min(end);
        let end_internal = start.max(end);
        Self {
            start: UEC::from(Coord::new(0.0, start_internal)).x,
            end: UEC::from(Coord::new(0.0, end_internal)).x,
        }
    }
    fn overlaps(&self, tile: &Tile) -> bool {
        if self.start > self.end {
            self.start - 1.0 < tile.position.x + tile.size.x && self.end > tile.position.x     
        } else {
            self.start < tile.position.x + tile.size.x && self.end > tile.position.x
        }
    }
    fn len(&self) -> f64 {
        let mut end_internal = self.end;
        while end_internal < self.start {
            end_internal += 1.0;
        }
        end_internal - self.start
    }
}
#[derive(Debug)]
struct IndexRange {
    first: usize,
    last: usize,
}
impl IndexRange {
    fn new(first: usize, last: usize) -> Self {
        IndexRange {
            first,
            last,
        }
    }
    fn len(&self) -> usize {
        self.last + 1 - self.first
    }
}

impl NetCDFSource {
    fn get_netcdf_data_rect(
        &self,
        var: &Variable,
        lonb: &OneDimensionalSamplingParameters,
        latb: &OneDimensionalSamplingParameters,
        time: usize,
        height: usize,
    ) -> Result<Vec<f32>, Box<dyn std::error::Error + Send + Sync>> {
        let indices: Vec<usize> = var
            .dimensions()
            .iter()
            .map(|d| {
                if d.name().as_str() == self.lat_name_internal {
                    latb.start
                } else if d.name().as_str() == self.lon_name_internal {
                    lonb.start
                } else if d.name().as_str() == self.time_name_internal {
                    time as usize
                } else if d.name().as_str() == self.z_name_internal {
                    height as usize
                } else {
                    0
                }
            })
            .collect();
        let counts: Vec<usize> = var
            .dimensions()
            .iter()
            .map(|d| {
                if d.name().as_str() == self.lat_name_internal {
                    latb.count
                } else if d.name().as_str() == self.lon_name_internal {
                    lonb.count
                } else {
                    1
                }
            })
            .collect();
        let strides: Vec<isize> = var
            .dimensions()
            .iter()
            .map(|d| {
                if d.name().as_str() == self.lat_name_internal {
                    latb.stride
                } else if d.name().as_str() == self.lon_name_internal {
                    lonb.stride
                } else {
                    1
                }
            })
            .collect();

        let mut array: Vec<f32> = vec![0.0; latb.count * lonb.count];
        tileserver_model::tokio::task::block_in_place(|| {
            var.values_strided_to(&mut array, Some(&indices), Some(&counts), &strides)
        })?;

        let mut latrank = 0;
        let mut lonrank = 0;
        var.dimensions().iter().enumerate().for_each(|(i, d)| {
            if d.name().as_str() == self.lat_name_internal {
                latrank = i;
            } else if d.name().as_str() == self.lon_name_internal {
                lonrank = i;
            }
        });
        /*
         * `i` from 0 to latcount
         * `j` from 0 to loncount
         */
        if lonrank < latrank {
            array = (0..lonb.count)
                .into_iter()
                .map(|i| (0..latb.count).into_iter().map(move |j| (i, j)))
                .flatten()
                .map(|(i, j)| array[i * latb.count + j])
                .collect();
        }

        var.attribute("_FillValue")
            .and_then(|a| a.value().ok())
            .and_then(|v| v.val())
            .map(|v: f32| {
                array.iter_mut().for_each(|e| {
                    if *e == v {
                        *e = std::f32::NAN;
                    }
                })
            });
            
        var.attribute("missing_value")
            .and_then(|a| a.value().ok())
            .and_then(|v| v.val())
            .map(|v: f32| {
                array.iter_mut().for_each(|e| {
                    if *e == v {
                        *e = std::f32::NAN;
                    }
                })
            });
        var.attribute("scale_factor")
            .and_then(|a| a.value().ok())
            .and_then(|v| v.val())
            .map(|v: f32| {
                array.iter_mut().for_each(|e| {
                    *e *= v;
                })
            });
        var.attribute("add_offset")
            .and_then(|a| a.value().ok())
            .and_then(|v| v.val())
            .map(|v: f32| {
                array.iter_mut().for_each(|e| {
                    *e += v;
                })
            });

        Ok(array)
    }

    fn get_netcdf_lat_cellbounds(
        var: Option<netcdf::Variable>,
        centered: bool,
    ) -> Result<Vec<LatCellBound>, Box<dyn std::error::Error + Send + Sync>> {
        var.filter(|v| v.dimensions().len() == 1)
            .and_then(|v| {
                let d = v.dimensions()[0].len();
                let mut a: Vec<f64> = vec![0.0; d];
                v.values_to(&mut a, Some(&[0]), Some(&[d]))
                    .ok()
                    .map(|_| a)
                    .filter(|a| a.len() > 1)
            })
            .map(|a| {
                let cs = (a[1] - a[0]).abs();
                if centered {
                    a.iter()
                        .map(|x| LatCellBound::from_coord(*x - cs / 2.0, *x + cs / 2.0))
                        .collect()
                } else {
                    a.iter()
                        .map(|x| LatCellBound::from_coord(*x, *x + cs))
                        .collect()
                }
            })
            .ok_or_else(|| "Could not determine cell spacing and size on 'latitude'".into())
    }

    fn get_netcdf_lon_cellbounds(
        var: Option<netcdf::Variable>,
        centered: bool,
    ) -> Result<Vec<LonCellBound>, Box<dyn std::error::Error + Send + Sync>> {
        var.filter(|v| v.dimensions().len() == 1)
            .and_then(|v| {
                let d = v.dimensions()[0].len();
                let mut a: Vec<f64> = vec![0.0; d];
                v.values_to(&mut a, Some(&[0]), Some(&[d]))
                    .ok()
                    .map(|_| a)
                    .filter(|a| a.len() > 1)
            })
            .map(|a| {
                let cs = (a[1] - a[0]).abs();
                if centered {
                    a.iter()
                        .map(|x| LonCellBound::from_coord(*x - cs / 2.0, *x + cs / 2.0))
                        .collect()
                } else {
                    a.iter()
                        .map(|x| LonCellBound::from_coord(*x, *x + cs))
                        .collect()
                }
            })
            .ok_or_else(|| "Could not determine cell spacing and size on dimension 'longitude'.".into())
    }

    fn get_continuous_subregions(
        lat_cell_bounds: &[LatCellBound],
        lon_cell_bounds: &[LonCellBound],
        tile: &Tile,
    ) -> Option<(Vec<IndexRange>, Vec<IndexRange>)> {
        let mut latcb_iter = lat_cell_bounds
            .iter()
            .enumerate()
            .filter_map(|(i, lat_cell)| {
                if lat_cell.overlaps(tile) {
                    Some(i)
                } else {
                    None
                }
            });
        let lat_first = latcb_iter.next()?;
        let lat_ranges = latcb_iter.fold(
            vec![IndexRange::new(lat_first, lat_first)],
            |mut state, i| {
                if i > (state.last().unwrap().last + 1) {
                    state.push(IndexRange::new(i, i));
                } else {
                    state.last_mut().unwrap().last = i;
                }
                state
            },
        );

        let mut loncb_iter = lon_cell_bounds
            .iter()
            .enumerate()
            .filter_map(|(i, lon_cell)| {
                if lon_cell.overlaps(tile) {
                    Some(i)
                } else {
                    None
                }
            });
        let lon_first = loncb_iter.next()?;
        let lon_ranges = loncb_iter.fold(
            vec![IndexRange::new(lon_first, lon_first)],
            |mut state, i| {
                if i > (state.last().unwrap().last + 1) {
                    state.push(IndexRange::new(i, i));
                } else {
                    state.last_mut().unwrap().last = i;
                }
                state
            },
        );

        Some((lon_ranges, lat_ranges))
    }

    fn select_subregions_for_tileregions(
        tile: &Tile,
        lat_cell_bounds: &[LatCellBound],
        lat_ranges: &[IndexRange],
        lat_reversed: bool,
        lon_cell_bounds: &[LonCellBound],
        lon_ranges: &[IndexRange],
        lon_reversed: bool,
    ) -> (
        Vec<((usize, usize, bool), OneDimensionalSamplingParameters)>,
        Vec<((usize, usize, bool), OneDimensionalSamplingParameters)>,
    ) {
        let latregions: Vec<((usize, usize, bool), OneDimensionalSamplingParameters)> = lat_ranges
            .iter()
            .map(|lat_range| {
                if lat_reversed {
                    let region_bounds = (
                        lat_cell_bounds[lat_range.last].start,
                        lat_cell_bounds[lat_range.first].start
                            + lat_cell_bounds[lat_range.first].len(),
                    );
                    let region_in_tile = (
                        (region_bounds.0 - tile.position.y) / tile.size.y,
                        (region_bounds.1 - tile.position.y) / tile.size.y,
                    );
                    let pixel_region = (
                        ((region_in_tile.0.max(0.0) * constants::TILESIZE.1 as f64) as usize)
                            .min(constants::TILESIZE.1 - 1),
                        ((region_in_tile.1.max(0.0) * constants::TILESIZE.1 as f64) as usize)
                            .min(constants::TILESIZE.1 - 1),
                    );
                    let stride = lat_range.len() / (pixel_region.1 - pixel_region.0 + 1) + 1;
                    let count = lat_range.len() / stride;
                    //TODO: Shift first so the sampled range is more symmetrical inside the required range
                    (
                        (pixel_region.0, pixel_region.1 - pixel_region.0 + 1, true),
                        OneDimensionalSamplingParameters::new(
                            lat_range.first,
                            stride as isize,
                            count,
                        ),
                    )
                } else {
                    let region_bounds = (
                        lat_cell_bounds[lat_range.first].start,
                        lat_cell_bounds[lat_range.last].start
                            + lat_cell_bounds[lat_range.last].len(),
                    );
                    let region_in_tile = (
                        (region_bounds.0 - tile.position.y) / tile.size.y,
                        (region_bounds.1 - tile.position.y) / tile.size.y,
                    );
                    let pixel_region = (
                        ((region_in_tile.0.max(0.0) * constants::TILESIZE.1 as f64) as usize)
                            .min(constants::TILESIZE.1 - 1),
                        ((region_in_tile.1.max(0.0) * constants::TILESIZE.1 as f64) as usize)
                            .min(constants::TILESIZE.1 - 1),
                    );
                    let stride = lat_range.len() / (pixel_region.1 - pixel_region.0 + 1) + 1;
                    let count = lat_range.len() / stride;
                    //TODO: Shift first so the sampled range is more symmetrical inside the required range
                    (
                        (pixel_region.0, pixel_region.1 - pixel_region.0 + 1, false),
                        OneDimensionalSamplingParameters::new(
                            lat_range.first,
                            stride as isize,
                            count,
                        ),
                    )
                }
            })
            .collect();
        let lonregions: Vec<((usize, usize, bool), OneDimensionalSamplingParameters)> = lon_ranges
            .iter()
            .map(|lon_range| {
                if lon_reversed {
                    unimplemented!()
                } else {
                    let region_bounds = if lon_cell_bounds[lon_range.first].start > lon_cell_bounds[lon_range.last].start + lon_cell_bounds[lon_range.last].len() {
                            (
                                lon_cell_bounds[lon_range.first].start - 1.0,
                                lon_cell_bounds[lon_range.last].start
                                + lon_cell_bounds[lon_range.last].len()
                            )
                        } else {
                            (
                                lon_cell_bounds[lon_range.first].start,
                                lon_cell_bounds[lon_range.last].start
                                + lon_cell_bounds[lon_range.last].len(),
                        )};
                    let region_in_tile = (
                        (region_bounds.0 - tile.position.x) / tile.size.x,
                        (region_bounds.1 - tile.position.x) / tile.size.x,
                    );
                    let pixel_region = (
                        ((region_in_tile.0.max(0.0) * constants::TILESIZE.0 as f64) as usize)
                            .min(constants::TILESIZE.0 - 1),
                        ((region_in_tile.1.max(0.0) * constants::TILESIZE.0 as f64) as usize)
                            .min(constants::TILESIZE.0 - 1),
                    );
                    let stride = lon_range.len() / (pixel_region.1 - pixel_region.0 + 1) + 1;
                    let count = lon_range.len() / stride;
                    trace!("tile: {:?}, region_bounds: {:?}", tile.path, region_bounds);
                    //TODO: Shift first so the sampled range is more symmetrical inside the required range
                    (
                        (pixel_region.0, pixel_region.1 - pixel_region.0 + 1, false),
                        OneDimensionalSamplingParameters::new(
                            lon_range.first,
                            stride as isize,
                            count,
                        ),
                    )
                }
            })
            .collect();
        (lonregions, latregions)
    }
}

#[async_trait]
impl SourceInit for NetCDFSource {
    fn accepts_file(file: &std::path::Path) -> bool {
        file.extension()
            .map(|ext| ext.to_string_lossy().ends_with(".nc"))
            .unwrap_or(false) // Netcdf-files end with .nc
    }

    async fn initialize(
        settings: SourceConfig,
        mut source_info: SourceInfo,
    ) -> SourceInitResult {
        if settings.input_files.is_empty() {
            return Err("No netcdf files given.".into());
        }
        let lat_name = settings
            .settings
            .get("lat_name").cloned()
            .unwrap_or_else(|| "lat".to_string());
        let lon_name = settings
            .settings
            .get("lon_name").cloned()
            .unwrap_or_else(|| "lon".to_string());
        let time_name = settings
            .settings
            .get("time_name").cloned()
            .unwrap_or_else(|| "time".to_string());
        let z_name = settings
            .settings
            .get("height_name").cloned()
            .unwrap_or_else(|| "height".to_string());

        let file = netcdf::open(&settings.input_files[0])?;
        file.variable(&lat_name).ok_or(format!(
            "netCDF file has no latitude variable \"{}\".",
            &lat_name
        ))?;
        file.variable(&lon_name).ok_or(format!(
            "netCDF file has no longitude variable \"{}\".",
            &lon_name
        ))?;

        let mut lat_name_internal: String = lat_name.clone();
        let mut lat_reversed: bool = false;
        let lat_bounds: Option<(f64, f64)> = file.variable(&lat_name).and_then(|v| {
            let d = v.dimensions();
            if d.len() != 1 {
                warn!("Latitude has more than one dimension!");
                return None;
            };
            lat_name_internal = d[0].name();
            let mut a: Vec<f64> = vec![0.0; d[0].len()];
            match v.values_to(&mut a, Some(&[0]), Some(&[d[0].len()])) {
                Ok(()) => (),
                Err(_e) => return None,
            };
            if a[1] > a[0] {lat_reversed = true;}
            let r = a
                .iter()
                .map(|v| {
                    let c = Coord::new(*v, 0.0);
                    UEC::from(c).y
                })
                .fold((f64::INFINITY, f64::NEG_INFINITY), |(lmin, lmax), val| {
                    (val.min(lmin), val.max(lmax))
                });
            if r.0.is_finite() && r.1.is_finite() {
                Some(r)
            } else {
                None
            }
        });

        let mut lon_name_internal: String = lon_name.clone();
        let mut lon_reversed = false;
        let lon_bounds: Option<(f64, f64)> = file.variable(&lon_name).and_then(|v| {
            let d = v.dimensions();
            if d.len() != 1 {
                warn!("Longitude has more than one dimension!");
                return None;
            };
            lon_name_internal = d[0].name();
            let mut a: Vec<f64> = vec![0.0; d[0].len()];
            match v.values_to(&mut a, Some(&[0]), Some(&[d[0].len()])) {
                Ok(()) => (),
                Err(_e) => return None,
            };
            if a[0] > a[1] {lon_reversed = true;}
            let r = a
                .iter()
                .map(|v| {
                    let c = Coord::new(0.0, *v);
                    UEC::from(c).x
                })
                .fold((f64::INFINITY, f64::NEG_INFINITY), |(lmin, lmax), val| {
                    (val.min(lmin), val.max(lmax))
                });
            if r.0.is_finite() && r.1.is_finite() {
                Some(r)
            } else {
                None
            }
        });

        let extent: Option<UECArea> = match (lat_bounds, lon_bounds) {
            (Some(lab), Some(lob)) => {
                Some(UECArea::new(UEC::xy(lob.0, lab.0), UEC::xy(lob.1 - lob.0, lab.1 - lab.0)))
            }
            _ => None,
        };

        let mut time_name_internal: String = time_name.clone();
        let timesteps_ = file.variable(&time_name).and_then(|v| {
            let ds = v.dimensions();
            let tform = match v.attribute("units").and_then(|a| match a.value() {
                Ok(netcdf::attribute::AttrValue::Str(u)) => Some(u),
                _ => None,
            }) {
                Some(s) => s,
                _ => return None,
            };
            if ds.len() != 1 {
                return None;
            };
            time_name_internal = ds[0].name();
            let mut a: Vec<f64> = vec![0.0; ds[0].len()];
            match v.values_to(&mut a, Some(&[0]), Some(&[ds[0].len()])) {
                Ok(()) => (),
                Err(_e) => return None,
            };
            a.iter()
                .map(|t| time_from_parseable_scheme(*t, &tform))
                .collect::<Result<Vec<i64>, _>>()
                .ok()
        });

        let mut z_name_internal: String = z_name.clone();
        let heightsteps_ = file.variable(&z_name).and_then(|v| {
            let ds = v.dimensions();
            debug!("ds: {:?}", ds);
            if ds.len() != 1 {
                return None;
            };
            z_name_internal = ds[0].name();
            let mut a = vec![0f32; ds[0].len()];
            if let Err(e) = v.values_to(&mut a, Some(&[0]), Some(&[ds[0].len()])) {
                error!("Error sampling height steps: {:?}", e);
                return None;
            }
            Some(a)
        });

        let max_zoom_level_ = file
            .variable(&lon_name)
            .filter(|v| v.dimensions().len() == 1)
            .and_then(|v| {
                v.value(Some(&[1]))
                    .and_then(|v1: f64| v.value(Some(&[0])).map(|v0: f64| (v, v1 - v0)))
                    .ok()
            })
            .map(|(v, rdelta)| {
                /*  Get the overall longitudinal size of the dataset. This assumes a uniform cell size!
                 */
                let dlen = v.dimensions()[0].len() as f64;
                let rsize = dlen * rdelta;
                let tcells: f64 = 360.0 / rsize * dlen;
                (tcells / (constants::TILESIZE.0 as f64)).log2().ceil() as u8
            })
            .ok_or_else(|| "Zoomlevel could not be determined!".to_string())? + 1;

        source_info.layers = file.variables().filter_map(|x| {
            if x.name() == lon_name
            || x.name() == lat_name
            || x.name() == time_name
            || x.name() == z_name
            || !x.dimensions().iter().any(|d| d.name().as_str() == lat_name_internal)
            || !x.dimensions().iter().any(|d| d.name().as_str() == lon_name_internal)
            {
                None
            } else {
                Some(LayerInfo{
                    name:
                        x.name(),
                    long_name: 
                        x.attribute("long_name")
                        .and_then(|a| match a.value() {
                            Ok(netcdf::attribute::AttrValue::Str(n)) => Some(n),
                            _ => None
                        }),
                    unit:
                        x.attribute("units")
                        .and_then(|a| match a.value() {
                            Ok(netcdf::attribute::AttrValue::Str(u)) => Some(u),
                            _ => None
                        }),
                    extent,
                    layer_type:
                        GeoDataType::ScalarTiles,
                    timesteps:
                        /* Copies the global timesteps
                         */
                        timesteps_.as_ref().map(|t| t.iter().copied().collect()),
                    timerange: timesteps_.as_ref().map(|t| {Some((t[0], *t.get(t.len() - 1).unwrap_or(&t[0])))}).flatten(),
                    zsteps: heightsteps_.as_ref().map(|hsteps| hsteps.iter().map(|h| *h ).collect()).clone(),
                    datarange:
                        /*  Attempt to find the actual_range attribute. If the attribute does not
                         *  exist, check the number of points in the variable. Sample all points,
                         *  unless the number of points exceeds the maximum set in
                         *  MAX_SAMPLEPOINTS, if it does, sample strided. Then calculate min/max
                         *  of the sampled points, filtering NODATA values. Finally, if one is 
                         *  given for the variable, apply the packing transformation to the output
                         *  values. 
                         * 
                         *  Currently missing: Filtering by valid_range, valid_min or valid_max
                         */
                        x.attribute("actual_range")
                        .and_then(|a| a.value().ok())
                        .and_then(|v| v.val())
                        .filter(|v: &Vec<f64>| v.len() == 2)
                        .map(|v| (v[0] as f32, v[1] as f32))
                        .or_else(||{
                            let d = x.dimensions();
                            let fill_value: Option<f32> = x.attribute("_FillValue")
                                .and_then(|a| a.value().ok())
                                .and_then(|v| v.val());
                            let missing_value: Option<f32> = x.attribute("missing_value")
                                .and_then(|a| a.value().ok())
                                .and_then(|v| v.val());
                            let scale_factor: f32 = x.attribute("scale_factor")
                                .and_then(|a| a.value().ok())
                                .and_then(|v| v.val())
                                .unwrap_or(1.0);
                            let add_offset: f32 = x.attribute("add_offset")
                                .and_then(|a| a.value().ok())
                                .and_then(|v| v.val())
                                .unwrap_or(0.0);
                            let totalsize: usize = d.iter().map(|d| d.len()).product();
                            let mut a: Vec<f32>;
                            (if totalsize <= MAX_SAMPLEPOINTS{
                                /* Sample directly
                                 */
                                a = vec![0.0;totalsize];
                                x.values_to(
                                    &mut a,
                                    Some(&d.iter().map(|_| 0).collect::<Vec<usize>>()),
                                    Some(&d.iter().map(|d| d.len()).collect::<Vec<usize>>()),
                                ).ok()
                            }else{
                                let mut strides = d.iter().map(|_| 1).collect::<Vec<isize>>();
                                let lens = d.iter().map(|d| d.len()).collect::<Vec<usize>>();
                                let mut counts = d.iter().map(|d| d.len()).collect::<Vec<usize>>();
                                let mut tsize = counts.iter().product();
                                /* Increase stride one-by-one.
                                 * Adds extra stride steps to the dimension where most steps would be
                                 * sampled, until the prospective size is less than MAX_SAMPLEPOINTS.
                                 * The resulting range can thus only be an inner bound.
                                 */
                                while tsize > MAX_SAMPLEPOINTS {
                                    let i: usize = counts.iter()
                                        .enumerate()
                                        .max_by_key(|(_i, e)| *e)
                                        .unwrap().0;
                                    strides[i] += 1;
                                    counts[i] = lens[i] / strides[i] as usize;
                                    tsize = counts.iter().product();
                                }
                                /* Sample strided
                                 */ 
                                a = vec![0.0;tsize];
                                x.values_strided_to(
                                    &mut a,
                                    Some(&d.iter().map(|_| 0).collect::<Vec<usize>>()),
                                    Some(&counts),
                                    &strides
                                ).map(|_| ()).map_err(|e| error!("{}", e)).ok()
                            }).and_then(|_| {
                                let rt = a.iter().filter(|e|
                                    {(match fill_value {
                                        Some(fv) => !(**e == fv),
                                        _ => true
                                    }) && (match missing_value {
                                        Some(mv) => !(**e == mv),
                                        _ => true
                                    }) && !e.is_nan()}
                                ).fold((std::f32::INFINITY, std::f32::NEG_INFINITY), |(min, max), e| 
                                    (if min > *e {*e} else {min}, if max < *e {*e} else {max})
                                );
                                if rt.0.is_finite() && rt.1.is_finite() {
                                    if scale_factor > 0.0 {
                                        Some((rt.0 as f32 * scale_factor + add_offset, rt.1 as f32 * scale_factor + add_offset))
                                    }else{
                                        Some((rt.1 * scale_factor + add_offset, rt.0 * scale_factor + add_offset))
                                    }
                                } else {
                                    None
                                }
                            })
                        }),
                    zrange: heightsteps_.as_ref().map(|h|{ let mut sorted_h = h.clone(); sorted_h.sort_unstable_by(|a,b| a.partial_cmp(b).unwrap_or(Ordering::Less)); if h.len() == 1 {(h[0], h[0])} else {(h[0], h[h.len() -1])} }),
                    max_zoom_level:
                        max_zoom_level_,
                    default_overlay:
                        None,
                    })
            }}).collect();

        source_info.instance_name = {
            let p = file.path();
            const DEFAULT: &str = "Unidentified NetCDF File";
            match p {
                Ok(a) => {
                    let p2 = Path::new(&a);
                    p2.file_name()
                        .map(|x| x.to_string_lossy().into_owned())
                        .unwrap_or_else(|| DEFAULT.to_string())
                }
                _ => DEFAULT.to_string(),
            }
        };

        let lon_cell_bounds =
            Self::get_netcdf_lon_cellbounds(file.variable(&lon_name), true)?;
        let lat_cell_bounds =
            Self::get_netcdf_lat_cellbounds(file.variable(&lat_name), true)?;

        Ok((source_info.clone(),
            Box::new(NetCDFSource {
            netcdf: file,
            layers: source_info.layers.clone(),
            timesteps: timesteps_.unwrap_or_else(|| vec![0]),
            heightsteps: heightsteps_.unwrap_or_else(|| vec![0f32]),
            lat_name_internal,
            lat_reversed,
            lon_name_internal,
            lon_reversed,
            lat_cell_bounds,
            lon_cell_bounds,
            time_name_internal,
            z_name_internal,
        })))
    }
}

#[async_trait]
impl Source for NetCDFSource {
    
    async fn sample(
        &self,
        query: &SampleQuery,
    ) -> Result<Option<GeoData>, Box<dyn std::error::Error + Send + Sync>> {
        self.timesteps
            .get(query.tpage as usize)
            .ok_or_else(|| format!("No timestep with index `{}`.", query.tpage))?;
        self.heightsteps
            .get(query.zpage as usize)
            .ok_or_else(|| format!("No zstep with index {}", query.zpage))?;

        let var: Variable;
        match self.netcdf.variable(&query.layername).and_then(|v| {
            self.layers
                .iter()
                .find(|l| *l.name == query.layername)
                .map(|_| v)
        }) {
            Some(v) => var = v,
            _ => {
                return Err("Layer does not exist in this file.".into());
            }
        }
        /*  Identify time step
         */
        let tindex = query.tpage;
        let zindex = query.zpage;

        let (lon_regions, lat_regions) = match Self::get_continuous_subregions(
            &self.lat_cell_bounds,
            &self.lon_cell_bounds,
            &query.tile,
        ) {
            Some(x) => x,
            _ => return Ok(None),
        };

        let (lon_regions_in_tile, lat_regions_in_tile) = Self::select_subregions_for_tileregions(
            &query.tile,
            &self.lat_cell_bounds,
            &lat_regions,
            self.lat_reversed,
            &self.lon_cell_bounds,
            &lon_regions,
            self.lon_reversed,
        );
        let mut res_array = vec![f32::NAN; constants::TILESIZE.0 * constants::TILESIZE.1];
        if lon_regions_in_tile
            .iter()
            .all(|((tile_lonstart, tile_loncount, lon_reverse), lonb)| {
                lat_regions_in_tile.iter().all(
                    |((tile_latstart, tile_latcount, lat_reverse), latb)| {
                        if latb.count == 0 || lonb.count == 0 {
                            true
                        } else {
                            self.get_netcdf_data_rect(
                                &var,
                                lonb,
                                latb,
                                tindex as usize,
                                zindex as usize,
                            )
                            .map(|a| {
                                let source_lon_count = lonb.count;
                                let source_lat_count = latb.count;
                                (*tile_lonstart..(*tile_lonstart + *tile_loncount))
                                    .into_iter()
                                    .map(|x| {
                                        (*tile_latstart..(*tile_latstart + *tile_latcount))
                                            .into_iter()
                                            .map(move |y| (x, y))
                                    })
                                    .flatten()
                                    .for_each(|(x, y)| {
                                        let source_x = ((x - *tile_lonstart) * source_lon_count)
                                            / *tile_loncount;
                                        let source_y = ((y - *tile_latstart) * source_lat_count)
                                            / *tile_latcount;
                                        res_array[x + y * constants::TILESIZE.0] =
                                            a[if *lon_reverse {
                                                source_lon_count - source_x - 1
                                            } else {
                                                source_x
                                            } + (if *lat_reverse {
                                                source_lat_count - source_y - 1
                                            } else {
                                                source_y
                                            }) * source_lon_count]
                                    });
                                true
                            })
                            .unwrap_or_else(|e| {
                                trace!("netcdf error: {:?}", e);
                                false
                            })
                        }
                    },
                )
            })
        {
            let tiledata = TileData::new_from_tile_and_raw_data(query.tile.clone(), res_array);
            if tiledata.is_empty() {
                Ok(None)
            } else {
                Ok(Some(GeoData::ScalarTiles(tiledata)))
            }
        } else {
            Err("Error sampling from netcdf.".into())
        }
    }
}
