//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

fn main(){
    //Speed up manual build
    std::env::set_var("CMAKE_BUILD_PARALLEL_LEVEL", format!("{}", num_cpus::get()));
}