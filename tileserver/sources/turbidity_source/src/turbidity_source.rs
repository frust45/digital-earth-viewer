//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use std::fs;
use serde::{Deserialize, Serialize};
use std::error::Error;
use tileserver_model::{Coord, GeoData, Source, SourceConfig, SourceInfo, SourceInit, SourceInitResult, UEC};
use tileserver_model::async_trait;
use tileserver_model::MultiColumnPointSourceBuilder;
use tileserver_model::Timestamp;

#[allow(non_snake_case)]
#[derive(Serialize, Deserialize)]
struct TurbidityHeader {
    name: String,
    campaign: String,
    station: String,
    platform: String,
    latitude_dec_deg: f32,
    longitude_dec_deg: f32,
    depth_m: f32,
    dateStart: String,
    timeStart: String,
    value_name: [String; 5], 
    units: [String; 5],  
    comments: String,

}

pub struct TurbiditySource {
    builder: MultiColumnPointSourceBuilder

}

#[derive(Debug)]
pub struct TurbidityLine {
    timestamp: i64,
    depth: f32,
    temperature: f32,
    turbidity_m: f32,
    tspm: f32,
    batt: f32,
}

#[async_trait]
impl SourceInit for TurbiditySource{

    async fn initialize(settings: SourceConfig, mut source_info: SourceInfo) -> SourceInitResult {
        if settings.input_files.len()<1 {
            return Err("No input file found".into());
        }
        let filename = &settings.input_files[0];
    
        let header_lines = 13;
        
        let contents = fs::read_to_string(filename)
            .map_err(|error|format!("Something went wrong reading the file:{}", error))?;
        
        let mut header = String::new();

        for (index, line ) in contents.lines().enumerate(){

            if index == header_lines{
                break;
            }
            header.push_str(line);

        }

        let header: TurbidityHeader = serde_json::from_str(&header).map_err(|error|format!("JSON was not well-formatted: {}",error))?;

        let mut source_builder = MultiColumnPointSourceBuilder::new(
            source_info.clone(),
            &filename.file_name().map(|filename| filename.to_string_lossy().to_string()).unwrap_or("unknown file".to_string()),
            &["Depth", "Temperature", "Turbidity_m", "TSPM", "Batt"]);

        source_builder.set_unit("Depth","m");
        source_builder.set_unit("Temperature","°C");
        source_builder.set_unit("Turbidity_m","FTU");
        source_builder.set_unit("TSPM","mg/L");
        source_builder.set_unit("Batt","V");

        source_builder.set_default_overlay("TSPM", "TurbidityOverlay");

        let position:UEC = Coord::new(header.latitude_dec_deg as f64, header.longitude_dec_deg as f64).into();

        for line in contents.lines().skip(header_lines + 1){

            let parts = line.split(",").collect::<Vec<&str>>();
            
            let timestamp: Timestamp =  (parts[0].parse::<i64>()?)*1000;
            let depth: f32 =  parts[1].parse().unwrap_or_else(|_| std::f32::NAN);
            let temperature: f32 = parts[2].parse().unwrap_or_else(|_| std::f32::NAN);
            let turbidity_m: f32 = parts[3].parse().unwrap_or_else(|_| std::f32::NAN);
            let tspm: f32 = parts[4].parse().unwrap_or_else(|_| std::f32::NAN);
            let batt: f32 = parts[5].parse().unwrap_or_else(|_| std::f32::NAN);

            source_builder.add_value_row(position, depth, timestamp, &[depth, temperature, turbidity_m, tspm, batt]);

        }

        source_builder.finish();
        
        source_info = source_builder.get_source_info();
        
        let source = TurbiditySource{    
            builder:source_builder
        };

       

        Ok((source_info, Box::new(source)))

    }

    fn accepts_file(file: &std::path::Path) -> bool {
        file.extension().map(|ext| ext.to_string_lossy().ends_with(".txt")).unwrap_or(false) //txt files have to end with .txt

    }
}

#[async_trait]
impl Source for TurbiditySource {
    async fn sample(&self, query: &tileserver_model::SampleQuery) -> Result<Option<GeoData>, Box<dyn Error + Send + Sync>> {
        self.builder.sample(query)
    }
}