//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use tileserver_model::*;

use gpx;
use gpx::{Gpx};
use std::fs::File;
use std::io::BufReader;
use tileserver_model::Coord;
use tileserver_model::async_trait;

pub struct GPXSource{
    gpx: Gpx //We only want to read the Gpx file once
}

impl SourceInit for GPXSource{
    ///Initializes the source with the options given in the server configuration file.
    fn initialize(settings: &SourceConfig, source_info: &mut SourceInfo) -> Result<Box<dyn Source>, Box<dyn std::error::Error + Send + Sync>>{
        if settings.input_files.len() < 1{
            return Err("No input file given".into());
        }
        let mut reader = BufReader::new(File::open(&settings.input_files[0])?);

        let gpx = gpx::read(&mut reader).map_err(|e| format!("Error reading gpx file: {}", e))?;

        let layerinfo: Vec<LayerInfo> = gpx.tracks
            .iter() //Iterate all tracks
            .enumerate() //Count each element we come across (used later)
            .map(|(i, t)| { //Transform each element (tuple of number, track, since we used enumerate())
                LayerInfo{ //Create and return new LayerInfo-struct
                    name: t.name.clone().unwrap_or_else(|| format!("track {}",i + 1)), //Either the track has a name in the file, or we use the number we created through enumerate to create a new name for it
                    long_name: t.description.clone(), //Copy the description over
                    max_zoom_level: 1, //Points don't have zoom levels, for a tile source we would need further computation
                    layer_type: GeoDataType::ScalarPoints, //This is the correct source type for a point source that has scalar float information
                    unit: None, //The points in our example gpx file don't really have data, so we don't report a unit, datarange or zrange
                    datarange: None,
                    zrange: None,
                    extent: None,
                    timesteps: None, //Obviously broken
                    zsteps: None,
                    timerange: None,
                    default_overlay: None
                }
            }).collect();

        source_info.instance_name = gpx.metadata.clone()
                .map(|m| m.name) //If we have metadata in the gpx, we might have a name in it
                .flatten() //But the name is an option as well
                .or_else(|| settings.input_files[0].file_name().map(|f| f.to_string_lossy().to_string())) //If we don't have a name, try getting the file name
                .unwrap_or(settings.input_files[0].to_string_lossy().to_string()); //And if we can't get the file name, just return the whole path
        source_info.layers = layerinfo;

        let source = GPXSource{
            gpx,
        };

        Ok(Box::new(source))
    }

    fn accepts_file(file: &std::path::Path) -> bool {
        file.extension().map(|ext| ext.to_string_lossy().ends_with(".gpx")).unwrap_or(false) //gpx files have to end with .gpx
    }
}

#[async_trait]
impl Source for GPXSource{
    ///Returns the queried tile data, or whatever error occurs during sampling.
    ///The new style requires support for querying the lat/lon/height data of individual tracks, and returns only a single component per query.
    async fn sample(&self, query: &SampleQuery) -> Result<Option<GeoData>, Box<dyn std::error::Error + Send + Sync>>{
        let track = self.gpx.tracks.iter().find(|t| t.name == Some(query.layername.clone()));
        if let Some(track) = track{
            //Track found
            Ok( //Need to return a result
                Some( //And we have a result!
                GeoData::ScalarPoints( //Points are unstructured data
                    PointsData::new( //we want to store points with scalar data, this is the datatype for this
                    track.segments
                    .iter() //Iterate track segments
                    .map(|s| s.points.iter()) //For each track segment, iterate the points
                    .flatten() //Concatenate these iterators
                    .filter_map(|p| { //Filter_map to simulataneously convert the points into the correct format and filter out points that are not in the area
                        let point = p.point(); //Retrieve the lat/lon-coordinates from the waypoint
                        let pos: UEC = Coord::new(point.y(), point.x()).into(); //Convert into UEC
                        if pos.in_area(query.tile.to_uec_tuple()){ //Check if the point is inside the tile
                            let height = p.elevation.unwrap_or(0f64) as f32; //Convert elevation to f32 or return 0 if there's no height // return array
                            Some(GeoPoint::new_from_uec(pos, height, p.time.map(|t| t.timestamp_millis()).unwrap_or(0), 0f32))
                        }else{
                            None
                        }
                    })
                    .collect()) //And store them in a vector
                    )
                )
            )
        }else{
            return Err(format!("No track with name {}", query.layername).into());
        }
    }
}