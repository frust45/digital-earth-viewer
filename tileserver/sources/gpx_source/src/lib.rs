//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

mod gpx_source;
pub use gpx_source::GPXSource;