//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use tileserver_model::*;

use std::error::Error;

use image;

use reqwest::{self, StatusCode};

use std::io::Cursor;

use tileserver_model::async_trait;

use crate::xml;

pub struct WMSSource{
    endpoint_url: String,

    capabilities: xml::WMSCapabilities,

    client: reqwest::Client,

    //cache: Mutex<LruCache<String, Vec<u8>>>,

}

#[async_trait]
impl SourceInit for WMSSource {
    async fn initialize(settings: SourceConfig, mut source_info: SourceInfo) -> SourceInitResult{
        let htclient = reqwest::ClientBuilder::new().user_agent(constants::TILESERVER_USER_AGENT).build()?;

        let endpoint_url = if let Some(settings_endpoint_url) = settings.settings.get("endpoint_url"){
            settings_endpoint_url.clone()
        } else {
            return Err("No Endpoint URL given.".into())
        };

        let max_zoom_level = settings.settings.get("max_zoom_level").map(|s| s.parse::<u8>().ok()).flatten().unwrap_or(18);

        let req_url = endpoint_url.clone() + "?request=GetCapabilities&service=wms";

        let text_result: Result<String, Box<dyn Error + Send + Sync>> = std::thread::spawn(move || {
                let init_client = reqwest::blocking::ClientBuilder::new().user_agent(constants::TILESERVER_USER_AGENT).build()?;
                Ok(init_client.get(&req_url)
                    .send().map_err(|e| format!("Error sending request: {:?}",e))?
                    .text().map_err(|e| format!("Error receiving text: {:?}", e))?
                    .as_str()
                    .to_string()
                )
            }).join().map_err(|_| format!("Error joining thread!"))?;

        let caps =  xml::deserialize_wms_caps(&text_result?)?;

        let wms_layers = caps.get_available_layers();

        let render_layers: Vec<LayerInfo> = wms_layers.into_iter().filter_map(|l| {
            let name: String = l.get_name()?.into();
            let long_name: String = l.get_title().into();
            /*let bounds = l.get_bounding_box("CRS:84")
                .or_else(|| l.get_bounding_box("EPSG:4326")
                    .map(|((ymin, xmin), (ymax, xmax))| ((xmin, ymin), (xmax, ymax)))
                );*/
            
            let bounds = l.get_bounding_box("CRS:84").map(|((ymin, xmin), (ymax, xmax))| ((xmin, ymin), (xmax, ymax)))
                .or(l.get_bounding_box("EPSG:4326").map(|((ymin, xmin), (ymax, xmax))| ((ymin, xmin), (ymax, xmax)))); //EPSG 4326 has bounds swapped


            let timesteps = l.get_timestamps();
            let timerange = timesteps.as_ref().and_then(|ts| ts.first().and_then(|first| ts.last().map(|last| (*first, *last))));
            Some(LayerInfo{
                name,
                long_name: Some(long_name),
                unit: None,
                layer_type: GeoDataType::ColorTiles,
                timesteps,
                timerange,
                datarange: None,
                zrange: None,
                zsteps: None,
                extent: bounds.map(|b|
                    UECArea::extend_area_with_point(UECArea::new_from_point(
                        UEC::from(
                            Coord::new(b.0.1, b.0.0)
                            )
                        ),
                        UEC::from(
                            Coord::new(b.1.1, b.1.0)
                            )
                        )),
                max_zoom_level,
                default_overlay: None
            })
        }).collect();

        source_info.layers = render_layers;

        Ok((source_info, Box::new(WMSSource{
            endpoint_url: endpoint_url,
            client: htclient,
            capabilities: caps
        })))

    }
    fn accepts_file(_file: &std::path::Path) -> bool {
        false
    }
}

#[async_trait]
impl Source for WMSSource {
    async fn sample(&self, query: &SampleQuery) -> SourceResult {
        let layer = self.capabilities.get_layer(&query.layername)
            .ok_or_else(|| GenericError(format!("No layer named `{}` in WMS instance.", query.layername)))?;
        let bounds: (Coord, Coord) = (query.tile.corner(false, true).into(), query.tile.corner(true, false).into());
        
        
        //let style: String = layer.get_first_style().ok_or_else(|| GenericError("No applicable style found.".into()))?.name;
        let style = layer.get_first_style().map(|s| s.name.clone());
        let style_str = style.map(|s| format!("&styles={}", s)).unwrap_or(String::new());
        
        let time = layer.get_timestamps().and_then(|ts| ts.get(query.tpage as usize).map(|i| *i));
        let time_str = time.map(|time| format!("&time={}", TimeParser::unix_milli_time_to_iso8601_conforming(time))).unwrap_or(String::new());

        let crs = layer.projections.iter().filter(|&proj| proj == "EPSG:4326" || proj == "CRS:84").next().map(|s| s.to_string()).unwrap_or("CRS:84".to_string());
        trace!("Selected crs {}", crs);

        let bbox_str = match crs.as_str(){
            "EPSG:4326" => format!("{},{},{},{}", bounds.0.lat, bounds.0.lon, bounds.1.lat, bounds.1.lon),
            _ =>           format!("{},{},{},{}", bounds.0.lon, bounds.0.lat, bounds.1.lon, bounds.1.lat),
        };

        let query_url: String = self.endpoint_url.clone()
            + &format!("?service=wms&request=GetMap&version=1.3.0&crs={}{}&layers={}&bbox={}&format=image/png&width={}&height={}&transparent=true{}", 
                crs, style_str, query.layername, bbox_str, constants::TILESIZE.0, constants::TILESIZE.1, time_str);

        let image_buffer = {
            let response = self.client.get(&query_url).send().await?;
            //TODO: Check if response is image or if it's xml...
            if response.status().is_success(){
                response.bytes().await?.iter().copied().collect::<Vec<u8>>()
            }else{
                let emsg = format!("HTTP request to {} failed with status {}: {:?}", query_url, response.status().to_string(), response);
                warn!("{}", emsg);
                if
                    response.status().as_u16() >= 400 &&
                    response.status().as_u16() < 500 &&
                    response.status() != StatusCode::TOO_MANY_REQUESTS &&
                    response.status() != StatusCode::REQUEST_TIMEOUT
                {
                    //todo: mark these missing tiles as failed but not possible to retry?
                    return Ok(None)
                }else {
                    return Err(emsg.into());
                }
            }
        };
        let mut cursor = Cursor::new(&image_buffer);
        let image_reader = image::io::Reader::new(&mut cursor).with_guessed_format()?;
        let image = image_reader.decode()?;
        let rgba = image.to_rgba8();

        let mut result_array: Array2D<Color> = Array2D::new(Point2D::xy(constants::TILESIZE.0 as f64, constants::TILESIZE.1 as f64));

        let array_size = result_array.size.clone();
        result_array.data.iter_mut().enumerate().for_each(|(i,v)|{
            let x = i as u32 % array_size.x as u32;
            let y = i as u32 / array_size.x as u32;
            *v = rgba[(x,y)].0.into();
        });
        let tiledata = TileData::new_from_tile_and_array_2d(query.tile.clone(), result_array);
        if tiledata.is_empty() {
            Ok(None)
        } else {
            Ok(Some(GeoData::ColorTiles(tiledata)))
        }
    }
}