//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use std::error::Error;

use roxmltree::{Node, ParsingOptions};
use tileserver_model::{SyncError, TimeParser, Timestamp, chrono::{Datelike, NaiveDate, NaiveDateTime, NaiveTime, Duration as Cduration}, smallvec::SmallVec};
use tileserver_model::log::*;

#[derive(Default, PartialEq, Debug)]
pub struct WMSCapabilities {
    pub service: Service,
    pub capability: Capability,
}

#[derive(Default, PartialEq, Debug)]
pub struct Service {
    pub title: String,
    pub max_width: Option<usize>,
    pub max_height: Option<usize>,
}

#[derive(Default, PartialEq, Debug)]
pub struct Capability {
    pub request: Request,
    pub exception: Exception,
    pub layer: Layer
}

#[derive(Default, PartialEq, Debug)]
pub struct Request {
    pub get_capabilities: GetCapabilities,
    pub get_map: GetMap,
    pub get_feature_info: Option<GetFeatureInfo>
}

#[derive(Default, PartialEq, Debug)]
pub struct GetCapabilities {
    pub mimetypes: Vec<String>
}

#[derive(Default, PartialEq, Debug)]
pub struct GetMap {
    pub mimetypes: Vec<String>
}

#[derive(Default, PartialEq, Debug)]
pub struct GetFeatureInfo {
    pub mimetypes: Vec<String>
}

#[derive(Default, PartialEq, Debug)]
pub struct Exception {
    pub mimetypes: Vec<String>
}

#[derive(Default, PartialEq, Debug, Clone)]
pub struct Layer {
    pub queryable: Option<u8>,
    pub title: String,
    pub bounding_boxes: Vec<BoundingBox>,
    pub layers: Vec<Layer>,
    pub name: Option<String>,
    pub projections: Vec<String>,
    pub styles: Vec<Style>,
    pub dimensions: Vec<Dimension>
}

#[derive(Default, PartialEq, Debug, Clone)]
pub struct Dimension {
    pub units: String,
    pub name: String,
    pub default: String,
    pub value: DimensionValue
}

#[derive(Debug, PartialEq, Clone)]
pub enum DimensionValue {
    Timestamps(Vec<Timestamp>),
    Other(String),
    Nothing
}


#[derive(Default, PartialEq, Debug, Clone)]
pub struct Style {
    pub name: String,
    pub title: String,
    pub abstract_text: Option<String>
}

#[derive(Default, PartialEq, Debug, Clone)]
pub struct BoundingBox {
    pub projection: String,
    pub min_x: f64,
    pub min_y: f64,
    pub max_x: f64,
    pub max_y: f64
}

impl WMSCapabilities {
    pub fn get_available_layers(&self) -> Vec<Layer> {
        self.capability.layer.get_available_layers()
    }

    pub fn get_layer(&self, name: &str) -> Option<Layer> {
            self.capability.layer.get_layer_by_name(name)
    }
}



impl DimensionValue{
    pub fn to_timestamps(&self) -> Result<Self, Box<dyn Error + Send + Sync>>{
        match self{
            timestamps @ DimensionValue::Timestamps(_) => Ok(timestamps.clone()),
            DimensionValue::Other(content) => {
                let timestamps: Vec<Timestamp> = content.split(',')
                    .map(|triple| time_triple_to_timestamps(triple))
                    .inspect(|val| {
                        if let Err(e) = val {
                            trace!("Error: {:?}", e);
                        }
                    })
                    .filter_map(|val| val.ok())
                    .flatten()
                    .map(|ts| ts.timestamp_millis())
                    .collect();
                    //.map(|x| x.unwrap().map(|x| x.timestamp_millis()).collect()).collect::<Vec<Vec<Timestamp>>>().concat();
                Ok(DimensionValue::Timestamps(timestamps))
            }
            DimensionValue::Nothing => Err("Called to_timestamps on DimensionValue without data.".to_string().into())
        }
    }

    pub fn get_timestamps(&self) -> Option<Vec<Timestamp>>{
        match self{
            DimensionValue::Timestamps(t) => Some(t.clone()),
            DimensionValue::Nothing => None,
            other @ DimensionValue::Other(_) => other.to_timestamps().ok().map(|dim| dim.get_timestamps()).flatten(),
        }
    }
}

pub fn time_triple_to_timestamps(triplet: &str) -> Result<impl Iterator<Item = NaiveDateTime>, Box<dyn Error + Send + Sync>> {
    let parts: SmallVec<[&str;3]> = triplet.split("/").collect();

    let start_date_str = parts.get(0).ok_or_else(|| format!("No start date in triplet!{:?}", triplet))?;
    
    let zerotime = NaiveTime::from_hms(0,0,0);
    let start_time;
    let end_time;
    let interval;

    if let Ok(datetime) = TimeParser::time_from_inconsistent_iso8601_str(start_date_str){
        start_time = TimeParser::timestamp_to_naivedatetime(datetime);
    }else{
        //Time probably doesn't contain seconds
        let start_date = date_from_iso8601_date_str(start_date_str)?;
        start_time = NaiveDateTime::new(start_date, zerotime);
    }

    if parts.len() == 1 {
        end_time = start_time + tileserver_model::chrono::Duration::seconds(1);
        interval = iso8601_duration::Duration::new(0f32, 0f32, 0f32, 0f32, 0f32, 1f32);
    }else{
        let end_date_str = parts.get(1).ok_or_else(|| format!("No end date in triplet! {:?}", triplet))?;
        if let Ok(datetime) = TimeParser::time_from_inconsistent_iso8601_str(end_date_str){
            end_time = TimeParser::timestamp_to_naivedatetime(datetime);
        }else{
            //Time probably doesn't contain seconds
            let end_date = date_from_iso8601_date_str(end_date_str)?;
            end_time = NaiveDateTime::new(end_date, zerotime);
        }

        let duration_str = parts.get(2).ok_or_else(|| format!("No duration in triplet! {:?}", triplet))?;
        interval = iso8601_duration::Duration::parse(duration_str).map_err(|e| format!("error parsing duration: {:?}", e))?;
    }


    
    
    

    struct DurationIterator{
        next_date: NaiveDateTime,
        end_date: NaiveDateTime,
        interval: iso8601_duration::Duration
    }

    impl Iterator for DurationIterator{
        type Item = NaiveDateTime;
        fn next(&mut self) -> Option<Self::Item> {
            //trace!("Start: {}, End: {}, Smaller: {}",self.next_date, self.end_date,self.next_date < self.end_date);
            if self.next_date < self.end_date {
                let old_date = self.next_date.clone();
                self.next_date = {
                    let mut date: NaiveDate = self.next_date.date();
                    date = NaiveDate::from_ymd(date.year() + self.interval.year as i32, date.month(), date.day());
                    date = add_n_months(date, self.interval.month as u32);
                    self.next_date = NaiveDateTime::new(date, self.next_date.time());
                    //add time and days (Fractions of seconds is technically specified in Iso8601 but not implemented here)
                    self.next_date + Cduration::days(self.interval.day as i64) 
                        + Cduration::days(self.interval.day as i64) 
                        + Cduration::hours(self.interval.hour as i64) 
                        + Cduration::minutes(self.interval.minute as i64) 
                        + Cduration::seconds(self.interval.second as i64) 
                        + Cduration::nanoseconds(((self.interval.second % 1.0) * 1000.0 * 1000.0 * 1000.0) as i64)
                };
                //trace!("Date: {}", old_date);
                Some(old_date)
            } else {
                None
            }
        }
    }

    let duration_iterator: DurationIterator = DurationIterator {
        next_date: start_time,
        end_date: end_time,
        interval,
    };

    Ok(duration_iterator)
}

fn date_from_iso8601_date_str(iso8601: &str) -> Result<tileserver_model::chrono::NaiveDate, Box<dyn Error + Send + Sync>>{
    let dateparts: Vec<&str> = iso8601.split("-").collect();
    let year = dateparts.get(0).map(|y| y.parse::<i32>().ok()).flatten().ok_or_else(|| format!("No year in timestring"))?;
    let month = dateparts.get(1).map(|m| m.parse::<u32>().ok()).flatten().unwrap_or(1);
    let day = dateparts.get(2).map(|d| d.parse::<u32>().ok()).flatten().unwrap_or(1);
    let date = tileserver_model::chrono::NaiveDate::from_ymd_opt(year, month, day).ok_or_else(|| format!("Invalid date: {}-{}-{}", year, month, day))?;

    //let time = NaiveTime::from_hms_milli(0, 0, 0, 0);
    //let dt: NaiveDateTime = NaiveDateTime::new(date, time);
    Ok(date)
}

impl Default for DimensionValue{
    fn default() -> Self {
        DimensionValue::Nothing
    }
}

fn add_n_months(d: NaiveDate, n: u32) -> NaiveDate{
    let y = (n / 12) as i32;
    let m = n % 12;

    if let Some(date) = NaiveDate::from_ymd_opt(d.year() + y, d.month() + m, d.day()){
        date
    }else{
        let mut month = m + 2;
        if d.month() < month{
            month = month - d.month()
        }else{
            month = 0;
        }
        if let Some(date) = NaiveDate::from_ymd_opt(d.year() + 1 + y, month, d.day()){
            date
        }else{
            let dur = tileserver_model::chrono::Duration::days(30);
            d + dur
        }
    }

    /*NaiveDate::from_ymd_opt(d.year() + y, d.month() + m, d.day()).unwrap_or_else(||
        NaiveDate::from_ymd(d.year() + 1 + y, m - d.month() + 2, d.day())
    )*/
}



impl Layer {
    pub fn get_name(&self) -> Option<&str> {
        self.name.as_ref().map(|s| s.as_str())
    }
    pub fn get_bounding_box(&self, name: &str) -> Option<((f64, f64), (f64, f64))> {
        self.bounding_boxes.iter()
            .find(|b| b.projection == name)
            .map(|b| ((b.min_x, b.min_y), (b.max_x, b.max_y)))
    }
    pub fn get_title(&self) -> &str {
        self.title.as_str()
    }

    #[allow(dead_code)]
    pub fn get_style(&self, name: &str) -> Option<Style> {
        self.styles.iter()
            .find(|s| s.name == name)
            .map(|s| s.clone())
    }

    pub fn get_first_style(&self) -> Option<Style> {
        self.styles.first().map(|s| s.clone())
    }

    fn is_available(&self) -> bool {
        self.name.is_some() && self.projections.iter().any(|proj| proj == "EPSG:4326" || proj == "CRS:84")
        //    && self.styles.len() > 0 //doesn't work for every layer
    }

    fn get_available_layers(&self) -> Vec<Layer> {
        let mut result: Vec<Layer> = self.layers
            .iter()
            .map(|l| l.get_available_layers())
            .flatten()
            .collect();
        if self.is_available() {
            result.push(self.clone());
        }
        result
    }

    fn get_layer_by_name(&self, name: &str) -> Option<Layer> {
        self.layers
            .iter()
            .find_map(|l| l.get_layer_by_name(name))
            .or_else(|| self.name.as_ref()
                .filter(|n| **n == name && self.is_available())
                .map(|_| self.clone())
            )
    }

    pub fn get_timestamps(&self) -> Option<Vec<Timestamp>>{
        self.dimensions.iter()
            .find(|dim| dim.name == "time")
            .and_then(|time_dimension| time_dimension.value.get_timestamps())
    }
}

pub fn deserialize_wms_caps(data: &str) -> Result<WMSCapabilities, SyncError> {
    let document = roxmltree::Document::parse_with_options(data, ParsingOptions{allow_dtd: true});
    if document.is_err() {
        warn!("Error parsing xml document: {:#?}", document);
    }
    let document = document?;
    
    let root_node = document.root().children().find(|c| c.has_tag_name("WMS_Capabilities") || c.has_tag_name("WMT_MS_Capabilities")).ok_or_else(|| "No WMS_Capabilities in root node")?;

    fn get_child_by_tag_name<'a, 'b>(node: Node<'a, 'b>, tag: &str) -> Result<Node<'a, 'b>, String>{
        node.children()
            .find(|c| c.has_tag_name(tag))
            .ok_or_else(|| format!("Node {} has no child {}", node.tag_name().name(), tag))
    }

    fn get_all_children_by_tag_name<'a, 'b>(node: Node<'a, 'b>, tag: &'a str) -> impl Iterator<Item=Node<'a, 'b>>{
        node.children().filter(move |c| c.has_tag_name(tag))
    }

    let mut capabilities = WMSCapabilities::default();

    let service_node = get_child_by_tag_name(root_node, "Service")?;

    capabilities.service.title = get_child_by_tag_name(service_node, "Title")?.text().unwrap_or("").to_string();
    capabilities.service.max_width = get_child_by_tag_name(service_node, "MaxWidth").ok().map(|n| n.text()).flatten().map(|t| t.parse().ok()).flatten();
    capabilities.service.max_width = get_child_by_tag_name(service_node, "MaxHeight").ok().map(|n| n.text()).flatten().map(|t| t.parse().ok()).flatten();

    let capability_node = get_child_by_tag_name(root_node, "Capability")?;
    let request_node = get_child_by_tag_name(capability_node,"Request")?;

    let get_capabilities_node = get_child_by_tag_name(request_node, "GetCapabilities")?;
    capabilities.capability.request.get_capabilities.mimetypes = get_all_children_by_tag_name(get_capabilities_node, "Format")
        .filter_map(|node| node.text())
        .map(|s| s.to_string())
        .collect();
    
    let get_feature_info_node = get_child_by_tag_name(request_node, "GetFeatureInfo");
    if let Ok(get_feature_info_node) = get_feature_info_node{
        capabilities.capability.request.get_feature_info = Some(GetFeatureInfo{
            mimetypes: get_all_children_by_tag_name(get_feature_info_node, "Format")
        .filter_map(|node| node.text())
        .map(|s| s.to_string())
        .collect()
        });
    }

    let get_map_node = get_child_by_tag_name(request_node, "GetMap")?;
    capabilities.capability.request.get_map.mimetypes = get_all_children_by_tag_name(get_map_node, "Format")
    .filter_map(|node| node.text())
    .map(|s| s.to_string())
    .collect();

    let exception_node = get_child_by_tag_name(capability_node, "Exception")?;
    capabilities.capability.exception.mimetypes = get_all_children_by_tag_name(exception_node, "Format")
    .filter_map(|node| node.text())
    .map(|s| s.to_string())
    .collect();

    fn deserialize_layer(node: Node) -> Result<Layer, String>{
        if node.tag_name().name() != "Layer"{
            return Err("Node is not a layer node".into());
        }else{
            let mut layer = Layer::default();
            layer.title = get_child_by_tag_name(node, "Title")?.text().unwrap_or_default().to_string();
            layer.queryable = node.attribute("queryable").map(|attr| attr.parse().ok()).flatten();
            layer.bounding_boxes = get_all_children_by_tag_name(node, "BoundingBox").filter_map(|bbox| {
                let projection = bbox.attribute("SRS").or(bbox.attribute("CRS")).map(|attr| attr.to_string())?;
                let min_x = bbox.attribute("minx").map(|s| s.parse::<f64>().ok()).flatten()?;
                let min_y = bbox.attribute("miny").map(|s| s.parse::<f64>().ok()).flatten()?;
                let max_x = bbox.attribute("maxx").map(|s| s.parse::<f64>().ok()).flatten()?;
                let max_y = bbox.attribute("maxy").map(|s| s.parse::<f64>().ok()).flatten()?;
                Some(
                    BoundingBox{
                        projection,
                        min_x,
                        min_y,
                        max_x,
                        max_y
                    }
                )
            }).collect();
            layer.name = get_child_by_tag_name(node, "Name").ok().map(|n| n.text()).flatten().map(|t| t.to_string());
            layer.projections = get_all_children_by_tag_name(node, "SRS")
                .chain(get_all_children_by_tag_name(node, "CRS"))
                .filter_map(|n| n.text())
                .map(|s| s.to_string())
                .collect();

            layer.styles = get_all_children_by_tag_name(node, "Style")
                .filter_map(|n| {
                    let name = get_child_by_tag_name(n, "Name").ok().and_then(|t| t.text()).map(|t| t.to_string())?;
                    let title = get_child_by_tag_name(n, "Title").ok().and_then(|t| t.text()).map(|t| t.to_string())?;
                    let abstract_text = get_child_by_tag_name(n, "Abstract").ok().and_then(|t| t.text()).map(|t| t.to_string());
                    Some(Style{
                        name, 
                        title,
                        abstract_text
                    })
                }).collect();

            layer.dimensions = get_all_children_by_tag_name(node, "Dimension")
                .filter_map(|n| {
                    let units = n.attribute("units").map(|s| s.to_string())?;
                    let name = n.attribute("name").map(|s| s.to_string())?;
                    let default = n.attribute("default").map(|s| s.to_string())?;
                    let value = n.text().map(|t| DimensionValue::Other(t.to_string()))?;
                    Some(
                        Dimension{
                            units,
                            name,
                            default,
                            value
                        }
                    )
                }).collect();

            layer.layers = get_all_children_by_tag_name(node, "Layer").map(deserialize_layer).collect::<Result<Vec<_>, String>>()?;

            Ok(layer)
        }
    }

        let layer_node = get_child_by_tag_name(capability_node, "Layer")?;

        capabilities.capability.layer = deserialize_layer(layer_node)?;

        //info!("Capabilities: {:?}", capabilities);
        Ok(capabilities)
}