//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

mod wms_source;
mod xml;

pub use wms_source::WMSSource;
