//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

mod mipmap_source;
pub use mipmap_source::MipmapSource;