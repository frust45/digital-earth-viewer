//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer


use tileserver_model::*;

pub struct FilterKernelSource{
    kernel: Box<dyn Fn(usize, usize) -> f32 + Send + Sync>,
    kernel_size_meters: f64,
    kernel_side_length: usize,
    layer_info: LayerInfo,
    dependent_source_name: String,
    dependent_source_layer: String
}

#[async_trait]
impl SourceInit for FilterKernelSource{
    async fn initialize(settings: SourceConfig, mut source_info: SourceInfo) -> SourceInitResult {
        let instance_name = settings.settings.get("instance").map(|s| s.as_str()).ok_or_else(|| "No instance given for derivative source")?;
        let layer_name = settings.settings.get("layer").map(|s| s.as_str()).ok_or_else(|| "No layer given for derivative source")?;
        let kernelstring = settings.settings.get("kernel").map(|s| s.as_str()).ok_or_else(|| "No k given!")?;
        let kernel_parts = kernelstring.split_ascii_whitespace()
            .map(|k| k.parse::<f32>())
            .collect::<Result<Vec<f32>, _>>()
            .map_err(|e| format!("Error parsing kernelstring: {:?}", e))?;
        let kernel_dimensions = (kernel_parts.len() as f32).sqrt().floor() as usize;
        let get_kernel_coeff = Box::new(move |y: usize, x: usize| {
            kernel_parts[(y * kernel_dimensions) + x]
        });
        let kernel_size_meters: f64 = settings.settings.get("kernel_size_meters").map(|s| s.parse().ok()).flatten().unwrap_or(250f64);

        trace!("Created kernel with side length {}", kernel_dimensions);

        //Get Source Info
        let r_source_info = DATAPROVIDER.get().ok_or_else(|| "Could not access dataprovider")?.wait_for_source_info(instance_name.to_string()).await?;

        //Find layerinfo

        let layer_info = r_source_info.layers.iter().find(|l| l.name == layer_name)
            .ok_or_else(|| format!("Source {} does not contain layer {} (options are {})", instance_name, layer_name, r_source_info.layers.iter().map(|l| l.name.clone()).collect::<Vec<_>>().join(", ")))?;


        let mut filtered_layer = layer_info.clone();
        filtered_layer.name = filtered_layer.name + " (filtered)";
        if let Some(long_name) = &mut filtered_layer.long_name{
            *long_name =long_name.to_string() + " (filtered)";
        }

        if let Some(extent) = filtered_layer.extent{
            let max_zoom_level = Tile::max_zoom_level_from_area_and_pixelsize(extent, (kernel_size_meters as u32, kernel_size_meters as u32));
            filtered_layer.max_zoom_level = filtered_layer.max_zoom_level.min(max_zoom_level);

        }

        source_info.add_layer(filtered_layer.clone());


        Ok((source_info, Box::new(FilterKernelSource{
            kernel: get_kernel_coeff,
            layer_info: filtered_layer,
            kernel_side_length: kernel_dimensions,
            kernel_size_meters,
            dependent_source_name: instance_name.to_string(),
            dependent_source_layer: layer_name.to_string(),
        })))
    }

    fn accepts_file(_file: &std::path::Path) -> bool {
        true
    }
}

#[async_trait]
impl Source for FilterKernelSource{
    async fn sample(&self, query: &SampleQuery) -> Result<Option<GeoData>, Box<dyn std::error::Error + Send + Sync>> {
        if query.layername == self.layer_info.name{
            let tile_area = query.tile.to_uec_area();
            let sampling_area = UECArea::area_from_two_points(
                tile_area.position.move_by_meters(-self.kernel_size_meters / 2.0, self.kernel_size_meters / 2.0), 
                (tile_area.position + tile_area.extent).move_by_meters(self.kernel_size_meters / 2.0 , -self.kernel_size_meters / 2.0));

            let tiles_in_sampling_area = Tile::covering_area(sampling_area,13, query.tile.path.len() as u8);
            trace!("Need to sample {} tile to create tile {}", tiles_in_sampling_area.len(), &query.tile.path);
            let mut tiledatas: Vec<(UECArea, TileData<Scalar>)> = Vec::new();
            for tile in tiles_in_sampling_area{
                trace!("Loading tile {}", tile.path);
                let provider = DATAPROVIDER.get().ok_or_else(|| "Could not access dataprovider")?;
                let query = SampleQuery::new(
                    self.dependent_source_name.clone(), 
                    self.dependent_source_layer.clone(), 
                    tile.clone(),
                    query.tpage,
                    query.zpage
                );
                trace!("Query: {:?}", query);
                let geodata = provider
                    .get_geodata(
                        query,
                GeoDataType::ScalarTiles
                    ).await?;
                trace!("Loading tile {} finished", tile.path);
                if let Some(GeoData::ScalarTiles(tiledata)) = geodata{
                    tiledatas.push((tile.to_uec_area(), tiledata));
                }
            }
            

            trace!("Loaded all tiles");

            let sample = move |uec: UEC, tiles: &[(UECArea, TileData<Scalar>)]| -> Result<Option<f32>, SyncError> {
                if let Some((_area, tiledata)) = tiles.iter().find(|(a,_t)| uec.in_area(*a)){
                    if let Some(value) = tiledata.sample_value_uec(uec){
                        Ok(Some(*value))
                    }else{
                        trace!("No tile found");
                        Ok(None)
                    }
                }else{
                    Ok(None)
                    //Err(format!("No selected tile matches position {:?}", uec).into())
                }
            };

            //This could probably be optimized a little
            let mut result_data = TileData::new(query.tile.clone());
            let coords = result_data.uecs_for_tile().collect::<Vec<_>>();

            for (i,&c) in coords.iter().enumerate(){
                let mut kernel_sum = 0.0;
                let mut kernel_entries_ok = 0.0;
                for y in 0 .. self.kernel_side_length{
                    for x in 0 .. self.kernel_side_length{
                        let x_offset = ((((x as f64) / (self.kernel_side_length as f64)) * 2.0) - 1.0) * self.kernel_size_meters;
                        let y_offset = ((((y as f64) / (self.kernel_side_length as f64)) * 2.0) - 1.0) * self.kernel_size_meters;
                        let sample_coord = c.move_by_meters(x_offset, -y_offset);
                        let sampled = sample(sample_coord,&tiledatas[..]);
                        match sampled{
                            Ok(Some(value)) => {
                                kernel_sum += value * (self.kernel)(y,x);
                                kernel_entries_ok += 1.0;
                            },
                            Ok(None) => {
                                continue;
                            }
                            Err(e) => {
                                return Err(e);
                            }
                        }
                    }
                }
                result_data.data[i] = kernel_sum / kernel_entries_ok;
            }

            if result_data.is_empty() {
                trace!("Tile {} is empty", query.tile.path);
            }


            Ok(Some(GeoData::ScalarTiles(result_data)))
        }else{
            Err(format!("No layer with name {}", &query.layername).into())
        }
    }
}