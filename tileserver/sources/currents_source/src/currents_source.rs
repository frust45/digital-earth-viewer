//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use regex::Regex;
use serde::{Deserialize, Serialize};
use smallvec::SmallVec;
use std::{convert::TryFrom, error::Error, fs::File, io::{BufRead, BufReader}};
use tileserver_model::{Coord, GeoData, GeoDataType, GeoLine, LayerInfo, LinesData, Position, Scalar, Source, SourceConfig, SourceInfo, SourceInit, SourceInitResult, UEC, UECArea};
use tileserver_model::async_trait;
use tileserver_model::Timestamp;
use tileserver_model::log::*;

const USUAL_NUM_BINS: usize = 24;// TODO: use serde rename to allow "rusty" field names
#[derive(Serialize, Deserialize, Debug)]
struct CurrentHeader {
    name: String,
    campaign: String,
    station: String,
    platform: String,
    orientation: String,
    latitude_dec_deg: f32,
    longitude_dec_deg: f32,
    depth_m: f32,
    #[serde(rename="dateStart")]
    date_start: String,
    #[serde(rename="timeStart")]
    time_start: String,
    #[serde(rename="ensembleInterval_s")]
    ensemble_interval_s: f32,
    #[serde(rename="numEnsembles")]
    num_ensembles: f32,
    #[serde(rename="firstBinRange_m")]
    first_bin_range_m: f32,
    #[serde(rename="binSize_m")]
    bin_size_m: f32,
    #[serde(rename="numBins")]
    num_bins: usize,
    comments: String,
}

pub struct Bin{
    pub z_pos: f32,
    pub vectors: Vec<CurrentVector>
}

#[derive(Clone, Copy)]
pub struct CurrentVector{
    pub u_mps: f64,
    pub v_mps: f64,
    pub direction: f32,
    pub speed: f32,
    pub timestamp: Timestamp
}

impl CurrentVector{
    pub fn end_point(&self, start_point: UEC, next_vector: &CurrentVector) -> UEC{
        let delta_seconds = (next_vector.timestamp - self.timestamp) as f64 / 1000.0;
        let x_meters = self.u_mps * delta_seconds;
        let y_meters = self.v_mps * delta_seconds;
        Position::UEC(start_point).move_by_meters(x_meters, y_meters).to_uec()
      }
}

enum LayerKind{
    Speed,
    Direction
}

impl TryFrom<&str> for LayerKind{
    type Error = String;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value{
            "Speed" => Ok(LayerKind::Speed),
            "Direction" => Ok(LayerKind::Direction),
            other => Err(format!("Unknown Layerkind: {}", other))
        }
    }
}

impl LayerKind{
    pub fn get_value(&self, vector: &CurrentVector) -> f32{
        match self{
            LayerKind::Speed => vector.speed,
            LayerKind::Direction => vector.direction
        }
    }
}

pub struct CurrentsSource {
    bins: Vec<Bin>,
    device_position: UEC,
    layer_regex: Regex
}


#[async_trait]
impl SourceInit for CurrentsSource{

    async fn initialize(settings: SourceConfig, mut source_info: SourceInfo) -> SourceInitResult {

        if settings.input_files.is_empty() {
            return Err("No input file found".into());
        }
        let filename = &settings.input_files[0];

        trace!("currents source opening filename: {:?}", filename);

        let bunching: f64 = settings.settings.get("merging_distance").and_then(|r| r.parse().ok()).unwrap_or_else(|| {warn!("using default merging distance of 5.0m"); 5f64});

        const HEADER_LINES_COUNT: usize = 17;

        //Create buf reader
        let rdr = BufReader::new(
            File::open(filename)
                .map_err(|error|format!("Something went wrong reading the file:{}", error))?
        );

        //We only read lines that don't error out
        let mut input_lines = rdr.lines().filter_map(|l| l.ok());

        //Collect the header part of the file
        let header = input_lines.by_ref().take(HEADER_LINES_COUNT).collect::<Vec<_>>().join("\n");
    
        //Parse the header (it's json)
        let header: CurrentHeader = serde_json::from_str(&header)
                                    .expect("Failed reading header JSON");
        
        //Calculate the heights of the individual bins
        let orientation_prefix = if header.orientation == "upward"{1.0} else{-1.0}; 

        let heights_vec: Vec<f32> = (0..header.num_bins).into_iter().map(|bin_id| -header.depth_m + 
                        (orientation_prefix * (header.first_bin_range_m + bin_id as f32 * header.bin_size_m))).collect();

        //Calculate the zrange of the source
        let mut height_min = heights_vec[0];
        let mut height_max = heights_vec[heights_vec.len()-1];
        if height_max < height_min{
            std::mem::swap(&mut height_min, &mut height_max);
        }


        let inputs: Vec<(Timestamp, SmallVec<[f32; USUAL_NUM_BINS]>, SmallVec<[f32; USUAL_NUM_BINS]>, SmallVec<[[f64; 2]; USUAL_NUM_BINS]>)>
        = input_lines.skip(1).filter_map(|line| {
            //Parse line
            let parts = line.split(' ').collect::<Vec<&str>>();

            let timestamp: Timestamp = (parts[0].parse::<f64>().ok()?) as i64 * 1000;

            let speed: SmallVec<[f32; USUAL_NUM_BINS]> = parts[1..(&header.num_bins+1) as usize].iter()
                .map(|vec|{vec.parse().unwrap_or(std::f32::NAN)})
                .collect();

            let directions: SmallVec<[f32; USUAL_NUM_BINS]> = parts[(&header.num_bins+1) as usize ..].iter()
                    .map(|vec|{vec.parse().unwrap_or(std::f32::NAN)})
                    .collect();

            //Calculate 2D directions
            let uvs: SmallVec<[[f64; 2]; USUAL_NUM_BINS]> = speed.iter()
                .zip(directions.iter())
                .map(|(speed, direction)| 
                    [
                        (90.0 - *direction as f64).to_radians().cos() * *speed as f64 / 1000f64
                        , -1.0 * (90.0 - *direction as f64).to_radians().sin() * *speed as f64 / 1000f64
                    ]
                ).collect();
            Some((timestamp, speed, directions, uvs))
        }).collect();

        trace!("Loaded {} lines of input", inputs.len());

        if inputs.is_empty(){
            return Err("No line parsed successfully!".into());
        }

        let mut bins: Vec<Bin> = inputs.into_iter().fold(
          (0 .. header.num_bins).into_iter()
                    .map(|i| Bin{
                        z_pos: heights_vec[i],
                        vectors: Vec::new()
                    }).collect(),
                |mut bins, (timestamp, speed, directions, uvs)| {
                    bins.iter_mut()
                        .zip(std::iter::repeat(timestamp))
                        .zip(speed.iter())
                        .zip(directions.iter())
                        .zip(uvs.iter())
                        .for_each(|((((bin,timestamp), &speed), &direction), &uv)| {
                            bin.vectors.push(
                                CurrentVector{
                                    timestamp,
                                    speed,
                                    direction,
                                    u_mps: uv[0],
                                    v_mps: uv[1]
                                }
                            )
                    });
                    return bins;
                } 
        );

        let bunching2 = bunching.powi(2);

        bins.iter_mut().for_each(|bin| {
            bin.vectors = bin.vectors.iter()
            .scan((0f64, 0f64, bin.vectors[0].timestamp), |(px, py, t), v| {
                let tdelta = v.timestamp - *t;
                *px += v.u_mps / 1000.0 * (tdelta) as f64;
                *py += v.v_mps / 1000.0 * (tdelta) as f64;
                *t = v.timestamp;
                Some(((*px, *py), v))
            })
            .scan((0.0, 0.0, 0), |(lex, ley, le_t), ((cpx, cpy), v)| {
                if (*lex - cpx).powi(2) + (*ley - cpy).powi(2) < bunching2 {
                    Some(None)
                } else {
                    let xdelta = cpx - *lex;
                    let ydelta = cpy - *ley;
                    let tdelta = v.timestamp - *le_t;
                    *lex = cpx;
                    *ley = cpy;
                    *le_t = v.timestamp;
                    let mut new_vec = *v;
                    new_vec.u_mps = xdelta * 1000.0 / tdelta as f64;
                    new_vec.v_mps = ydelta * 1000.0 / tdelta as f64;
                    Some(Some(new_vec))
                }
            }
            )
            .filter_map(|v| v)
            .collect();
        });

        trace!("Parsed into bins");

        let device_position = Position::Coord(Coord::new(header.latitude_dec_deg as f64, header.longitude_dec_deg as f64)).to_uec();

        for (bin_id, bin) in bins.iter().enumerate(){
            //Calculate timesteps
            let timestamps: Vec<Timestamp> = bin.vectors.iter().map(|vector| vector.timestamp).collect();

            //Calculate extent
            let start_position = device_position;
            let end_position = bin.vectors.iter().zip(bin.vectors.iter().skip(1)).fold(start_position, |pos, (vec, next_vec)| vec.end_point(pos, next_vec));
            let extent = UECArea::area_from_two_points(start_position, end_position);
            //info!("start: {:?}, end: {:?}", Position::UEC(start_position).to_coord(), Position::UEC(end_position).to_coord());

            //Create speed layer
            let mut speed_layer = LayerInfo::new(&format!("Speed (Bin {})", bin_id + 1), GeoDataType::ScalarLines);
            speed_layer.set_extent(extent);
            speed_layer.set_timesteps(&timestamps);
            speed_layer.set_unit("mm/s");
            speed_layer.override_zrange(bin.z_pos, bin.z_pos);
            speed_layer.set_datarange_from_value_iterator(bin.vectors.iter().map(|vec| vec.speed));

            //Create direction layer
            let mut direction_layer = LayerInfo::new(&format!("Direction (Bin {})", bin_id + 1), GeoDataType::ScalarLines);
            direction_layer.set_extent(extent);
            direction_layer.set_timesteps(&timestamps);
            direction_layer.set_unit("°");
            direction_layer.override_zrange(bin.z_pos, bin.z_pos);
            direction_layer.set_datarange_from_value_iterator(bin.vectors.iter().map(|vec| vec.direction));

            source_info.add_layer(speed_layer);
            source_info.add_layer(direction_layer);
        }

       

        //We will need the lines sorted by timestamp later on
        let source = CurrentsSource{    
            device_position,
            bins,
            layer_regex: Regex::new(r#"(Speed|Direction) \(Bin (\d+)\)"#)?
        };


        Ok((source_info,
            Box::new(source)))
    }

    fn accepts_file(file: &std::path::Path) -> bool {
        file.extension().map(|ext| ext.to_string_lossy().ends_with(".txt")).unwrap_or(false) //txt files have to end with .txt
    }
}

#[async_trait]
impl Source for CurrentsSource {
    async fn sample(&self, query: &tileserver_model::SampleQuery) -> Result<Option<GeoData>, Box<dyn Error + Send + Sync>> {
        //1. Find correct layer
        if let Some(captures) = self.layer_regex.captures(&query.layername){
            if let (Some(Ok(kind)), Some(Ok(id))) = (captures.get(1).map(|v| LayerKind::try_from(v.as_str())), captures.get(2).map(|v| v.as_str().parse::<usize>())){
                if let Some(bin) = self.bins.get(id - 1){
                    //2. Find start point
                    if(query.tpage as usize) < bin.vectors.len() - 1{
                        //3. Assemble lines
                        let vectors = &bin.vectors[query.tpage as usize..];
                        let lines = vectors.iter()
                            .zip(vectors.iter().skip(1))
                            .scan(self.device_position,
                                |position, (vector, next_vector)|{
                                    let start_position = *position;
                                    let end_position = vector.end_point(start_position, next_vector);
                                    *position = end_position;
                                    Some(
                                        GeoLine::new_from_uec(
                                            start_position, 
                                            bin.z_pos,
                                            end_position, 
                                            bin.z_pos,
                                            vector.timestamp, 
                                            next_vector.timestamp,
                                            kind.get_value(vector), 
                                            kind.get_value(next_vector)
                                        )
                                    )
                                }
                        ).collect::<Vec<GeoLine<Scalar>>>();
                        if lines.len() > 0{
                            Ok(
                                Some(
                                    GeoData::ScalarLines(
                                        LinesData::from_lines(
                                            lines
                                        )
                                    )
                                ))
                        }else{
                            Ok(None)
                        }
                    }else{
                        Err(format!("Requested bin {}, layer has only {}", query.tpage, bin.vectors.len() - 2).into())
                    }
                }else{
                    Err(format!("No speed bin with id {}", id).into())
                }                
            }else{
                Err(format!("Malformed layer: {}", query.layername).into())
            }
        }else{
            Err(format!("Unknown layer: {}", query.layername).into())
        }
    }
}
