//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

mod geotiff_source;
mod geokey;
pub use crate::geotiff_source::GeoTiffSource;
