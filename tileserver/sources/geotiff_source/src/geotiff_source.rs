//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use tiff::{ColorType, decoder::{Decoder, DecodingResult, Limits}, tags::Tag};
use std::borrow::Borrow;
use tileserver_model::*;
use tileserver_model::async_trait;
use std::path::Path;
use std::io::{BufReader, Read, Seek};
use std::error::Error;
use std::fs::File;
use crate::geokey::*;

struct GeoTiffFloatImage{
    dimensions: (u32, u32),
    extent: UECArea,
    name: String,
    data: Vec<f32>,
    projection: Option<u32>,
    //pixel_space_transform: Mat4,
    pixel_space_reverse_transform: Mat4
}

struct GeoTiffRgbaImage{
    dimensions: (u32, u32),
    extent: UECArea,
    name: String,
    data: Vec<Color>,
    projection: Option<u32>,
    //pixel_space_transform: Mat4,
    pixel_space_reverse_transform: Mat4
}

pub struct GeoTiffSource{}

pub struct GeoTiffFloatSource{
    images: Vec<GeoTiffFloatImage>
}

pub struct GeoTiffRgbaSource{
    images: Vec<GeoTiffRgbaImage>
}

impl GeoTiffSource {
    fn get_transform_matrix<R: Read + Seek>(dec: &mut Decoder<R>) -> Result<Mat4, Box<dyn Error + Send + Sync>> {
        let tiepoints = dec.find_tag(Tag::ModelTiepointTag)?
            .and_then(|list| list.into_f64_vec().ok())
            .filter(|v| v.len() == 6);
        let pixel_scale = dec.find_tag(Tag::ModelPixelScaleTag)?
            .and_then(|list| list.into_f64_vec().ok())
            .filter(|v| v.len() == 3);
        let model_transformation = dec.find_tag(Tag::ModelTransformationTag)?
            .and_then(|list| list.into_f64_vec().ok())
            .filter(|v| v.len() == 16);

        tiepoints.and_then(|tps| pixel_scale.map(|ps| (tps, ps)))
            .and_then(|(tps, ps)| {
                let sx = *ps.get(0)?;
                let sy = *ps.get(1)?;
                let sz = ps.get(2)
                    .map(|v| if *v == 0.0 {1.0} else {*v})?;
                let tx = tps.get(3)? - tps.get(0)? / sx;
                let ty = tps.get(4)? - tps.get(1)? / sy;
                let tz = tps.get(5)? - tps.get(2)? / sz;
                Some(Mat4::from_lins(
                    Vec4::new(sx, 0.0, 0.0, tx),
                    Vec4::new(0.0, sy, 0.0, ty),
                    Vec4::new(0.0, 0.0, sz, tz),
                    Vec4::new(0.0, 0.0, 0.0, 1.0),
                ))
            }) 
            .or(model_transformation.and_then(|mut t| {
                t.get_mut(10).map(|v| if *v == 0.0 {*v = 1.0;})?;
                Mat4::from_slice(
                    &t
                )
            })).ok_or_else(|| "Insufficient Pixel Space Transformation info.".into())
        
    }
}

#[async_trait]
impl SourceInit for GeoTiffSource{
    async fn initialize(settings: SourceConfig, source_info: SourceInfo) -> SourceInitResult {
        //Check that we actually have an image
        if settings.input_files.len() < 1 {
            return Err("No image file given".into());
        }

        let reader = BufReader::new(File::open(&settings.input_files[0])?);
        let mut image_decoder = Decoder::new(reader)?.with_limits(Limits::unlimited());

        let image_type = image_decoder.colortype()?;

        match image_type {
            ColorType::Gray(_) => GeoTiffFloatSource::new(settings, source_info, image_decoder),
            ColorType::GrayA(_) => GeoTiffRgbaSource::new(settings, source_info, image_decoder, 2),
            ColorType::RGB(_) => GeoTiffRgbaSource::new(settings, source_info, image_decoder, 3),
            ColorType::RGBA(_) => GeoTiffRgbaSource::new(settings, source_info, image_decoder, 4),
            _ => Err("Unsupported color data types.".into())
        }

       
    }

    fn accepts_file(_file: &Path) -> bool {
        true
    }
}

impl GeoTiffFloatSource {
    fn new<R>(settings: SourceConfig, mut source_info: SourceInfo, mut decoder: Decoder<R>) -> SourceInitResult where R: Read + Seek {

        let mut images: Vec<GeoTiffFloatImage> = Vec::new();
        let geokeydir = crate::geokey::parse_geo_keys(&mut decoder).map_err(|e| format!("Error reading geokey directory: {:?}", e))?;

        loop{

            let nodata_gdal: Option<f32> = decoder.find_tag(Tag::GdalNodata)?.and_then(|v| v.clone().into_string().ok().and_then(|v| v.parse().ok()).or_else(|| v.into_f32().ok()));
            let nodata_value: Option<f32> = settings.settings.get("nodata_value").and_then(|v| v.parse().ok());
            let projection_code: Option<u32> = settings.settings.get("epsg_projected").and_then(|v| v.parse().ok());

            let image_size = decoder.dimensions()?;

            let name = format!("[{}] Gray32({}x{})", images.len(), image_size.0, image_size.1);

            let matrix: Mat4 = GeoTiffSource::get_transform_matrix(&mut decoder)?;

            let map_nodata = |val: f32| {
                if val.is_finite() && nodata_value.map(|n| n != val).unwrap_or(true) && nodata_gdal.map(|n| n != val).unwrap_or(true) {
                    val
                } else {
                    f32::NAN
                }
            };

            let fdata: Vec<f32> = match decoder.read_image()? {
                DecodingResult::U8(v) => v.into_iter().map(|x| x as f32).map(map_nodata).collect(),
                DecodingResult::I8(v) => v.into_iter().map(|x| x as f32).map(map_nodata).collect(),
                DecodingResult::U16(v) => v.into_iter().map(|x| x as f32).map(map_nodata).collect(),
                DecodingResult::I16(v) => v.into_iter().map(|x| x as f32).map(map_nodata).collect(),
                DecodingResult::U32(v) => v.into_iter().map(|x| x as f32).map(map_nodata).collect(),
                DecodingResult::I32(v) => v.into_iter().map(|x| x as f32).map(map_nodata).collect(),
                DecodingResult::U64(v) => v.into_iter().map(|x| x as f32).map(map_nodata).collect(),
                DecodingResult::I64(v) => v.into_iter().map(|x| x as f32).map(map_nodata).collect(),
                DecodingResult::F32(v) => v.into_iter().map(map_nodata).collect(),
                DecodingResult::F64(v) => v.into_iter().map(|x| x as f32).map(map_nodata).collect()
            };

            let pcode = match geokeydir.get(&GeoKey::ProjectedCSTypeGeoKey) {
                Some(GeoKeyType::Short(v)) => if *v < 32767 {Some(*v as u32)} else {None},
                _ => None
            }.or(projection_code);

            let cconv = pcode.and_then(|v| tileserver_model::projections::get_coord_transform(v as u32));
            let bbox = fdata.iter().enumerate()
                .filter_map(|(i, v)| {
                    if v.is_finite() {
                        let px = i % image_size.0 as usize;
                        let py = i / image_size.0 as usize;
                        let pv = Vec4::new(px as f64, py as f64 * -1.0, 0.0, 1.0);
                        let projv = matrix * pv;
                        let wv = cconv.borrow().as_ref().map(|c| c.to_deg(projv.x1, projv.x2)).unwrap_or((projv.x1, projv.x2));
                        Some(Vec3::new(wv.0, wv.1, *v as f64))
                    } else {
                        None
                    }
                })
                .fold(AaBb3::fold_initial(), |p, c| p + c);

            let extent = UECArea::area_from_two_points(
                Coord::new(bbox.min().x2, bbox.min().x1).into(),
                Coord::new(bbox.max().x2, bbox.max().x1).into()
            );


            let mut layerinfo = LayerInfo::new(&name, datatypes::GeoDataType::ScalarTiles);
            layerinfo.set_extent(extent);
            layerinfo.set_datarange(bbox.min().x3 as f32, bbox.max().x3 as f32);
            layerinfo.set_max_zoom_level(Tile::max_zoom_level_from_area_and_pixelsize(extent, image_size));

            images.push(GeoTiffFloatImage{
                data: fdata,
                dimensions: image_size,
                extent,
                name,
                //pixel_space_transform: matrix,
                pixel_space_reverse_transform: matrix.inverse().ok_or_else(|| "Need regular matrix for pixel space transform!")?,
                projection: pcode

            });
            source_info.layers.push(layerinfo);

            if !decoder.more_images(){
                break;
            }else{
                decoder.next_image()?;
            }
        }


        Ok(
            (source_info, Box::new(
                GeoTiffFloatSource{
                    images
                }
            ))
        )
    }
}

#[async_trait]
impl Source for GeoTiffFloatSource{
    async fn sample(&self, query: &SampleQuery) -> SourceResult {
        let im = self.images.iter().find(|v| v.name == query.layername).ok_or_else(||"Layer not in source.")?;
        if !UECArea::areas_overlap(im.extent, query.tile.to_uec_area()) {
            return Ok(None)
        }
        let cconv = im.projection.and_then(|q| projections::get_coord_transform(q as u32));
        let td = TileData::new_from_coord_closure(query.tile.clone(), |c: Coord| {
            let mspace = cconv.borrow().as_ref().map(|p| p.from_deg(c.lon, c.lat)).unwrap_or((c.lon, c.lat));
            let mut pspace = im.pixel_space_reverse_transform * Vec4::new(mspace.0, mspace.1, 0.0, 1.0);
            pspace.x2 = -pspace.x2;
            if 
                pspace.x1 >= 0.0 && 
                pspace.x1 < (im.dimensions.0 - 1) as f64 &&
                pspace.x2 >= 0.0 &&
                pspace.x2 < (im.dimensions.1 - 1) as f64
            {
                sampling::linear_from_slice_nodata(&im.data, (pspace.x1, pspace.x2), (im.dimensions.0 as usize, im.dimensions.1 as usize), 1, 0)
            } else {
                Scalar::nodata()
            }
        });
        if td.is_empty() {
            Ok(None)
        } else {
            Ok(Some(GeoData::ScalarTiles(td)))
        }

    }
}

impl GeoTiffRgbaSource {
    fn new<R>(settings: SourceConfig, mut source_info: SourceInfo, mut decoder: Decoder<R>, channels: usize) -> SourceInitResult where R: Read + Seek {

        let mut images: Vec<GeoTiffRgbaImage> = Vec::new();
        let geokeydir = crate::geokey::parse_geo_keys(&mut decoder).map_err(|e| format!("Error reading geokey directory: {:?}", e))?;

        loop{

            let image_size = decoder.dimensions()?;

            let projection_code: Option<u32> = settings.settings.get("epsg_projected").and_then(|v| v.parse().ok());

            let name = format!("[{}] Color({}x{})", images.len(), image_size.0, image_size.1);

            let matrix: Mat4 = GeoTiffSource::get_transform_matrix(&mut decoder)?;


            let fdata: Vec<Color> = match decoder.read_image()? {
                DecodingResult::U8(v) => v.chunks_exact(channels).map(|x| match x.len() {
                    2 => Color::from_rgba(
                        x[0],
                        x[0],
                        x[0],
                        x[1]
                    ),
                    3 => Color::from_rgb(
                        x[0],
                        x[1],
                        x[2]
                    ),
                    _ => Color::from_rgba(
                        x[0],
                        x[1],
                        x[2],
                        x[3]
                    )
                }).collect(),
                DecodingResult::U16(v) => v.chunks_exact(channels).map(|x| match x.len() {
                    2 => Color::from_rgba(
                        x[0].to_be_bytes()[0],
                        x[0].to_be_bytes()[0],
                        x[0].to_be_bytes()[0],
                        x[1].to_be_bytes()[0]
                    ),
                    3 => Color::from_rgb(
                        x[0].to_be_bytes()[0],
                        x[1].to_be_bytes()[0],
                        x[2].to_be_bytes()[0]
                    ),
                    _ => Color::from_rgba(
                        x[0].to_be_bytes()[0],
                        x[1].to_be_bytes()[0],
                        x[2].to_be_bytes()[0],
                        x[3].to_be_bytes()[0]
                    )
                }).collect(),
                DecodingResult::U32(v) => v.chunks_exact(channels).map(|x| match x.len() {
                    2 => Color::from_rgba(
                        x[0].to_be_bytes()[0],
                        x[0].to_be_bytes()[0],
                        x[0].to_be_bytes()[0],
                        x[1].to_be_bytes()[0]
                    ),
                    3 => Color::from_rgb(
                        x[0].to_be_bytes()[0],
                        x[1].to_be_bytes()[0],
                        x[2].to_be_bytes()[0]
                    ),
                    _ => Color::from_rgba(
                        x[0].to_be_bytes()[0],
                        x[1].to_be_bytes()[0],
                        x[2].to_be_bytes()[0],
                        x[3].to_be_bytes()[0]
                    )
                }).collect(),
                DecodingResult::U64(v) => v.chunks_exact(channels).map(|x| match x.len() {
                    2 => Color::from_rgba(
                        x[0].to_be_bytes()[0],
                        x[0].to_be_bytes()[0],
                        x[0].to_be_bytes()[0],
                        x[1].to_be_bytes()[0]
                    ),
                    3 => Color::from_rgb(
                        x[0].to_be_bytes()[0],
                        x[1].to_be_bytes()[0],
                        x[2].to_be_bytes()[0]
                    ),
                    _ => Color::from_rgba(
                        x[0].to_be_bytes()[0],
                        x[1].to_be_bytes()[0],
                        x[2].to_be_bytes()[0],
                        x[3].to_be_bytes()[0]
                    )
                }).collect(),
                DecodingResult::F32(v) => v.chunks_exact(channels).map(|x| match x.len() {
                    2 => Color::from_rgba(
                        (x[0] * 255.0).round().min(255.0).max(0.0) as u8,
                        (x[0] * 255.0).round().min(255.0).max(0.0) as u8,
                        (x[0] * 255.0).round().min(255.0).max(0.0) as u8,
                        (x[1] * 255.0).round().min(255.0).max(0.0) as u8
                    ),
                    3 => Color::from_rgb(
                        (x[0] * 255.0).round().min(255.0).max(0.0) as u8,
                        (x[1] * 255.0).round().min(255.0).max(0.0) as u8,
                        (x[2] * 255.0).round().min(255.0).max(0.0) as u8
                    ),
                    _ => Color::from_rgba(
                        (x[0] * 255.0).round().min(255.0).max(0.0) as u8,
                        (x[1] * 255.0).round().min(255.0).max(0.0) as u8,
                        (x[2] * 255.0).round().min(255.0).max(0.0) as u8,
                        (x[3] * 255.0).round().min(255.0).max(0.0) as u8
                    )
                }).collect(),
                DecodingResult::F64(v) => v.chunks_exact(channels).map(|x| match x.len() {
                    2 => Color::from_rgba(
                        (x[0] * 255.0).round().min(255.0).max(0.0) as u8,
                        (x[0] * 255.0).round().min(255.0).max(0.0) as u8,
                        (x[0] * 255.0).round().min(255.0).max(0.0) as u8,
                        (x[1] * 255.0).round().min(255.0).max(0.0) as u8
                    ),
                    3 => Color::from_rgb(
                        (x[0] * 255.0).round().min(255.0).max(0.0) as u8,
                        (x[1] * 255.0).round().min(255.0).max(0.0) as u8,
                        (x[2] * 255.0).round().min(255.0).max(0.0) as u8
                    ),
                    _ => Color::from_rgba(
                        (x[0] * 255.0).round().min(255.0).max(0.0) as u8,
                        (x[1] * 255.0).round().min(255.0).max(0.0) as u8,
                        (x[2] * 255.0).round().min(255.0).max(0.0) as u8,
                        (x[3] * 255.0).round().min(255.0).max(0.0) as u8
                    )
                }).collect(),
                _ => return Err("Signed Data cannot be interpreted as color.".into())
            };

            let pcode = match geokeydir.get(&GeoKey::ProjectedCSTypeGeoKey) {
                Some(GeoKeyType::Short(v)) => if *v < 32767 {Some(*v as u32)} else {None},
                _ => None
            }.or(projection_code);
            
            let cconv = pcode.and_then(|v| tileserver_model::projections::get_coord_transform(v as u32));
            let bbox = fdata.iter().enumerate()
                .filter_map(|(i, v)| {
                    if !v.is_nodata() {
                        let px = i % image_size.0 as usize;
                        let py = i / image_size.0 as usize;
                        let pv = Vec4::new(px as f64, py as f64 * -1.0, 0.0, 1.0);
                        let projv = matrix * pv;
                        let wv = cconv.borrow().as_ref().map(|c| c.to_deg(projv.x1, projv.x2)).unwrap_or((projv.x1, projv.x2));
                        Some(Vec3::new(wv.0, wv.1, 0.0))
                    } else {
                        None
                    }
                })
                .fold(AaBb3::fold_initial(), |p, c| p + c);

            let extent = UECArea::area_from_two_points(
                Coord::new(bbox.min().x2, bbox.min().x1).into(),
                Coord::new(bbox.max().x2, bbox.max().x1).into()
            );

            let mut layerinfo = LayerInfo::new(&name, datatypes::GeoDataType::ColorTiles);
            layerinfo.set_extent(extent);
            layerinfo.set_max_zoom_level(Tile::max_zoom_level_from_area_and_pixelsize(extent, image_size));

            images.push(GeoTiffRgbaImage{
                data: fdata,
                dimensions: image_size,
                extent,
                name,
                //pixel_space_transform: matrix,
                pixel_space_reverse_transform: matrix.inverse().ok_or_else(|| "Need regular matrix for pixel space transform!")?,
                projection: pcode

            });
            source_info.layers.push(layerinfo);

            if !decoder.more_images(){
                break;
            }else{
                decoder.next_image()?;
            }
        }


        Ok(
            (source_info, Box::new(
                GeoTiffRgbaSource{
                    images
                }
            ))
        )
    }
}


#[async_trait]
impl Source for GeoTiffRgbaSource{
    async fn sample(&self, query: &SampleQuery) -> SourceResult {
        let im = self.images.iter().find(|v| v.name == query.layername).ok_or_else(||"Layer not in source.")?;
        if !UECArea::areas_overlap(im.extent, query.tile.to_uec_area()) {
            return Ok(None)
        }
        let cconv = im.projection.and_then(|q| projections::get_coord_transform(q as u32));
        let td = TileData::new_from_coord_closure(query.tile.clone(), |c: Coord| {
            let mspace = cconv.borrow().as_ref().map(|p| p.from_deg(c.lon, c.lat)).unwrap_or((c.lon, c.lat));
            let mut pspace = im.pixel_space_reverse_transform * Vec4::new(mspace.0, mspace.1, 0.0, 1.0);
            pspace.x2 = -pspace.x2;
            if 
                pspace.x1 >= 0.0 && 
                pspace.x1 < (im.dimensions.0 - 1) as f64 &&
                pspace.x2 >= 0.0 &&
                pspace.x2 < (im.dimensions.1 - 1) as f64
            {
                sampling::linear_from_slice(&im.data, (pspace.x1, pspace.x2), (im.dimensions.0 as usize, im.dimensions.1 as usize), 1, 0)
            } else {
                Color::nodata()
            }
        });
        if td.is_empty() {
            Ok(None)
        } else {
            Ok(Some(GeoData::ColorTiles(td)))
        }

    }
}