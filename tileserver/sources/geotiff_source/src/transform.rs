//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use tileserver_model::{Position, UEC, CoordTransform};
use std::{error::Error, fmt::Debug};

#[derive(Clone)]
pub enum CoordStyle{
    WGS84Equirect,
}

impl Debug for CoordStyle{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self{
            CoordStyle::WGS84Equirect => write!(f, "CoordStyle::WGS84Equirect")
        }
    }
}

impl CoordStyle{
    pub fn from_epsg(epsg: u32) -> Result<Self, Box<dyn Error + Send + Sync>>{
        match epsg{
            4326 => Ok(CoordStyle::WGS84Equirect),
            other => Err("bluh".into())
        }
    }
}

///stupid stupid stupid
//northing, easting, zone -> lat, lon
fn utm_wgs84_to_latlon_interval_method(northing: f64, easting: f64, zone: u8) -> (f64,f64){
    //1. guess closest initial point solution
    let mut lat_initial = 0f64;
    let mut lon_initial = 0f64;
    let mut best_distance_squared = std::f64::MAX;
    for lat in 0..180{
        for lon in 0..360{
            let lat = lat as f64 - 90.0;
            let lon = lon as f64;
            let target = latlon_to_utm_wgs84(lat, lon, zone);
            let distance_squared = (target.0 - northing).powi(2) + (target.1 - easting).powi(2);
            if distance_squared < best_distance_squared{
                lat_initial = lat;
                lon_initial = lon;
                best_distance_squared = distance_squared;
            }
        }
    }
    //2. Do the iterations
    let mut offset = 0.5;
    const ITERATIONS: usize = 50;
    let factors = [-1f64, 1f64];
    for _ in 0..ITERATIONS{
        let mut iteration_best_lat = lat_initial;
        let mut iteration_best_lon = lon_initial;
        let mut iteration_best_distance = best_distance_squared;
        for lat_fac in factors.iter().copied(){
            for lon_fac in factors.iter().copied(){
                let lat = lat_initial + (offset * lat_fac);
                let lon = lon_initial + (offset * lon_fac);
                let target = latlon_to_utm_wgs84(lat, lon, zone);
                let distance_squared = (target.0 - northing).powi(2) + (target.1 - easting).powi(2);
                if distance_squared < iteration_best_distance{
                    iteration_best_lat = lat;
                    iteration_best_lon = lon;
                    iteration_best_distance = distance_squared;
                }
            }
        }
        best_distance_squared = iteration_best_distance;
        lat_initial = iteration_best_lat;
        lon_initial = iteration_best_lon;
        offset = offset / 2.0;
    }
    return (lat_initial, lon_initial);
}


///lat, lon, zone -> northing, easting
fn latlon_to_utm_wgs84(lat: f64, lon: f64, zone: u8) -> (f64, f64){
    //From https://github.com/gadomski/utm/blob/master/src/lib.rs
    let latitude = lat.to_radians();
    let longitude = lon.to_radians();

    struct Ellipsoid {
        a: f64,
        f: f64,
    }
    
    const WGS84: Ellipsoid = Ellipsoid {
        a: 6378137.0,
        f: 1.0 / 298.257222101,
    };

    let ellipsoid = WGS84;
    let long_origin = zone as f64 * 6.0 - 183.0;
    let e2 = 2.0 * ellipsoid.f - ellipsoid.f * ellipsoid.f;
    let ep2 = e2 / (1.0 - e2);

    let n = ellipsoid.a / (1.0 - e2 * latitude.sin() * latitude.sin()).sqrt();
    let t = latitude.tan() * latitude.tan();
    let c = ep2 * latitude.cos() * latitude.cos();
    let a = latitude.cos() * (longitude - (long_origin.to_radians()));

    let term1 = 1.0 - e2 / 4.0 - (3.0 * e2 * e2) / 64.0 - (5.0 * e2 * e2 * e2) / 256.0;
    let term2 = (3.0 * e2) / 8.0 + (3.0 * e2 * e2) / 32.0 + (45.0 * e2 * e2 * e2) / 1024.0;
    let term3 = (15.0 * e2 * e2) / 256.0 + (45.0 * e2 * e2 * e2) / 1024.0;
    let term4 = (35.0 * e2 * e2 * e2) / 3072.0;

    let m = ellipsoid.a
        * (term1 * latitude - term2 * (2.0 * latitude).sin() + term3 * (4.0 * latitude).sin()
            - term4 * (6.0 * latitude).sin());

    let x1 = ((1.0 - t + c) * a * a * a) / 6.0;
    let x2 = ((5.0 - 18.0 * t + t * t + 72.0 * c - 58.0 * ep2) * a * a * a * a * a) / 120.0;
    let x = 0.9996 * n * (a + x1 + x2);

    let y1 = (5.0 - t + 9.0 * c + 4.0 * c * c) * (a * a * a * a) / 24.0;
    let y2 = (61.0 - 58.0 * t + t * t + 600.0 * c - 330.0 * ep2) * (a * a * a * a * a * a) / 720.0;
    let y3 = (a * a) / 2.0 + y1 + y2;
    let y = 0.9996 * (m + n * latitude.tan() * y3);

    let northing = y;
    let easting = x + 500000.0;

    return (northing, easting);
}

pub struct CoordTransformation{
    tiepoint_pixels: (f64, f64),
    tiepoint_location: (f64,f64),
    scale: (f64,f64),
    style: CoordStyle
}

impl CoordTransformation{
    pub fn new(tiepoint_pixels: (f64, f64), tiepoint_location: (f64,f64), scale: (f64,f64), style: CoordStyle) -> Result<CoordTransformation, String>{
        match style{
            style => Ok(CoordTransformation{
                tiepoint_pixels,
                tiepoint_location,
                scale,
                style
            })
        }
    }


    pub fn position_to_pixels(&self, position: UEC) -> (f64, f64){
        match self.style{
            CoordStyle::WGS84Equirect => {
                let latlon = Position::UEC(position).to_coord();

                let x = ((latlon.lon - self.tiepoint_location.0) / self.scale.0) + self.tiepoint_pixels.0;
                let y = ((latlon.lat - self.tiepoint_location.1) / self.scale.1) + self.tiepoint_pixels.1;

                return (x,y);
            },
            _ => unimplemented!()
        }
    }

    ///x,y -> lat, lon
    pub fn pixels_to_position(&self, pixels: (f64, f64)) -> UEC{
        match self.style{
            CoordStyle::WGS84Equirect => {
                let x = ((pixels.0 - self.tiepoint_pixels.0) * self.scale.0) + self.tiepoint_location.0;
                let y = ((pixels.1 - self.tiepoint_pixels.1) * self.scale.1) + self.tiepoint_location.1;

                return Position::new_coord(y, x).to_uec();
                
            },
            _ => unimplemented!()
        }
    }
}