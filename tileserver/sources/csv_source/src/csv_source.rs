
use tileserver_model::*;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::error::Error;
use tileserver_model::rayon::prelude::*;
use tileserver_model::smallvec::{SmallVec};

//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

#[allow(clippy::upper_case_acronyms)]
pub enum CoordMode{
    Coord,
    UTM,
    UEC
}

impl From<&str> for CoordMode{
    fn from(str: &str) -> Self {
        match str.to_lowercase().as_str(){
            "coord" => Self::Coord,
            "utm" => Self::UTM,
            "uec" => Self::UEC,
            _ => Self::Coord
        }
    }
}

#[allow(clippy::upper_case_acronyms)]
pub struct CSVSource{
    builder: MultiColumnPointSourceBuilder
}

#[async_trait]
impl SourceInit for CSVSource{
    #[allow(clippy::float_cmp)]
    async fn initialize(settings: SourceConfig, mut source_info: SourceInfo) -> SourceInitResult{
        if settings.input_files.is_empty(){
            return Err("No input file given".into());
        }

        let lat_name = settings.settings.get("lat_name").map(|l| l.as_str()).unwrap_or("latitude");
        let lon_name = settings.settings.get("lon_name").map(|l| l.as_str()).unwrap_or("longitude");
        let pos_mode: CoordMode = settings.settings.get("pos_mode").map(|l| l.as_str()).unwrap_or("coord").into();
        let time_parsing_mode: TimeParsingMode = settings.settings.get("time_mode").map(|l| l.as_str()).unwrap_or("iso").into();
        let time_name = settings.settings.get("time_name").map(|l| l.as_str()).unwrap_or("time");
        let height_positive = settings.settings.get("height_positive").map(|l| l.parse::<bool>().unwrap_or(true)).unwrap_or(true);
        let height_name = settings.settings.get("height_name").map(|l| l.as_str()).unwrap_or("height");
        let seperator = settings.settings.get("seperator").map(|l| l.as_str()).unwrap_or(",");
        let nodata_value = settings.settings.get("nodata_value").map(|l| l.parse::<f32>().unwrap_or(std::f32::NAN)).unwrap_or(std::f32::NAN);
        let zone_number = settings.settings.get("utm_zone_number").map(|l| l.parse::<u8>().unwrap_or(32)).unwrap_or(32);
        let zone_letter = settings.settings.get("utm_zone_letter").map(|l| l.as_str().chars().next().unwrap_or('U')).unwrap_or('U');
        let ignore_lines_starting_with = settings.settings.get("ignore_lines_starting_with").map(|l| l.as_str());

        let height_sign = if height_positive {1.0} else {-1.0};

        let mut bfr = BufReader::new(File::open(&settings.input_files[0])?);

        let filename = settings.input_files[0].file_name().map(|f| f.to_string_lossy().to_string()).unwrap_or_else(|| "Unidentified CSV file".to_string());

        //trace!("Filename {} Height Positive {:?} Height Sign {}", filename, settings.settings.get("height_positive"), height_sign);

        let mut header: String = String::new();
        bfr.read_line(&mut header)?;
        if let Some(ignore_line_sequence) = ignore_lines_starting_with{
            while header.starts_with(ignore_line_sequence) {
                header.clear();
                bfr.read_line(&mut header)?;
            }
        }

        let header_fields: Vec<&str> = header.split(seperator).map(|field| field.trim()).collect();

        debug!("Header read");

        let lat_index = header_fields.iter().position(|&field| field == lat_name).ok_or_else(|| format!("No latitude found under name {}, column names are {:?}", lat_name, header_fields))?;
        let lon_index = header_fields.iter().position(|&field| field == lon_name).ok_or_else(|| format!("No longitude found under name {}, column names are {:?}", lon_name, header_fields))?;
        let height_index = header_fields.iter().position(|&field| field == height_name).ok_or_else(|| format!("No height found under name {}, column names are {:?}", height_name, header_fields))?;

        let time_names = ["year", "month", "day", "hour", "minute", "second", "millisecond"];

        //Okay, so since the time might be in seperate fields (GLODAP, cough cough), we need a collection of indices
        let mut time_indices: Vec<usize> = header_fields.iter().enumerate().filter_map(|(i, &field)|{
            if time_parsing_mode == TimeParsingMode::Seperate{
                if time_names.contains(&field){
                    Some(i)
                }else{
                    None
                }
            }else if field == time_name{
                Some(i)
            }else{
                None
            }
        }).collect();

        
        //Which then will be sorted by their position in time_names
        if time_parsing_mode == TimeParsingMode::Seperate{
            time_indices.sort_by_key(|k| time_names.iter().position(|&timename| timename == header_fields[*k]).unwrap_or(9999));
        }        
        if time_indices.is_empty(){
            return Err("No time found in file!".to_string().into());
        }

        let field_filter = |index| index != lat_index && index != lon_index && index != height_index && (! time_indices.contains(&index));
        
        let value_headers: Vec<&str> = header_fields.iter().enumerate().filter(|(index, &_field)| field_filter(*index)).map(|(_index, &field)| field).collect();

        let mut builder = MultiColumnPointSourceBuilder::new(source_info.clone(),  &filename, &value_headers);
 
        type CsvLine = Result<(UEC, f32, Timestamp, SmallVec<[f32;128]>), Box<dyn Error + Send + Sync>>;

        let rows: Vec<CsvLine> = bfr.lines()
            .enumerate()
            .par_bridge()
            .filter(|(_line_number, line)| {
                if let Some(ignore_line_sequence) = ignore_lines_starting_with{
                    if let Ok(line) = line{
                        if line.starts_with(ignore_line_sequence){
                            return false;
                        }
                    }
                }
                true 
            })
            .map(|(linenumber, line)| {
                let line = line?;
                let parts: SmallVec<[&str;128]> = line.split(seperator).map(|part| part.trim()).collect();
                let parts = parts.iter().copied().collect::<SmallVec<[&str;128]>>();

                if parts.len() < 4{
                    return Err("line too short".into());
                }

                let lat: f64 = parts.get(lat_index).ok_or_else(|| "No latitude in row!".to_string())?.parse().map_err(|e| format!("Error parsing latitude: {:?}", e))?;
                let lon: f64 = parts.get(lon_index).ok_or_else(|| "No longitude in row!".to_string())?.parse().map_err(|e| format!("Error parsing longitude: {:?}", e))?;
                let pos: UEC = match pos_mode{
                    CoordMode::Coord => Position::Coord(Coord::new(lat, lon)),
                    CoordMode::UEC => Position::UEC(UEC::xy(lon, lat)),
                    CoordMode::UTM => Position::UTM(UTM::new(lon, lat, zone_number, zone_letter))
                }.to_uec();

                let height = parts.get(height_index).ok_or_else(|| "No height in row!".to_string())?.parse::<f32>().map_err(|e| format!("Error parsing height: {:?}", e))? * height_sign;

                let time_parts: SmallVec<[&str; 8]> = time_indices.iter().map(|i| parts.get(*i).unwrap_or(&"")).copied().collect();

                let timestamp = time_parsing_mode.parse_default(&time_parts)?;

                let mut values: SmallVec<[f32;128]> = parts.iter().enumerate().filter_map(|(index, &field)| 
                    if field_filter(index) {
                        Some(field.parse::<f32>().map(|v| if v == nodata_value { std::f32::NAN} else {v}).unwrap_or(std::f32::NAN))
                    }else{
                        None
                    }
                ).collect();
                let value_count = values.len();
                let missing_value_count = value_headers.len() - value_count;
                if missing_value_count > 0{
                    values = values.into_iter().chain(std::iter::repeat(std::f32::NAN).take(missing_value_count)).collect();
                    //warn!("Filling in {} NaNs in line {}", missing_value_count, linenumber);
                }

                Ok((pos, height, timestamp, values))
        }).collect();

        for (index, row) in rows.into_iter().enumerate(){
            if let Ok(row) = row{
                builder.add_value_row(row.0, row.1, row.2, &row.3);

            }else{
                error!("Error in CSV {} row {}: {:?}", filename, index, row.unwrap_err());
            }
        }

        builder.finish();

        let mut new_source_info = builder.get_source_info();
        new_source_info.set_attribution(&source_info.attribution);
        new_source_info.set_category(&source_info.category);

        source_info = new_source_info;

        Ok((
            source_info,
            Box::new(CSVSource{
            builder
        })))
    }

    fn accepts_file(file: &std::path::Path) -> bool {
        file.extension().map(|e| e.to_string_lossy().ends_with(".txt")).unwrap_or(true) //Accept files without extensions or ending with .txt
    }
}

#[async_trait]
impl Source for CSVSource{
    async fn sample(&self,query: &SampleQuery) -> Result<Option<GeoData>, Box<dyn std::error::Error + Send + Sync>>{
        self.builder.sample(query)
    }
}