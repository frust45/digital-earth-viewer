//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use tileserver_model::*;
use image::*;
use rand::Rng;
use rand::random;
use rand_pcg::Pcg64;
use rand_seeder::Seeder;

pub struct TestSource{

}

impl TestSource{
    fn create_image_from_tile_path(tilepath: &str) -> TileData<Color>{
        let mut img = RgbaImage::new(constants::TILESIZE.0 as u32, constants::TILESIZE.1 as u32);
        let color_from_tilepath_hue = tilepath.bytes().enumerate().map(|(i,b)| (i as f32) * 12.0 + (b as f32) * 34.0).sum::<f32>() % 360.0;
        let color: palette::rgb::Rgb = palette::Hsl::new(palette::RgbHue::from_degrees(color_from_tilepath_hue),1.0f32,0.66f32).into();
        imageproc::drawing::draw_filled_rect_mut(
            &mut img, 
            imageproc::rect::Rect::at(0,0).of_size(constants::TILESIZE.0 as u32, constants::TILESIZE.1 as u32) , 
            Rgba([0u8,0u8,0u8,0u8]));
        imageproc::drawing::draw_filled_rect_mut(
            &mut img, 
            imageproc::rect::Rect::at(4,4).of_size(constants::TILESIZE.0 as u32 - 4, constants::TILESIZE.1 as u32 - 4) , 
            Rgba([(color.red * 255f32) as u8,(color.green * 255f32) as u8,(color.blue * 255f32) as u8,255u8]));
        let font = rusttype::Font::try_from_bytes(include_bytes!("../UbuntuMono.ttf")).unwrap();
        let pixels_per_char = (constants::TILESIZE.1 as f32 - 8f32) / tilepath.len() as f32;
        let scale = rusttype::Scale::uniform(pixels_per_char);
        let glyphs = font.layout(tilepath, scale, rusttype::point(0.0, font.v_metrics(scale).ascent));
        let mut max_x = 0u32;
        let mut max_y = 0u32;
        for bbox in glyphs.filter_map(|g| g.pixel_bounding_box()){
            max_x += bbox.width() as u32;
            max_y += bbox.height() as u32;
        }
        max_y /= tilepath.len() as u32;
        imageproc::drawing::draw_text_mut(
            &mut img,
            Rgba([255u8,255u8,255u8,0u8]),
            ((constants::TILESIZE.0 as u32 - 8) - max_x) / 2, 
            ((constants::TILESIZE.1 as u32 - 8) - max_y) / 2, 
            rusttype::Scale{x: pixels_per_char, y: pixels_per_char}, 
            &font, 
            tilepath);

        let data:Vec<Color> = img.as_flat_samples().samples.chunks_exact(4).map(|c| Color::from_rgba(c[0], c[1], c[2],c[3])).collect();
        
        TileData::new_from_tile_and_raw_data(Tile::from_tilepath(tilepath).unwrap(), data)
    }
    
    fn create_solid_scalar_tile(tilepath: &str, value: f32) -> TileData<Scalar>{
        TileData::new_from_uec_closure(Tile::from_tilepath(tilepath).unwrap(),&|_uec| value)
    }

    fn create_random_scalar_tile(tilepath: &str) -> TileData<Scalar>{
        let mut rng: Pcg64 = Seeder::from(tilepath).make_rng();
        if tilepath.len() <= 1{
            TileData::new_from_mut_uec_closure(Tile::from_tilepath(tilepath).unwrap(), &mut|_uec| rng.gen::<f32>() - 0.5)
        }else{
            let parent_tile = TestSource::create_random_scalar_tile(&tilepath[0..tilepath.len()-1]);
            TileData::new_from_mut_uec_closure(Tile::from_tilepath(tilepath).unwrap(), &mut|uec| parent_tile.value_at_uec(uec) + ((rng.gen::<f32>() - 0.5) / (tilepath.len() as f32 - 1.0)))
        }
    }

    fn create_julia_scalar_tile(tilepath: &str) -> TileData<Scalar>{
        const JULIA_ITERATIONS: usize = 1000;
        const R: f64 = 0.5;
        const CX: f64 = -0.70176;
        const CY: f64 = -0.3842;
        TileData::new_from_uec_closure(Tile::from_tilepath(tilepath).unwrap(), |uec: UEC| {
            let mut zx = (uec.x - 0.5) * 2.0;
            let mut zy = (uec.y - 0.5) * 2.0;
            let mut iteration = 0;
            while zx.powi(2) + zy.powi(2) >= R.powi(2) && iteration < JULIA_ITERATIONS{
                let xtemp = zx.powi(2) - zy.powi(2);
                zy = 2.0 * zx * zy + CY;
                zx = xtemp + CX;
                iteration += 1;
            }
            if iteration == JULIA_ITERATIONS{
                0f32
            }else{
                (iteration as f32 / 10.0).sin()
            }
        })
    }

    fn create_mandelbrot_scalar_tile(tilepath: &str) -> TileData<Scalar>{
        //from https://dev.to/foqc/mandelbrot-set-in-js-480o
        const MAX_ITERATION: usize = 80;
        fn mandelbrot(cx: f64, cy: f64) -> (usize, bool) {
            let mut zx: f64 = 0.0;
            let mut zy: f64 = 0.0;
            let mut n = 0;
            let mut px;
            let mut py;
            let mut d;
            loop{
                px = zx.powi(2) - zx.powi(2);
                py = 2.0 * zx * zy;
                zx = px + cx;
                zy = py + cy;
                d = (zx.powi(2) + zy.powi(2)).sqrt();
                n += 1;
                if d > 2.0 || n >= MAX_ITERATION{
                    break;
                }
            }
            (n, d <= 2.0)
        }

        TileData::new_from_uec_closure(Tile::from_tilepath(tilepath).unwrap(), |uec: UEC| {
            let cx = -2.0 + uec.x;
            let cy = -1.0 + uec.y;
            let (m, _is_mandelbrot_set) = mandelbrot(cx, cy);
            //if isMandelbrotSet{
                (m % 16) as f32 / 16.0
            //}else{
            //    0.0
            //}
        })
    }

    fn create_normals() -> LinesData<Scalar>{
        let mut lines = Vec::new();
        for sx in 0 .. 100{
            for sy in 0 ..100{
                let pos = UEC::xy(sx as f64 / 100.0, sy as f64 / 100.0);
                lines.push(GeoLine::new_from_uec(pos, 0f32, pos, 10000.0, 0,0, 0f32, 1f32));
            }
        }
        LinesData{
            lines
        }
    }
 
    fn create_random_line() -> LinesData<Scalar>{
        let mut rng: Pcg64 = Seeder::from(64).make_rng();
        let mut pos_start = UEC::xy(0.5, 0.5);
        let mut pos_end = UEC::xy(0.501, 0.501);
        let mut height_start = 0.0;
        let mut height_end = 0.1;
        let mut lines = Vec::new();
        let mut value_start: f32 = rng.gen_range(0.0..1.0);
        let mut value_end: f32 = rng.gen_range(0.0..1.0);

        for _i in 0 .. 1000{
            lines.push(GeoLine::new_from_uec(pos_start, height_start, pos_end, height_end, 0,0,  value_start, value_end));
            pos_start = pos_end;
            height_start = height_end;
            value_start = value_end;
            pos_end = pos_start + UEC::xy(rng.gen_range(0.0001..0.0003), rng.gen_range(0.0001..0.0003));
            height_end = height_start + rng.gen_range(0.0..1.0);
            value_end = rng.gen_range(0.0..1.0);
        }

        LinesData{
            lines
        }
    }

    fn create_zigzag_line() -> LinesData<Scalar>{
        let mut pos_start = Position::UEC(UEC::xy(0.5, 0.5));
        let mut pos_end = pos_start.move_by_meters(1.0, 1.0);
        let mut lines = Vec::new();
        for i in 0 .. 10{
            lines.push(GeoLine::new_from_position(pos_start, 0.0, pos_end, 0.0, 0, 0, 0.0, 1.0));
            pos_start = pos_end;
            pos_end = pos_end.move_by_meters(
                1.0,
                if i % 2 == 0{
                    -1.0
                }else{
                    1.0
                }
            )
        }
        LinesData{
            lines
        }
    }

    fn create_coordinate_grid() -> LinesData<Scalar>{
        let mut lines = Vec::new();
        const SEGMENTS: i32 = 100;
        for lon in 0 .. 360{
            for subdiv in 0 .. SEGMENTS{
                let lat = (360.0 * (subdiv as f64 / SEGMENTS as f64)) - 180.0;
                let next_lat = (360.0 * ((subdiv + 1) as f64 / SEGMENTS as f64)) - 180.0;
                lines.push(GeoLine::new_from_position(
                    Position::Coord(Coord::new(lat, lon as f64)),
                    0.0,
                    Position::Coord(Coord::new(next_lat, lon as f64)),
                    0.0,
                    0,
                    0,
                    lon as f32 / 360.0,
                    lon as f32 / 360.0
                ));
            }
        }

        for lat in 0 .. 360{
            for subdiv in 0 .. SEGMENTS{
                let lon = 360.0 * (subdiv as f64 / SEGMENTS as f64);
                let next_lon = 360.0 * ((subdiv + 1) as f64 / SEGMENTS as f64);
                lines.push(GeoLine::new_from_position(
                    Position::Coord(Coord::new(lat as f64 - 180.0, lon as f64)),
                    0.0,
                    Position::Coord(Coord::new(lat as f64 - 180.0, next_lon as f64)),
                    0.0,
                    0,
                    0,
                    lat as f32 / 360.0,
                    lat as f32 / 360.0
                ));
            }
        }
        
        LinesData{
            lines
        }
    }

    fn create_random_points() -> PointsData<Scalar>{
        const NUMBER_OF_POINTS: usize = 10000;
        let mut rng: Pcg64 = Seeder::from("Dischidäl Örf Wieuuur").make_rng();
        let points = (0..NUMBER_OF_POINTS).into_iter().map(|_i|{
            GeoPoint::new_from_uec(
                UEC::xy(
                        rng.gen_range(0.0..1.0),
                        rng.gen_range(0.0..1.0)
                    ), 
                    rng.gen_range(0.0..1.0),
                    rng.gen_range(0..100000),
                    rng.gen_range(0.0..1.0)
                )
        }).collect::<Vec<_>>();
        PointsData::new(points)
    }

    fn create_100_random_points() -> PointsData<Scalar>{
        const NUMBER_OF_POINTS: usize = 100;
        let mut rng: Pcg64 = Seeder::from("Fohr Deeh Dardah Wieuuur").make_rng();
        let points = (0..NUMBER_OF_POINTS).into_iter().map(|_i|{
            GeoPoint::new_from_uec(
                UEC::xy(
                        rng.gen_range(0.0..1.0),
                        rng.gen_range(0.0..1.0)
                    ), 
                    rng.gen_range(0.0..1.0),
                    rng.gen_range(0..100000),
                    rng.gen_range(0.0..1.0)
                )
        }).collect::<Vec<_>>();
        PointsData::new(points)
    }
}


#[async_trait]
impl SourceInit for TestSource{
    async fn initialize(_settings: SourceConfig, mut source_info: SourceInfo) -> SourceInitResult {
        source_info.instance_name = "Test".to_string();
        
        let mut named_tiles_layer = LayerInfo::new("TileNames", GeoDataType::ColorTiles);
        named_tiles_layer.set_max_zoom_level(25);
        named_tiles_layer.set_extent(UECArea::new(UEC::xy(0.0,0.0), UEC::xy(1.0, 1.0)));
        source_info.add_layer(named_tiles_layer);

        let mut named_tiles_layer_fallible = LayerInfo::new("TileNamesFailing", GeoDataType::ColorTiles);
        named_tiles_layer_fallible.set_max_zoom_level(25);
        named_tiles_layer_fallible.set_extent(UECArea::new(UEC::xy(0.0,0.0), UEC::xy(1.0, 1.0)));
        source_info.add_layer(named_tiles_layer_fallible);

        let mut zero_layer = LayerInfo::new("Zero", GeoDataType::ScalarTiles);
        zero_layer.set_max_zoom_level(1);
        zero_layer.set_extent(UECArea::new(UEC::xy(0.0,0.0), UEC::xy(1.0, 1.0)));
        zero_layer.set_datarange(-1.0, 1.0);
        source_info.add_layer(zero_layer);

        let mut one_layer = LayerInfo::new("One", GeoDataType::ScalarTiles);
        one_layer.set_max_zoom_level(1);
        one_layer.set_extent(UECArea::new(UEC::xy(0.0,0.0), UEC::xy(1.0, 1.0)));
        one_layer.set_datarange(-1.0, 1.0);
        source_info.add_layer(one_layer);

        let mut neg_one_layer = LayerInfo::new("NegativeOne", GeoDataType::ScalarTiles);
        neg_one_layer.set_max_zoom_level(1);
        neg_one_layer.set_extent(UECArea::new(UEC::xy(0.0,0.0), UEC::xy(1.0, 1.0)));
        neg_one_layer.set_datarange(-1.0, 1.0);
        source_info.add_layer(neg_one_layer);

        let mut random_tiles_layer = LayerInfo::new("RandomTiles", GeoDataType::ScalarTiles);
        random_tiles_layer.set_max_zoom_level(255);
        random_tiles_layer.set_extent(UECArea::new(UEC::xy(0.0,0.0), UEC::xy(1.0, 1.0)));
        random_tiles_layer.set_datarange(0.0, 2.0);
        source_info.add_layer(random_tiles_layer);

        let mut julia_layer = LayerInfo::new("JuliaSet", GeoDataType::ScalarTiles);
        julia_layer.set_max_zoom_level(255);
        julia_layer.set_extent(UECArea::new(UEC::xy(0.0,0.0), UEC::xy(1.0, 1.0)));
        julia_layer.set_datarange(-1.0, 1.0);
        source_info.add_layer(julia_layer);

        let mut mandelbrot_layer = LayerInfo::new("MandelbrotSet", GeoDataType::ScalarTiles);
        mandelbrot_layer.set_max_zoom_level(255);
        mandelbrot_layer.set_extent(UECArea::new(UEC::xy(0.0,0.0), UEC::xy(1.0, 1.0)));
        mandelbrot_layer.set_datarange(0.0, 1.0);
        source_info.add_layer(mandelbrot_layer);

        let mut randomlines_layer = LayerInfo::new("RandomLines", GeoDataType::ScalarLines);
        randomlines_layer.set_max_zoom_level(255);
        randomlines_layer.set_extent(UECArea::area_from_two_points(UEC::xy(0.5,0.5), UEC::xy(1.0, 1.0)));
        randomlines_layer.set_datarange(0.0, 1.0);
        source_info.add_layer(randomlines_layer);

        let mut zigzag_lines = LayerInfo::new("ZigZag", GeoDataType::ScalarLines);
        zigzag_lines.set_max_zoom_level(255);
        zigzag_lines.set_extent(UECArea::area_from_two_points(UEC::xy(0.5,0.5), Position::UEC(UEC::xy(0.5, 0.5)).move_by_meters(10.0, 1.0).to_uec()));
        zigzag_lines.set_datarange(0.0, 1.0);
        source_info.add_layer(zigzag_lines);

        let mut normals = LayerInfo::new("Normals", GeoDataType::ScalarLines);
        normals.set_max_zoom_level(255);
        normals.set_extent(UECArea::area_from_two_points(UEC::xy(0.0,0.0), UEC::xy(1.0, 1.0)));
        normals.set_datarange(0.0, 1.0);
        source_info.add_layer(normals);

        let mut coordinate_grid = LayerInfo::new("CoordinateGrid", GeoDataType::ScalarLines);
        coordinate_grid.set_max_zoom_level(255);
        coordinate_grid.set_extent(UECArea::new(UEC::xy(0.0,0.0), UEC::xy(1.0,1.0)));
        coordinate_grid.set_datarange(0.0, 1.0);
        source_info.add_layer(coordinate_grid);

        let mut random_points = LayerInfo::new("RandomPoints", GeoDataType::ScalarPoints);
        random_points.set_extent(UECArea::new(UEC::xy(0.0,0.0), UEC::xy(1.0,1.0)));
        random_points.set_datarange(0.0, 1.0);
        random_points.set_timesteps(&[0]);
        random_points.timerange = Some((0, 100000));
        source_info.add_layer(random_points);

        let mut random_100_points = LayerInfo::new("Random100Points", GeoDataType::ScalarPoints);
        random_100_points.set_extent(UECArea::new(UEC::xy(0.0,0.0), UEC::xy(1.0,1.0)));
        random_100_points.set_datarange(0.0, 1.0);
        random_100_points.set_timesteps(&[0]);
        random_100_points.timerange = Some((0, 100000));
        source_info.add_layer(random_100_points);


        Ok((source_info, Box::new(TestSource{

        })))
    }

    fn accepts_file(_file: &std::path::Path) -> bool {
        true
    }
}

#[async_trait]
impl Source for TestSource{
    async fn sample(&self, query: &SampleQuery) -> Result<Option<GeoData>, Box<dyn std::error::Error + Send + Sync>> {
        match query.layername.as_ref(){
            "TileNames" => {
                Ok(Some(GeoData::ColorTiles(TestSource::create_image_from_tile_path(&query.tile.path))))
            },
            "TileNamesFailing" => {
                if random::<f32>() > 0.5f32 {
                    Ok(Some(GeoData::ColorTiles(TestSource::create_image_from_tile_path(&query.tile.path))))
                } else {
                    Err("Failed intentionally.".into())
                }
            }
            "Zero" => {
                Ok(Some(GeoData::ScalarTiles(TestSource::create_solid_scalar_tile(&query.tile.path, 0.0))))
            },
            "One" => {
                Ok(Some(GeoData::ScalarTiles(TestSource::create_solid_scalar_tile(&query.tile.path, 1.0))))
            },
            "NegativeOne" => {
                Ok(Some(GeoData::ScalarTiles(TestSource::create_solid_scalar_tile(&query.tile.path, -1.0))))
            },
            "RandomTiles" => {
                Ok(Some(GeoData::ScalarTiles(TestSource::create_random_scalar_tile(&query.tile.path))))
            },
            "JuliaSet" => {
                Ok(Some(GeoData::ScalarTiles(TestSource::create_julia_scalar_tile(&query.tile.path))))
            },
            "MandelbrotSet" => {
                Ok(Some(GeoData::ScalarTiles(TestSource::create_mandelbrot_scalar_tile(&query.tile.path))))
            },
            "RandomLines" => {
                Ok(Some(GeoData::ScalarLines(TestSource::create_random_line())))
            },
            "ZigZag" => {
                Ok(Some(GeoData::ScalarLines(TestSource::create_zigzag_line())))
            },
            "CoordinateGrid" => {
                Ok(Some(GeoData::ScalarLines(TestSource::create_coordinate_grid())))
            },
            "RandomPoints" => {
                Ok(Some(GeoData::ScalarPoints(TestSource::create_random_points())))
            },
            "Random100Points" => {
                Ok(Some(GeoData::ScalarPoints(TestSource::create_100_random_points())))
            },
            "Normals" => {
                Ok(Some(GeoData::ScalarLines(TestSource::create_normals())))
            }
            _ => Err(format!("No layer with name {}", &query.layername).into())
        }
    }
}