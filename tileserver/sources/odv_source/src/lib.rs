//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

mod odv;
mod odv_source;
pub use odv_source::ODVSource;