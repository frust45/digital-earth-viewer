//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use tileserver_model::*;

use std::{convert::TryFrom, io::{BufReader, BufRead}};
use std::fs::File;
use tileserver_model::async_trait;

use crate::odv::*;
pub struct ODVSource{
    builder: MultiColumnPointSourceBuilder
}

#[async_trait]
impl SourceInit for ODVSource {
    async fn initialize(settings: SourceConfig, mut source_info: SourceInfo) -> SourceInitResult{
        //Check we have files to parse
        if settings.input_files.is_empty(){
            return Err("No input file given".into());
        }

        let depth_name = settings.settings.get("depth").map(|l| l.as_str()).unwrap_or("Depth [salt water, m]");

        //Extract filename
        //let _filename = settings.input_files[0].file_stem().map(|f| f.to_string_lossy().to_string()).unwrap_or_else(||"Unidentified ODV file".to_string());
 
        //Create buffered reader
        let bfr = BufReader::new(File::open(&settings.input_files[0])?);

        ///Helper struct since MetaVariables and DataVariables are just lumped together in ODV
        #[derive(Debug, Clone)]
        enum HeaderVariable{
            MetaVariable(MetaVariable),
            DataVariable(DataVariable)
        }

        impl HeaderVariable{
            ///label accessor
            pub fn label(&self) -> &str{
                match self{
                    HeaderVariable::MetaVariable(mv) => &mv.label,
                    HeaderVariable::DataVariable(dv) => &dv.label
                }
            }

            ///significant_digits accessor
            pub fn significant_digits(&self) -> i32{
                match self{
                    HeaderVariable::MetaVariable(mv) => mv.significant_digits,
                    HeaderVariable::DataVariable(dv) => dv.significant_digits
                }
            }

            ///name accessor
            pub fn name(&self) -> &str{
                match self{
                    HeaderVariable::MetaVariable(mv) => &mv.label,
                    HeaderVariable::DataVariable(dv) => &dv.name
                }
            }

            //unit accessor
            pub fn unit(&self) -> Option<&str>{
                match self{
                    HeaderVariable::DataVariable(dv) => dv.unit.as_deref(),
                    _ => None,
                }
            }

            //Returns the metavariable type of a given variable, if applicable
            pub fn metavariable_type(&self) -> Option<MetaVarType>{
                match self{
                    HeaderVariable::MetaVariable(mv) => Some(mv.var_type.clone()),
                    _ => None
                }
            }
        }

        ///File parser state
        #[derive(Debug)]
        enum ParserState{
            Metadata{ //Metadata: collect meta variable and data variable metadata
                meta_variables: Vec<MetaVariable>,
                data_variables: Vec<DataVariable>,                
            },
            Header{ //Header: collect a list of variables and sort the by the order they appear in the file
                variables: Vec<HeaderVariable>
            },
            Data{ //Data: find the data in the file, possibly backfilling from the last line, add it to a builder
                header: Vec<HeaderVariable>,
                last_lon: f64,
                last_lat: f64,
                last_depth: f32,
                last_time: Timestamp,
                last_line: Vec<f32>,
                builder: Box<MultiColumnPointSourceBuilder>
            }
        }

        //initial state of the parser: we're reading metadata (and we already know about the time)
        let mut parser_state = ParserState::Metadata{
            meta_variables: vec![
                MetaVariable{
                    comment: "Automatically added time variable since it isn't mentioned in ODV format".to_string(),
                    label: "yyyy-mm-ddThh:mm:ss.sss".to_string(),
                    significant_digits: 0,
                    qf_schema: QfSchema::DigitalEarthViewer,
                    value_type: ValueType::TEXT,
                    var_type: MetaVarType::METATIME
                }
            ],
            data_variables: Vec::new()
        };

        //Go through each line in the file
        for (_, l) in bfr.lines().enumerate(){
           let line = l?; //Check that we didn't run into an error reading the line
           loop{//Parse the line (we might need to look at some lines twice, i.e. to figure out if they're a comment)
                match &mut parser_state{
                    ParserState::Metadata { meta_variables, data_variables } => {//Metadata
                        if !line.starts_with("//"){//All metadata lines start with a comment, so if it doesn't move on to the next state
                            parser_state = ParserState::Header{ //The header consists of both metavariables and datavariables
                                variables: meta_variables.iter().cloned()
                                    .map(HeaderVariable::MetaVariable)
                                    .chain(
                                        data_variables.iter().cloned()
                                        .map(HeaderVariable::DataVariable)
                                    )
                                    .collect()
                            };
                            continue; //Continue means parse this line again, with the new state
                        //if the lines DOES start with a comment, try parsing a metavariable definition from it
                        }else if let Ok(mv) = MetaVariable::try_from(line.as_str()){
                            meta_variables.push(mv);
                            break; //Break means move on to next line
                        //could also be a data variable
                        }else if let Ok(dv) = DataVariable::try_from(line.as_str()){
                            //Special case for height
                            if dv.label == depth_name{
                                let depth_mv = MetaVariable{
                                    label: dv.label,
                                    var_type: MetaVarType::METADEPTH,
                                    value_type: ValueType::FLOAT,
                                    qf_schema: QfSchema::DigitalEarthViewer,
                                    significant_digits: dv.significant_digits,
                                    comment: dv.comment,
                                };
                                meta_variables.push(depth_mv)
                            }else{
                                data_variables.push(dv);
                            }
                            break;
                        //or just a user comment, which we ignore
                        }else{
                            break; //Ignore lines we don't recognize
                        }
                    },
                    ParserState::Header{variables} => {
                        //Time to assemble the header
                        let headerparts = line.split('\t').collect::<Vec<_>>();
                        //If there are variables in the file which aren't annotated, those are errors
                        if headerparts.len() != variables.len(){
                            return Err(format!("Metadata mentions {} variables, but header contains {} variables\nMetadata: {:#?}\nHeader:{:#?}", variables.len(), headerparts.len(), variables, headerparts).into());
                        }
                        //Sort the variables by their occurence in the file
                        variables.sort_by_cached_key(|v| headerparts.iter().position(|p| p == &v.label()));
                        //We need some variables as metainformation for our datapoints
                        /*let lat_pos = variables.iter().position(|v| v.is_metavariable(MetaVarType::METALATITUDE)).ok_or("Metavariable latitude not found")?;
                        let lon_pos = variables.iter().position(|v| v.is_metavariable(MetaVarType::METALONGITUDE)).ok_or("Metavariable longitude not found")?;
                        let depth_pos = variables.iter().position(|v| v.is_metavariable(MetaVarType::METABOTDEPTH)).ok_or("Metavariable bot. depth not found")?;
                        let time_pos = variables.iter().position(|v| v.is_metavariable(MetaVarType::METATIME)).ok_or("Metavariable time not found")?;*/
                        let last_lon = f64::NAN;
                        let last_lat = f64::NAN;
                        let last_depth = f32::NAN;
                        let last_time = 0;

                        //Collect all variable names
                        let column_names = variables.iter().map(|v| v.name()).collect::<Vec<_>>();

                        //And construct a builder from it
                        let mut builder = Box::new(MultiColumnPointSourceBuilder::new(
                            source_info.clone(), 
                            "ODV", 
                             &column_names));

                        //Set the unit for those columns that have one
                        for v in variables.iter(){
                            if let Some(unit) = v.unit(){
                                builder.set_unit(v.name(), unit);
                            }
                        }

                        //trace!("Variables: {:+?}", variables);

                        //And pass all of this on to the next stage
                        parser_state = ParserState::Data{
                            last_lat,
                            last_lon,
                            last_depth,
                            last_time,
                            header: variables.clone(),
                            last_line: vec![std::f32::NAN; variables.len()],
                            builder
                        };
                        break;
                    }
                    ParserState::Data { last_lon, last_lat, last_depth, last_time, header, last_line, builder} => {
                        if line.starts_with("//"){ //Intra-Data-Comment, skip this
                            break;
                        }else{
                            line.split('\t').zip(header.iter()).zip(last_line.iter_mut()).for_each(|((part,variable), previous_value): ((&str, &HeaderVariable), &mut f32)| {
                                if !part.is_empty(){

                                    //Missing specialization in rust means we have this function twice. Yay or so.
                                    fn round_to_digits_f32(number: f32, digits: i32) -> f32{
                                        if digits == 0{
                                            number
                                        }else{
                                            (number * 10f32.powi(digits)).round() / (10f32.powi(digits))
                                        }
                                    }

                                    fn round_to_digits_f64(number: f64, digits: i32) -> f64{
                                        if digits == 0{
                                            number
                                        }else{
                                            (number * 10f64.powi(digits)).round() / (10f64.powi(digits))
                                        }
                                    }

                                    match variable.metavariable_type(){
                                        Some(MetaVarType::METALATITUDE) => {
                                            if let Ok(mut new_lat) = part.parse::<f64>(){
                                                new_lat = round_to_digits_f64(new_lat, variable.significant_digits());
                                                *last_lat = new_lat;
                                                *previous_value = new_lat as f32;
                                            }
                                        },
                                        Some(MetaVarType::METALONGITUDE) => {
                                            if let Ok(mut new_lon) = part.parse::<f64>(){
                                                new_lon = round_to_digits_f64(new_lon, variable.significant_digits());
                                                *last_lon = new_lon;
                                                *previous_value = new_lon as f32;
                                            }
                                        },
                                        Some(MetaVarType::METADEPTH) => {
                                            if let Ok(mut new_depth) = part.parse::<f32>(){
                                                new_depth = round_to_digits_f32(new_depth, variable.significant_digits());
                                                *last_depth = new_depth;
                                                *previous_value = new_depth;
                                            }
                                        },
                                        Some(MetaVarType::METATIME) => {
                                            if let Ok(new_time) = TimeParser::time_from_odv_str_without_millis(part){
                                                *last_time = new_time;
                                                *previous_value = new_time as f32;
                                            }else{
                                                //trace!("Could not parse time from {}", part);
                                            }
                                        }
                                        _ => {
                                            if let Ok(mut value) = part.parse::<f32>(){
                                                value = round_to_digits_f32(value, variable.significant_digits());
                                                *previous_value = value;
                                            }else{
                                                *previous_value = f32::NAN;
                                            }
                                        }
                                    }
                                    if previous_value.is_nan() {
                                    }
                                }
                            });
                            builder.add_value_row(Position::Coord(Coord::new(*last_lat, *last_lon)).to_uec(), -1f32 * *last_depth, *last_time, last_line);
                            break;
                        }
                    }
                }
            }
        }


        if let ParserState::Data { mut builder, .. } = parser_state{
            builder.finish();
            //eprintln!("Dumping data");
            //builder.dump_tsv(&mut File::create(format!("dump_{}.tsv", filename))?)?;
            source_info = builder.get_source_info();
            Ok((source_info, Box::new(ODVSource{
                builder: *builder
            })))
        }else{
            Err(format!("ODV Parser not in final state: {:#?}", parser_state).into())
        }
    }

    fn accepts_file(file: &std::path::Path) -> bool {
        file.extension().map(|e| e.to_string_lossy().ends_with(".txt")).unwrap_or(true) //Accept files without extensions or ending with .txt
    }
}

#[async_trait]
impl Source for ODVSource {
    async fn sample(&self,query: &SampleQuery) -> Result<Option<GeoData>, Box<dyn std::error::Error + Send + Sync>>{
        self.builder.sample(query)    
    }
}