//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use std::{convert::TryFrom, error::Error};
use lazy_static::lazy_static;
use regex::Regex;

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum MetaVarType{
    METACRUISE,
    METASTATION,
    METATYPE,
    METALONGITUDE,
    METALATITUDE,
    METABOTDEPTH,
    METADEPTH,
    METATIME,
    Other(String)
}

impl From<&str> for MetaVarType{
    fn from(str: &str) -> Self {
        match str{
            "METACRUISE" => MetaVarType::METACRUISE,
            "METASTATION" => MetaVarType::METASTATION,
            "METATYPE" => MetaVarType::METATYPE,
            "METATIME" => MetaVarType::METATIME,
            "METALONGITUDE" => MetaVarType::METALONGITUDE,
            "METALATITUDE" => MetaVarType::METALATITUDE,
            "METABOTDEPTH" => MetaVarType::METABOTDEPTH,
            other => MetaVarType::Other(other.to_string())
        }
    }
}

#[allow(non_camel_case_types)]
#[derive(Debug, Clone)]
pub enum ValueType{
    INDEXED_TEXT,
    TEXT,
    FLOAT,
    Other(String)
}

impl From<&str> for ValueType{
    fn from(str: &str) -> Self {
        match str{
            "INDEXED_TEXT" => ValueType::INDEXED_TEXT,
            "TEXT" => ValueType::TEXT,
            "FLOAT" => ValueType::FLOAT,
            other => ValueType::Other(other.to_string())
        }
    }
}

#[derive(Debug, Clone)]
pub enum QfSchema{
    ODV,
    DigitalEarthViewer,
    Other(String)
}

impl From<&str> for QfSchema{
    fn from(str: &str) -> Self {
        match str{
            "ODV" => QfSchema::ODV,
            other => QfSchema::Other(other.to_string())
        }
    }
}

#[derive(Debug, Clone)]
pub struct MetaVariable{
    pub label: String,
    pub var_type: MetaVarType,
    pub value_type: ValueType,
    pub qf_schema: QfSchema,
    pub significant_digits: i32,
    pub comment: String
}

lazy_static! {
    static ref RE_METAVAR: Regex = Regex::new(r#"(//)?(<MetaVariable>)?label="(.+)"\s+var_type="(.+)"\s+value_type="(.+)"\s+qf_schema="(.+)"\s+significant_digits="(.+)"\s+comment="(.?)"(</MetaVariable>)"#).unwrap();
}

impl TryFrom<&str> for MetaVariable{
    type Error = Box<dyn Error + Send + Sync>;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let parts = RE_METAVAR.captures(value).ok_or("Line not recognized as MetaVariable")?;
        Ok(MetaVariable{
            label: parts.get(3).map(|m| m.as_str()).ok_or("label not found")?.into(),
            var_type: parts.get(4).map(|m| m.as_str()).ok_or("var_type not found")?.into(),
            value_type: parts.get(5).map(|m| m.as_str()).ok_or("value_type not found")?.into(),
            qf_schema: parts.get(6).map(|m| m.as_str()).ok_or("qf_schema not found")?.into(),
            significant_digits: parts.get(7).map(|m| m.as_str()).ok_or("significant_digits not found")?.parse::<i32>()?,
            comment: parts.get(8).map(|m| m.as_str()).unwrap_or("").into()
        })
    }
}

lazy_static! {
    static ref RE_DATAVAR: Regex = Regex::new(r#"(//)?(<DataVariable>)?label="(.+)"\s+value_type="(.+)"\s+qf_schema="(.+)"\s+significant_digits="(.+)"\s+is_primary_variable="(.+)"\s+comment="(.?)"(.*)(</DataVariable>)"#).unwrap();
    static ref RE_NAME_UNIT: Regex = Regex::new(r#"(.+)\s\[(.+)\]"#).unwrap();
}

#[derive(Debug, Clone)]
pub struct DataVariable{
    pub label: String,
    pub name: String,
    pub unit: Option<String>,
    pub value_type: ValueType,
    pub significant_digits: i32,
    pub is_primary_variable: bool,
    pub qf_schema: QfSchema,
    pub comment: String
}

impl TryFrom<&str> for DataVariable{
    type Error = Box<dyn Error + Send + Sync>;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let parts = RE_DATAVAR.captures(value).ok_or("Line not recognized as MetaVariable")?;
        let label = parts.get(3).map(|m| m.as_str()).ok_or("label not found")?.to_string();
        let value_type = parts.get(4).map(|m| m.as_str()).ok_or("value_type not found")?.into();
        let qf_schema = parts.get(5).map(|m| m.as_str()).ok_or("qf_schema not found")?.into();
        let significant_digits = parts.get(6).map(|m| m.as_str()).ok_or("significant_digits not found")?.parse::<i32>()?;
        let is_primary_variable = parts.get(7).map(|m| m.as_str() == "T").ok_or("is_primary_variable not found")?;
        let comment = parts.get(8).map(|m| m.as_str()).unwrap_or("").into();
        let name;
        let unit;
        let label_parts = RE_NAME_UNIT.captures(&label);
        if let Some(label_parts) = label_parts{
            if let (Some(name_part), Some(unit_part)) = (label_parts.get(1), label_parts.get(2)){
                name = name_part.as_str().into();
                unit = Some(unit_part.as_str().into());
            }else{
                name = label.clone();
                unit = None;
            }
        }else{
            name = label.clone();
            unit = None;
        }
        Ok(DataVariable{
            label,
            name,
            unit,
            value_type,
            significant_digits,
            is_primary_variable,
            qf_schema,
            comment
        })
    }
}