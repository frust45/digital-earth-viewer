//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use tileserver_model::*;

use std::error::Error;

use std::fs::File;

use std::io::{Read, BufReader, BufWriter};

use std::path::{PathBuf, Path};

use tileserver_model::rayon::prelude::*;

use tileserver_model::chrono::*;

use std::sync::Mutex;

use lru::LruCache;

use tileserver_model::async_trait;


/// Converts a millisecond timestamp into `NaiveDateTime`.
fn datetime_from_milliseconds(ms: i64) -> chrono::NaiveDateTime{
    let s = ms / 1000;
    let ns = (ms % 1000) * 1000000;
    chrono::NaiveDateTime::from_timestamp(s, ns as u32)
}

/// Source based around [NASA's "Blue Marble"](https://visibleearth.nasa.gov/collection/1484/blue-marble) artwork.
///
/// Has one layer per month of the year. Relatively slow at startup since the PNG files are very large.
pub struct BlueMarbleSource{
    /// Source file paths.
    pub files: Vec<PathBuf>,
    /// Storage of the decoded bitmaps in memory.
    pub cache: Mutex<LruCache<u8, Array2D<Color>>>,
    /// Number of subdivisions after which one output pixel is smaller than one input pixel.
    pub max_zoom_level: u8,
    pub timesteps: Vec<Timestamp>
}

impl BlueMarbleSource{

    fn png_to_raw(pngfile: &Path) -> Result<(), Box<dyn Error>>{
        info!("Reshaping {}", pngfile.display());
        let rgba = image::load(BufReader::new(File::open(pngfile)?), image::ImageFormat::Png).unwrap().into_rgba8();
        let mut array: Array2D<Color> = Array2D::new(Point2D::xy(rgba.width() as f64, rgba.height() as f64));
        let array_size = array.size;
        array.data.par_iter_mut().enumerate().for_each(|(i,v)|{
            let x = i as u32 % array_size.x as u32;
            let y = i as u32 / array_size.x as u32;
            *v = rgba[(x,y)].0;
        });
        let mut p = PathBuf::from(pngfile);
        p.set_extension("array2dcolor");
        let mut writer = BufWriter::new(File::create(&p)?);
        bincode::serialize_into(&mut writer, &array)?;
        Ok(())
    }

    /*
    #[allow(dead_code)]
    fn raw_to_png(raw: &Array2D<Color>, pngfile: &Path){
        let rgba = image::RgbaImage::from_fn(raw.size.x as u32, raw.size.y as u32, |x, y| {
              raw.data[x as usize + (y as usize * raw.size.x as usize)].into()
        });
        rgba.save(pngfile).unwrap();
    }
    */
}

impl SourceInit for BlueMarbleSource{
    fn initialize(settings: &SourceConfig, source_info: &mut SourceInfo) -> Result<Box<dyn Source>, Box<dyn Error + Send + Sync>>{
        if settings.input_files.is_empty() {
            return Err("No directory with blue marble files given".into());
        }

        let imagefolder = settings.input_files[0].clone();

        let imagepath = imagefolder.join("filelist.txt");

        let mut max_zoom_level = 1;

        let pixel_size = constants::CIRCUMFERENCE_EQUATOR_METRES /  21600f64;

        while Tile::pixel_size_in_meters_from_path_length(max_zoom_level) > pixel_size {
            max_zoom_level += 1;
        }

        let mut timesteps: Vec<Timestamp> = Vec::new();
        let y = 2004;
        for m in 1 .. 13{
            let t0 = chrono::NaiveDateTime::new(chrono::NaiveDate::from_ymd(y, m, 1), chrono::NaiveTime::from_num_seconds_from_midnight(0, 0));
            timesteps.push(t0.timestamp_millis());
        }

        let tn = chrono::NaiveDateTime::new(chrono::NaiveDate::from_ymd(2005, 1, 1), chrono::NaiveTime::from_num_seconds_from_midnight(0, 0)).timestamp_millis();

        source_info.instance_name = "Blue Marble".into();
    
        let mut layer = LayerInfo::new("bluemarble", GeoDataType::ColorTiles);
        layer.set_timesteps(timesteps.as_ref());
        layer.override_timerange(timesteps[0], tn);
        layer.set_long_name("Satellite Image Data");
        layer.set_max_zoom_level(max_zoom_level);
        source_info.add_layer(layer);

        let mut bms = BlueMarbleSource{
            files: Vec::new(),
            cache: Mutex::new(LruCache::new(1)),
            max_zoom_level,
            timesteps       
        };

        if imagepath.exists(){
            let mut s = String::new();
            File::open(imagepath).unwrap().read_to_string(&mut s).unwrap();
            let files = s.split('\n').map(|l| imagefolder.join(l.trim())).collect::<Vec<PathBuf>>();
            for f in files{
                if f.extension() == None{
                    continue;
                }
                if f.extension().unwrap() == "png"{
                    let mut rawfile = f.clone();
                    rawfile.set_extension("array2dcolor");
                    if ! rawfile.exists(){
                        BlueMarbleSource::png_to_raw(&f).map_err(|e| format!("Could not create raw file from png: {}", e))?;                
                    }
                    bms.files.push(rawfile);
                }else{
                    continue;
                }

            }
            if bms.files.is_empty(){
                return Err("Not enough images in folder".into());
            }
            while bms.files.len() < 12{
                bms.files.push(bms.files[0].clone());
            }
        }else{
            return Err("No listing in imagefolder".into());
        }

        Ok(Box::new(bms))
    }

    fn accepts_file(file: &std::path::Path) -> bool {
        file.is_dir()
    }
}

#[async_trait]
impl Source for BlueMarbleSource{
    /// Utilizes [`resample_subset_to`](../model/struct.Array2D.html#method.resample_subset_to) to get the correct area from the source array. 
    async fn sample(&self, query: &SampleQuery) -> Result<Option<GeoData>, Box<dyn Error + Send + Sync>>{
        let timestamp = self.timesteps.get(query.tpage as usize).ok_or_else(|| format!("No timestep with index {}", query.tpage))?;
        let m = (datetime_from_milliseconds(*timestamp).month() - 1) as u8;
        
        let incache = (self.cache.lock().map_err(|e| format!("{:?}" ,e))?.contains(&m)) as bool;
        if !incache{
            let array: Array2D<Color> = bincode::deserialize_from(BufReader::new(File::open(&self.files[m as usize])?))?;
            self.cache.lock().map_err(|e| format!("{:?}" ,e))?.put(m, array);
        }

        let array = self.cache.lock().map_err(|e| format!("{:?}" ,e))?.get(&m).unwrap().clone();

        //Test to see if tiles survive caching the month images
        //let ap = format!("./test/m_{}_tile_{}.png", m, query.tile.path);
        //BlueMarbleSource::raw_to_png(&array, &Path::new(&ap));

        let offset = Point2D::xy(query.tile.position.x * array.size.x, query.tile.position.y * array.size.y);
        let subset_size = Point2D::xy(query.tile.size.x * array.size.x, query.tile.size.y * array.size.y);
        let target_size = Point2D::xy(constants::TILESIZE.0 as f64, constants::TILESIZE.1 as f64);
        let data = array.resample_subset_to(offset, subset_size, target_size);

        //Test to see if tiles survive resampling
        //let rp = format!("./test/{}.png", query.tile.path);
        //BlueMarbleSource::raw_to_png(&data, &Path::new(&rp));

        Ok(Some(GeoData::ColorTiles(TileData::new_from_tile_and_array_2d(query.tile.clone(), data))))
    }
}