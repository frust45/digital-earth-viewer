//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use tileserver_model::*;
use tract_onnx::prelude::*;

pub struct OnnxSource{
    model: SimplePlan<TypedFact, Box<dyn TypedOp>, Graph<TypedFact, Box<dyn TypedOp>>>
}

#[async_trait]
impl SourceInit for OnnxSource{
    async fn initialize(settings: SourceConfig, mut source_info: SourceInfo) -> SourceInitResult {
        //Check we have files to parse
        if settings.input_files.is_empty(){
            return Err("No input file given".into());
        }

        debug!("loading model");

        let model = tract_onnx::onnx()
        // load the model
        .model_for_path(&settings.input_files[0])?
        // specify input type and shape
        .with_input_fact(0, InferenceFact::dt_shape(f32::datum_type(), tvec![512*512,2]))?
        //.with_outlet_fact(OutletId::new(39, 0), InferenceFact::dt_shape(f32::datum_type(), tvec![1]))?
        // optimize graph
        .into_optimized()?
        // make the model runnable and fix its inputs and outputs
        .into_runnable()?;

        debug!("loaded model {:?}", model);
    

        //Extract filename
        //let _filename = settings.input_files[0].file_stem().map(|f| f.to_string_lossy().to_string()).unwrap_or_else(||"Unidentified ODV file".to_string());
 

        source_info.instance_name = "OnnxSource".to_string();

        
        let mut onnx_layer = LayerInfo::new("Onnx", GeoDataType::ScalarTiles);
        onnx_layer.set_max_zoom_level(55);
        onnx_layer.set_extent(UECArea::new(UEC::xy(0.0,0.0), UEC::xy(1.0, 1.0)));
        source_info.add_layer(onnx_layer);



        Ok((source_info, Box::new(OnnxSource{
            model
        })))
    }

    fn accepts_file(_file: &std::path::Path) -> bool {
        true
    }
}

#[async_trait]
impl Source for OnnxSource{
    async fn sample(&self, query: &SampleQuery) -> Result<Option<GeoData>, Box<dyn std::error::Error + Send + Sync>> {
        match query.layername.as_ref(){
            "Onnx" => {
                // Input the generated data into the model

                let mut result_tile = TileData::new(query.tile.clone());

                let vals = result_tile.uecs_for_tile()
                .map(|u| Position::UEC(u).to_coord())
                .flat_map(|c| [c.lat as f32, c.lon as f32])
                .collect::<Vec<f32>>();

                let input = tract_ndarray::arr1(&vals).into_shape((vals.len() / 2, 2)).unwrap();

                let result = self.model.run(tvec!(input.into())).unwrap();
                debug!("Ran model");
                let to_show = result[0].to_array_view::<f32>().unwrap();
                let slice = to_show.as_slice().unwrap();
                result_tile.data.iter_mut().zip(slice.iter()).for_each(|(to, from)| {
                        *to = *from;
                });

                Ok(Some(GeoData::ScalarTiles(result_tile)))
            },
            _ => Err(format!("No layer with name {}", &query.layername).into())
        }
    }
}