use tileserver_model::*;

use std::{error::Error, fs::File, io::{BufRead, BufReader}};

use std::io::{Read};

use tileserver_model::async_trait;

///Source based around the base layers of [NASA's "Blue Marble"](https://visibleearth.nasa.gov/collection/1484/blue-marble) artwork and https://www.gebco.net/ bathymetry.
pub struct AsciiXYZSource{
}

impl SourceInit for AsciiXYZSource{
    fn initialize(settings: &SourceConfig, source_info: &mut SourceInfo) -> Result<Box<dyn Source>, Box<dyn Error + Send + Sync>>{
        if settings.input_files.is_empty(){
            return Err("No input file given".into());
        }

        let mut bfr = BufReader::new(File::open(&settings.input_files[0])?);

        for line in bfr.lines(){
            if let Ok(content)= line{
                //eprintln!("{}",content);
            }
            
        }

        let mut layer = LayerInfo::new("AreaData", GeoDataType::ScalarTiles);

        

        source_info.layers.push(layer);
        source_info.source_name= "AsciiXYZ".to_string();
        Ok(Box::new(AsciiXYZSource{}))
    }

    fn accepts_file(_file: &std::path::Path) -> bool {
        true //Accept any file, so we can join any terrain data onto this
    }
}

#[async_trait]
impl Source for AsciiXYZSource{
    /// Utilizes [`resample_subset_to`](../model/struct.Array2D.html#method.resample_subset_to) to get the correct area from the source array. 
    async fn sample(&self, query: &SampleQuery) -> Result<Option<GeoData>, Box<dyn Error + Send + Sync>>{
        unimplemented!();
        //TileData::new_from_coord_closure(query.tile
    }
}