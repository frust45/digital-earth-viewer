//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

mod geoimage_source;
pub use geoimage_source::GeoImageSource;