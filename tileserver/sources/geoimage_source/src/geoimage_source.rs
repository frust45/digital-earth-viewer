//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use tileserver_model::*;
use tileserver_model::async_trait;
use image::*;
use std::path::{Path, PathBuf};
use std::io::{BufRead, BufReader};
use std::error::Error;
use std::fs::File;

#[derive(Copy, Clone)]
enum CoordMode{
    LatLon,
    UTM{zone: u8, letter: char}
}

enum ImageMode{
    Static(RgbaImage),
    Dynamic(PathBuf)
}

pub struct GeoImageSource{
    image: ImageMode,
    worldfile: Worldfile,
    extent: UECArea,
    mode: CoordMode,
    extent_points: [GeoPoint<Scalar>;4]
}



#[derive(Debug)]
struct Worldfile{
    pub affine_matrix: Mat2,
    pub affine_matrix_inverse: Mat2,
    pub offset_vector: Vec2
}

impl Worldfile{
    pub fn try_from_file(file: &Path) -> Result<Worldfile, Box<dyn Error + Send + Sync>>{
        //Read 6 lines from file
        let lines: Vec<String> = BufReader::new(File::open(file)?) //Open file
            .lines() //read line-wise
            .filter_map(|line| line.ok()) //filter out lines we couldn't read
            .take(6) //read at most 6
            .collect(); //into a vector
        //Check we got enough lines
        if lines.len() < 6{
            return Err("World file does not contain enough lines".into());
        }
        //Parse lines into worldfile
        //Matrix first
        let affine_matrix: Mat2 = Mat2::new(
            lines[0].parse()?,
            lines[2].parse()?,
            lines[1].parse()?,
            lines[3].parse()?
        );
        //Rest next
        Ok( Worldfile{
            affine_matrix_inverse: affine_matrix.inverse().ok_or_else(|| "Could not invert transformation matrix.")?,
            affine_matrix,
            offset_vector: Vec2::new(
                lines[4].parse()?, //b1
                lines[5].parse()?  //b2
            )
        })
    }

    pub fn pixel_to_coord(&self, pixel: (u32, u32), mode: CoordMode) -> Coord{
        let pixel_vec = Vec2::new(pixel.0 as f64, pixel.1 as f64);
        match mode{
            CoordMode::LatLon => {
                let coords_vec = self.affine_matrix * pixel_vec + self.offset_vector;
                Coord::new(coords_vec.x2, coords_vec.x1)
            },
            CoordMode::UTM{zone, letter} =>{
                let coords_vec = self.affine_matrix * pixel_vec + self.offset_vector;
                let utm = UTM::new(coords_vec.x1, coords_vec.x2,zone,letter);
                utm.into()
            }
        }
    }

    pub fn coord_to_pixel(&self, coord: Coord, mode: CoordMode) -> (u32, u32){
        let coords_vec = match mode{
            CoordMode::LatLon => {
                Vec2::new(coord.lon, coord.lat)
            },
            CoordMode::UTM{zone, letter} => {
                let utm: UTM = UTM::from_coord_and_zone_info(coord, zone, letter);
                assert_eq!(utm.zone_number, zone);
                assert_eq!(utm.zone_letter, letter);
                Vec2::new(utm.easting, utm.northing)
            }
        };
        let vec_without_offset = coords_vec - self.offset_vector;
        let pixel_vec = self.affine_matrix_inverse * vec_without_offset;
        (pixel_vec.x1.round() as u32, pixel_vec.x2.round() as u32)
    }
}

impl GeoImageSource{
    fn find_worldfile(imagepath: &Path, worldfileext: Option<&str>) -> Result<Worldfile, Box<dyn Error + Send + Sync>>{
        //Extend / Modify this if you encounter weird world files
        const EXTENSIONSLIST: [&str; 18] = ["jgw", "jpw", "jpgw", "jpegw", "j2w", "pgw", "pnw", "pngw", "gfw", "giw", "gifw", "tfw", "tiw", "tiffw", "bpw", "bmw", "bpmw", "wld"];
        
        let mut worldfilepath = PathBuf::from(imagepath);

        let mut extensions = Vec::new();

        if let Some(ext) = worldfileext{
            extensions.push(ext);
        }
        extensions.extend_from_slice(&EXTENSIONSLIST);

        let mut errors: Vec<Box<dyn Error + Send + Sync>> = Vec::new();

        for ext in extensions.iter(){
            worldfilepath.set_extension(ext);
            if worldfilepath.exists(){
                match Worldfile::try_from_file(&worldfilepath) {
                    Ok(worldfile) => return Ok(worldfile),
                    Err(e) => errors.push(e)
                }
            }
        }

        return Err(
            format!("Tried file extensions {:?}, encountered errors: {:?}", extensions, errors).into()   
        );
    }
}

#[async_trait]
impl SourceInit for GeoImageSource{
    async fn initialize(settings: SourceConfig, mut source_info: SourceInfo) -> SourceInitResult {
        //Check that we actually have an image
        if settings.input_files.is_empty() {
            return Err("No image file given".into());
        }
        //Gather the filename
        let filename = settings.input_files[0].file_name().map(|f| f.to_string_lossy().to_string()).unwrap_or_else(|| "Image File".to_string());
        //Load the image, we need at least width and height
        let imagedims ;
        let image_mode;
        image_mode = if settings.settings.get("image_mode").map(|s| s.as_str()).unwrap_or("static") == "dynamic" {
            imagedims = image::image_dimensions(&settings.input_files[0]).map_err(|e| format!("Error reading image dimensions for file {:?}: {:?}", &settings.input_files[0], e))?;
            info!("Image mode: dynamic");
            ImageMode::Dynamic(settings.input_files[0].clone())
        } else {
            info!("Image mode: static");
            let image = image::open(&settings.input_files[0]).map_err(|e| format!("Error reading image file {:?}: {:?}", settings.input_files[0],e))?.to_rgba8();
            imagedims = (image.width(), image.height());
            ImageMode::Static(image)
        };

        //Load the worldfile
        let worldfile = GeoImageSource::find_worldfile(&settings.input_files[0], settings.settings.get("worldfileext").as_ref().map(|s| s.as_ref())).map_err(|e| format!("Error finding world file: {:?}",e))?;

        //Check mode
        let mode = match settings.settings.get("coordinate_mode").map(|s| s.as_str()).unwrap_or("latlon"){
            "latlon" => Ok(CoordMode::LatLon),
            "utm" => {
                let zone = settings.settings.get("utm_zone").cloned().unwrap_or_else(|| String::from("32 N"));
                let zoneparts: Vec<&str> = zone.split(' ').map(|p| p.trim()).collect();
                let zone_number: u8 = zoneparts.get(0).map(|p| p.parse().ok()).flatten().ok_or_else(|| format!("Could not parse utm zone from input '{}'", zone))?;
                let zone_letter = zoneparts.get(1).map(|p| p.chars().next()).flatten().ok_or_else(|| format!("Could not parse utm zone letter from input '{}'", zone))?;
                Ok(CoordMode::UTM{
                    zone: zone_number,
                    letter: zone_letter
                })
            }
            unknown => Err(format!("Unknown coordinate mode: {}", unknown))
        }?;

        //Calculate bounds
        let area = UECArea::area_from_points_iter(
            [0, imagedims.0]
                .iter()
                .zip([0, imagedims.1].iter())
                .map(|(&x,&y)| {
                    worldfile.pixel_to_coord((x,y), mode).into()
                })
        ).ok_or("No extent computable from worldfile!")?;

        let points: [GeoPoint<Scalar>;4] = [
            GeoPoint::new_from_uec(area.position, 0.0, 0, 1.0),
            GeoPoint::new_from_uec(area.position + UEC::xy(area.extent.x, 0.0), 0.0, 0, 2.0),
            GeoPoint::new_from_uec(area.position + area.extent, 0.0, 0, 3.0),
            GeoPoint::new_from_uec(area.position + UEC::xy(0.0, area.extent.y), 0.0, 0, 4.0)
        ];

        //Calculate maximum zoom level
        //TODO rework this to be more accurate
        let area_width = UEC::haversine_distance(&area.position, area.position + area.extent);
        let image_width = ((imagedims.0 as f64).powi(2) + (imagedims.1 as f64).powi(2)).sqrt();
        let pixel_size_in_meters = area_width / image_width;
        let mut max_zoom_level = 1;
        while Tile::pixel_size_in_meters_from_path_length(max_zoom_level) > pixel_size_in_meters{
            max_zoom_level += 1;
        }

        let image_layer_name = settings.settings.get("layer_name").unwrap_or(&filename);
        let mut imagelayer = LayerInfo::new(image_layer_name,GeoDataType::ColorTiles);
        imagelayer.set_extent(area);
        imagelayer.set_max_zoom_level(max_zoom_level);
        source_info.add_layer(imagelayer);
        source_info.instance_name = filename.clone();

        let mut extentlayer = LayerInfo::new("Extent", GeoDataType::ScalarPoints);
        extentlayer.set_extent(area);
        extentlayer.set_datarange(0.0, 4.0);
        source_info.add_layer(extentlayer);

        Ok((source_info,
            Box::new(GeoImageSource{
            worldfile,
            image: image_mode,
            extent: area,
            mode, 
            extent_points: points
        })))
    }

    fn accepts_file(file: &std::path::Path) -> bool {
        const EXTENSIONSLIST: [&str; 7] = ["jpg", "jpeg", "png", "gif", "tif", "tiff", "bmp"];
        EXTENSIONSLIST.iter().any(|ext| 
            file.extension().map(|e| e.to_string_lossy().ends_with(ext)).unwrap_or(false) 
        )
    }
}

#[async_trait]
impl Source for GeoImageSource{
    async fn sample(&self, query: &SampleQuery) -> SourceResult {
        if query.layername == "Extent"{
            Ok(
                Some(
                    GeoData::ScalarPoints(
                        PointsData::new(self.extent_points.to_vec())
                    )
                )
            )
        }else if UECArea::areas_overlap(self.extent, query.tile.to_uec_area()){
            let dynimage = if let ImageMode::Dynamic(path) = &self.image{
                Some(image::open(&path)
                    .map_err(|e| format!("Error reading image file {:?}: {:?}", path,e))?
                    .to_rgba8())
            }else{
                None
            };
            let image = if let ImageMode::Static(image) = &self.image{
                &image
            }else{
                dynimage.as_ref().unwrap()
            };
            let tiledata = TileData::new_from_coord_closure(query.tile.clone(), &|coord: Coord| {
                let (x,y) = self.worldfile.coord_to_pixel(coord, self.mode);
                if x > 0 && x < image.width() && y > 0 && y < image.height(){
                    image.get_pixel(x, y).0.into()
                }else{
                    Color::default()
                }
                });
            if tiledata.is_empty() {
                Ok(None)
            } else {
                Ok(Some(GeoData::ColorTiles(tiledata)))
            }
        }else{
            Ok(None)
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_pixel_coords_sym() {
        let offset_vector = Vec2::xy(-16.0, 28.0);
        let affine_matrix = Mat2{
            a11: 0.0002777778,
            a12: 0.0,
            a21: 0.0,
            a22: -0.0002777778
        };
        let affine_matrix_inverse = affine_matrix.invert();
        let w = Worldfile{
            affine_matrix,
            affine_matrix_inverse,
            offset_vector
        };

        for y in 0 .. 512{
            for x in 0 .. 512{
                assert_eq!(w.coord_to_pixel(w.pixel_to_coord((x,y), CoordMode::LatLon),CoordMode::LatLon), (x,y));
            }
        }
    }

    #[test]
    fn test_pixel_utm_sym() {
        let offset_vector = Vec2::xy(250000.1000100000, 5887999.9000000004);
        let affine_matrix = Mat2{
            a11: 0.2,
            a12: 0.0,
            a21: 0.0,
            a22: -0.2
        };
        let affine_matrix_inverse = affine_matrix.invert();
        let w = Worldfile{
            affine_matrix,
            affine_matrix_inverse,
            offset_vector
        };

        let utm_mode = CoordMode::UTM{
            letter: 'U',
            zone: 32
        };

        for y in 0 .. 512{
            for x in 0 .. 512{
                assert_eq!(w.coord_to_pixel(w.pixel_to_coord((x,y), utm_mode),utm_mode), (x,y));
            }
        }
    }
}