//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Vec2{
    pub x: f64,
    pub y: f64
}

impl Vec2{
    pub fn xy(x: f64, y: f64) -> Vec2{
        Vec2{
            x,
            y
        }
    }

    #[allow(unused)]
    pub fn length(&self) -> f64{
        (self.x.powi(2) + self.y.powi(2)).sqrt()
    }
}

impl std::ops::Add for Vec2{
    type Output = Vec2;

    fn add(self, rhs: Self) -> Self::Output {
        Vec2{
            x: self.x + rhs.x,
            y: self.y + rhs.y
        }
    }
}

impl std::ops::Sub for Vec2{
    type Output = Vec2;

    fn sub(self, rhs: Self) -> Self::Output {
        Vec2{
            x: self.x - rhs.x,
            y: self.y - rhs.y
        }
    }
}

impl std::ops::Mul<f64> for Vec2{
    type Output = Vec2;

    fn mul(self, rhs: f64) -> Self::Output {
        Vec2{
            x: self.x * rhs,
            y: self.y * rhs
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub struct Mat2{
    pub a11: f64,
    pub a12: f64, // ⎡a11 a12⎤
    pub a21: f64, // ⎣a21 a22⎦
    pub a22: f64
}

impl Mat2{
    pub fn invert(&self) -> Mat2{
        let det = (self.a11 * self.a22) - (self.a12 - self.a21);
        let det_1 = 1.0 / det;
        Mat2{
            a11: det_1 * self.a22,
            a12: det_1 * -1.0 * self.a12,
            a21: det_1 * -1.0 * self.a21,
            a22: det_1 * self.a11
        }
    }

    pub fn mat_mul_vec(&self, vec: Vec2) -> Vec2{
        Vec2{
           x: (self.a11 * vec.x) + (self.a12 * vec.y),
           y: (self.a21 * vec.x) + (self.a22 * vec.y)
        }
    }
}