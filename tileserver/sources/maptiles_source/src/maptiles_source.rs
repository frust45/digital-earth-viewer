//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use tileserver_model::*;

use std::error::Error;

use image::RgbaImage;

use std::io::Cursor;

use tileserver_model::async_trait;

use tileserver_model::tokio::sync::Mutex;

use lru::LruCache;

use futures::future::try_join_all;

/// Source based around open street map tile servers
/// No time information
pub struct MapTilesSource{
    ///The base tileserver url for the OSM instance
    pub base_url: String,
    ///The pattern in the base url that will be replaced with the x value of the tile
    pub x_pattern: String,
    ///The pattern in the base url that will be replaced with the y value of the tile
    pub y_pattern: String,
    ///The pattern in the base url that will be replaced with the zoom level of the tile
    pub z_pattern: String,
    ///The instance name of the server delivering the tiles. Just for cosmetic purposes at the moment
    pub instance_name: String,
    /// Maximum zoom level, defaults to 18
    pub max_zoom_level: u8,
    ///HTTP client so we only use one outgoing connection (could be optimized to i.e. use multiple connections)
    client: reqwest::Client,
    ///Cache for tiles so we don't request them quite as often
    cache: Mutex<LruCache<String, Vec<u8>>>,
}

#[test]
fn test_tile_algo(){
    eprintln!("{:?}", MapTilesSource::tile_to_webmercator_xyz(&Tile::from_tilepath("WADADADDB").unwrap()));
}

impl MapTilesSource{
    //https://wiki.openstreetmap.org/wiki/Slippy_map_tilenameshttps://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
    fn uec_zoom_to_webmercator_xy(c: UEC, zoom: i32) -> (f64, f64){
        let n = 2f64.powi(zoom);
        let x = n * c.x;
        let y_rad = std::f64::consts::PI * (0.5 - c.y);
        let y = (1f64 - ((y_rad.tan() + 1f64 / y_rad.cos()).ln() / std::f64::consts::PI)) * n / 2f64;
        (x, y)
    }

    fn tile_to_webmercator_bounds(tile: &Tile) -> ((usize, usize), (usize, usize), usize) {
        let zoom = tile.path.len();
        //Enable for higher res but worse legibility.
        //let zoom = tile.path.len() + 1;
        let (xmin, ymin) = MapTilesSource::uec_zoom_to_webmercator_xy(tile.corner(true, true), zoom as i32);
        let (xmax, ymax) = MapTilesSource::uec_zoom_to_webmercator_xy(tile.corner(false, false), zoom as i32);
        let webmercator_tile_limit = 2f64.powi(zoom as i32);
        let bounded_xmin = xmin.floor().min(webmercator_tile_limit).max(0.0) as usize;
        let bounded_ymin = ymin.floor().min(webmercator_tile_limit).max(0.0) as usize;
        let bounded_xmax = xmax.ceil().min(webmercator_tile_limit).max(0.0) as usize;
        let bounded_ymax = ymax.ceil().min(webmercator_tile_limit).max(0.0) as usize;
        debug!("Path: {}, needs {} webmercator tiles to fill.", tile.path, (bounded_xmax - bounded_xmin) * (bounded_ymax - bounded_ymin));
        ((bounded_xmin, bounded_xmax), (bounded_ymin, bounded_ymax), zoom)
    }

    async fn request_map_tile_png(&self, req_url: String) -> Result<RgbaImage, Box<dyn Error + Send + Sync>>{
        //Lock the cache, both for throttling, as well as for accessing it
        let mut cache = self.cache.lock().await;
        //Get the bytes either from the cache or the online data source
        let bytes: &[u8] = 
            if let Some(cache_bytes) = cache.get(&req_url){
                cache_bytes
            }else{
                let response = self.client.get(&req_url).send().await?;
                if response.status().is_success(){
                    let response_bytes: Vec<u8> = response.bytes().await?.iter().copied().collect();
                    cache.put(req_url.clone(),response_bytes);
                    cache.get(&req_url).unwrap() //this seems safe because we just inserted the value
                }else{
                    let emsg = format!("HTTP request to {} failed with status {}: {:?}", req_url, response.status().to_string(), response);
                    warn!("{}", emsg);
                    return Err(emsg.into());
                }
            };

        //Read http response into buffer
        //Create read + bufread + seek
        let mut cursor = Cursor::new(bytes);
        let image_reader = image::io::Reader::new(&mut cursor).with_guessed_format()?;    
        //Actually read image
        let image = image_reader.decode()?;
        let rgba = image.to_rgba8();

        Ok(rgba)
    }

    async fn get_tile_as_color_vec(&self, x: usize, y: usize, z: usize)->Result<(Vec<Color>, (usize, usize)), Box<dyn Error + Send + Sync>>{
        let base_url = &self.base_url;
        //Create request url
        let req_url = base_url.replace(&self.x_pattern, &x.to_string()).replace(&self.y_pattern, &y.to_string()).replace(&self.z_pattern, &z.to_string());
        //Request png image
        let rgba = self.request_map_tile_png(req_url).await?;
        //Load image into 2D array
        let mut array: Vec<Color> = vec![Color::nodata(); rgba.width() as usize * rgba.height() as usize];
        array.iter_mut().enumerate().for_each(|(i,v)|{
            let x = i as u32 % rgba.width() as u32;
            let y = i as u32 / rgba.width() as u32;
            *v = rgba[(x,y)].0.into();
        });
        return Ok((array, (rgba.width() as usize, rgba.height() as usize)))
    }
}

#[async_trait]
impl SourceInit for MapTilesSource{
    fn accepts_file(_file: &std::path::Path) -> bool {
        false //We don't accept files here
    }

    async fn initialize(settings: SourceConfig, mut source_info: SourceInfo) -> SourceInitResult{
        let mut base_url = "http://b.tile.openstreetmap.org/${z}/${x}/${y}.png".to_string();
        let mut instance_name = "tile.openstreetmap.org".to_string();
        let mut x_pattern = "${x}".to_string();
        let mut y_pattern = "${y}".to_string();
        let mut z_pattern = "${z}".to_string();
        let mut max_zoom_level = 18u8;
        let mut max_tiles_cached: u32 = 1000; //Tiles are on the order of 30kb, so I think we can safely save a thousand of them without really even using much RAM
        
        if let Some(settings_base_url) = settings.settings.get("base_url"){
            base_url = settings_base_url.to_string();
        }

        if let Some(settings_x_pattern) = settings.settings.get("x_pattern"){
            x_pattern = settings_x_pattern.to_string();
        }

        if let Some(settings_y_pattern) = settings.settings.get("y_pattern"){
            y_pattern = settings_y_pattern.to_string();
        }

        if let Some(settings_z_pattern) = settings.settings.get("z_pattern"){
            z_pattern = settings_z_pattern.to_string();
        }

        if let Some(settings_instance_name) = settings.settings.get("instance_name"){
            instance_name = settings_instance_name.to_string();
        }

        if let Some(Ok(settings_max_zoom_level)) = settings.settings.get("max_zoom_level").map(|s| s.parse::<u8>()){
            max_zoom_level = settings_max_zoom_level;
        }

        if let Some(Ok(settings_max_tiles_cached)) = settings.settings.get("max_tiles_cached").map(|s| s.parse::<u32>()){
            max_tiles_cached = settings_max_tiles_cached;
        }

        let client = reqwest::ClientBuilder::new().user_agent(constants::TILESERVER_USER_AGENT).build().unwrap();


        source_info.instance_name = instance_name.clone();

        let mut layer = LayerInfo::new(&instance_name,GeoDataType::ColorTiles);
        layer.set_long_name("Web Map Tile Layer");
        layer.set_max_zoom_level(max_zoom_level);
        source_info.add_layer(layer);

        let maptilessource = MapTilesSource{
            base_url,
            x_pattern,
            y_pattern,
            z_pattern,
            max_zoom_level,
            instance_name,
            client,
            cache: Mutex::new(LruCache::new(max_tiles_cached as usize))
        };

        Ok((source_info,
            Box::new(maptilessource)))
    }
}

#[async_trait]
impl Source for MapTilesSource{
    async fn sample(&self, query: &SampleQuery) -> Result<Option<GeoData>, Box<dyn Error + Send + Sync>>{
        let ((xmin, xmax), (ymin, ymax), zoom) = MapTilesSource::tile_to_webmercator_bounds(&query.tile);
        let arrays: Vec<(Vec<Color>, (usize, usize))> = try_join_all(
            (ymin..ymax).into_iter()
                .map(|y| (xmin..xmax).into_iter().map(move |x| (x, y, zoom)))
                .flatten()
                .map(|(x, y, zoom)| self.get_tile_as_color_vec(x,y,zoom))
            ).await?;
        if arrays.is_empty(){
            return Ok(None);
        }
        let ref_array: Vec<&[Color]>= arrays.iter().map(|(v,_)| v.as_slice()).collect();
        let tiled = 
            sampling::SliceTiles::new(
                ref_array, 
                (arrays[0].1.0, arrays[0].1.1),
                (xmax - xmin, ymax - ymin)
            ).ok_or_else(|| format!("inconsistent tile sizes!"))?;
        let xoffs = xmin as f64;
        let yoffs = ymin as f64;
        let xscale = arrays[0].1.0 as f64;
        let yscale = arrays[0].1.1 as f64;

        let tiledata = TileData::new_from_uec_closure(query.tile.clone(), |c| {
            let wm_coord = Self::uec_zoom_to_webmercator_xy(c, zoom as i32);
            sampling::linear_from_slice(&tiled, (
               (wm_coord.0 - xoffs) * xscale,
               (wm_coord.1 - yoffs) * yscale
            ), tiled.size(), 1, 0)
        });
        /*
        let mut result_array: Array2D<Color> = Array2D::new(Point2D::xy(constants::TILESIZE.0 as f64, constants::TILESIZE.1 as f64));

        result_array.data.iter_mut().enumerate().for_each(|(i,value)| {
            let x_data_frac_ul = (i % constants::TILESIZE.0) as f64 / constants::TILESIZE.0 as f64;
            let y_data_frac_ul = (i / constants::TILESIZE.0) as f64 / constants::TILESIZE.1 as f64;
            let x_data_frac_br = ((i % constants::TILESIZE.0) + 1) as f64 / constants::TILESIZE.0 as f64;
            let y_data_frac_br = ((i / constants::TILESIZE.0) + 1) as f64 / constants::TILESIZE.1 as f64;
            let x_uec_ul = query.tile.position.x + (query.tile.size.x * x_data_frac_ul);
            let y_uec_ul = query.tile.position.y + (query.tile.size.y * y_data_frac_ul);
            let x_uec_br = query.tile.position.x + (query.tile.size.x * x_data_frac_br);
            let y_uec_br = query.tile.position.y + (query.tile.size.y * y_data_frac_br);
            let webmercator_pos_ul = MapTilesSource::uec_zoom_to_webmercator_xy(UEC::xy(x_uec_ul, y_uec_ul), zoom as i32);
            let _webmercator_pos_br = MapTilesSource::uec_zoom_to_webmercator_xy(UEC::xy(x_uec_br, y_uec_br), zoom as i32);

            let sample_i = (((webmercator_pos_ul.0.floor() as usize) - xmin) + ((webmercator_pos_ul.1.floor() as usize) - ymin) * (xmax - xmin)).min(arrays.len() - 1);
            let sample_x_frac = webmercator_pos_ul.0.fract();
            let sample_y_frac = webmercator_pos_ul.1.fract();

            //trace!("Sample_X_FRAC: {}, SAMPLE_Y_FRAC: {}", sample_x_frac, sample_y_frac);

            let sample_x_index = (sample_x_frac * arrays[sample_i].1.0 as f64) as usize;
            let sample_y_index = (sample_y_frac * arrays[sample_i].1.1 as f64) as usize;

            let sample_index = (sample_y_index * arrays[sample_i].1.0 as usize) + sample_x_index;
            *value = arrays[sample_i].0[sample_index.min(arrays[sample_i].0.len() - 1)];
        });
        */
        if tiledata.is_empty() {
            Ok(None)
        } else {
            Ok(Some(GeoData::ColorTiles(tiledata)))
        }
    }
}