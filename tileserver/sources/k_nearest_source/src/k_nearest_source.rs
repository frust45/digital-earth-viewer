//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use tileserver_model::*;
use rstar::{RTree, primitives::PointWithData};
pub struct KNearestSource{
    layer_info: LayerInfo,
    instance_name: String,
    layer_name: String,
    k_points: usize,
    z: f32,
    zrange: f32
}

#[async_trait]
impl SourceInit for KNearestSource{
    async fn initialize(settings: SourceConfig, mut source_info: SourceInfo) -> SourceInitResult {
        let instance_name = settings.settings.get("instance").map(|s| s.as_str()).ok_or_else(|| "No instance given for derivative source")?;
        let layer_name = settings.settings.get("layer").map(|s| s.as_str()).ok_or_else(|| "No layer given for derivative source")?;
        let k_points: usize = settings.settings.get("k").map(|s| s.parse()).ok_or_else(|| "No k given!")??;
        let z = settings.settings.get("z").map(|s| s.parse()).transpose()?.unwrap_or(0f32);
        let zrange = settings.settings.get("zrange").map(|s| s.parse()).transpose()?.unwrap_or(10f32);
        
        //Get Source Info
        let r_source_info = DATAPROVIDER.get().ok_or_else(|| "Could not access dataprovider")?.wait_for_source_info(instance_name.to_string()).await?;

        //Find layerinfo
        let layer_info = r_source_info.layers.iter().find(|l| l.name == layer_name)
            .ok_or_else(|| format!("Source {} does not contain layer {} (options are {})", instance_name, layer_name, r_source_info.layers.iter().map(|l| l.name.clone()).collect::<Vec<_>>().join(", ")))?;

        let mut knearest_layer = LayerInfo::new(&format!("{} ({}-nearest)", layer_name, k_points), GeoDataType::ScalarTiles);
        knearest_layer.max_zoom_level = 20; //Just a guess, we need a good metric for this.

        //if let Some(extent) = layer_info.extent{
        //    knearest_layer.set_extent(extent);
        //}

        if let Some(timesteps) = &layer_info.timesteps{
            knearest_layer.set_timesteps(&timesteps);
        }

        if let Some((min, max)) = layer_info.datarange{
            knearest_layer.set_datarange(min, max);
        }

        if let Some(unit) = &layer_info.unit{
            knearest_layer.set_unit(&unit);
        }

        source_info.add_layer(knearest_layer.clone());

        Ok((source_info.clone(), Box::new(KNearestSource{
            layer_info: knearest_layer.clone(),
            instance_name: instance_name.to_string(),
            layer_name: layer_name.to_string(),
            k_points,
            z,
            zrange
        })))
    }

    fn accepts_file(_file: &std::path::Path) -> bool {
        false
    }
}

type TreeGeoPoint = PointWithData<GeoPoint<Scalar>, [f64; 3]>;

fn uec_2_geocentric(u: UEC) -> [f64;3] {
    let phi = u.y * std::f64::consts::PI;
    let lambda = u.x * std::f64::consts::TAU;
    let z = phi.cos();
    let r = phi.sin();
    let x = lambda.cos() * r;
    let y = lambda.sin() * r;
    [x, y, z]
}

#[async_trait]
impl Source for KNearestSource{
    async fn sample(&self, query: &SampleQuery) -> SourceResult {
        if query.layername == self.layer_info.name{
            if (query.tpage as usize) < self.layer_info.timesteps.as_ref().map(|tsteps| tsteps.len()).unwrap_or(0usize){
                //Request data
                let provider = DATAPROVIDER.get().ok_or_else(|| "Could not access dataprovider")?;
                let tile = query.tile.clone();
                let geodata = provider
                    .get_geodata(
                        SampleQuery::new(
                            self.instance_name.clone(), 
                            self.layer_name.clone(), 
                            tile,
                            query.tpage,
                            query.zpage
                        ),
                        GeoDataType::ScalarPoints).await?;
                
                if let Some(GeoData::ScalarPoints(points)) = geodata{
                    let rtree = RTree::bulk_load(points.points.into_iter().filter_map(|p| {
                        if p.height < self.z + self.zrange && p.height > self.z - self.zrange{
                            let uec = p.pos.to_uec();
                            Some(TreeGeoPoint::new(p, uec_2_geocentric(uec)))
                        }else{
                            None
                        }
                    }).collect());

                    let tiledata: TileData<Scalar> = TileData::new_from_uec_closure(query.tile.clone(), &|uec: UEC| {
                        let k_nearest = rtree.nearest_neighbor_iter(&uec_2_geocentric(uec)).filter(|p| !p.data.value.is_nodata()).take(self.k_points).collect::<Vec<_>>();

                        //Maybe weight by distance in the future
                        if k_nearest.len() > 0 {
                            k_nearest.iter().map(|p| p.data.value as f32).sum::<f32>() / k_nearest.len() as f32
                        } else {
                            f32::NAN
                        }
                    });

                    Ok(
                        Some(
                            GeoData::ScalarTiles(
                                tiledata
                            )
                        )
                    )

                }else{
                    Ok(None)
                }
            }else{
                Err(format!("Tpage {} requested but only {} tpages in source", query.tpage, self.layer_info.timesteps.as_ref().map(|tsteps| tsteps.len()).unwrap_or(0usize)).into())
            }
        }else{
            Err(format!("Layer {} not found in source", query.layername).into())
        }
    }
}