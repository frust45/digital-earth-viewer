//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use tileserver_model::*;

use std::error::Error;

use std::io::{Read};

use tileserver_model::async_trait;

///Source based around the base layers of [NASA's "Blue Marble"](https://visibleearth.nasa.gov/collection/1484/blue-marble) artwork and https://www.gebco.net/ bathymetry.
pub struct BasicEarthSource{
}

#[async_trait]
impl SourceInit for BasicEarthSource{
    async fn initialize(_settings: SourceConfig, mut _source_info: SourceInfo) -> SourceInitResult{
        let mut source_info = SourceInfo::new("Basic Earth Source");
        source_info.set_uncacheable();

        let mut bluemarble = LayerInfo::new("BlueMarble", GeoDataType::ColorTiles);
        bluemarble.set_long_name("Satellite Image Data");
        bluemarble.set_max_zoom_level(1);
        bluemarble.set_extent(UECArea::new(UEC::xy(0.0,0.0), UEC::xy(1.0,1.0)));
        source_info.add_layer(bluemarble);

        let mut gebco = LayerInfo::new("GEBCO", GeoDataType::ScalarTiles);
        gebco.set_datarange( -10497.0, 6_827.941_4);
        gebco.set_long_name("GEBCO Bathymetry");
        gebco.set_unit("m");
        gebco.set_max_zoom_level(1);
        gebco.set_extent(UECArea::new(UEC::xy(0.0,0.0), UEC::xy(1.0,1.0)));
        source_info.add_layer(gebco);

        source_info.set_attribution("GEBCO based on data from GEBCO Compilation Group (2020) [GEBCO 2020 Grid](https://www.gebco.net/data_and_products/gridded_bathymetry_data/) (doi:10.5285/a29c5465-b138-234d-e053-6c86abc040b9)
        BlueMarble based on data from [NASA's \"Blue Marble\"](https://visibleearth.nasa.gov/collection/1484/blue-marble) artwork");

        Ok((source_info, Box::new(BasicEarthSource{})))
    }

    fn accepts_file(_file: &std::path::Path) -> bool {
        true //Accept any file, so we can join any terrain data onto this
    }
}

#[async_trait]
impl Source for BasicEarthSource{
    /// Utilizes [`resample_subset_to`](../model/struct.Array2D.html#method.resample_subset_to) to get the correct area from the source array. 
    async fn sample(&self, query: &SampleQuery) -> Result<Option<GeoData>, Box<dyn Error + Send + Sync>>{
        let compressed = match &query.layername[..]{
            "BlueMarble" => {
                match &query.tile.path[..]{
                    "E" => include_bytes!("../data/Bluemarble_E.tile.gz").to_vec(),
                    "W" => include_bytes!("../data/Bluemarble_W.tile.gz").to_vec(),
                    _ => return Err(String::from("BasicEarthSource only supports zoom level 1").into())
                }
            },
            "GEBCO" => {
                match &query.tile.path[..]{
                    "E" => include_bytes!("../data/GEBCO_E.tile.gz").to_vec(),
                    "W" => include_bytes!("../data/GEBCO_W.tile.gz").to_vec(),
                    _ => return Err(String::from("BasicEarthSource only supports zoom level 1").into())
                }
            },
            _ => return Err(format!("Layer unknown: {}", &query.layername).into())
        };

        let mut decoder = flate2::read::GzDecoder::new(&compressed[..]);

        let mut decoded = Vec::with_capacity(512 * 512 *4);

        decoder.read_to_end(&mut decoded)?;

        debug!("Compressed {} decompressed {}", compressed.len(), decoded.len());

        if &query.layername == "BlueMarble"{
            let mut data: Vec<Color> = Vec::with_capacity(512 * 512);
            for i in 0 .. (512 * 512){
                let d = i * 4;
                data.push(Color::from_rgba(decoded[d], decoded[d + 1], decoded[d + 2], decoded[d + 3]));
            }
            return Ok(Some(GeoData::ColorTiles(TileData::new_from_tile_and_raw_data(query.tile.clone(), data))));
        }else{
            let mut data: Vec<Scalar> = Vec::with_capacity(512 * 512);
            for i in 0 .. (512 * 512){
                let d = i * 4;
                data.push(f32::from_le_bytes([decoded[d], decoded[d + 1], decoded[d + 2], decoded[d + 3]]));
            }
            return Ok(Some(GeoData::ScalarTiles(TileData::new_from_tile_and_raw_data(query.tile.clone(), data))));
        }
    }
}