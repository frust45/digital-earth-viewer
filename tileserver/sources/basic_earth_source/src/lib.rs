//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

mod basic_earth_source;

pub use basic_earth_source::BasicEarthSource;