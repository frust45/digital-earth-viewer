//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use std::process::{Command, Stdio};
use std::fs;
use std::path::{Path, PathBuf};
use std::env;
use std::fmt::Debug;

fn files_recursive<P: AsRef<Path> + Debug>(dir: P) -> Vec<PathBuf> {
    fs::read_dir(&dir).expect(&format!("cargo:warning=Could not build tree from {:?}.", dir))
        .flat_map(|f| {
            let e = f.unwrap();
            let md = e.metadata().unwrap();
            if md.is_file() {
                vec![e.path()].into_iter()
            } else if md.is_dir() {
                files_recursive(e.path()).into_iter()
            } else {
                Vec::new().into_iter()
            }
        }).collect()
}

fn gen_tileclient_include(dir: &Path) -> String {
    let mut r: String = String::new();
    r += "pub fn bundled_file(p: &str) -> Option<&'static [u8]> {\n";
    r += "\tmatch p {\n";
    let files = files_recursive(dir);
    if files.is_empty() {panic!("No tileclient files found in `{:?}`.", dir);}
    files.iter().for_each(|p| {  
        r += &format!(
            "\t\t\"{}\" => Some(include_bytes!(\"{}\")),\n", 
            p.strip_prefix(&dir).unwrap().to_str().unwrap().replace("\\", "/"),
            p.to_str().unwrap().replace("\\", "\\\\")
        );
    });
    r += "\t\t_ => None,\n\t}\n}\n";
    r
}

fn print_str_cargo_warn(s: &str) {
    s.split("\n").for_each(|c| {
        println!("cargo:warning={}", c);
    })
}

fn print_cargo_change_recursive<P: AsRef<Path>>(dir: P) {
    println!("cargo:rerun-if-changed={}", dir.as_ref().to_string_lossy());
    fs::read_dir(&dir).unwrap()
        .for_each(|f| {
            let e = f.unwrap();
            let md = e.metadata().unwrap();
            if md.is_file() {
                println!("cargo:rerun-id-changed={}", e.path().to_string_lossy());
            } else if md.is_dir() {
                print_cargo_change_recursive(e.path());
            }
        })
}

fn main() {
    if env::var_os("CARGO_FEATURE_BUNDLED").is_some() {
        let output_dir: PathBuf = Path::new(&env::var_os("OUT_DIR").unwrap()).into();
        let tileclient_include = output_dir.join("tileclient.rs");
        let tileserver_dir = PathBuf::from(&env::var_os("CARGO_MANIFEST_DIR").unwrap());
        let tileclient_target_dir = tileserver_dir
            .parent().unwrap()
            .join("dist/");
        let tileclient_source_dir = tileserver_dir
            .parent().unwrap()
            .parent().unwrap()
            .join("tileclient/");
        //let tileclient_cache_dir = tileclient_source_dir.join(".cache/");
        fs::remove_dir_all(&tileclient_target_dir).unwrap_or(());
        //fs::remove_dir_all(&tileclient_cache_dir).unwrap_or(());
        {
            let mut com;
            if cfg!(target_os = "windows") {
                com = Command::new("cmd");
                com.arg("/C");
                com.arg("npm");
            } else {
                com = Command::new("npm");
            }
            com.arg("install")
            .current_dir(&tileclient_source_dir)
            .stdin(Stdio::null())
            .stdout(Stdio::null())
            .stderr(Stdio::null())
            .output().ok()
            .and_then(|o| {
                if !o.status.success() {
                    print_str_cargo_warn(&String::from_utf8_lossy(&o.stdout));
                    println!("cargo:warning=Return code of `npm install` was {}.", o.status);
                }
                if !o.stderr.is_empty() {
                    print_str_cargo_warn(&String::from_utf8_lossy(&o.stderr));
                }
                if o.status.success() {
                    Some(())
                } else {
                    None
                }
            })
            .expect("Failed to run `npm install`.");
        }

        let mut com;
        if cfg!(target_os = "windows") {
            com = Command::new("cmd");
            com.arg("/C");
            com.arg("npm");
        } else {
            com = Command::new("npm");
        };
        com.arg("run")
            .arg("compileAssets")
            .current_dir(&tileclient_source_dir)
            .stdin(Stdio::null())
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .output().ok()
            .and_then(|o| {
                if !o.status.success() {
                    print_str_cargo_warn(&String::from_utf8_lossy(&o.stdout));
                    println!("cargo:warning=Return code of `npm run compileShaders` was {}.", o.status);
                }
                if !o.stderr.is_empty() {
                   print_str_cargo_warn(&String::from_utf8_lossy(&o.stderr));
                }
                if o.status.success() {
                    Some(())
                } else {
                    None
                }
            })
            .expect("Failed to run `npm run compileShaders`.");
        {
            let mut com;
        if cfg!(target_os = "windows") {
            com = Command::new("cmd");
            com.arg("/C");
            com.arg("npm");
        } else {
            com = Command::new("npm");
        };
        com.arg("run")
            .arg("build")
            //.arg("--dest")
            //.arg(tileclient_target_dir.as_os_str())
            .current_dir(&tileclient_source_dir)
            .stdin(Stdio::null())
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .output()
            .map(|o| {
                    if !o.status.success() {
                        print_str_cargo_warn(&String::from_utf8_lossy(&o.stdout));
                        println!("cargo:warning=Return code of `npm run build` was {}.", o.status);
                    }
                    if o.stderr.len() > 0 {
                        print_str_cargo_warn(&String::from_utf8_lossy(&o.stderr));
                    }})
                .expect("Failed to run `npm run build`.");

        };
        fs::write(
            &tileclient_include,
            &gen_tileclient_include(tileclient_target_dir.as_path())
        ).expect("Could not generate source for bundling `tileclient`.");
        print_cargo_change_recursive(tileclient_source_dir);
        //add selective rebuilding here
    }
}