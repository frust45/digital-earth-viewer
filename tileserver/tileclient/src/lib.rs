//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

#[cfg(feature = "bundled")]
include!(concat!(env!("OUT_DIR"), "/tileclient.rs"));