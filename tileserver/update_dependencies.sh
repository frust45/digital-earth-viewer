# This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer
echo "Updating base program dependencies"
cargo update
echo "Updating model dependencies"
cd tileserver-model
cargo update
cd epsg-geodetic-parameters
cargo update
cd epsg-geodetic-parameter-gen
cargo update
cd ../../..
echo "Updating dependencies for source plugins"
cd sources
for source in */; do echo "Updating $source" && cd $source && cargo update && cd ..; done
cd ..
