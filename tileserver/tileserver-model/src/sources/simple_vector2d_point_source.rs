//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use crate::*;

pub struct SimpleVector2DPointSourceBuilder{
    source_info: SourceInfo,
    layers: Vec<(LayerInfo, Vec<Vec<GeoPoint<Vector2D>>>)>, //Each layer consists of a layer_info and pages of points
}

impl SimpleVector2DPointSourceBuilder{
    pub fn new(mut source_info: SourceInfo, _source_name: &str, instance_name: &str) -> SimpleVector2DPointSourceBuilder{
        source_info.instance_name = instance_name.to_string();
        SimpleVector2DPointSourceBuilder{
            source_info,
            layers: Vec::new()
        }
    }

    pub fn get_source_info(&self) -> SourceInfo{
        self.source_info.clone()
    } 

    pub fn set_category(&mut self, category: &str) -> &mut Self{
        self.source_info.set_category(category);
        self
    }

    pub fn set_attribution(&mut self, category: &str) -> &mut Self{
        self.source_info.set_attribution(category);
        self
    }

    pub fn add_points_column(&mut self, name: &str, long_name: Option<&str>, unit: Option<&str>, mut points: Vec<GeoPoint<Vector2D>>) -> &mut Self{
        let mut layer_info = LayerInfo::new(name, GeoDataType::Vector2DPoints);
        if let Some(unit) = unit{
            layer_info.set_unit(unit);
        }
        if let Some(long_name) = long_name{
            layer_info.set_long_name(long_name);
        }
        
        points.sort_by_key(|p| p.timestamp);

        layer_info.set_datarange_from_value_iterator(points.iter().map(|p| (p.value[0].powi(2) + p.value[1].powi(2)).sqrt()));

        let pages: Vec<Vec<GeoPoint<Vector2D>>> = points.chunks(crate::constants::RECOMMENDED_PAGE_SIZE).map(|page| page.to_vec()).collect();

        layer_info.set_timesteps(pages.iter().filter_map(|p| p.first().map(|p| p.timestamp)).collect::<Vec<Timestamp>>().as_ref());

        let tmin = points.first().map(|p| p.timestamp);
        let tmax = points.last().map(|p|p.timestamp);
        if let Some(tmin) = tmin {
            if let Some(tmax) = tmax{
                layer_info.override_timerange(tmin, tmax);
            }
        }

        let mut hmin = std::f32::INFINITY;
        let mut hmax = std::f32::NEG_INFINITY;
        for p in points.iter(){
            if p.height < hmin{
                hmin = p.height;
            }
            if p.height > hmax{
                hmax = p.height;
            }
        }

        if hmin != std::f32::INFINITY && hmax != std::f32::NEG_INFINITY{
            layer_info.override_zrange(hmin, hmax);
        }


        let p0 = points.first().map(|p| p.pos);

        if let Some(p0) = p0{
            let extent = points.iter().fold(UECArea::new_from_point(p0.to_uec()),|area, point| UECArea::extend_area_with_point(area, point.pos.to_uec()));
            layer_info.set_extent(extent);
        }

        self.source_info.layers.push(layer_info.clone());

        self.layers.push((layer_info, pages));

        self
    }

    pub fn sample(&self, query: &SampleQuery) -> Result<Option<GeoData>, Box<dyn std::error::Error + Send + Sync>>{
        //1. Find layer
        if let Some(layer) = self.layers.iter().find(|l| l.0.name == query.layername){
            //2. Find page
            if layer.1.len() > query.tpage as usize{
                //3. Format as geodata
                Ok(Some(GeoData::Vector2DPoints(layer.1[query.tpage as usize].clone().into())))
            }else{
                Err(format!("Layer {} only has {} pages, but page index {} was requested", layer.0.name, layer.1.len(), query.tpage).into())
            }
        }else{
            Err(format!("No layer with name {} in this source", query.layername).into())
        }

    }

}