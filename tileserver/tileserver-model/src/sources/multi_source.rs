//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use crate::*;
use std::{error::Error, path::Path};
use crate::async_trait;

type AcceptFn = fn(&Path) -> bool;
type InitFn = fn(&SourceConfig, &mut SourceInfo) -> Result<Box<dyn Source>, Box<dyn std::error::Error + std::marker::Send + std::marker::Sync>>;


fn merge_source_infos(original: &mut SourceInfo, additional: &SourceInfo){
    if original.instance_name != additional.instance_name{
        original.instance_name = format!("{}, {}", original.instance_name, additional.instance_name);
    }

    if original.category.is_empty() {
        original.category = additional.category.clone();
    }

    for additional_layer in additional.layers.iter(){
        if let Some(original_layer) = original.layers.iter_mut().find(|l| l.name == additional_layer.name){
            if additional_layer.layer_type != original_layer.layer_type{
                let mut additional_layer_clone = additional_layer.clone();
                additional_layer_clone.name = format!("{} ({})", additional_layer.name, additional_layer.layer_type.as_str());
                original.layers.push(additional_layer_clone);
                continue;
            }
            match (&mut original_layer.timesteps, &additional_layer.timesteps){
                (None, None) => (),
                (Some(_t), None) => (),
                (None, Some(_t)) => original_layer.timesteps = additional_layer.timesteps.clone(),
                (Some(original), Some(additional)) => {
                    for t in additional{
                        if original.iter().find(|ot| **ot == *t).is_none(){
                            original.push(*t);
                        }
                    }
                }
            }
            
            match(&mut original_layer.timerange, additional_layer.timerange){
                (None, None) => (),
                (Some(_t), None) => (),
                (None, Some(_t)) => original_layer.timerange =additional_layer.timerange,
                (Some(r_orig), Some(r_add)) => {
                    *r_orig = (r_orig.0.min(r_add.0), r_orig.1.max(r_add.1));
                }
            }

            match(&mut original_layer.zsteps, additional_layer.zsteps.as_ref()){
                (None, None) => (),
                (Some(_t), None) => (),
                (None, Some(_t)) => original_layer.zsteps = additional_layer.zsteps.clone(),
                (Some(original), Some(additional)) => {
                    for t in additional{
                        original.push(*t);
                    }
                }
            }

            match(&mut original_layer.zrange, additional_layer.zrange){
                (None, None) => (),
                (Some(_t), None) => (),
                (None, Some(_t)) => original_layer.zrange =additional_layer.zrange,
                (Some(r_orig), Some(r_add)) => {
                    *r_orig = (r_orig.0.min(r_add.0), r_orig.1.max(r_add.1));
                }
            }

            match(&mut original_layer.datarange, additional_layer.datarange){
                (None, None) => (),
                (Some(_t), None) => (),
                (None, Some(_t)) => original_layer.datarange =additional_layer.datarange,
                (Some(r_orig), Some(r_add)) => {
                    *r_orig = (r_orig.0.min(r_add.0), r_orig.1.max(r_add.1));
                }
            }

            match(&mut original_layer.extent, additional_layer.extent){
                (None, None) => (),
                (Some(_t), None) => (),
                (None, Some(_t)) => original_layer.extent =additional_layer.extent,
                (Some(r_orig), Some(r_add)) => {
                    *r_orig = UECArea::extend_area_with_point(UECArea::extend_area_with_point(*r_orig, r_add.position),r_add.position + r_add.extent);
                }
            }

            if original_layer.long_name != additional_layer.long_name{
                match(&mut original_layer.long_name, &additional_layer.long_name){
                    (None, None) => (),
                    (Some(_t), None) => (),
                    (None, Some(_t)) => original_layer.long_name =additional_layer.long_name.clone(),
                    (Some(r_orig), Some(r_add)) => {
                        r_orig.push_str(" + ");
                        r_orig.push_str(&r_add);
                    }
                }
            }

            if original_layer.unit != additional_layer.unit{
                match(&mut original_layer.unit, &additional_layer.unit){
                    (None, None) => (),
                    (Some(_t), None) => (),
                    (None, Some(_t)) => original_layer.unit =additional_layer.unit.clone(),
                    (Some(r_orig), Some(r_add)) => {
                        r_orig.push_str(" + ");
                        r_orig.push_str(&r_add);
                    }
                }
            }

            original_layer.max_zoom_level = original_layer.max_zoom_level.max(additional_layer.max_zoom_level);

        }else{
            original.layers.push(additional_layer.clone());
        }
    }
}

pub struct MultiSource{
    source_info: SourceInfo,
    sources: Vec<(SourceInfo, Box<dyn Source>)>,
}

impl MultiSource{
    pub fn new(files: &[&Path], source_constructors: &[(&AcceptFn, &InitFn)], _source_name: &str, source_config: &SourceConfig) -> MultiSource{
        let mut multi_source = MultiSource{
            source_info: SourceInfo::default(),
            sources: Vec::new(),
        };

        for file in files{
            for (acceptor, constructor) in source_constructors{
                if acceptor(file){
                    let filename = file.file_name().map(|f| f.to_string_lossy().to_string()).unwrap_or("unknown file".to_string());
                    let mut source_info = SourceInfo::new(&filename);
                    let source = constructor(source_config, &mut source_info);
                    match source{
                        Ok(source) => {
                            //debug!("SourceInfo before {:#?}", source_info);
                            merge_source_infos(&mut multi_source.source_info, &source_info);
                            //debug!("SourceInfo after {:#?}", source_info);
                            multi_source.sources.push((source_info, source));
                        },
                        Err(e) => warn!("Source did not initialize successfully for file {:?} with error {}", file, e)
                    }
                    
                }
            }
        }
        
        multi_source
    }

    pub fn new_from_sourcetype(files: &[std::path::PathBuf], source_constructor: (&AcceptFn, &InitFn), source_info: &mut SourceInfo, source_config: &SourceConfig) -> Result<MultiSource, Box<dyn Error + Send + Sync>>{
        debug!("Creating new multifile source from {} files", files.len());
        let mut multi_source = MultiSource{
            source_info: source_info.clone(),
            sources: Vec::new(),
        };

        let mut errs: String = String::new();

        for (index,file) in files.iter().enumerate(){
            trace!("Processing file {} of {}", index + 1, files.len());
            if !source_constructor.0(file){
                continue; //File not accepted by source
            }
            let filename = file.file_name().map(|f| f.to_string_lossy().to_string()).unwrap_or_else(|| "unknown file".to_string());
            let mut new_config = source_config.clone();
            new_config.input_files = vec![file.clone()];

            let mut source_info = SourceInfo::new(&filename);
            let source = source_constructor.1(&new_config, &mut source_info);
            match source{
                Ok(source) => {
                    //debug!("SourceInfo before {:#?}", source_info);
                    merge_source_infos(&mut multi_source.source_info, &source_info);
                    //debug!("SourceInfo after {:#?}", source_info);
                    multi_source.sources.push((source_info, source));
                 },
                Err(e) => errs += &format!("Source did not initialize successfully for file {:?} with error {}\n\n", file, e)
            }
        }
        
        *source_info = multi_source.source_info.clone();

        debug!("Finished constructing multi source {}", multi_source.source_info.instance_name);

        if errs.is_empty() {
            Ok(multi_source)
        }else{
            Err(errs.into())
        }
    }

    pub fn get_source_info(&self) -> SourceInfo{
        self.source_info.clone()
    }

}

#[async_trait]
impl Source for MultiSource{
    async fn sample(&self, query: &SampleQuery) -> Result<Option<GeoData>, Box<dyn std::error::Error + Send + Sync>> {
        let eligible_sources: Vec<_> = self.sources.iter()
        //Only sources that contain the requested time and zsteps or that don't contain any info for them at all
        .filter(|(info, _source)|{
            if let Some(layer) = info.layers.iter().find(|l| l.name == query.layername){
                layer.timesteps.as_ref().map(|tsteps| tsteps.len() > query.tpage as usize).unwrap_or(true) && 
                layer.zsteps.as_ref().map(|zsteps| zsteps.len() > query.zpage as usize).unwrap_or(true)
            }else{
                false
            }
        }).collect();
        
        let mut results = Vec::new();
        
        for s in eligible_sources{
            results.push(s.1.sample(query).await);
        }

        results.into_iter().fold(Ok(None), |prev, next| {
            match (prev, next){
                (Err(e1), Err(e2)) => Err(format!("{:?}, {:?}",e1,e2).into()),
                (Ok(v), Err(e)) => {warn!("Error occurred: {:?}", e); Ok(v)},
                (Err(e),Ok(v)) => {warn!("Error occurred: {:?}", e); Ok(v)},
                (Ok(v1), Ok(v2)) => {
                    Ok(match(v1, v2){
                        (None, None) => None,
                        (Some(v), None) => Some(v),
                        (None, Some(v)) => Some(v),
                        (Some(mut a), Some(b)) => {
                            match (&mut a,&b){
                                (GeoData::ScalarTiles(ta), GeoData::ScalarTiles(tb)) => {
                                    if ta.tile != tb.tile{
                                        warn!("Sources returned different tiles! Returning first tile.");
                                        return Ok(Some(a));
                                    }

                                    ta.data.iter_mut().enumerate().for_each(|(index, a_data)|{
                                        if a_data.is_nodata() {
                                            *a_data = tb.data[index];
                                        }
                                    });
                                    Some(a)
                                },
                                (GeoData::ColorTiles(ta), GeoData::ColorTiles(tb)) => {
                                    if ta.tile != tb.tile{
                                        warn!("Sources returned different tiles! Returning first tile.");
                                        return Ok(Some(a));
                                    }

                                    ta.data.iter_mut().enumerate().for_each(|(index, a_data)|{
                                        if a_data.is_nodata() {
                                            *a_data = tb.data[index];
                                        }
                                    });
                                    Some(a)
                                },
                                (GeoData::ScalarPoints(pa), GeoData::ScalarPoints(pb)) => {
                                    pa.points.extend_from_slice(&pb.points);
                                    Some(a)
                                },
                                (GeoData::ColorPoints(pa), GeoData::ColorPoints(pb)) => {
                                    pa.points.extend_from_slice(&pb.points);
                                    Some(a)
                                },
                                (GeoData::Vector2DPoints(pa), GeoData::Vector2DPoints(pb)) => {
                                    pa.points.extend_from_slice(&pb.points);
                                    Some(a)
                                },
                                (_,_) => {
                                    warn!("Sources returned different geodata types! Returning first result.");
                                    Some(a)
                                }
                            }
                        }
                    })
                }
            }
        })
    }
}