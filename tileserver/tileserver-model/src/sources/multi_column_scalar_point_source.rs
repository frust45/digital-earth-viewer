//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use std::io::{BufWriter, Write};
use std::error::Error;

use crate::*;
#[derive(Debug)]
pub struct MultiColumnPointSourceBuilder{
    source_info: SourceInfo,
    //time_steps: Vec<usize>,
    column_names: Vec<String>,
    values: Vec<(UEC, f32, Timestamp, Vec<f32>)>
}

impl MultiColumnPointSourceBuilder{
    ///Initializes a new mcpsb from a source and instance name, as well as a list of columns
    pub fn new(mut source_info: SourceInfo, instance_name: &str, column_names: &[&str]) -> MultiColumnPointSourceBuilder{
        source_info.instance_name = instance_name.to_string();
        source_info.layers = column_names.iter().map(|name| LayerInfo::new(name, GeoDataType::ScalarPoints)).collect();
        MultiColumnPointSourceBuilder{
            source_info,
            //time_steps: Vec::new(),
            column_names: column_names.iter().map(|v| v.to_string()).collect(),
            values: Vec::new()
        }
    }

    pub fn get_source_info(&self) -> SourceInfo{
        self.source_info.clone()
    } 

    pub fn set_category(&mut self, category: &str) -> &mut Self{
        self.source_info.set_category(category);
        self
    }

    pub fn set_attribution(&mut self, category: &str) -> &mut Self{
        self.source_info.set_attribution(category);
        self
    }

    pub fn add_value_row(&mut self, position: UEC, height: f32, timestamp: Timestamp, values: &[f32]){
        assert_eq!(values.len(), self.column_names.len());
        self.values.push((position, height, timestamp, Vec::from(values)));
    }

    pub fn set_unit(&mut self, column_name: &str, unit: &str){
        let ci = self.column_names.iter().enumerate().find(|(_i,n)|*n == column_name).map(|(i,_n)| i);
        if let Some(ci) = ci{
            self.source_info.layers[ci].set_unit(unit);
        }
    }

    pub fn set_default_overlay(&mut self, column_name: &str, overlay: &str){
        let ci = self.column_names.iter().enumerate().find(|(_i,n)|*n == column_name).map(|(i,_n)| i);
        if let Some(ci) = ci{
            self.source_info.layers[ci].set_default_overlay(overlay);
        }
    }

    pub fn set_long_name(&mut self, column_name: &str, long_name: &str){
        let ci = self.column_names.iter().enumerate().find(|(_i,n)|*n == column_name).map(|(i,_n)| i);
        if let Some(ci) = ci{
            self.source_info.layers[ci].set_long_name(long_name);
        }
    }

    /// Calculates Value range and sets timing information
    pub fn finish(&mut self){
        if self.values.is_empty(){
            return;
        }
        self.values.sort_by_key(|(_p,_h,t,_v)| *t);

        let timesteps: Vec<i64> = self.values
            .chunks(crate::constants::RECOMMENDED_PAGE_SIZE)
            .filter_map(|c| c.first())
            .map(|(_p,_h,t, _v)| *t)
            .collect();

        let mut times: Vec<i64> = self.values.iter()
            .map(|(_p,_h,t, _v)| *t)
            .collect();
        times.sort_unstable();


        let mut mins = vec![std::f32::INFINITY; self.column_names.len()];
        let mut maxs = vec![std::f32::NEG_INFINITY; self.column_names.len()];
        for values in self.values.iter().map(|(_p,_h ,_t,v)| v){
            for i in 0 .. self.column_names.len(){
                mins[i] = mins[i].min(values[i]);
                maxs[i] = maxs[i].max(values[i]);
            }
        }

        let mut hmin = std::f32::INFINITY;
        let mut hmax = std::f32::NEG_INFINITY;
        for &height in  self.values.iter().map(|(_pos, h, _t, _v)| h){
            if height < hmin{
                hmin = height;
            }
            if height > hmax{
                hmax = height;
            }
        }

        let extent: Option<UECArea> = self.values
            .first()
            .map(|(p, _h,_t,_v)| p)
            .map(|&p0| 
                self.values.iter()
                .map(|(p, _h,_t,_c)| p)
                .fold(UECArea::new_from_point(p0),|area, point| UECArea::extend_area_with_point(area, *point))
            );

        for (index, _column_name) in self.column_names.iter().enumerate(){
            let li = &mut self.source_info.layers[index];
            li.set_datarange(mins[index], maxs[index]);
            li.set_timesteps(&timesteps);

            li.override_timerange(*times.first().unwrap(), *times.last().unwrap() );
            if hmin != std::f32::INFINITY && hmax != std::f32::NEG_INFINITY{
                li.override_zrange(hmin, hmax);
            }
            if let Some(extent) = extent{
                li.set_extent(extent);
            }
        }
    }

    pub fn sample(&self, query: &SampleQuery) -> Result<Option<GeoData>, Box<dyn std::error::Error + Send + Sync>>{
        //1. Find layer
        if let Some((index, layer)) = self.source_info.layers.iter().enumerate().find(|(_i,l)| l.name == query.layername){
            //2. Find page
                if let Some(values) = self.values
                    .chunks(crate::constants::RECOMMENDED_PAGE_SIZE) //Go through the chunks
                    .nth(query.tpage as usize) //Find the page we are looking for
                    .map(|values|  //Make GeoPoints
                        values.iter()
                        .map(|(p,h,t,v)| GeoPoint::new_from_uec(*p, *h, *t, v[index]))
                        .collect::<Vec<_>>()
                    ){
                        Ok(Some(GeoData::ScalarPoints(PointsData::new(values))))
                }else{
                    Err(format!("Layer {} only has {} pages, but page index {} was requested", layer.name, layer.timesteps.as_ref().map(|t| t.len()).unwrap_or(0), query.tpage).into())
                }
        }else{
            Err(format!("No layer with name {} in this source", query.layername).into())
        }
    }

    pub fn dump_tsv(&self, w: &mut dyn Write) -> Result<(), Box<dyn Error + Send + Sync>>{
        let mut bfw = BufWriter::new(w);
        //1. Write header
        writeln!(bfw, "Latitude\tLongitude\tHeight\tTimestamp\t{}\n", self.column_names.join("\t"))?;
        //2. Write values
        for (uec, height, time, values) in self.values.iter(){
            let c = Position::UEC(*uec).to_coord();
            let timestring = TimeParser::unix_milli_time_to_iso8601(*time);
            let valuesstr: String = values.iter().map(|v| v.to_string()).collect::<Vec<_>>().join("\t");
            writeln!(bfw, "{}\t{}\t{}\t{}\t{}\n", c.lat, c.lon, height, timestring, valuesstr)?;
        }
        Ok(())
    }

}

#[cfg(test)]
mod tests{
    use super::*;

    fn setup(){
        let _ = env_logger::builder().is_test(true).try_init();
    }
    
    fn create_empty_multi_column_scalar_point_source(){
        setup();
        todo!();
    }

    fn add_some_points_and_check_if_dataranges_correct(){
        setup();
        todo!();
    }

    fn check_if_chunking_works(){
        setup();
        todo!();
    }

    fn check_if_spatial_extent_is_calculated_correctly(){
        setup();
        todo!();
    }

    fn check_if_sampling_returns_the_correct_chunk(){
        setup();
        todo!();
    }

}