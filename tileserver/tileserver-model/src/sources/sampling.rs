//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use crate::datatypes::{Nodata, WeightedMix};
use std::ops::Index;

pub struct SliceTiles<'a, T> {
    tiles: Vec<&'a [T]>,
    tileh: usize, 
    tilew: usize,
    tilec_x: usize,
    width: usize,
    height: usize
}
impl<'a, T> SliceTiles<'a, T> {
    pub fn new(t: Vec<&'a [T]>, tile_size: (usize, usize), tile_count: (usize, usize)) -> Option<Self> {
        if t.iter().map(|t| t.len()).all(|l| l == tile_size.0 * tile_size.1) && tile_count.0 * tile_count.1 == t.len() {
            Some(Self{
                tiles: t,
                tilew: tile_size.0,
                tileh: tile_size.1,
                tilec_x: tile_count.0,
                width: tile_count.0 * tile_size.0,
                height: tile_count.1 * tile_size.1
            })
        } else {
            None
        }
    }

    pub fn size(&self) -> (usize, usize) {
        (self.width, self.height)
    }
}

impl<'a, T> Index<usize> for SliceTiles<'a, T> where T: Sized {
    type Output = T;
    fn index(&self, index: usize) -> &'a T {
        let gx = index % self.width;
        let gy = index / self.width;
        let tx = gx / self.tilew;
        let ty = gy / self.tileh;
        let tile = self.tiles[tx + ty * self.tilec_x];
        let lx = gx - tx * self.tilew;
        let ly = gy - ty * self.tileh;
        &tile[lx + ly * self.tilew]
    }
}

pub fn linear_from_slice_nodata<T, U>(a: &U, point: (f64, f64), size: (usize, usize), stride: usize, offset: usize) -> T where T: Clone + WeightedMix + Nodata, U: Index<usize, Output=T> {
    let esize = size.0 * size.1 * stride + offset;
    let point2 = (point.0 - 0.5, point.1 - 0.5);

    let x0 = point2.0.floor().max(0.0) as usize;
    let y0 = point2.1.floor().max(0.0) as usize;
    let x0f = (point2.0 - point2.0.floor()) as f32;
    let y0f = (point2.1 - point2.1.floor()) as f32;
    let x1f = 1.0 - x0f;
    let y1f = 1.0 - y0f;

    let i0 = ((x0.min(size.0 - 1) as usize + size.0 * y0.min(size.1 - 1) as usize) * stride + offset).min(esize - 1);
    let i1 = (((x0 as usize + 1).min(size.0 - 1) + size.0 * y0.min(size.1 - 1) as usize) * stride + offset).min(esize - 1);
    let i2 = ((x0.min(size.0 - 1) as usize + size.0 * (y0 as usize + 1).min(size.1 - 1)) * stride + offset).min(esize - 1);
    let i3 = (((x0 as usize + 1).min(size.0 - 1) + size.0 * (y0 as usize + 1).min(size.1 - 1)) * stride + offset).min(esize - 1);

    let v0 = a[i0].clone();
    let v1 = a[i1].clone();
    let v2 = a[i2].clone();
    let v3 = a[i3].clone();

    match (v0.is_nodata(), v1.is_nodata(), v2.is_nodata(), v3.is_nodata()) {
        (false, false, false, false) => {
            T::mix4(
                v0, x1f * y1f,
                v1, x0f * y1f,
                v2, x1f * y0f,
                v3, x0f * y0f
            )
        },
        (false, false, false, true) => {
            T::mix3(
                v0, x1f * y1f,
                v1, x0f * y1f, 
                v2, x1f * y0f
            )
           
        },
        (false, false, true, false) => {
            T::mix3(
                v0, x1f * y1f,
                v1, x0f * y1f, 
                v3, x0f * y0f
            )
        },
        (false, false, true, true) => {
            T::mix2(
                v0, x1f,
                v1, x0f
            )
        },
        (false, true, false, false) => {
            T::mix3(
                v0, x1f * y1f, 
                v2, x1f * y0f,
                v3, x0f * y0f
            )
            
        },
        (false, true, false, true) => {
            T::mix2(
                v0, y1f,
                v2, y0f
            )

        },
        (false, true, true, false) => {
            T::mix2(
                v0, x1f * y1f, 
                v3, x0f * y0f
            )
        },
        (false, true, true, true) => {
            v0
        },
        (true, false, false, false) => {
            T::mix3(
                v1, x0f * y1f,
                v2, x1f * y0f,
                v3, x0f * y0f
            )           
        },
        (true, false, false, true) => {
            T::mix2(
                v1, x0f * y1f,
                v2, x1f * y0f
            )
        },
        (true, false, true, false) => {
            T::mix2(
                v1, y1f,
                v3, y0f
            )
        },
        (true, false, true, true) => {
            v1
        },
        (true, true, false, false) => {
            T::mix2(
                v2, x1f,
                v3, x0f
            )
        },
        (true, true, false, true) => {
            v2
        },
        (true, true, true, false) => {
            v3
        },
        (true, true, true, true) => {
            T::nodata()
        }
    }
    
}

pub fn linear_from_slice<T, U>(a: &U, point: (f64, f64), size: (usize, usize), stride: usize, offset: usize) -> T where T: Clone + WeightedMix, U: Index<usize, Output = T> {
    let esize = size.0 * size.1 * stride + offset;
    let point2 = ((point.0 - 0.5).max(0.0), (point.1 - 0.5).max(0.0));

    let x0 = point2.0.floor() as usize;
    let y0 = point2.1.floor() as usize;
    let x0f = (point2.0 - point2.0.floor()) as f32;
    let y0f = (point2.1 - point2.1.floor()) as f32;
    let x1f = 1.0 - x0f;
    let y1f = 1.0 - y0f;

    let i0 = ((x0.min(size.0 - 1).max(0) as usize + size.0 * y0.min(size.1 - 1).max(0) as usize) * stride + offset).min(esize - 1);
    let i1 = (((x0 as usize + 1).min(size.0 - 1).max(0) + size.0 * y0.min(size.1 - 1).max(0) as usize) * stride + offset).min(esize - 1);
    let i2 = ((x0.min(size.0 - 1).max(0) as usize + size.0 * (y0 as usize + 1).min(size.1 - 1).max(0)) * stride + offset).min(esize - 1);
    let i3 = (((x0 as usize + 1).min(size.0 - 1).max(0) + size.0 * (y0 as usize + 1).min(size.1 - 1).max(0)) * stride + offset).min(esize - 1);

    let v0 = a[i0].clone();
    let v1 = a[i1].clone();
    let v2 = a[i2].clone();
    let v3 = a[i3].clone();

    T::mix4(
        v0, x1f * y1f,
        v1, x0f * y1f,
        v2, x1f * y0f,
        v3, x0f * y0f
    )
}