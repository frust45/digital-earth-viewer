//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use crate::SyncError;
use crate::UECArea;

use super::datatypes::*;
use super::queries::*;
use serde::{Serialize, Deserialize};
use std::collections::HashMap;
use std::path::PathBuf;

///Key parameters of a source. Its JSON representation is digested by the client.
#[derive(Debug, Serialize, Clone, Deserialize)]
pub struct SourceInfo{
    ///Reasonably short name of the dataset. Defaults to the source filename unless overridden in the server configuration
    pub instance_name: String,
    ///Information on the source's available layers.
    pub layers: Vec<LayerInfo>,
    //Whether this source is cacheable or not
    pub cacheable: bool,
    ///The category this source falls into
    pub category: String,
    //The attribution data for this source
    pub attribution: String
}

impl SourceInfo{
    pub fn new(instance_name: &str) -> SourceInfo{
        SourceInfo{
            instance_name: instance_name.to_string(),
            cacheable: true,
            category: "Other".to_string(),
            attribution: "No explicit attribution given. All rights reserved by the original author.".to_string(),
            layers: Vec::new()
        }
    }

    pub fn default() -> SourceInfo{
        SourceInfo{
            instance_name: String::new(),
            cacheable: true,
            category: "Other".to_string(),
            attribution: "No explicit attribution given. All rights reserved by the original author.".to_string(),
            layers: Vec::new()
        }
    }

    pub fn set_category(&mut self, category: &str) -> &mut Self{
        self.category = category.to_string();
        self
    }

    pub fn set_attribution(&mut self, attribution: &str) -> &mut Self{
        self.attribution = attribution.to_string();
        self
    }

    pub fn set_uncacheable(&mut self) -> &mut Self{
        self.cacheable = false;
        self
    }

    pub fn add_layer(&mut self, layer: LayerInfo) -> &mut Self{
        self.layers.push(layer);
        self
    }
}

///Serialized to inform the client about a layer's metadata.
#[derive(Debug, Clone, Serialize, Default, Deserialize)]
pub struct LayerInfo{
    ///Internal name, as used to query data from the layer.
    pub name: String,
    ///Display name, should not contain excessive explanation.
    pub long_name: Option<String>,
    ///Format this reasonably, they are displayed as plain text by the client.
    pub unit: Option<String>,
    ///Hint on how the data that can be requested from this layer is to be interpreted.
    pub layer_type: GeoDataType,
    ///Time range starts of tiles or pages
    pub timesteps: Option<Vec<Timestamp>>,
    ///Temporal bounds
    pub timerange: Option<(Timestamp,Timestamp)>,
    ///The lowest and the highest value. Not applicable to color layers. Used for colormap ranging in the client.
    pub datarange: Option<(f32, f32)>,
    ///Z-Steps as starts of z-regions
    pub zsteps: Option<Vec<f32>>,
    ///Z-Bounds
    pub zrange: Option<(f32,f32)>,
    //Layer extent
    pub extent: Option<UECArea>,
    //Measure this at the equator, anything else doesn't really make a lot of sense.
    pub max_zoom_level: u8,
    //Default overlay
    pub default_overlay: Option<String>
}

impl LayerInfo{
    pub fn new(name: &str, layer_type: GeoDataType) -> LayerInfo{
        LayerInfo{
            name: name.to_string(),
            long_name: None,
            unit: None,
            layer_type,
            timesteps: None,
            timerange: None,
            datarange: None,
            zrange: None,
            zsteps: None,
            extent: None,
            max_zoom_level: 0,
            default_overlay: None
        }
    }

    pub fn set_long_name(&mut self, long_name: &str) -> &mut Self{
        self.long_name = Some(long_name.to_string());
        self
    }

    pub fn set_unit(&mut self, unit: &str) -> &mut Self{
        self.unit = Some(unit.to_string());
        self
    }

    pub fn set_timesteps(&mut self, time_pages: &[Timestamp]) -> &mut Self{
        let min = time_pages.iter().min();
        let max = time_pages.iter().max();
        if let Some(min) = min{
            if let Some(max) = max{
                self.timesteps = Some(time_pages.to_vec());
                self.timerange = Some((*min, *max));
            }
        }
        self
    }

    pub fn override_timerange(&mut self, min: Timestamp, max: Timestamp) -> &mut Self{
        self.timerange = Some((min, max));
        self
    }

    pub fn set_datarange(&mut self, min: f32, max: f32) -> &mut Self{
        self.datarange = Some((min, max));
        self
    }

    pub fn set_datarange_from_value_iterator(&mut self, iter: impl Iterator<Item = f32>) -> &mut Self{
        let (min, max) = iter.fold((std::f32::INFINITY, std::f32::NEG_INFINITY), |(min, max), val| {
            if (val as f32) < min && (val as f32) > max{
                (val as f32, val as f32)
            }else if (val as f32) < min{
                (val as f32, max)
            }else if (val as f32) > max{
                (min, val as f32)
            }else{
                (min, max)
            }
        });
        self.set_datarange(min, max);
        self
    }

    pub fn set_zsteps(&mut self, z_pages: &[f32]) -> &mut Self{
        let (min, max) = z_pages.iter().fold((std::f32::INFINITY, std::f32::NEG_INFINITY), |(min, max), val| {
            if *val < min && *val > max{
                (*val, *val)
            }else if *val < min{
                (*val, max)
            }else if *val > max{
                (min, *val)
            }else{
                (min, max)
            }
        });
        if min != std::f32::INFINITY && max != std::f32::NEG_INFINITY{
            self.zsteps = Some(z_pages.to_vec());
            self.zrange = Some((min, max));

        }
        self
    }

    pub fn override_zrange(&mut self, min: f32, max: f32) -> &mut Self{
        self.zrange = Some((min, max));
        self
    }

    pub fn set_extent(&mut self, extent: UECArea) -> &mut Self{
        self.extent = Some(extent);
        self
    }

    pub fn set_max_zoom_level(&mut self, max_zoom_level: u8) -> &mut Self{
        self.max_zoom_level = max_zoom_level;
        self
    }

    pub fn set_default_overlay(&mut self, default_overlay: &str) -> &mut Self{
        self.default_overlay = Some(default_overlay.to_string());
        self
    }

}


///As loaded from the server configuration.
#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct SourceConfig{
    ///Input paths.
    pub input_files: Vec<PathBuf>,
    ///Additional options by name, serialized to `String`.
    pub settings: HashMap<String, String>
}
pub use async_trait::async_trait;

pub type SourceInitResult = Result<(SourceInfo,Box<dyn Source>), Box<dyn std::error::Error + Send + Sync>>;
///Allows unified behavior of all sources at initialization. 
#[async_trait]
pub trait SourceInit: Send + Sync + Sized{
    ///Initializes the source with the options given in the server configuration file.
    async fn initialize(settings: SourceConfig, mut source_info: SourceInfo) -> SourceInitResult;
    //Checks whether the source accepts a given file
    fn accepts_file(_file: &std::path::Path) -> bool { false }
}

pub type SourceResult = Result<Option<GeoData>, Box<dyn std::error::Error + Send + Sync>>;
///Generalized functionality of data sources.
#[async_trait]
pub trait Source: Send + Sync{
    ///Returns the queried tile data, or whatever error occurs during sampling.
    async fn sample(&self, query: &SampleQuery) -> SourceResult;
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum SourceStatus{
    Loading,
    Loaded,
    Error(String)
}

#[async_trait]
pub trait Metadata: Send + Sync{
    async fn get_metadata(&self, metadata: &str) -> Result<Vec<u8>, SyncError>;
}

#[async_trait]
impl Metadata for dyn Source{
    async fn get_metadata(&self, _metadata: &str) -> Result<Vec<u8>, SyncError> {
        Err("Source has no metadata".into())
    }
}