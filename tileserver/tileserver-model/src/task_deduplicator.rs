//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use std::{
    collections::HashMap,
    hash::Hash,
    ops::DerefMut,
    sync::Arc
};

use futures::future::Future;
use tokio::sync::{
    broadcast::{
        channel,
        Sender
    },
    Mutex
};

pub struct TaskDeduplicator<K, V> {
    tasks: Arc<Mutex<HashMap<K, Sender<V>>>>
}

impl<K, V> TaskDeduplicator<K, V> {
    pub fn new() -> Self {
        Self {
            tasks: Arc::new(Mutex::new(HashMap::new()))
        }
    }

    pub async fn run_or_wait<F, Fut>(&self, key: K, closure: F) -> V 
    where 
        F: FnOnce(K) -> Fut,
        Fut: Future<Output = V>,
        K: Hash + Eq + Clone,
        V: Clone,
    {
        let mut lock = self.tasks.lock().await;
        match 
            lock.deref_mut()
                .get_mut(&key)
                .map(|s| s.subscribe())
        {
            Some(mut r) => {
                drop(lock);
                r.recv().await.unwrap()
            },
            None => {
                let (sender, _): (Sender<V>, _) = channel(1);
                lock.deref_mut()
                    .insert(key.clone(), sender);
                drop(lock);
                
                let output = closure(key.clone()).await;

                self.tasks.lock()
                    .await
                    .deref_mut()
                    .remove(&key)
                    .map(|s| 
                        s.send(output.clone())
                            .unwrap_or(0)
                        );
                output
            }
        }
    }
}