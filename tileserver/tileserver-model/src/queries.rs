//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use std::hash::{
    Hash,
    Hasher,
};

use crate::*;

use serde::Deserialize;

///A leaner type for sending sample queries from client to server
#[derive(Debug, Clone, Deserialize)]
pub struct ClientQuery{
    ///The instance name of the data source
    pub sourcename: String,
    ///The layer identifier
    pub layername: String,
    ///The path of the tile that is being requested
    pub tilepath: String,
    ///The height at which data shall be sampled
    pub height: u8,
    ///The timestamp that is being requested
    pub timestamp: Timestamp
}

///A request type that contains information on the data region the client wants to display.
#[derive(Debug, Clone, Deserialize, PartialEq, Eq)]
pub struct SampleQuery{
    ///The internal name of the data source.
    pub sourcename: String,
    ///The internal layer identifier of the requested layer. Usually a short name.
    pub layername: String,
    ///A serialized representation of `sourcename`, `layername`, `tile` and `timestamp`. Used for deduplication.
    pub str_repr: String,
    ///Additional query parameters. Ignored by most sources.
    //pub additional_info: HashMap<String,String>,
    ///The query region.
    pub tile: Tile,
    ///The time region as an index into the timesteps field of the source_info
    pub tpage: u32,
    ///The height region as an index into the z_steps field of the source_info
    pub zpage: u32
}

impl Hash for SampleQuery {
    fn hash<H>(&self, state: &mut H) where H: Hasher{
        self.str_repr.hash(state);
    }
}

impl SampleQuery{
    ///Creates an empty `SampleQuery`. Its region is the entire western hemisphere (pole to pole, 180°W to the prime meridian).
    pub fn empty()-> SampleQuery{
        SampleQuery{
            sourcename: String::new(),
            layername: String::new(),
            str_repr: String::new(),
            //additional_info: HashMap::new(),
            tile: Tile::default(),
            zpage: 0,
            tpage: 0
        }
    }

    ///Constructor. Automatically fills the `str_repr` field with the correct serialization.
    pub fn new(source: String, layer: String, tile: Tile, tpage: u32, zpage: u32) -> SampleQuery{
        let mut tmp = SampleQuery{
            str_repr: String::new(),
            sourcename: source,
            layername: layer,
            //additional_info: HashMap::new(),
            tile,
            zpage,
            tpage,
        };
        tmp.recalculate_str_repr();
        tmp
    }

    ///Recalculates the serialization in `str_repr`. 
    pub fn recalculate_str_repr(&mut self){
        self.str_repr = format!("source_{}_layer_{}_tile_{}_zpage_{}_tpage_{}", self.sourcename, self.layername, self.tile.path, self.zpage, self.tpage);
    }

    ///Returns the serialization.
    pub fn to_string(&self) -> String{
        self.str_repr.clone()
    }
}