//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use std::error::Error;
use crate::*;
use crate::chrono::{NaiveDateTime, NaiveDate, NaiveTime};
#[allow(clippy::upper_case_acronyms)]
#[derive(Ord, PartialOrd, Eq, PartialEq)]
pub enum TimeParsingMode{
    ISO8601,
    ODV,
    Seperate,
    UnixSeconds
}
impl TimeParsingMode{
    pub fn parse_default(&self, strs: &[&str]) -> Result<Timestamp, Box<dyn Error + Send + Sync>>{
        match self{
            TimeParsingMode::ISO8601 => {
                TimeParser::time_from_inconsistent_iso8601_str(
                    strs.get(0)
                        .ok_or_else(||"No input string given for time".to_string())?
                )
            }
            TimeParsingMode::ODV => {
                TimeParser::time_from_odv_str_varying(strs.get(0).ok_or_else(||"No input string given for time".to_string())?)
            }
            TimeParsingMode::Seperate => {
                TimeParser::time_from_seperate_strs_varying(
                    strs.get(0).ok_or_else(||"No input string given for year".to_string())?,
                    strs.get(1).unwrap_or(&"1"),
                    strs.get(2).unwrap_or(&"1"),
                    strs.get(3).unwrap_or(&"0"),
                    strs.get(4).unwrap_or(&"0"),
                    strs.get(5).unwrap_or(&"0"),
                    strs.get(6).unwrap_or(&"0"),
                )
            }
            TimeParsingMode::UnixSeconds => {
                TimeParser::time_from_unix_seconds_str(strs.get(0).ok_or_else(||"No input string given for time".to_string())?)
            }
        }
    }
}

impl From<&str> for TimeParsingMode{
    fn from(mode: &str) -> Self {
        match mode{
            "odv" => TimeParsingMode::ODV,
            "seperate" => TimeParsingMode::Seperate,
            "iso" => TimeParsingMode::ISO8601,
            "unixseconds" => TimeParsingMode::UnixSeconds,
            _ => TimeParsingMode::ISO8601
        }
    }
}

pub struct TimeParser{}

impl TimeParser{
    ///Parses a timestamp from a string like the ones found in the GLODAP dataset (almost ISO 8601 but missing a few letters and where 2018-02-29 is a day that exists outside leap years)
    pub fn time_from_inconsistent_iso8601_str(almost_iso8601: &str) -> Result<Timestamp, Box<dyn Error + Send + Sync>>{
        //Add T instead of space inbetween date and time
        let mut new_str = almost_iso8601.to_string().trim().replace(" ", "T");
        //Add missing utc identifier
        if ! new_str.ends_with('Z'){
            new_str += "Z";
        }
        //Get rid of days that don't exist
        if new_str.starts_with("2018-02-29"){ //Thank you data
            new_str = new_str.replace("2018-02-29", "2018-03-01");
        }

        TimeParser::time_from_iso8601_str(&new_str)
    }

    ///Parses a timestamp from a conforming string of a ISO 8601 UTC Datetime
    //New implementation is not as nice, but more correct
    pub fn time_from_iso8601_str(iso8601: &str) -> Result<Timestamp, Box<dyn Error + Send + Sync>>{
        let date_seperator_index = iso8601.find('T').ok_or_else(|| format!("No Z in timestring {}", iso8601))?;
        let datestr = &iso8601[0..date_seperator_index];
        let timestr = &iso8601[date_seperator_index+1..iso8601.len()-1];
        let dateparts: Vec<&str> = datestr.split('-').collect();
        let year = dateparts.get(0).map(|y| y.parse::<i32>().ok()).flatten().ok_or_else(|| "No year in timestring".to_string())?;
        let month = dateparts.get(1).map(|m| m.parse::<u32>().ok()).flatten().unwrap_or(1);
        let day = dateparts.get(2).map(|d| d.parse::<u32>().ok()).flatten().unwrap_or(1);
        let date = NaiveDate::from_ymd_opt(year, month, day).ok_or_else(|| format!("Invalid date: {}-{}-{}", year, month, day))?;
        let timeparts: Vec<&str> = timestr.split(':').collect();
        let hour = timeparts.get(0).map(|h| h.parse::<u32>().ok()).flatten().ok_or_else(|| "No hour in timestring".to_string())?;
        let minute = timeparts.get(1).map(|m| m.parse::<u32>().ok()).flatten().unwrap_or(0);
        let second = timeparts.get(2).map(|s| s.parse::<f32>().ok()).flatten().unwrap_or(0.0);
        let whole_seconds = second.floor();
        let milliseconds = (second % 1f32) * 1000f32;
        let time = NaiveTime::from_hms_milli_opt(hour, minute, whole_seconds as u32, milliseconds as u32).ok_or_else(|| format!("Invalid time: {}:{}:{}.{}", hour, minute, whole_seconds, milliseconds))?;
        let dt: NaiveDateTime = NaiveDateTime::new(date, time);
        Ok(dt.timestamp_millis())
    }

    //Thanks for the consistent time format in exports ODV
    pub fn time_from_odv_str_varying(odv: &str) -> Result<Timestamp, Box<dyn Error + Send + Sync>>{
        //Let's see, the time could contain milliseconds, or it could not
        if odv.contains('.'){
            match TimeParser::time_from_odv_str(odv){//If it contains a dot, let's try to parse it that way
                success @ Ok(_)=> success,
                Err(e) => {//This error could also be that the time just contains a dot at the end, but no milliseconds, so lets get rid of them and try again
                    if let Some(time_without_dot) = odv.split('.').next(){
                        TimeParser::time_from_odv_str_without_millis(time_without_dot)
                    }else{ //This should never happen, we checked for the dot in the beginning
                        Err(e)
                    }
                }
            }
        }else{//Time doesn't contain a dot, so we just need the normal parser
            TimeParser::time_from_odv_str_without_millis(odv)
        }
    }

    ///Parses a timestamp from a string with the format of an odv export (yyyy-mm-ddThh:mm:ss.sss)
    pub fn time_from_odv_str(odv: &str) -> Result<Timestamp, Box<dyn Error + Send + Sync>>{
        Ok(NaiveDateTime::parse_from_str(
            odv,
            "%Y-%m-%dT%H:%M:%S.%3f")
            .map(|t| t.timestamp_millis())?)
    }

    ///Parses a timestamp from a string with the format of an odv export (yyyy-mm-ddThh:mm:ss)
    pub fn time_from_odv_str_without_millis(odv: &str) -> Result<Timestamp, Box<dyn Error + Send + Sync>>{
        Ok(NaiveDateTime::parse_from_str(
            odv,
            "%Y-%m-%dT%H:%M:%S")
            .map(|t| t.timestamp_millis())?)
    }

    //Parses a timestamp from several strings containing the constituents of a timestamp
    pub fn time_from_seperate_strs_with(year: &str, month: &str, day: &str, hour: &str, minute: &str, second: &str) -> Result<Timestamp, Box<dyn Error + Send + Sync>>{
        let year: String = year.chars().filter(|c| c.is_numeric()).collect();
        let month: String = month.chars().filter(|c| c.is_numeric()).collect();
        let day: String = day.chars().filter(|c| c.is_numeric()).collect();
        let hour: String = hour.chars().filter(|c| c.is_numeric()).collect();
        let minute: String = minute.chars().filter(|c| c.is_numeric()).collect();
        let second: String = second.chars().filter(|c| c.is_numeric()).collect();
        let date = NaiveDate::from_ymd_opt(year.parse()?, month.parse()?, day.parse()?).ok_or_else(|| format!("Invalid date: {}-{}-{}", year, month, day))?;
        let time = NaiveTime::from_hms_opt(hour.parse()?, minute.parse()?, second.parse()?).ok_or_else(|| format!("Invalid time: {}:{}:{}", hour, minute, second))?;
        Ok(NaiveDateTime::new(date, time).timestamp_millis())
    }

    pub fn time_from_seperate_strs_varying(year: &str, month: &str, day: &str, hour: &str, minute: &str, second: &str, milli: &str) -> Result<Timestamp, Box<dyn Error + Send + Sync>>{
        //Let's first try the cheapest method
        match TimeParser::time_from_seperate_strs_with_millis(year, month, day, hour, minute, second, milli){
            success @ Ok(_) => success,
            Err(_e) => {//That didn't work, maybe the strs are floats
                TimeParser::time_from_seperate_float_strs_with_millis(year, month, day, hour, minute, second, milli)
            }
        }
    }

    //Parses a timestamp from several strings containing the constituents of a timestamp, including milliseconds
    pub fn time_from_seperate_strs_with_millis(year: &str, month: &str, day: &str, hour: &str, minute: &str, second: &str, milli: &str) -> Result<Timestamp, Box<dyn Error + Send + Sync>>{
        let date = NaiveDate::from_ymd_opt(year.parse()?, month.parse()?, day.parse()?).ok_or_else(|| format!("Invalid date: {}-{}-{}", year, month, day))?;
        let time = NaiveTime::from_hms_milli_opt(hour.parse()?, minute.parse()?, second.parse()?, milli.parse()?).ok_or_else(|| format!("Invalid time: {}:{}:{}.{}", hour, minute, second, milli))?;
        Ok(NaiveDateTime::new(date, time).timestamp_millis())
    }

    //Parses a timestamp from several strings containing the constituents of a timestamp, including milliseconds
    pub fn time_from_seperate_float_strs_with_millis(year: &str, month: &str, day: &str, hour: &str, minute: &str, second: &str, milli: &str) -> Result<Timestamp, Box<dyn Error + Send + Sync>>{
        let year = year.trim_end_matches('.');
        let month = month.trim_end_matches('.');
        let day = day.trim_end_matches('.');
        let hour = hour.trim_end_matches('.');
        let minute = minute.trim_end_matches('.');
        let second = second.trim_end_matches('.');
        let date = NaiveDate::from_ymd_opt(year.parse::<f32>()? as i32, month.parse::<f32>()? as u32, day.parse::<f32>()? as u32).ok_or_else(|| format!("Invalid date: {}-{}-{}", year, month, day))?;
        let time = NaiveTime::from_hms_milli_opt(hour.parse::<f32>()? as u32, minute.parse::<f32>()? as u32, second.parse::<f32>()?.min(59.0) /*avoid shift seconds*/ as u32, milli.parse::<f32>()? as u32).ok_or_else(|| format!("Invalid time: {}:{}:{}.{}", hour, minute, second, milli))?;
        Ok(NaiveDateTime::new(date, time).timestamp_millis())
    }

    pub fn time_from_unix_seconds_str(seconds_str: &str) -> Result<Timestamp, Box<dyn Error + Send + Sync>>{
        let time = seconds_str.parse::<f64>()? * 1000f64;
        Ok(time as Timestamp)
    }

    pub fn unix_milli_time_to_iso8601(t: Timestamp) -> String{
        NaiveDateTime::from_timestamp(t / 1000, (t.abs() % 1000) as u32).format("%Y-%m-%d %H:%M:%S").to_string()
    }

    pub fn unix_milli_time_to_iso8601_conforming(t: Timestamp) -> String{
        NaiveDateTime::from_timestamp(t / 1000, (t.abs() % 1000) as u32).format("%Y-%m-%dT%H:%M:%SZ").to_string()
    }

    pub fn timestamp_to_naivedatetime(t: Timestamp) -> NaiveDateTime{
        NaiveDateTime::from_timestamp(t / 1000, (t.abs() % 1000) as u32)
    }

    pub fn duration_string_to_milliseconds(str: &str) -> Result<Timestamp, SyncError>{
        let mut result = 0f64;
        let mut current_number_string = String::new();

        let duration_map = [
            ('d', 24.0 * 60.0 * 60.0 * 1000.0),
            ('h', 60.0 * 60.0 * 1000.0),
            ('m', 60.0 * 1000.0),
            ('s', 1.0 * 1000.0),
            ('w', 7.0 * 24.0 * 60.0 * 60.0 * 1000.0),
            ('y', 365.25 * 24.0 * 60.0 * 60.0 * 1000.0)
        ];

        for char in str.chars(){
            if char.is_whitespace(){
                continue;
            }
            if char.is_numeric(){
                current_number_string.push(char);
            }else{
                if let Some(duration) = duration_map.iter().find(|(name, _value)| name == &char){
                    result += current_number_string.parse::<f64>()? * duration.1;
                    current_number_string.clear();
                }else{
                    return Err(format!("Unknown formatting letter: '{}'", char).into());
                }
            }
        }

        return Ok(result as i64);

    }
}


#[cfg(test)]
mod tests{
    use super::*;

    fn setup(){
        let _ = env_logger::builder().is_test(true).try_init();
    }

    #[test]
    fn test_time_from_iso8601_str(){
        setup();
        let time = TimeParser::time_from_iso8601_str("2020-09-08T11:46:00.00Z").unwrap();
        assert_eq!(time, 1599565560000);
    }

    #[test]
    fn test_broken_iso8601_str_fails(){
        setup();
        let time = TimeParser::time_from_iso8601_str("2020-09-08G11:46:00.00Z");
        assert!(time.is_err())
    }

    #[test]
    fn test_28th_february_iso8601_str_fails(){
        setup();
        let time = TimeParser::time_from_iso8601_str("2018-02-29T11:46:00.00Z");
        assert!(time.is_err())
    }

    #[test]
    fn test_29th_february_2018_iso8601_str_succeeds_with_inconsistent_parser(){
        setup();
        let time = TimeParser::time_from_inconsistent_iso8601_str("2018-02-29T11:46:00.00Z").unwrap();
        assert_eq!(time, 1519904760000);
    }

    
    #[test]
    fn test_misformatted_iso8601_str_succeeds_with_inconsistent_parser(){
        setup();
        let time = TimeParser::time_from_inconsistent_iso8601_str("2018-02-29 11:46:00.00").unwrap();
        assert_eq!(time, 1519904760000);
    }

    #[test]
    fn test_odv_time_format(){
        setup();
        let time = TimeParser::time_from_odv_str("2018-07-25T21:26:25.267").unwrap();
        assert_eq!(time, 1532553985267);
    }

}