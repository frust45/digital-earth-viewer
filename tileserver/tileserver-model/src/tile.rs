//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use std::{
    error::Error,
    hash::{
        Hash,
        Hasher,
    },
};

use crate::{
    Coord,
    UEC,
    UECArea,
    UTM
};

use log::*;
use serde::{
    Serialize,
    Deserialize
};


///The basic concept of digital globe's data model is the tile.
///
///Tiles are given regions of the earth's surface.
///They are recursively defined and can be generated to arbitrary precision levels.
///Each `tile` has bounds and an unambiguous path which can be used to determine the bounds.
#[derive(Debug, Clone, Serialize, Default, Deserialize)]
pub struct Tile{
    pub position: UEC,
    pub size: UEC,
    pub path: String
}
impl Hash for Tile {
    fn hash<H>(&self, state: &mut H) where H: Hasher{
        self.path.hash(state);
    }
}

impl From<&Tile> for (Coord, Coord) {
    ///Returns a tuple of position and extent in degrees.
    fn from(v: &Tile) -> Self {
        let ul: Coord = v.position.into();
        let lr: Coord = (v.position + v.size).into();
        let size = Coord::new(lr.lat - ul.lat, lr.lon - ul.lon);
        (ul, size)
    }
}

impl Tile{

    pub fn suggest_tilesize_two_features_and_pixels_between_them(f1: UEC, f2: UEC, pixels: f64) -> u8{
        let distance_meters = UEC::haversine_distance(&f1, f2);
        let pixel_meters = distance_meters / pixels;
        for i in 1 .. 255{
            if Tile::pixel_size_in_meters_from_path_length(i) < pixel_meters{
                return i;
            }
        }
        return 64; //Default max zoom level
    }

    ///Suggests a maximum zoom level from a UECArea and a size in pixels
    pub fn max_zoom_level_from_area_and_pixelsize(area: UECArea, pixelsize: (u32, u32)) -> u8{
        let x_meters_distance = UEC::haversine_distance(&area.position, UEC::xy(area.position.x + area.extent.x, area.position.y));
        let y_meters_distance = UEC::haversine_distance(&area.position, UEC::xy(area.position.y, area.position.y + area.extent.y));
        let x_meters_per_pixel = x_meters_distance / pixelsize.0 as f64;
        let y_meters_per_pixel = y_meters_distance / pixelsize.1 as f64;
        let min_meters_per_pixel = x_meters_per_pixel.min(y_meters_per_pixel);
        for i in 1 .. 255{
            if Tile::pixel_size_in_meters_from_path_length(i) < min_meters_per_pixel{
                return i;
            }
        }
        return 64; //Default max zoom level
    }

    ///Returns the size of one raster tile pixel at the given path length near the equator.
    ///
    ///Since tiles split adaptively, the return value is not globally valid, but gives a reasonable upper bound to all local values.
    pub fn pixel_size_in_meters_from_path_length(pathlen: u8) -> f64{
        (crate::constants::CIRCUMFERENCE_EQUATOR_METRES / 2f64.powi(pathlen.into())) / (crate::constants::TILESIZE.0 as f64)
    }

    pub fn corner(&self, top: bool, left: bool) -> UEC {
        UEC::xy(
            if left {
                self.position.x 
            } else {
                self.position.x + self.size.x
            },
            if top {
                self.position.y
            } else {
                self.position.y + self.size.y
            }
        )
    }

    pub fn center(&self) -> UEC{
        (self.position + self.size) * 0.5
    }

    pub fn center_pixel_size_meters(&self) -> (f64, f64){
        let top_left = self.center();
        let bottom_right = self.center() + UEC::xy(self.size.x * (1.0 / crate::constants::TILESIZE.0 as f64), self.size.y * (1.0 / crate::constants::TILESIZE.1 as f64));
        let top_left_utm: UTM = top_left.into();
        let bottom_right_utm: UTM = UTM::from_coord_and_zone_info(bottom_right.into(),top_left_utm.zone_number, top_left_utm.zone_letter);
        ((bottom_right_utm.easting - top_left_utm.easting).abs(), (bottom_right_utm.northing - top_left_utm.northing).abs())
    }

    ///Creates a tile from the four components of UEC coordinates.
    ///
    ///`path` has to be manually provided.
    ///Avoid using this function unless you are doing careful optimizations or working on internals of tile functionality.
    pub fn from_xywh(x: f64, y: f64, w: f64, h: f64, path: String) -> Tile{
        Tile{
            position: UEC{
                x,
                y
            },
            size: UEC{
                x: w,
                y: h
            },
            path
        }
    }

    ///Returns a tuple of position, extent in UEC.
    pub fn to_uec_area(&self) -> UECArea {
        UECArea::new(self.position, self.size)
    }

    pub fn covering_area(area: UECArea, max_count: usize, max_depth: u8) -> Vec<Self> {
        let mut result = Vec::new();
        result.push(Tile::from_tilepath("W").unwrap());
        result.push(Tile::from_tilepath("E").unwrap());
        let mut cd = 1;
        while cd < max_depth {
            let tmp: Vec<Tile> = result.iter().flat_map(|t| t.split().into_iter()).filter(|t| {
                UECArea::areas_overlap(area, t.to_uec_area())
            }).collect();
            debug!("{} tiles. (covering_area)", tmp.len());
            if tmp.len() > max_count {
                return result
            } else {
                cd += 1;
                result = tmp
            }
        };
        return result
    }

    ///Gets the northwestern subtile. Do not use this unless you are checking that splitting the tile by four is valid. Use [`split`](./struct.Tile.html#method.split) instead.
    pub fn a(&self) -> Tile{
        Tile::from_xywh(
            self.position.x,
            self.position.y,
            self.size.x / 2.0,
            self.size.y / 2.0,
            self.path.clone() + "A"
        )
    }

    ///Gets the northeastern subtile. Do not use this unless you are checking that splitting the tile by four is valid. Use [`split`](./struct.Tile.html#method.split) instead.
    pub fn b(&self) -> Tile{
        Tile::from_xywh(
            self.position.x + (self.size.x / 2.0),
            self.position.y,
            self.size.x / 2.0,
            self.size.y / 2.0,
            self.path.clone() + "B"
        )
    }

    ///Gets the southwestern subtile. Do not use this unless you are checking that splitting the tile by four is valid. Use [`split`](./struct.Tile.html#method.split) instead.
    pub fn c(&self) -> Tile{
        Tile::from_xywh(
            self.position.x,
            self.position.y + (self.size.y / 2.0),
            self.size.x / 2.0,
            self.size.y / 2.0,
            self.path.clone() + "C"
        )
    }

    ///Gets the southeastern subtile. Do not use this unless you are checking that splitting the tile by four is valid. Use [`split`](./struct.Tile.html#method.split) instead.
    pub fn d(&self) -> Tile{
        Tile::from_xywh(
            self.position.x + (self.size.x / 2.0),
            self.position.y + (self.size.y / 2.0),
            self.size.x / 2.0,
            self.size.y / 2.0,
            self.path.clone() + "D"
        )
    }

    /// Gets the northern subtile. Do not use this unless you are checking that splitting the tile by two is valid. Use [`split`](./struct.Tile.html#method.split) instead.
    pub fn u(&self) -> Tile{
        Tile::from_xywh(
            self.position.x,
            self.position.y,
            self.size.x,
            self.size.y / 2.0,
            self.path.clone() + "U"
        )
    }

    /// Gets the southern subtile. Do not use this unless you are checking that splitting the tile by two is valid. Use [`split`](./struct.Tile.html#method.split) instead.
    pub fn l(&self) -> Tile{
        Tile::from_xywh(
            self.position.x,
            self.position.y + (self.size.y / 2.0),
            self.size.x,
            self.size.y / 2.0, 
            self.path.clone() + "L"
        )
    }

    /// Gets the tile's UEC aspect ratio, corrected for width. Used in conjunction with `model::constants::ASPECT_RATIO_CUTOFF` to determine the splitting mode.
    pub fn aspect_ratio(&self) -> f64{
        let width = 2.0 * self.size.x * ((self.position.y + self.size.y / 2.0) * std::f64::consts::PI).sin();
        let height = self.size.y;
        width / height
    }

    /// Gets this tile's direct subtiles, taking into account the splitting heuristic.
    pub fn split(&self) -> Vec<Tile>{
        if self.aspect_ratio() < crate::constants::ASPECT_RATIO_CUTOFF{
            vec![self.u(), self.l()]
        }else{
            vec![self.a(), self.b(), self.c(), self.d()]
        }
    }

    /// Creates a `Tile` from a path.
    ///
    /// Paths begin with the hemisphere id `E` or `W`, followed by the subdivisions indicated by a string of any of the following: `ABCDUL`.
    /// Paths with invalid splits yield an error message instead of a `Tile`.
    pub fn from_tilepath(tilepath: &str) -> Result<Tile, Box<dyn Error>>{
        if tilepath.is_empty() {return Err("Tile Path Is Empty".into())}
        let mut t = match &tilepath[0..1] {
            "W" => Tile::from_xywh(0.0, 0.0, 0.5, 1.0, "W".to_string()),
            "E" => Tile::from_xywh(0.5, 0.0, 0.5, 1.0, "E".to_string()),
            _ => return Err(format!("Invalid Tile Path Root: {:?}", tilepath).into())
        };

        for c in tilepath[1..].chars(){
            if t.aspect_ratio() < crate::constants::ASPECT_RATIO_CUTOFF{
                match c {
                   'U' => t = t.u(),
                   'L' => t = t.l(),
                   _ => return Err(format!("Invalid Tile Split In Path: {:?}", tilepath).into())
                }
            }else{
                match c {
                    'A' => t = t.a(),
                    'B' => t = t.b(),
                    'C' => t = t.c(),
                    'D' => t = t.d(),
                    _ => return Err(format!("Invalid Tile Split In Path: {:?}", tilepath).into())
                }
            }
        }

        Ok(t)
    }

    ///Generates the aspect ratios for the first 6 split levels for debugging.
    pub fn test_aspect_ratios() -> String{
        let mut basetiles = vec![Tile::from_tilepath("W").unwrap(), Tile::from_tilepath("E").unwrap()];
        let mut s = String::new();
        for _i in 0 .. 6 {
            for t in basetiles.iter(){
                s = s + "\n" + &format!("{}\t{}", t.path, t.aspect_ratio());
            }
            basetiles = basetiles.iter().map(|t| t.split()).flatten().collect();
        }
        s
    }
}


impl PartialEq for Tile{
    fn eq(&self, other: &Self) -> bool {
        self.path == other.path
    }
}

impl Eq for Tile{}