//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use crate::{Color, DataVecType, LinesData, PointsData, Scalar, TileData, ToU8Vec, Vector2D, Vector3D};
use bytes::Bytes;
use serde::{Serialize, Deserialize, Serializer};


///Union type for handling data abstractly.
#[derive(Clone)]
pub enum GeoData{
    ScalarTiles(TileData<Scalar>),
    ColorTiles(TileData<Color>),
    ScalarPoints(PointsData<Scalar>),
    ColorPoints(PointsData<Color>),
    Vector2DPoints(PointsData<Vector2D>),
    Vector3DPoints(PointsData<Vector3D>),
    ScalarLines(LinesData<Scalar>)
    
}

#[derive(Debug, Clone, Copy, Deserialize, Eq, PartialEq)]
pub enum GeoDataType{
    ScalarTiles,
    ColorTiles,
    ScalarPoints,
    ColorPoints,
    Vector2DPoints,
    Vector3DPoints,
    ScalarLines
}

impl From<GeoData> for GeoDataType{
    fn from(gd: GeoData) -> Self {
        match gd{
            GeoData::ScalarTiles(_) => GeoDataType::ScalarTiles,
            GeoData::ColorTiles(_) => GeoDataType::ColorTiles,
            GeoData::ScalarPoints(_) => GeoDataType::ScalarPoints,
            GeoData::ColorPoints(_) => GeoDataType::ColorPoints,
            GeoData::Vector2DPoints(_) => GeoDataType::Vector2DPoints,
            GeoData::Vector3DPoints(_) => GeoDataType::Vector3DPoints,
            GeoData::ScalarLines(_) => GeoDataType::ScalarLines,
        }
    }
}

impl Default for GeoDataType{
    fn default() -> Self {
       GeoDataType::ScalarPoints
    }
}

impl Serialize for GeoDataType{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> where S: Serializer
    {
        serializer.serialize_str(self.as_str())
    }
}

impl GeoDataType{
    pub fn as_str(&self) -> &'static str{
        match self{
            GeoDataType::ScalarTiles => "ScalarTiles",
            GeoDataType::ColorTiles => "ColorTiles",
            GeoDataType::ScalarPoints => "ScalarPoints",
            GeoDataType::ColorPoints => "ColorPoints",
            GeoDataType::Vector2DPoints => "Vector2DPoints",
            GeoDataType::Vector3DPoints => "Vector3DPoints",
            GeoDataType::ScalarLines => "ScalarLines"
        }
    }

    pub fn from_str(str: &str) -> Result<Self, Box<dyn std::error::Error>>{
        match str{
            "ScalarTile" => Ok(GeoDataType::ScalarTiles),
            "ColorTile" => Ok(GeoDataType::ColorTiles),
            "ScalarPoints" => Ok(GeoDataType::ScalarPoints),
            "ColorPoints" => Ok(GeoDataType::ColorPoints),
            "Vector2DPoints" => Ok(GeoDataType::Vector2DPoints),
            "Vector3DPoints" => Ok(GeoDataType::Vector3DPoints),
            "ScalarLines" => Ok(GeoDataType::ScalarLines),
            _ => Err(format!("No GeoDataType called {}", str).into())
        }
    }

    pub fn is_point_type(&self) -> bool{
        match &self {
            GeoDataType::ColorPoints | GeoDataType::ScalarPoints | GeoDataType::Vector2DPoints | GeoDataType::Vector3DPoints | GeoDataType::ScalarLines => true,
            _ => false,
        }
    }

    pub fn rehydrate(&self, bytes: &[u8]) -> GeoData{
        match self{
            GeoDataType::ScalarTiles => GeoData::ScalarTiles(bytes.into()),
            GeoDataType::ColorTiles => GeoData::ColorTiles(bytes.into()),
            GeoDataType::ScalarPoints => GeoData::ScalarPoints(bytes.into()),
            GeoDataType::ColorPoints => GeoData::ColorPoints(bytes.into()),
            GeoDataType::Vector2DPoints => GeoData::Vector2DPoints(bytes.into()),
            GeoDataType::Vector3DPoints => GeoData::Vector3DPoints(bytes.into()),
            GeoDataType::ScalarLines => GeoData::ScalarLines(bytes.into())
        }
    }
}

impl ToU8Vec for GeoData{
    fn to_u8_vec(&self) -> DataVecType{
        match self{
            GeoData::ScalarTiles(d) => d.to_u8_vec(),
            GeoData::ColorTiles(d) => d.to_u8_vec(),
            GeoData::ScalarPoints(d) => d.to_u8_vec(),
            GeoData::ColorPoints(d) => d.to_u8_vec(),
            GeoData::Vector2DPoints(d) => d.to_u8_vec(),
            GeoData::Vector3DPoints(d) => d.to_u8_vec(),
            GeoData::ScalarLines(d) => d.to_u8_vec()
        }
    }
}

impl From<GeoData> for Bytes{
    fn from(gd: GeoData) -> Self {
        match gd{
            GeoData::ScalarTiles(d) => d.into(),
            GeoData::ColorTiles(d) => d.into(),
            GeoData::ScalarPoints(d) => d.into(),
            GeoData::ColorPoints(d) => d.into(),
            GeoData::Vector2DPoints(d) => d.into(),
            GeoData::Vector3DPoints(d) => d.into(),
            GeoData::ScalarLines(d) => d.into()
        }
    }
}