//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use bytes::Bytes;

use crate::{Color, Coord, DataVecType, Position, Scalar, Timestamp, ToU8Vec, ToVector3D, UEC, UECArea, Vector2D, Vector3D};

#[derive(Debug, Clone, Copy)]
pub struct GeoPoint<T>{
    pub pos: Position,
    pub height: f32,
    pub timestamp: Timestamp,
    pub value: T
}

impl<T> GeoPoint<T>{
    pub fn new_from_coord(pos: Coord, height: f32, timestamp: Timestamp, value: T) -> Self{
        GeoPoint{
            pos: pos.into(),
            height,
            timestamp, 
            value
        }
    }

    pub fn new_from_uec(pos: UEC, height: f32, timestamp: Timestamp, value: T) -> Self{
        GeoPoint{
            pos: pos.into(),
            height,
            timestamp, 
            value
        }
    }

    pub fn new_from_position(pos: Position, height: f32, timestamp: Timestamp, value: T) -> Self{
        GeoPoint{
            pos, 
            height, 
            timestamp, 
            value
        }
    }
}



impl<T> ToU8Vec for GeoPoint<T> where T: ToU8Vec{
    fn to_u8_vec(&self) -> DataVecType{
        let mut r = DataVecType::new();
        let uec_pos: UEC = self.pos.into();
        r.extend_from_slice(&(uec_pos.x as f32).to_le_bytes());
        r.extend_from_slice(&(uec_pos.y as f32).to_le_bytes());
        r.extend_from_slice(&(self.height.to_le_bytes()));
        r.extend(self.value.to_u8_vec());
        r.extend_from_slice(&(self.timestamp as f32).to_le_bytes());
        r
    }
}

//Todo: rewrite to be more generic. If time were to come before value, we could just use the From<&[u8]> for the T
impl From<&[u8]> for GeoPoint<Color>{
    fn from(data: &[u8]) -> Self {
        let pos_x = f32::from_le_bytes([data[0], data[1], data[2], data[3]]);
        let pos_y = f32::from_le_bytes([data[4], data[5], data[6], data[7]]);
        let height = f32::from_le_bytes([data[8], data[9], data[10], data[11]]);
        let value: Color = Color::from_rgba(data[12], data[13], data[14], data[15]);
        let timestamp = f32::from_le_bytes([data[16], data[17], data[18], data[19]]);
        GeoPoint::new_from_uec(UEC::xy(pos_x as f64,pos_y as f64), height, timestamp as i64, value)
    }
}

impl From<&[u8]> for GeoPoint<Scalar>{
    fn from(data: &[u8]) -> Self {
        let pos_x = f32::from_le_bytes([data[0], data[1], data[2], data[3]]);
        let pos_y = f32::from_le_bytes([data[4], data[5], data[6], data[7]]);
        let height = f32::from_le_bytes([data[8], data[9], data[10], data[11]]);
        let value = f32::from_le_bytes([data[12], data[13], data[14], data[15]]);
        let timestamp = f32::from_le_bytes([data[16], data[17], data[18], data[19]]);
        GeoPoint::new_from_uec(UEC::xy(pos_x as f64,pos_y as f64), height, timestamp as i64, value)
    }
}

impl From<&[u8]> for GeoPoint<Vector2D>{
    fn from(data: &[u8]) -> Self {
        let x = f32::from_le_bytes([data[0], data[1], data[2], data[3]]);
        let y = f32::from_le_bytes([data[4], data[5], data[6], data[7]]);
        let h = f32::from_le_bytes([data[8], data[9], data[10], data[11]]);
        let v1 = f32::from_le_bytes([data[12], data[13], data[14], data[15]]);
        let v2 = f32::from_le_bytes([data[16], data[17], data[18], data[19]]);
        let t = f32::from_le_bytes([data[20], data[21], data[22], data[23]]);
        GeoPoint::new_from_uec(UEC::xy(x as f64,y as f64), h, t as i64, [v1,v2])
    }
}

impl From<&[u8]> for GeoPoint<Vector3D>{
    fn from(data: &[u8]) -> Self {
        let x = f32::from_le_bytes([data[0], data[1], data[2], data[3]]);
        let y = f32::from_le_bytes([data[4], data[5], data[6], data[7]]);
        let h = f32::from_le_bytes([data[8], data[9], data[10], data[11]]);
        let v1 = f32::from_le_bytes([data[12], data[13], data[14], data[15]]);
        let v2 = f32::from_le_bytes([data[16], data[17], data[18], data[19]]);
        let v3 = f32::from_le_bytes([data[20], data[21], data[22], data[23]]);
        let t = f32::from_le_bytes([data[24], data[25], data[26], data[27]]);
        GeoPoint::new_from_uec(UEC::xy(x as f64,y as f64), h, t as i64, [v1,v2,v3])
    }
}

#[derive(Debug, Clone)]
pub struct PointsData<T> where GeoPoint<T>: ToU8Vec{
    pub points: Vec<GeoPoint<T>>
}

impl<T> From<Vec<GeoPoint<T>>> for PointsData<T> where GeoPoint<T>: ToU8Vec{
    fn from(vec: Vec<GeoPoint<T>>) -> PointsData<T>{
        PointsData{
            points: vec
        }
    }
}

impl<T> From<&[GeoPoint<T>]> for PointsData<T> where GeoPoint<T>: ToU8Vec + Clone{
    fn from(slice: &[GeoPoint<T>]) -> PointsData<T>{
        PointsData{
            points: slice.to_vec()
        }
    }
}

impl<T> PointsData<T> where GeoPoint<T>: ToU8Vec{
    pub fn new(mut points: Vec<GeoPoint<T>>) -> PointsData<T>{
        points.sort_by_key(|p| p.timestamp);
        PointsData{
            points
        }
    }
}

impl<T> From<PointsData<T>> for Bytes where GeoPoint<T>: ToU8Vec, T: Copy{
    fn from(pd: PointsData<T>) -> Self {
        Bytes::from(pd.to_u8_vec().to_vec())
    }
}

impl<T> ToU8Vec for PointsData<T> where GeoPoint<T>: ToU8Vec, T: Copy {
    fn to_u8_vec(&self) -> DataVecType{
        let extent_area = UECArea::area_from_points_iter(self.points.iter().map(|p| p.pos.into())).unwrap_or(UECArea::new(UEC::xy(0.0, 0.0), UEC::xy(1.0,1.0)));
        let offset_point = extent_area.position;
        offset_point.x.to_le_bytes().iter().copied().chain(
            offset_point.y.to_le_bytes().iter().copied()
        ).chain(
            self.points.iter().map(|p| {
                let mut p = *p;
                p.pos = Position::UEC(p.pos.to_uec() - offset_point);
                p
            }).map(|p|
                p.to_u8_vec()
            ).flatten()
        ).collect()
    }
}

impl<'a, T> From<&'a[u8]> for PointsData<T> where T: Sized + ToU8Vec, GeoPoint<T>: From<&'a[u8]>{
    fn from(data: &'a[u8]) -> Self {
        let offset_x = f64::from_le_bytes([data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7]]);
        let offset_y = f64::from_le_bytes([data[8], data[9], data[10], data[11], data[12], data[13], data[14], data[15]]);
        let offset_uec = UEC::xy(offset_x as f64, offset_y as f64);
        PointsData::new(
        data[16..].chunks_exact(std::mem::size_of::<T>() + 16).map(GeoPoint::from).map(|mut p| {
                p.pos = Position::UEC(p.pos.to_uec() + offset_uec);
                p
            }).collect()
        )
    }
}

impl<T> From<&PointsData<T>> for PointsData<Vector3D> where T: ToVector3D + ToU8Vec{
    fn from(points: &PointsData<T>) -> Self{
        PointsData::new(
            points.points.iter().map(|p| 
                GeoPoint::new_from_position(p.pos, p.height, p.timestamp, p.value.to_vector3d())
            ).collect()
        )
    }
}

impl<T> From<PointsData<T>> for PointsData<Vector3D> where T: ToVector3D + ToU8Vec{
    fn from(points: PointsData<T>) -> Self{
        PointsData::new(
            points.points.iter().map(|p| 
                GeoPoint::new_from_position(p.pos, p.height, p.timestamp, p.value.to_vector3d())
            ).collect()
        )
    }
}