//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use smallvec::{SmallVec, ToSmallVec};

use crate::{Color, Scalar, Vector2D, Vector3D};

pub trait ToU8Array{
    fn to_u8_slice(&self) -> &[u8];
}

pub type DataVecType = SmallVec<[u8;32]>;
pub trait ToU8Vec{
    fn to_u8_vec(&self) -> DataVecType;
}

impl ToU8Vec for Scalar{
    fn to_u8_vec(&self) -> DataVecType{
        self.to_le_bytes().to_smallvec()
    }
}

impl ToU8Vec for Vector2D{
    fn to_u8_vec(&self) -> DataVecType {
        self.iter().flat_map(|e| SmallVec::from(e.to_le_bytes())).collect()
    }
}

impl ToU8Vec for Vector3D{
    fn to_u8_vec(&self) -> DataVecType {
        self.iter().flat_map(|e| SmallVec::from(e.to_le_bytes())).collect()
    }
}


impl ToU8Vec for Color{
    fn to_u8_vec(&self) -> DataVecType{
        let a: [u8;4] = self.clone().into();
        a.to_smallvec()
    }
}

impl<T> ToU8Vec for T where T: ToU8Array{
    fn to_u8_vec(&self) -> DataVecType{
        self.to_u8_slice().to_smallvec()
    }
}