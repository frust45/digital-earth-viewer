//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use bytes::Bytes;
use chrono::{TimeZone, Utc};

use crate::{Coord, DataVecType, GeoPoint, PointsData, Position, Scalar, Timestamp, ToU8Vec, UEC, UECArea, Vector3D};

#[derive(Debug, Clone, Copy)]
pub struct GeoLine<T>{
    pub pos: (Position, Position),
    pub height: (f32,f32),
    pub timestamp: (Timestamp, Timestamp),
    pub value: (T,T)
}

impl<T> GeoLine<T>{
    pub fn new_from_coord(pos_start: Coord, height_start: f32, pos_end: Coord, height_end: f32, timestamp_start: Timestamp, timestamp_end: Timestamp, value_start: T, value_end: T) -> Self{
        GeoLine{
            pos: (pos_start.into(), pos_end.into()),
            height: (height_start, height_end),
            timestamp: (timestamp_start, timestamp_end), 
            value: (value_start, value_end)
        }
    }

    pub fn new_from_uec(pos_start: UEC, height_start: f32, pos_end: UEC, height_end: f32, timestamp_start: Timestamp, timestamp_end: Timestamp, value_start: T, value_end: T) -> Self{
        GeoLine{
            pos: (pos_start.into(), pos_end.into()),
            height: (height_start, height_end),
            timestamp: (timestamp_start, timestamp_end), 
            value: (value_start, value_end)
        }
    }

    pub fn new_from_position(pos_start: Position, height_start: f32, pos_end: Position, height_end: f32, timestamp_start: Timestamp, timestamp_end: Timestamp, value_start: T, value_end: T) -> Self{
        GeoLine{
            pos: (pos_start, pos_end),
            height: (height_start, height_end),
            timestamp: (timestamp_start, timestamp_end), 
            value: (value_start, value_end)
        }
    }

    pub fn between_two_points(start_point: GeoPoint<T>, end_point: GeoPoint<T>) -> Self{
        GeoLine{
            pos: (start_point.pos, end_point.pos),
            height: (start_point.height, end_point.height),
            timestamp: (start_point.timestamp, end_point.timestamp),
            value: (start_point.value, end_point.value)
        }
    }

    pub fn center(&self) -> (UEC, f32){
        let center_uec = (self.pos.0.to_uec() + self.pos.1.to_uec()) * 0.5;
        let center_height = (self.height.0 + self.height.1) * 0.5;
        (center_uec, center_height)
    }

    pub fn length(&self) -> f64{
        self.pos.0.distance_meters(&self.pos.1)
    }
}



impl<T> ToU8Vec for GeoLine<T> where T: ToU8Vec{
    fn to_u8_vec(&self) -> DataVecType{
        let mut r = DataVecType::new();
        //First vertex
        r.extend_from_slice(&(self.pos.0.to_uec().x as f32).to_le_bytes());
        r.extend_from_slice(&(self.pos.0.to_uec().y as f32).to_le_bytes());
        r.extend_from_slice(&(self.height.0.to_le_bytes()));
        r.extend(self.value.0.to_u8_vec());
        r.extend_from_slice(&(self.timestamp.0 as f32).to_le_bytes());
        //Second vertex
        r.extend_from_slice(&(self.pos.1.to_uec().x as f32).to_le_bytes());
        r.extend_from_slice(&(self.pos.1.to_uec().y as f32).to_le_bytes());
        r.extend_from_slice(&(self.height.1.to_le_bytes()));
        r.extend(self.value.1.to_u8_vec());
        r.extend_from_slice(&(self.timestamp.1 as f32).to_le_bytes());
        r
    }
}

#[derive(Debug, Clone)]
pub struct LinesData<T> where GeoLine<T>: ToU8Vec{
    pub lines: Vec<GeoLine<T>>
}

impl<T> LinesData<T> where GeoLine<T>: ToU8Vec{
    pub fn new(mut lines: Vec<GeoLine<T>>) -> LinesData<T>{
        lines.sort_by_key(|p| p.timestamp);
        LinesData{
            lines
        }
    }
}

impl<T> From<Vec<GeoLine<T>>> for LinesData<T> where T: ToU8Vec{
    fn from(lines: Vec<GeoLine<T>>) -> Self {
        LinesData{
            lines
        }
    }
}

impl<T> LinesData<T> where GeoLine<T>: ToU8Vec, T: Clone{
    pub fn from_lines(lines: Vec<GeoLine<T>>) -> Self{
        LinesData{
            lines
        }
    }

    pub fn total_length(&self) -> f64{
        self.lines.iter().map(|l| l.length()).sum()
    }

    pub fn average_length(&self) -> f64{
        self.total_length() / self.lines.len() as f64
    }

    ///Creates a new set of lines from a set of points, where between each two consecutive points a line should be drawn
    pub fn from_points(points: Vec<GeoPoint<T>>) -> Self{
        LinesData{
            lines: points.iter().zip(points.iter().skip(1)).map(|(first_point, second_point)| {
                GeoLine::new_from_position(
                    first_point.pos, 
                    first_point.height,
                    second_point.pos, 
                    second_point.height, 
                    first_point.timestamp,
                    second_point.timestamp,
                    first_point.value.clone(), 
                second_point.value.clone()
                )
            }).collect()
        }
    }

    pub fn dump_to_gpx_string(&self) -> String{
        let gpx_header = r#"<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
        <gpx xmlns="http://www.topografix.com/GPX/1/1" version="1.1" creator="Digital Earth Viewer"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">
            <trk>
                <trkseg>"#;

        let gpx_body = self.lines.iter().map(|l| {
            let coord: Coord = l.pos.0.into();
            let time = Utc.timestamp_millis((l.timestamp.0 + l.timestamp.1) / 2).to_rfc3339();
            format!(
            r#"<trkpt lat="{}" lon="{}">
            <ele>{}</ele>
            <time>{}</time>
           </trkpt>"#, coord.lat, coord.lon, l.height.0, time)
            }).fold(String::new(), |everything, next| everything + &next);

        let gpx_footer = r#"</trkseg>
            </trk>
        </gpx>
        "#;

        gpx_header.to_string() + &gpx_body + gpx_footer
    }
}


impl<T> ToU8Vec for LinesData<T> where GeoLine<T>: ToU8Vec + Copy{
    fn to_u8_vec(&self) -> DataVecType {
        let extent_area = UECArea::area_from_points_iter(self.lines.iter().map(|&p| p.pos.0.to_uec())).unwrap_or(UECArea::new(UEC::xy(0.0, 0.0), UEC::xy(1.0,1.0)));
        let offset_point = extent_area.position;
        offset_point.x.to_le_bytes().iter().copied().chain(
            offset_point.y.to_le_bytes().iter().copied()
        ).chain(
            self.lines.iter().map(|l| {
                let mut l = *l;
                l.pos.0 = Position::UEC(l.pos.0.to_uec() - offset_point);
                l.pos.1 = Position::UEC(l.pos.1.to_uec() - offset_point);
                l
            }).map(|l|
                l.to_u8_vec()
            ).flatten()
        ).collect()
    }
}

impl From<LinesData<Scalar>> for PointsData<Vector3D>{
    fn from(lines: LinesData<Scalar>) -> Self {
        PointsData::new(
            lines.lines.iter().map(|l| {
                let tuple = l.pos.0.distance_meter_tuple(&l.pos.1);
                GeoPoint::new_from_position(
                    l.pos.0, 
                    l.height.0, 
                    l.timestamp.0, 
                    [
                        tuple.0 as f32,
                        tuple.1 as f32,
                        l.value.0
                    ]
                )
            }).collect::<Vec<_>>()
        )
    }
}

impl From<&[u8]> for GeoLine<Scalar>{
    fn from(data: &[u8]) -> Self {

        let pos1_x = f32::from_le_bytes([data[0], data[1], data[2], data[3]]);
        let pos1_y = f32::from_le_bytes([data[4], data[5], data[6], data[7]]);
        let height1 = f32::from_le_bytes([data[8], data[9], data[10], data[11]]);
        let value1 = f32::from_le_bytes([data[12], data[13], data[14], data[15]]);
        let timestamp1 = f32::from_le_bytes([data[16], data[17], data[18], data[19]]);

        let pos2_x = f32::from_le_bytes([data[20], data[21], data[22], data[23]]);
        let pos2_y = f32::from_le_bytes([data[24], data[25], data[26], data[27]]);
        let height2 = f32::from_le_bytes([data[28], data[29], data[30], data[31]]);
        let value2 = f32::from_le_bytes([data[32], data[33], data[34], data[35]]);
        let timestamp2 = f32::from_le_bytes([data[36], data[37], data[38], data[39]]);

        GeoLine::new_from_uec(UEC::xy(pos1_x as f64, pos1_y as f64), height1, UEC::xy(pos2_x as f64, pos2_y as f64), height2, timestamp1 as Timestamp, timestamp2 as Timestamp, value1, value2)
    }
}

impl<'a, T> From<&'a [u8]> for LinesData<T> where T: Sized + ToU8Vec, GeoLine<T>: From<&'a[u8]>{
    fn from(data: &'a[u8]) -> Self {
        let offset_x = f64::from_le_bytes([data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7]]);
        let offset_y = f64::from_le_bytes([data[8], data[9], data[10], data[11], data[12], data[13], data[14], data[15]]);
        let offset_uec = UEC::xy(offset_x as f64, offset_y as f64);
        LinesData::new(
        data[16..].chunks_exact((std::mem::size_of::<T>() + 16) * 2).map(GeoLine::from).map(|mut p| {
                p.pos = (Position::UEC(p.pos.0.to_uec() + offset_uec), Position::UEC(p.pos.1.to_uec() + offset_uec));
                p
            }).collect()
        )
    }
}

impl<T> From<LinesData<T>> for Bytes where GeoLine<T>: ToU8Vec, T: Copy{
    fn from(ld: LinesData<T>) -> Self {
        Bytes::from(ld.to_u8_vec().to_vec())
    }
}