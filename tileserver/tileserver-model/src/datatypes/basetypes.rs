//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use rayon::prelude::*;
use serde::{Serialize, Deserialize};
use std::{ops::{Add, Mul}, u8};

///Alias to `f32`.
pub type Scalar = f32;

///Two-component vector of `f32`.
pub type Vector2D = [f32; 2];

///Three-component vector of `f32`.
pub type Vector3D = [f32; 3];

///RGBA byte representation of a color.
#[derive(Copy, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Color ([u8;4]);

impl Color {
    pub fn r(&self) -> u8 {
        self.0[0]
    }

    pub fn g(&self) -> u8 {
        self.0[1]
    }

    pub fn b(&self) -> u8 {
        self.0[2]
    }

    pub fn a(&self) -> u8 {
        self.0[3]
    }

    pub fn from_rgba(r: u8, g: u8, b: u8, a: u8) -> Self {
        Self([r, g, b, a])
    }

    pub fn from_rgb(r: u8, g: u8, b: u8) -> Self {
        Self([r, g, b, 255u8])
    }

    pub fn mix(&self, other: &Color, factor: f32) -> Self{
        let a = ((self.a() as f32 * (1f32-factor)) + (other.a() as f32 * factor)).min(255f32);
        let r = ((self.r() as f32 * (1f32-factor)) + (other.r() as f32 * factor)).min(255f32);
        let g = ((self.g() as f32 * (1f32-factor)) + (other.g() as f32 * factor)).min(255f32);
        let b = ((self.b() as f32 * (1f32-factor)) + (other.b() as f32 * factor)).min(255f32);
        Self([
            r as u8,
            g as u8,
            b as u8,
            a as u8
        ])
    }
}

/*
/// Only scales transparency, useful for mixing colors with `Add`
impl Mul<f32> for Color {
    type Output = Color;
    fn mul(self, rhs: f32) -> Color {
        Color::from_rgba(
            self.0[0],
            self.0[1],
            self.0[2],
            (self.0[3] as f32 * rhs).round().min(255.0).max(0.0) as u8
        )
    }
}

/// Due to possible Saturation, for mixing colors it is strongly recommended to first scale them and then add the scaled values together.
impl Add<Color> for Color {
    type Output = Color;
    fn add(self, rhs: Self) -> Color {
        let a1 = self.0[3] as f32 / 255.0;
        let a2 = rhs.0[3] as f32 / 255.0;
        Color::from_rgba(
            (self.0[0] as f32 * a1 + rhs.0[0] as f32 * a2).round().min(255.0).max(0.0) as u8,
            (self.0[1] as f32 * a1 + rhs.0[1] as f32 * a2).round().min(255.0).max(0.0) as u8,
            (self.0[2] as f32 * a1 + rhs.0[2] as f32 * a2).round().min(255.0).max(0.0) as u8,
            self.0[3].saturating_add(rhs.0[3]),
        )
    }
}*/

/*
/// Alpha-unaware version
impl Mul<f32> for Color {
    type Output = Color;
    fn mul(self, rhs: f32) -> Color {
        Color::from_rgba(
            (self.0[0] as f32 * rhs).round().min(255.0).max(0.0) as u8,
            (self.0[1] as f32 * rhs).round().min(255.0).max(0.0) as u8,
            (self.0[2] as f32 * rhs).round().min(255.0).max(0.0) as u8,
            (self.0[3] as f32 * rhs).round().min(255.0).max(0.0) as u8
        )
    }
}

/// Alpha-unaware version
impl Add<Color> for Color {
    type Output = Color;
    fn add(self, rhs: Self) -> Color {
        Color::from_rgba(
            self.0[0].saturating_add(rhs.0[0]),
            self.0[1].saturating_add(rhs.0[1]),
            self.0[2].saturating_add(rhs.0[2]),
            self.0[3].saturating_add(rhs.0[3])
        )
    }
}
*/

impl WeightedMix for Color {
    fn mix2(a: Self, fa: f32, b: Self, fb: f32) -> Self {
        let fa2 = fa * a.0[3] as f32 / 255.0;
        let fb2 = fb * b.0[3] as f32 / 255.0;
        let x = fa2 + fb2;
        Color::from_rgba(
            (
                a.0[0] as f32 * fa2 / x + 
                b.0[0] as f32 * fb2 / x
            ).round().min(255.0).max(0.0) as u8,
            (
                a.0[1] as f32 * fa2 / x + 
                b.0[1] as f32 * fb2 / x
            ).round().min(255.0).max(0.0) as u8,
            (
                a.0[2] as f32 * fa2 / x + 
                b.0[2] as f32 * fb2 / x
            ).round().min(255.0).max(0.0) as u8,
            (
                a.0[3] as f32 / 2.0 + 
                b.0[3] as f32 / 2.0
            ).round().min(255.0).max(0.0) as u8
        )
    }

    fn mix3(a: Self, fa: f32, b: Self, fb: f32, c: Self, fc: f32) -> Self {
        let fa2 = fa * a.0[3] as f32 / 255.0;
        let fb2 = fb * b.0[3] as f32 / 255.0;
        let fc2 = fc * c.0[3] as f32 / 255.0;
        let x = fa2 + fb2 + fc2;
        Color::from_rgba(
            (
                a.0[0] as f32 * fa2 / x + 
                b.0[0] as f32 * fb2 / x +
                c.0[0] as f32 * fc2 / x
            ).round().min(255.0).max(0.0) as u8,
            (
                a.0[1] as f32 * fa2 / x + 
                b.0[1] as f32 * fb2 / x +
                c.0[1] as f32 * fc2 / x
            ).round().min(255.0).max(0.0) as u8,
            (
                a.0[2] as f32 * fa2 / x + 
                b.0[2] as f32 * fb2 / x +
                c.0[2] as f32 * fc2 / x
            ).round().min(255.0).max(0.0) as u8,
            (
                a.0[3] as f32 / 3.0 + 
                b.0[3] as f32 / 3.0 +
                c.0[3] as f32 / 3.0
            ).round().min(255.0).max(0.0) as u8
        )
    }

    fn mix4(a: Self, fa: f32, b: Self, fb: f32, c: Self, fc: f32, d: Self, fd: f32) -> Self {
        let fa2 = fa * a.0[3] as f32 / 255.0;
        let fb2 = fb * b.0[3] as f32 / 255.0;
        let fc2 = fc * c.0[3] as f32 / 255.0;
        let fd2 = fd * d.0[3] as f32 / 255.0;
        let x = fa2 + fb2 + fc2 + fd2;
        Color::from_rgba(
            (
                a.0[0] as f32 * fa2 / x + 
                b.0[0] as f32 * fb2 / x +  
                c.0[0] as f32 * fc2 / x +
                d.0[0] as f32 * fd2 / x
            ).round().min(255.0).max(0.0) as u8,
            (
                a.0[1] as f32 * fa2 / x + 
                b.0[1] as f32 * fb2 / x +
                c.0[1] as f32 * fc2 / x +
                d.0[1] as f32 * fd2 / x
            ).round().min(255.0).max(0.0) as u8,
            (
                a.0[2] as f32 * fa2 / x + 
                b.0[2] as f32 * fb2 / x +
                c.0[2] as f32 * fc2 / x +
                d.0[2] as f32 * fd2 / x
            ).round().min(255.0).max(0.0) as u8,
            (
                a.0[3] as f32 / 4.0 + 
                b.0[3] as f32 / 4.0 +
                c.0[3] as f32 / 4.0 +
                d.0[3] as f32 / 4.0
            ).round().min(255.0).max(0.0) as u8
        )
    }

}


impl From<[u8;4]> for Color {
    fn from(other: [u8; 4]) -> Self {
        Self(other)
    }
}

impl From<[u8;3]> for Color {
    fn from(other: [u8; 3]) -> Self {
        Self::from_rgb(other[0], other[1], other[2])
    }
}

impl From<Color> for [u8;4] {
    fn from(other: Color) -> Self {
        other.0
    }
}

impl Default for Color {
    fn default() -> Self {
        Self([0u8; 4])
    }
}

pub trait WeightedMix{
    fn mix2(a: Self, fa: f32, b: Self, fb: f32) -> Self;

    fn mix3(a: Self, fa: f32, b: Self, fb: f32, c: Self, fc: f32) -> Self;

    fn mix4(a: Self, fa: f32, b: Self, fb: f32, c: Self, fc: f32, d: Self, fd: f32) -> Self;
}

impl<T> WeightedMix for T where T: Add<T, Output = T> + Mul<f32, Output = T> {
    fn mix2(a: Self, fa: f32, b: Self, fb: f32) -> Self {
        let x = fa + fb;
        a * (fa / x) + 
        b * (fb / x)
    }

    fn mix3(a: Self, fa: f32, b: Self, fb: f32, c: Self, fc: f32) -> Self {
        let x = fa + fb + fc;
        a * (fa / x) +
        b * (fb / x) +
        c * (fc / x)
    }

    fn mix4(a: Self, fa: f32, b: Self, fb: f32, c: Self, fc: f32, d: Self, fd: f32) -> Self {
        let x = fa + fb + fc + fd;
        a * (fa / x) +
        b * (fb / x) +
        c * (fc / x) +
        d * (fd / x)
    }
}

///Guarantees an unambiguous "No Data" value, which can be used to signify elements that do not contain (valid) data.
pub trait Nodata{
    ///Returns the type's "No Data" value. It is not guaranteed to be `eq` to all "No Data" values.
    fn nodata() -> Self;
    ///Checks if a value is "No Data". Use this instead of comparing with `Nodata::nodata()`.
    fn is_nodata(&self) -> bool;
}


impl Nodata for Scalar{
    ///Uses NaN to signify absence of valid data.
    fn nodata() -> Scalar{
        std::f32::NAN
    }

    fn is_nodata(&self) -> bool {
        self.is_nan()
    }
}

impl Nodata for Vector2D{
    ///Uses NaN to signify absence of valid data.
    fn nodata() -> Vector2D{
        [std::f32::NAN, std::f32::NAN]
    }

    fn is_nodata(&self) -> bool {
        self[0].is_nan() && self[1].is_nan()
    }
}

impl Nodata for Vector3D{
    ///Uses NaN to signify absence of valid data.
    fn nodata() -> Vector3D{
        [std::f32::NAN, std::f32::NAN, std::f32::NAN]
    }

    fn is_nodata(&self) -> bool {
        self[0].is_nan() && self[1].is_nan() && self[2].is_nan()
    }
}

impl Nodata for Color{
    ///A black, transparent pixel is easily identified as missing data.
    fn nodata() -> Color{
        Self([0u8,0u8,0u8,0u8])
    }

    fn is_nodata(&self) -> bool {
        self.0[3] == 0
    }
}

///A two-dimensional Vector of two `f64`.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct Point2D{
    pub x: f64,
    pub y: f64
}

impl Point2D{
    pub fn xy(x: f64, y: f64) -> Point2D{
        Point2D{
            x,
            y
        }
    }
    ///Linear scaling of the vector's components by individual scaling factors.
    pub fn scale_xy(&self, x: f64, y: f64) -> Self {
        Point2D{
            x: self.x * x,
            y: self.y * y
        }
    }
}


///Linear scaling of the vector's components.
impl Mul<f64> for &Point2D {
    type Output = Point2D;
    fn mul(self, rhs: f64) -> Point2D{
        Point2D{
            x: self.x * rhs,
            y: self.y * rhs
        }
    }
}

///Linear scaling of the vector's components by individual scaling factors.
impl Mul<(f64, f64)> for &Point2D {
    type Output = Point2D;
    fn mul(self, rhs: (f64, f64)) -> Point2D {
        Point2D{
            x: self.x * rhs.0,
            y: self.y * rhs.1
        }
    }
}

///A three-dimensional vector of `f64`.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct Point3D{
    pub x: f64,
    pub y: f64,
    pub z: f64
}

///A two-dimensional array, stored in a flat `vec` of its type `T`.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Array2D<T>{
    ///The data array contains the concatenation of the data lines. 
    ///The x coordinate specifies the position within the line, the y coordinate specifies the line number.
    pub data: Vec<T>,
    ///Obviously, all lines are equal-length, with their length being the x size. The total number of lines is given by the y size.
    pub size: Point2D
}

impl<T> Array2D<T> where T: Default + Clone + Send + Sync + Copy + Nodata{
    ///Creates an empty array filled with `T`'s "No Data" value.
    pub fn new(size: Point2D) -> Array2D<T>{
        Array2D{
            data: vec![T::nodata(); (size.x * size.y) as usize],
            size
        }
    }


    ///Extracts a subset from self at `offset` of size `subset_size` and resizes it to `target_size`, using nearest-neighbour sampling.
    pub fn resample_subset_to(&self, offset: Point2D, subset_size: Point2D, target_size: Point2D) -> Array2D<T>{
        let mut result = Array2D::new(target_size);

        let result_size = result.size;

        result.data.par_iter_mut().enumerate().for_each(|(i,v)| {
            let result_x = (i as i64 % result_size.x as i64) as i64;
            let result_y = (i as i64 / result_size.x as i64) as i64;
            let source_x = (result_x * subset_size.x as i64) / result_size.x as i64 + offset.x as i64;
            let source_y = (result_y * subset_size.y as i64) / result_size.y as i64 + offset.y as i64;
            if source_x < 0 || source_x > (self.size.x as i64 - 1) ||source_y < 0 || source_y > (self.size.y as i64 - 1){
                *v = T::nodata(); //Set nodata if sample out of sampling area
            }else{
                //Otherwise sample nearest from source
                let source_index = (source_y * self.size.x as i64 ) + source_x;
                *v = self.data[source_index as usize];
            }
        });

        result
    }
}

pub trait ToVector3D{
    fn to_vector3d(&self) -> Vector3D;
}

impl ToVector3D for Color{
    fn to_vector3d(&self) -> Vector3D {
        [self.0[0] as f32 / 255.0, self.0[1] as f32 / 255.0, self.0[2] as f32 / 255.0]
    }
}

impl ToVector3D for Scalar{
    fn to_vector3d(&self) -> Vector3D {
        [*self, std::f32::NAN, std::f32::NAN]
    }
}

impl ToVector3D for Vector2D{
    fn to_vector3d(&self) -> Vector3D {
        [self[0], self[1], std::f32::NAN]
    }
}


///Timestamp: Milliseconds since Unix epoch (1970-1-1 0:00:00.0). Negative values are allowed.
pub type Timestamp = i64;