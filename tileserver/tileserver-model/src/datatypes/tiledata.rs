//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use bytes::Bytes;
use crate::{Array2D, Color, Coord, GeoPoint, Nodata, PointsData, Scalar, Tile, ToU8Array, ToU8Vec, UEC, constants::TILESIZE};
use rayon::prelude::*;

#[derive(Clone)]
pub struct TileData<T> where T: Default + Clone + Send + Sync + Copy + Nodata{
    pub tile: Tile,
    pub data: Box<[T;TILESIZE.0 * TILESIZE.1]>
}

impl<T> TileData<T> where T: Default + Clone + Send + Sync + Copy + Nodata{
    pub fn new(tile: Tile) -> Self{
        tile.into()
    }

    pub fn is_empty(&self) -> bool {
        self.data.is_empty() || self.data.iter().all(|x| x.is_nodata())
    }

    pub fn iter_values_uec<'a>(&'a self) -> impl Iterator<Item = (UEC, &'a T)> {
        let iter = self.data.iter().enumerate().map(move |(i, d)|{
            let x_pos = i % TILESIZE.0;
            let y_pos = i / TILESIZE.0;
            let x = ((x_pos as f64 + 0.5) / (TILESIZE.0 as f64)) * self.tile.size.x;
            let y = ((y_pos as f64 + 0.5) / (TILESIZE.1 as f64)) * self.tile.size.y;
            let pos = self.tile.position + UEC::xy(x, y);
            (pos, d)
        });
        iter
    }

    pub fn value_at_uec(&self, pos: UEC) -> T{
        let offset = pos - self.tile.position;
        let x_pos = (offset.x * (TILESIZE.0 as f64)).round();
        let y_pos = (offset.y * (TILESIZE.1 as f64)).round();
        if x_pos < 0.0 || y_pos < 0.0 || x_pos >= TILESIZE.0 as f64 || y_pos >= TILESIZE.1 as f64{
            T::nodata()
        }else{
            self.data[x_pos as usize + (y_pos as usize * TILESIZE.0)]
        }
    }

    pub fn iter_mut_values_uec<'a>(&'a mut self) -> impl Iterator<Item = (UEC, &'a mut T)> {
        let tile = self.tile.clone();
        let iter = self.data.iter_mut().enumerate().map(move |(i, d)|{
            let x_pos = i % TILESIZE.0;
            let y_pos = i / TILESIZE.0;
            let x = ((x_pos as f64 + 0.5) / (TILESIZE.0 as f64)) * tile.size.x;
            let y = ((y_pos as f64 + 0.5) / (TILESIZE.1 as f64)) * tile.size.y;
            let pos = tile.position + UEC::xy(x, y);
            (pos, d)
        });
        iter
    } 

    pub fn uecs_for_tile(&self) -> impl Iterator<Item = UEC>{
        let tile = self.tile.clone();
        (0 .. TILESIZE.1).into_iter().flat_map(move |y_pos| (0 .. TILESIZE.0).into_iter().map(move |x_pos| (x_pos, y_pos))).map(move |(x_pos, y_pos)| {
            let x = ((x_pos as f64 + 0.5) / (TILESIZE.0 as f64)) * tile.size.x;
            let y = ((y_pos as f64 + 0.5) / (TILESIZE.1 as f64)) * tile.size.y;
            let pos = tile.position + UEC::xy(x, y);
            pos
        })
    }


    pub fn new_from_mut_uec_closure_dyn(tile: Tile, closure: &mut dyn FnMut(UEC) -> T) -> Self{
        let mut td: Self = tile.into();
        td.iter_mut_values_uec().for_each(|(pos, val)| *val = closure(pos));
        td
    }

    pub fn new_from_mut_uec_closure<F>(tile: Tile, mut closure: F) -> Self where F: FnMut(UEC) -> T {
        let mut td: Self = tile.into();
        td.iter_mut_values_uec().for_each(|(pos, val)| *val = closure(pos));
        td
    }

    pub fn new_from_uec_closure_dyn(tile: Tile, closure: &dyn Fn(UEC) -> T) -> Self{
        let mut td: Self = tile.into();
        td.iter_mut_values_uec().for_each(|(pos, val)| *val = closure(pos));
        td
    }

    pub fn new_from_uec_closure<F>(tile: Tile, closure: F) -> Self where F: Fn(UEC) -> T {
        let mut td: Self = tile.into();
        td.iter_mut_values_uec().for_each(|(pos, val)| *val = closure(pos));
        td
    }

    pub fn new_from_coord_closure_dyn(tile: Tile, closure: &dyn Fn(Coord) -> T) -> Self{
        let mut td: Self = tile.into();
        td.iter_mut_values_uec().for_each(|(pos, val)| *val = closure(pos.into()));
        td
    }

    pub fn new_from_coord_closure<F>(tile: Tile, closure: F) -> Self where F: Fn(Coord) -> T {
        let mut td: Self = tile.into();
        td.iter_mut_values_uec().for_each(|(pos, val)| *val = closure(pos.into()));
        td
    }

    pub fn new_from_coord_closure_parallel<F>(tile: Tile, closure: F) -> Self where F: Fn(Coord) -> T + Send + Sync {
        let mut td: Self = tile.into();
        td.iter_mut_values_uec().par_bridge().for_each(|(pos, val)| *val = closure(pos.into()));
        td
    }

    /// Somewhat unsafe, since the array might not contain the required data
    pub fn new_from_tile_and_array_2d(tile: Tile, array: Array2D<T>) -> Self{
        let mut td: Self = tile.into();
        td.data.iter_mut().enumerate().for_each(|(i,v)| *v = *array.data.get(i).unwrap_or(&T::nodata()));
        td
    }

    pub fn new_from_tile_and_raw_data(tile: Tile, data: Vec<T>) -> Self{
        let mut td: Self = tile.into();
        td.data.iter_mut().enumerate().for_each(|(i,v)| *v = *data.get(i).unwrap_or(&T::nodata()));
        td
    }

    pub fn sample(&self, x_index: usize, y_index: usize) -> GeoPoint<T>{
        let value = self.data[x_index + (y_index * TILESIZE.0)];
        let x = ((x_index as f64) / (TILESIZE.0 as f64)) * self.tile.size.x;
        let y = ((y_index as f64) / (TILESIZE.1 as f64)) * self.tile.size.y;
        let pos = self.tile.position + UEC::xy(x, y);
        GeoPoint::new_from_uec(pos, 0f32, 0, value)
    }

    pub fn sample_value_uec(&self, uec: UEC) -> Option<&T>{
        let relative_position = (uec - self.tile.position).normalize();
        let relative_index_x = ((relative_position.x / self.tile.size.x) * TILESIZE.0 as f64) as usize;
        let relative_index_y = ((relative_position.y / self.tile.size.y) * TILESIZE.1 as f64) as usize;
        self.data.get((relative_index_y * TILESIZE.0) + relative_index_x)
    }
}

impl<T> From<Tile> for TileData<T> where T: Default + Clone + Send + Sync + Copy + Nodata{
    fn from(tile: Tile) -> Self {
        TileData{
            tile,
            data: Box::new([T::nodata(); TILESIZE.0 * TILESIZE.1])
        }
    }
}

impl<T> From<TileData<T>> for Bytes where T: Default + Clone + Send + Sync + Copy + Nodata{
    fn from(td: TileData<T>) -> Self {
        //Convert box of td.data into a vec of u8, which then serves as backing storage for a Bytes-struct
        let from = td.data;
        let mut boxed_slice = std::mem::ManuallyDrop::new(from);
        let len = boxed_slice.len() * std::mem::size_of::<T>();
        let cap = boxed_slice.len() * std::mem::size_of::<T>();
        let p = boxed_slice.as_mut_ptr();
        let rebuilt: Vec<u8> = unsafe{
            Vec::from_raw_parts(p as *mut u8, len, cap)
        };
        Bytes::from(rebuilt)
    }
}

//TODO UNSOUND needs lifetime bound so returned slice doesn't outlive tile data
impl<T> ToU8Array for TileData<T> where T: Default + Clone + Send + Sync + Copy + Nodata{
    fn to_u8_slice<'a>(&'a self) -> &'a [u8]{
        unsafe{
            std::slice::from_raw_parts(
                self.data.as_ptr() as *const u8,
                self.data.len() * std::mem::size_of::<T>())
        }
    }
}

impl From<&[u8]> for TileData<Scalar>{
    fn from(data: &[u8]) -> Self {
        let data: Vec<f32> = data.chunks_exact(4).map(|chunk| f32::from_le_bytes([chunk[0], chunk[1], chunk[2], chunk[3]])).collect();
        TileData::new_from_tile_and_raw_data(Tile::from_tilepath("W").unwrap(), data)
    }
}

impl From<&[u8]> for TileData<Color>{
    fn from(data: &[u8]) -> Self {
        let data: Vec<Color> = data.chunks_exact(4).map(|chunk| Color::from_rgba(chunk[0], chunk[1], chunk[2], chunk[3])).collect();
        TileData::new_from_tile_and_raw_data(Tile::from_tilepath("W").unwrap(), data)
    }
}

impl<T> From<&TileData<T>> for PointsData<T> where T: ToU8Vec + Default + Clone + Send + Sync + Copy + Nodata{
    fn from(tiledata: &TileData<T>) -> Self {
        PointsData::new(
            (0.. tiledata.data.len()).into_iter().map(|i| {
                let x_index = i % TILESIZE.0;
                let y_index = i / TILESIZE.1;
                let value = tiledata.data[i];
                let x = ((x_index as f64) / (TILESIZE.0 as f64)) * tiledata.tile.size.x;
                let y = ((y_index as f64) / (TILESIZE.1 as f64)) * tiledata.tile.size.y;
                let pos = tiledata.tile.position + UEC::xy(x, y);
                GeoPoint::new_from_uec(pos, 0f32, 0, value)
            }).collect()
        )
    }
}

impl<T> From<TileData<T>> for PointsData<T> where T: ToU8Vec + Default + Clone + Send + Sync + Copy + Nodata{
    fn from(tiledata: TileData<T>) -> Self {
        PointsData::new(
            (0.. tiledata.data.len()).into_iter().map(|i| {
                let x_index = i % TILESIZE.0;
                let y_index = i / TILESIZE.1;
                let value = tiledata.data[i];
                let x = ((x_index as f64) / (TILESIZE.0 as f64)) * tiledata.tile.size.x;
                let y = ((y_index as f64) / (TILESIZE.1 as f64)) * tiledata.tile.size.y;
                let pos = tiledata.tile.position + UEC::xy(x, y);
                GeoPoint::new_from_uec(pos, 0f32, 0, value)
            }).collect()
        )
    }
}