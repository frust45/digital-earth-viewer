//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use serde::{Serialize, Deserialize};

use crate::UTM;

#[derive(Debug, Clone, Copy, Serialize, Deserialize, Default, PartialEq)]
pub struct Coord{
    ///The normalized longitude.
    pub lat: f64,
    ///The normalized latitude.
    pub lon: f64
}
impl Coord{
    pub fn new(lat: f64, lon: f64) -> Self{
        Coord{lat, lon}
    }
}

impl From<UTM> for Coord{
    fn from(utm: UTM) -> Self {
        const A_CONST:f64 = 6378137.0;
        const ECC_SQUARED: f64 = 0.00669438;
        let e1 = (1.0 - (1.0 - ECC_SQUARED).sqrt()) / (1.0 + (1.0 - ECC_SQUARED).sqrt());
        let x = utm.easting - 500000.0; //remove 500,000 meter offset for longitude
        let mut y = utm.northing;
        let zone_number = utm.zone_number;
        let zone_letter = utm.zone_letter;

        if 'N' != zone_letter{
            y -= 10000000.0;
        }

        let long_origin = (zone_number as f64 - 1.0) * 6.0 - 180.0 + 3.0;

        let ecc_prime_squared = (ECC_SQUARED) / (1.0 - ECC_SQUARED);

        let m = y / 0.9996;
        let mu = m / (A_CONST * (1.0 - ECC_SQUARED / 4.0 - 3.0 * ECC_SQUARED * ECC_SQUARED / 64.0 - 5.0 * ECC_SQUARED * ECC_SQUARED * ECC_SQUARED / 256.0));

        let phi1_rad = mu + (3.0 * e1 / 2.0 - 27.0 * e1 * e1 * e1 / 32.0) * (2.0 * mu).sin()
                + (21.0 * e1 * e1 / 16.0 - 55.0 * e1 * e1 * e1 * e1 / 32.0) * (4.0 * mu).sin()
                + (151.0 * e1 * e1 * e1 / 96.0) * (6.0 * mu).sin();

        let n1 = A_CONST / (1.0 - ECC_SQUARED * phi1_rad.sin().powi(2)).sqrt();
        let t1 = phi1_rad.tan().powi(2);
        let c1 = ecc_prime_squared * phi1_rad.cos().powi(2);
        let r1 = A_CONST * (1.0 - ECC_SQUARED) / (1.0 - ECC_SQUARED * phi1_rad.sin().powi(2)).powf(1.5);
        let d = x / (n1 * 0.9996);

        let lat = phi1_rad - (n1 * phi1_rad.tan() / r1) * (d * d / 2.0 - (5.0 + 3.0 * t1 + 10.0 * c1 - 4.0 * c1 * c1 - 9.0 * ecc_prime_squared) * d * d * d * d / 24.0
                + (61.0 + 90.0 * t1 + 298.0 * c1 + 45.0 * t1 * t1 - 252.0 * ecc_prime_squared - 3.0 * c1 * c1) * d * d * d * d * d * d / 720.0);

        let long = (d - (1.0 + 2.0 * t1 + c1) * d * d * d / 6.0 + (5.0 - 2.0 * c1 + 28.0 * t1 - 3.0 * c1 * c1 + 8.0 * ecc_prime_squared + 24.0 * t1 * t1)
                * d * d * d * d * d / 120.0) / phi1_rad.cos();
        Coord{
            lat: lat.to_degrees(),
            lon: long_origin + long.to_degrees()
        }
    }
}