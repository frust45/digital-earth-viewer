//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use serde::{Serialize, Deserialize};
use crate::{Coord, Point2D, Position, UTM};

//Unified earth coordinates: map lon lat to 0..1, 0..1
//UEC0/0 is Lon -180, Lat 90

///Unified Earth Coordinates.
///
///The entire latitude and longitude ranges are remapped to [0, 1].
///UEC0/0 is equivalent to 180°W, 90°N, UEC1/1 is equivalent to 180°E, 90°S.
///0°N 0°E is represented by UEC0.5/0.5.
#[derive(Debug, Clone, Copy, Serialize, Deserialize, Default)]
pub struct UEC{
    ///The normalized longitude.
    pub x: f64,
    ///The normalized latitude.
    pub y: f64
}

impl PartialEq for UEC{
    fn eq(&self, other: &Self) -> bool {
        self.x == other.x && self.y == other.y
    }
}

impl Eq for UEC{}




impl From<UTM> for UEC{
    fn from(utm: UTM) -> Self {
        let coord: Coord = utm.into();
        coord.into()
    }
}

///A UEC area is given by its northwestern corner and its dimensions.
//pub type UECArea = (UEC, UEC);
#[derive(Debug, Copy, Clone, Serialize, Deserialize, Default)]
pub struct UECArea{
    pub position: UEC,
    pub extent: UEC
}

impl UECArea{
    pub fn new(position: UEC, extent: UEC) -> UECArea{
        UECArea{
            position, 
            extent
        }
    }

    ///Checks wether two UEC areas overlap.
    pub fn areas_overlap(a: UECArea, b: UECArea) -> bool{
        a.position.x < b.position.x + b.extent.x && 
        b.position.x < a.position.x + a.extent.x &&
        a.position.y < b.position.y + b.extent.y &&
        b.position.y < a.position.y + a.extent.y
    }

    pub fn new_from_point(p: UEC) -> UECArea{
        UECArea{
            position: p,
            extent: UEC::xy(0.0, 0.0)
        }
    }

    pub fn extend_area_with_point(area: UECArea, point: UEC) -> UECArea{
        let nw = area.position;
        let sw = area.position + area.extent;

        let nw_x_new = nw.x.min(point.x);
        let nw_y_new = nw.y.min(point.y);

        let sw_x_new = sw.x.max(point.x);
        let sw_y_new = sw.y.max(point.y);

        let nw = UEC::xy(nw_x_new, nw_y_new);
        let sw = UEC::xy(sw_x_new, sw_y_new);

        UECArea::new(nw, sw - nw)
    }

    pub fn extend_area_with_area(&self, area: UECArea) -> UECArea{
        UECArea::extend_area_with_point(
            UECArea::extend_area_with_point(*self, area.position),
            area.position + area.extent
        )
    }

    pub fn area_from_two_points(p1: UEC, p2: UEC) -> UECArea{
        UECArea::extend_area_with_point(
        UECArea::new_from_point(p1),
        p2)
    }

    pub fn area_from_points_iter(mut iter: impl Iterator<Item=UEC>) -> Option<UECArea>{
        if let Some(initial_point) = iter.next(){
            let mut area = UECArea::new_from_point(initial_point);
            for point in iter{
                area = UECArea::extend_area_with_point(area, point)
            }
            return Some(area);
        }
        None
    }
}


impl UEC{
    ///Constructor.
    pub fn xy(mut x: f64, mut y: f64) -> UEC{
        if y < 0f64 {
            x += 0.5f64;
            y = y.fract();
        }else if y > 1f64 {
            x -= 0.5f64;
            y = 1f64 - y.fract();
        }

        if x > 1f64 {
            x = x.fract();
        }else if x < 0f64 {
            x = 1f64 - x.fract();
        }

        UEC{
            x,
            y
        }
    }

    ///Squared distance in UEC to the other point as a UEC fraction. 
    pub fn uec_distance_squared(&self, other: UEC) -> f64{
        (self.x - other.x).powi(2) + (self.y - other.y).powi(2)
    }

    ///Moves a point by an amount of meters to the east and to the north
    pub fn move_by_meters(&self, east: f64, north: f64) -> UEC{
        Position::UEC(*self).move_by_meters(east, -north).to_uec().normalize()
    }

    pub fn normalize(&self) -> Self{
        UEC::xy(self.x, self.y)
    }

    ///Calculates the haversine distance in meters between two UEC coordinates
    #[allow(non_snake_case)]
    pub fn haversine_distance(&self, other: UEC) -> f64{
        let self_coord: Coord = (*self).into();
        let other_coord: Coord = other.into();
        let lat1 = self_coord.lat;
        let lat2 = other_coord.lat;
        let lon1 = self_coord.lon;
        let lon2 = other_coord.lon;
        //Function from https://www.movable-type.co.uk/scripts/latlong.html
        const R: f64 = 6371e3; // metres
        let φ1: f64 = lat1 * std::f64::consts::PI/180.0; // φ, λ in radians
        let φ2 = lat2 * std::f64::consts::PI/180.0;
        let Δφ = (lat2-lat1) * std::f64::consts::PI/180.0;
        let Δλ = (lon2-lon1) * std::f64::consts::PI/180.0;
        let a = (Δφ/2.0).sin() * (Δφ/2.0).sin() +
          (φ1).cos() * (φ2).cos() *
          (Δλ/2.0).sin() * (Δλ/2.0).sin();
        let c = 2.0 * a.sqrt().atan2((1.0-a).sqrt());
        R * c // in metres
    }

    //Calculates the distance in meters on the flat earth (equirect style). This is an approximation that works especially well for small distances, i.e. a few meters
    pub fn equirect_distance(&self, other: UEC) -> f64{
        let self_coord: Coord = (*self).into();
        let other_coord: Coord = other.into();
        let lat1 = self_coord.lat;
        let lat2 = other_coord.lat;
        let lon1 = self_coord.lon;
        let lon2 = other_coord.lon;
        //Function from https://www.movable-type.co.uk/scripts/latlong.html
        const R: f64 = 6371e3; // metres
        let φ1: f64 = lat1.to_radians(); // φ, λ in radians
        let φ2 = lat2.to_radians();
        let λ1: f64 = lon1.to_radians();
        let λ2: f64 = lon2.to_radians();
        let x = (λ2-λ1) * ((φ1+φ2)/2.0).cos();
        let y = φ2-φ1;
        (x*x + y*y).sqrt() * R
    }

    //Calculates the distance in meters on the flat earth (equirect style). This is an approximation that works especially well for small distances, i.e. a few meters
    pub fn equirect_distance_with_heights(&self, other: UEC, my_height: f64, their_height: f64) -> f64{
        let self_coord: Coord = (*self).into();
        let other_coord: Coord = other.into();
        let lat1 = self_coord.lat;
        let lat2 = other_coord.lat;
        let lon1 = self_coord.lon;
        let lon2 = other_coord.lon;
        //Function from https://www.movable-type.co.uk/scripts/latlong.html
        const R: f64 = 6371e3; // metres
        let φ1: f64 = lat1.to_radians(); // φ, λ in radians
        let φ2 = lat2.to_radians();
        let λ1: f64 = lon1.to_radians();
        let λ2: f64 = lon2.to_radians();
        let x = (λ2-λ1) * ((φ1+φ2)/2.0).cos();
        let y = φ2-φ1;
        (x.powi(2) + y.powi(2) + (their_height - my_height).sqrt()).sqrt() * R
    }

    

    ///Checks wether `self` is contained in `area`.
    pub fn in_area(&self, area: UECArea) -> bool {
        self.x > area.position.x && self.x <= (area.position + area.extent).x &&
        self.y > area.position.y && self.y <= (area.position + area.extent).y
    }

    ///Returns the coordinates as a `Point2D`.
    pub fn to_point_2d(&self) -> Point2D{
        Point2D{
            x: self.x,
            y: self.y
        }
    }
}

impl From<Coord> for UEC{
    ///Performs the coordinate transformation.
    fn from(c: Coord) -> UEC{
        let wlat = c.lat.min(90.0).max(-90.0);
        let mut wlon = c.lon;
        while wlon > 180.0 {wlon -= 360.0;}
        while wlon < -180.0 {wlon += 360.0;}
        UEC{
            x: (wlon / 360.0) + 0.5,
            y: -(wlat / 180.0) + 0.5
        }
    }
}

impl Into<Coord> for UEC{
    ///Performs the coordinate transformation.
    fn into(self) -> Coord{
        Coord::new(
            90.0 - (self.y * 180.0),
            (self.x * 360.0) - 180.0
        )
    }
}

///Shorthand for addition of UEC coordinates. No wrapping, no bounds checks.
impl std::ops::Add for UEC{
    type Output = UEC;
    fn add(self, other: UEC) -> Self::Output{
        UEC{
            x: self.x + other.x,
            y: self.y + other.y
        }
    }
}

impl std::ops::Mul<f64> for UEC{
    type Output = UEC;
    fn mul(self, other: f64) -> Self::Output{
        UEC{
            x: self.x * other,
            y: self.y * other
        }
    }
}

///Shorthand for subtraction of UEC coordinates. No wrapping, no bounds checks.
impl std::ops::Sub for UEC{
    type Output = UEC;
    fn sub(self, other: UEC) -> Self::Output{
        UEC{
            x: self.x - other.x,
            y: self.y - other.y
        }
    }
}