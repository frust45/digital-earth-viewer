//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

//UTM implementation from https://git.geomar.de/valentin-buck/graphs/-/blob/master/UTMCoordinate.cs
//currently only implemented for WGS 84
use serde::{Serialize, Deserialize};

use crate::{Coord, UEC};

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq)]
pub struct UTM{
    pub easting: f64,
    pub northing: f64,
    pub zone_number: u8,
    pub zone_letter: char
}

impl UTM{
    pub fn new(easting: f64, northing: f64, zone_number: u8, zone_letter: char) -> UTM{
        UTM{
            easting,
            northing,
            zone_number,
            zone_letter
        }
    }

    pub fn move_by_meters(&self, east: f64, north: f64) -> Self{
        let mut utm = *self;
        utm.easting += east;
        utm.northing += north;
        utm
    }

    pub fn distance_meters(&self, other: UTM) -> f64{
        ((self.easting - other.easting).powi(2) + (self.northing - other.northing).powi(2)).sqrt()
    }

    fn get_utm_letter_designator(latitude: f64) -> char
        {
            if (72.0..=84.0).contains(&latitude){
                'X'
            }else if (64.0..72.0).contains(&latitude){
                'W'
            }else if (56.0..64.0).contains(&latitude){
                'V'
            }else if (48.0..56.0).contains(&latitude){
                'U'
            }else if (40.0..48.0).contains(&latitude){
                'T'
            }else if (32.0..40.0).contains(&latitude){
                'S'
            }else if (24.0..32.0).contains(&latitude){
                'R'
            }else if (16.0..24.0).contains(&latitude){
                'Q'
            }else if (8.0..16.0).contains(&latitude){
                'P'
            }else if (0.0..8.0).contains(&latitude){
                'N'
            }else if (-8.0..0.0).contains(&latitude){
                'M'
            }else if (-16.0..-8.0).contains(&latitude){
                'L'
            }else if (-24.0..-16.0).contains(&latitude){
                'K'
            }else if (-32.0..-24.0).contains(&latitude){
                'J'
            }else if (-40.0..-32.0).contains(&latitude){
                'H'
            }else if (-48.0..-40.0).contains(&latitude){
                'G'
            }else if (-56.0..-48.0).contains(&latitude){
                'F'
            }else if (-64.0..-56.0).contains(&latitude){
                'E'
            }else if (-72.0..-64.0).contains(&latitude){
                'D'
            }else if (-80.0..-72.0).contains(&latitude){
                'C'
            }else{
                'Z'
            }
        }

        #[allow(clippy::many_single_char_names)]
        pub fn from_coord_and_zone_info(coord: Coord, zone_number: u8, zone_letter: char) -> Self {
            const A_CONST:f64 = 6378137.0;
            const ECC_SQUARED: f64 = 0.00669438;
            let latrad = coord.lat.to_radians();
            let lonrad = coord.lon.to_radians();
            
    
            let long_origin = (zone_number as f64 - 1.0) * 6.0 - 180.0 + 3.0;  //+3 puts origin in middle of zone
            let long_origin_rad = long_origin.to_radians();
    
            let utm_zone = zone_letter;
    
            let ecc_prime_squared = (ECC_SQUARED) / (1.0 - ECC_SQUARED);
    
            let n = A_CONST / (1.0 - ECC_SQUARED * latrad.sin() * latrad.sin()).sqrt();
            let t = latrad.tan().powi(2);
            let c = ecc_prime_squared * latrad.cos() * latrad.cos();
            let a = latrad.cos() * (lonrad - long_origin_rad);
    
            let m = A_CONST * ((1.0 - ECC_SQUARED / 4.0 - 3.0 * ECC_SQUARED * ECC_SQUARED / 64.0 - 5.0 * ECC_SQUARED * ECC_SQUARED * ECC_SQUARED / 256.0) * latrad
                    - (3.0 * ECC_SQUARED / 8.0 + 3.0 * ECC_SQUARED * ECC_SQUARED / 32.0 + 45.0 * ECC_SQUARED * ECC_SQUARED * ECC_SQUARED / 1024.0) * (2.0 * latrad).sin()
                    + (15.0 * ECC_SQUARED * ECC_SQUARED / 256.0 + 45.0 * ECC_SQUARED * ECC_SQUARED * ECC_SQUARED / 1024.0) * (4.0 * latrad).sin()
                    - (35.0 * ECC_SQUARED * ECC_SQUARED * ECC_SQUARED / 3072.0) * (6.0 * latrad).sin());
    
            let utm_easting = 0.9996 * n * (a + (1.0 - t + c) * a * a * a / 6.0
                    + (5.0 - 18.0 * t + t * t + 72.0 * c - 58.0 * ecc_prime_squared) * a * a * a * a * a / 120.0)
                    + 500000.0;
    
            let mut utm_northing = 0.9996 * (m + n * latrad.tan() * (a * a / 2.0 + (5.0 - t + 9.0 * c + 4.0 * c * c) * a * a * a * a / 24.0
                    + (61.0 - 58.0 * t + t * t + 600.0 * c - 330.0 * ecc_prime_squared) * a * a * a * a * a * a / 720.0));
    
            if coord.lat < 0.0{
                utm_northing += 10000000.0;
            }
    
            UTM{
                easting: utm_easting,
                northing: utm_northing,
                zone_number,
                zone_letter: utm_zone
            }
        }
}

impl From<UEC> for UTM{
    fn from(uec: UEC) -> Self{
        let coord: Coord = uec.into();
        coord.into()
    }
}

impl From<Coord> for UTM{
    #[allow(clippy::many_single_char_names)]
    fn from(coord: Coord) -> Self {
        const A_CONST:f64 = 6378137.0;
        const ECC_SQUARED: f64 = 0.00669438;
        let mut zone_number: u8;
        let long_temp = coord.lon;
        let latrad = coord.lat.to_radians();
        let lonrad = coord.lon.to_radians();
        if ((8.0..=13.0).contains(&long_temp) && (54.5 .. 58.0).contains(&coord.lat)) 
        || ((56.0 .. 64.0).contains(&coord.lat) && (3.0 .. 12.0).contains(&long_temp)){
                zone_number = 32;
        }else{
            zone_number = (((long_temp + 180.0) / 6.0) + 1.0) as u8;
            if coord.lat >= 72.0 && coord.lat < 84.0{
                if (0.0..9.0).contains(&long_temp){
                    zone_number = 31;
                }else if (9.0..21.0).contains(&long_temp){
                    zone_number = 33;
                }else if (21.0..33.0).contains(&long_temp){
                    zone_number = 35;
                }else if (33.0..42.0).contains(&long_temp){
                    zone_number = 37;
                }
            }
        }

        let long_origin = (zone_number as f64 - 1.0) * 6.0 - 180.0 + 3.0;  //+3 puts origin in middle of zone
        let long_origin_rad = long_origin.to_radians();

        let utm_zone = UTM::get_utm_letter_designator(coord.lat);

        let ecc_prime_squared = (ECC_SQUARED) / (1.0 - ECC_SQUARED);

        let n = A_CONST / (1.0 - ECC_SQUARED * latrad.sin() * latrad.sin()).sqrt();
        let t = latrad.tan().powi(2);
        let c = ecc_prime_squared * latrad.cos() * latrad.cos();
        let a = latrad.cos() * (lonrad - long_origin_rad);

        let m = A_CONST * ((1.0 - ECC_SQUARED / 4.0 - 3.0 * ECC_SQUARED * ECC_SQUARED / 64.0 - 5.0 * ECC_SQUARED * ECC_SQUARED * ECC_SQUARED / 256.0) * latrad
                - (3.0 * ECC_SQUARED / 8.0 + 3.0 * ECC_SQUARED * ECC_SQUARED / 32.0 + 45.0 * ECC_SQUARED * ECC_SQUARED * ECC_SQUARED / 1024.0) * (2.0 * latrad).sin()
                + (15.0 * ECC_SQUARED * ECC_SQUARED / 256.0 + 45.0 * ECC_SQUARED * ECC_SQUARED * ECC_SQUARED / 1024.0) * (4.0 * latrad).sin()
                - (35.0 * ECC_SQUARED * ECC_SQUARED * ECC_SQUARED / 3072.0) * (6.0 * latrad).sin());

        let utm_easting = 0.9996 * n * (a + (1.0 - t + c) * a * a * a / 6.0
                + (5.0 - 18.0 * t + t * t + 72.0 * c - 58.0 * ecc_prime_squared) * a * a * a * a * a / 120.0)
                + 500000.0;

        let mut utm_northing = 0.9996 * (m + n * latrad.tan() * (a * a / 2.0 + (5.0 - t + 9.0 * c + 4.0 * c * c) * a * a * a * a / 24.0
                + (61.0 - 58.0 * t + t * t + 600.0 * c - 330.0 * ecc_prime_squared) * a * a * a * a * a * a / 720.0));

        if coord.lat < 0.0{
            utm_northing += 10000000.0;
        }

        UTM{
            easting: utm_easting,
            northing: utm_northing,
            zone_number,
            zone_letter: utm_zone
        }
    }
}