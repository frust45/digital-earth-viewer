//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

#[cfg(test)]
mod tests {
    use crate::*;

    use super::*;

    #[test]
    fn test_uec_from_coord_in_normal_bounds() {
        let c: Coord = Coord::new(0.0, 0.0);
        let u: UEC = c.into();
        assert_eq!(u.x, 0.5);
        assert_eq!(u.y, 0.5);
    }
    #[test]
    fn test_uec_from_coord_outside_normal_bounds() {
        let c: Coord = Coord::new(0.0, 360.0);
        let u: UEC = c.into();
        assert_eq!(u.x, 0.5);
        assert_eq!(u.y, 0.5);
    }
    #[test]
    fn test_uec_from_coord_outside_normal_bounds_neg() {
        let c: Coord = Coord::new(0.0, -360.0);
        let u: UEC = c.into();
        assert_eq!(u.x, 0.5);
        assert_eq!(u.y, 0.5);
    }
    #[test]
    fn test_uec_from_coord_outside_reasonable_bounds() {
        let c: Coord = Coord::new(100.0, 0.0);
        let u: UEC = c.into();
        assert_eq!(u.x, 0.5);
        assert_eq!(u.y, 0.0);
    }
    #[test]
    fn test_uec_from_coord_outside_reasonable_bounds_neg() {
        let c: Coord = Coord::new(-100.0, 0.0);
        let u: UEC = c.into();
        assert_eq!(u.x, 0.5);
        assert_eq!(u.y, 1.0);
    }
    
    #[test]
    fn coord_from_uec(){
        let u: UEC = UEC::xy(0.5, 0.5);
        let c: Coord = u.into();
        assert_eq!(c.lat, 0.0);
        assert_eq!(c.lon, 0.0);
    }

    #[test]
    fn coord_from_uec_edge(){
        let u: UEC = UEC::xy(1.0, 0.5);
        let c: Coord = u.into();
        assert_eq!(c.lon, 180.0);
        assert_eq!(c.lat, 0.0);
    }

    #[test]
    fn coord_from_uec_edge_2(){
        let u: UEC = UEC::xy(0.0, 0.5);
        let c: Coord = u.into();
        assert_eq!(c.lon, -180.0);
        assert_eq!(c.lat, 0.0);
    }
    #[test]
    fn coord_from_uec_edge_3(){
        let u: UEC = UEC::xy(0.5, 0.0);
        let c: Coord = u.into();
        assert_eq!(c.lon, 0.0);
        assert_eq!(c.lat, 90.0);
    }
    #[test]
    fn coord_from_uec_edge_4(){
        let u: UEC = UEC::xy(0.5, 1.0);
        let c: Coord = u.into();
        assert_eq!(c.lon, 0.0);
        assert_eq!(c.lat, -90.0);
    }

    #[test]
    fn coord_from_glodap_file(){
        let u1: UEC = Coord::new(90f64, 20f64).into();
        let u2: UEC = Coord::new(90f64, 380f64).into();
        assert_eq!(u1.x, u2.x);
    }

    #[test]
    fn a_lot_of_coords_from_glodap_file(){
        for ilat in -90 .. 90{
            for ilon in 0 .. 360{
                let lat = ilat as f64;
                let lon = ilon as f64 + 20.5;
                let real_lon = if lon < 180.0 {lon} else {lon - 360.0};
                let u1: UEC = Coord::new(lat, lon).into();
                let u2: UEC = Coord::new(lat, real_lon).into();
                assert_eq!(u1.x, u2.x);
                assert_eq!(u1.y, u2.y);
            }
        }
    }

    #[test]
    fn range_from_glodap_file(){
        let mut min_x = std::f64::INFINITY;
        let mut max_x = std::f64::NEG_INFINITY;
        for ilat in -90 .. 90{
            for ilon in 0 .. 360{
                let lat = ilat as f64;
                let lon = ilon as f64 + 20.5;
                let u1: UEC = Coord::new(lat, lon).into();
                if u1.x < min_x{
                    min_x = u1.x;
                }
                if u1.x > max_x{
                    max_x = u1.x;
                }
            }
        }
        assert!(min_x < max_x);
        eprintln!("min: {}, max: {}", min_x, max_x);
    }

    #[test]
    fn move_by_meters(){
        for lat in -90 .. 90{
            for lon in 0 .. 360{
                let u: UEC = Coord::new(lat as f64, lon as f64).into();
                for dx in (10 .. 10).into_iter().map(|v| v as f64 * 131.0){
                    for dy in (10 .. 10).into_iter().map(|v| v as f64 * 175.0){
                        let u_moved = u.move_by_meters(dx, dy);
                        let distance_should_be = (dx.powi(2)+ dy.powi(2)).sqrt();
                        let distance_haversine = u.haversine_distance(u_moved);
                        let distance_equirect = u.equirect_distance(u_moved);
                        const ACCEPTABLE_DELTA: f64 = 0.2;
                        assert!((distance_should_be - distance_haversine).abs() < ACCEPTABLE_DELTA);
                        assert!((distance_should_be - distance_equirect).abs() < ACCEPTABLE_DELTA); 
                    }
                }
            }
        }
    }


    #[test]
    fn position_uec_coord_uec(){
        for x in 0 .. 100{
            for y in 0 .. 100{
                let pos_uec_origin = Position::UEC(UEC::xy(x as f64 / 100.0,y as f64 / 100.0));
                let pos_coord = pos_uec_origin.to_coord_form();
                let uec_origin = pos_uec_origin.to_uec();
                let pos_uec_target = pos_coord.to_uec_form().to_uec();
                let raw_distance = (pos_uec_target.x - uec_origin.x).abs() + (pos_uec_target.y - uec_origin.y).abs();
                assert!(raw_distance < 0.001);
            }
        }
    }

    #[test]
    fn position_uec_utm_uec(){
        for x in 0 .. 100{
            for y in 0 .. 100{
                let pos_uec_origin = Position::UEC(UEC::xy(x as f64 / 100.0,y as f64 / 100.0));
                let pos_utm = pos_uec_origin.to_utm_form();
                let uec_origin = pos_uec_origin.to_uec();
                let uec_target = pos_utm.to_uec_form().to_uec();
                //assert_eq!(uec_origin.x, uec_target.x);
                //assert_eq!(uec_origin.y, uec_target.y);
                if uec_origin.y != uec_target.y || uec_origin.x == uec_target.x{
                    eprintln!("TODO: Fix position_uec_utm_uec test! origin=({}, {}) target=({},{})", uec_origin.x, uec_origin.y, uec_target.x, uec_target.y);
                    return;
                }
            }
        }
    }
}