//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use crate::{Coord, UEC, UTM};

#[derive(Copy, Clone, Debug)]
pub enum Position{
    UEC(UEC),
    Coord(Coord),
    UTM(UTM)
}


impl Position{
    pub fn new_uec(x: f64, y: f64) -> Self{
        Position::UEC(UEC::xy(x,y))
    }

    pub fn new_coord(lat: f64, lon: f64) -> Self{
        Position::Coord(Coord::new(lat, lon))
    }

    pub fn new_utm(easting: f64, northing: f64, zone_letter: char, zone_number: u8) -> Self{
        Position::UTM(UTM::new(easting, northing, zone_number, zone_letter))
    }

    pub fn to_uec_form(&self) -> Self{
        match self{
            uec @ Position::UEC(_) => *uec,
            Position::Coord(coord) => Position::UEC((*coord).into()),
            Position::UTM(utm) => Position::UEC((*utm).into())
        }
    }

    pub fn to_coord_form(&self) -> Self{
        match self{
            Position::UEC(uec) => Position::Coord((*uec).into()),
            coord @ Position::Coord(_) => *coord,
            Position::UTM(utm) => Position::Coord((*utm).into())
        }
    }

    pub fn to_utm_form(&self) -> Self{
        match self{
            Position::UEC(uec) => Position::UTM((*uec).into()),
            Position::Coord(coord) => Position::UTM((*coord).into()),
            utm @ Position::UTM(_) => *utm
        }
    }

    /*pub fn move_by_meters(&self, east: f64, north: f64) -> Self{
        match self{
            Position::UTM(utm) => Position::UTM(utm.move_by_meters(east, north)),
            other => other.to_utm_form().move_by_meters(east, north)
        }
    }*/

    //I don't think this has very good accuracy
    pub fn move_by_meters(&self, east: f64, south: f64) -> Self{
        match self{
            Position::UEC(uec) => {
                let offset_x = east / (crate::constants::CIRCUMFERENCE_EQUATOR_METRES * (uec.y * std::f64::consts::PI).sin());
                let offset_y = south / (crate::constants::CIRCUMFERENCE_EQUATOR_METRES / 2.0);
                Position::UEC(*uec + UEC::xy(offset_x, offset_y))
            },
            other => other.to_uec_form().move_by_meters(east, south)
        }
    }

    pub fn distance_meters(&self, other: &Position) -> f64{
        match(self, other){
            (Position::UEC(uec1), Position::UEC(uec2)) => uec1.haversine_distance(*uec2),
            (Position::UTM(utm1), Position::UTM(utm2)) => utm1.distance_meters(*utm2),
            (p1 @ Position::UEC(_), p2) => p1.distance_meters(&p2.to_uec_form()),
            (p1, p2 @ Position::UEC(_)) => p1.to_uec_form().distance_meters(p2),
            (p1 @ Position::UTM(_), p2) => p1.distance_meters(&p2.to_utm_form()),
            (p1, p2 @ Position::UTM(_)) => p1.to_utm_form().distance_meters(p2),
            (p1, p2) => p1.to_uec_form().distance_meters(&p2.to_uec_form())
        }
    }

    pub fn distance_meter_tuple(&self, other: &Position) -> (f64, f64){
        match (self, other){
            (Position::UEC(p1), Position::UEC(p2)) => {
                let difference = *p2 - *p1;
                let avg_y = (p1.y + p2.y) / 2.0;
                let meters_x = difference.x * (crate::constants::CIRCUMFERENCE_EQUATOR_METRES * (avg_y * std::f64::consts::PI).sin());
                let meters_y = difference.y * (crate::constants::CIRCUMFERENCE_EQUATOR_METRES / 2.0);
                (meters_x, meters_y)
            },
            (different1, different2) => (different1.to_uec_form().distance_meter_tuple(&different2.to_uec_form()))
        }
    }

    pub fn to_uec(&self) -> UEC{
        match self{
            Position::UEC(uec) => *uec,
            other => other.to_uec_form().to_uec()
        }
    }

    pub fn to_coord(&self) -> Coord{
        match self{
            Position::Coord(coord) => *coord,
            other => other.to_coord_form().to_coord()
        }
    }

    pub fn to_utm(&self) -> UTM{
        match self{
            Position::UTM(utm) => *utm,
            other => other.to_utm_form().to_utm()
        }
    }

}

impl From<UEC> for Position{
    fn from(uec: UEC) -> Self {
        Position::UEC(uec)
    }
}

impl From<Coord> for Position{
    fn from(coord: Coord) -> Self {
        Position::Coord(coord)
    }
}

impl From<UTM> for Position{
    fn from(utm: UTM) -> Self {
        Position::UTM(utm)
    }
}

impl Into<UEC> for Position{
    fn into(self) -> UEC {
        match self{
            Position::UEC(uec) => uec,
            other => other.to_uec_form().into()
        }
    }
}

impl Into<Coord> for Position{
    fn into(self) -> Coord {
        match self{
            Position::Coord(coord) => coord,
            other => other.to_coord_form().into()
        }
    }
}

impl Into<UTM> for Position{
    fn into(self) -> UTM {
        match self{
            Position::UTM(utm) => utm,
            other => other.to_utm_form().into()
        }
    }
}

impl PartialEq for Position{
    fn eq(&self, other: &Self) -> bool {
        match (self, other){
            (Position::UEC(uec1), Position::UEC(uec2)) => uec1 == uec2,
            (Position::UTM(utm1), Position::UTM(utm2)) => utm1 == utm2,
            (Position::Coord(coord1), Position::Coord(coord2)) => coord1 == coord2,
            (uec @Position::UEC(_), other) => uec.eq(&other.to_uec_form()),
            (utm @Position::UTM(_), other) => utm.eq(&other.to_utm_form()),
            (coord @ Position::Coord(_), other) => coord.eq(&other.to_coord_form())
        }
    }
}