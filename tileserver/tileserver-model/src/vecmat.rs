//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

/* Basic Rust VecMat
 
Copyright 2020 FLEMMING STÄBLER

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#![allow(dead_code)]
use std::ops::*;

#[derive(Copy, Clone, PartialEq, Debug)]
pub struct Vec2 {
    pub x1: f64,
    pub x2: f64,
}
impl Vec2 {
    pub fn new(x1: f64, x2: f64) -> Vec2 {
        Vec2 { x1: x1, x2: x2 }
    }

    pub fn empty() -> Vec2 {
        Vec2 { x1: 0.0, x2: 0.0 }
    }
    #[inline(always)]
    pub fn dot(self, rhs: Self) -> f64 {
        self.x1 * rhs.x1 + self.x2 * rhs.x2
    }
    #[inline(always)]
    pub fn cross(self, rhs: Self) -> f64 {
        self.x1 * rhs.x2 - self.x2 * rhs.x1
    }
    #[inline(always)]
    pub fn abs(self) -> f64 {
        (self.x1 * self.x1 + self.x2 * self.x2).sqrt()
    }
    #[inline(always)]
    pub fn norm(self) -> Self {
        self * (1.0 / self.abs())
    }
}

impl Mul<f64> for Vec2 {
    type Output = Self;
    fn mul(self, rhs: f64) -> Self {
        Vec2::new(self.x1 * rhs, self.x2 * rhs)
    }
}
impl Add for Vec2 {
    type Output = Self;
    fn add(self, rhs: Self) -> Self {
        Vec2::new(self.x1 + rhs.x1, self.x2 + rhs.x2)
    }
}
impl Sub for Vec2 {
    type Output = Self;
    fn sub(self, rhs: Self) -> Self {
        Vec2::new(self.x1 - rhs.x1, self.x2 - rhs.x2)
    }
}
impl Neg for Vec2 {
    type Output = Self;
    fn neg(self) -> Self {
        Vec2::new(-self.x1, -self.x2)
    }
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub struct Vec3 {
    pub x1: f64,
    pub x2: f64,
    pub x3: f64,
}
impl Vec3 {
    pub fn new(x1: f64, x2: f64, x3: f64) -> Vec3 {
        Vec3 {
            x1: x1,
            x2: x2,
            x3: x3,
        }
    }

    pub fn empty() -> Vec3 {
        Vec3 {
            x1: 0.0,
            x2: 0.0,
            x3: 0.0,
        }
    }

    pub fn from_fv(x1: f64, x2x3: Vec2) -> Vec3 {
        Vec3 {
            x1: x1,
            x2: x2x3.x1,
            x3: x2x3.x2,
        }
    }
    pub fn from_vf(x1x2: Vec2, x3: f64) -> Vec3 {
        Vec3 {
            x1: x1x2.x1,
            x2: x1x2.x2,
            x3: x3,
        }
    }

    #[inline(always)]
    pub fn dot(self, rhs: Self) -> f64 {
        self.x1 * rhs.x1 + self.x2 * rhs.x2 + self.x3 * rhs.x3
    }

    #[inline(always)]
    pub fn cross(self, rhs: Self) -> Self {
        Vec3 {
            x1: self.x2 * rhs.x3 - self.x3 * rhs.x2,
            x2: self.x3 * rhs.x1 - self.x1 * rhs.x3,
            x3: self.x1 * rhs.x2 - self.x2 * rhs.x1,
        }
    }
    #[inline(always)]
    pub fn dyadic(self, rhs: Self) -> Mat3 {
        Mat3 {
            a11: self.x1 * rhs.x1,
            a12: self.x1 * rhs.x2,
            a13: self.x1 * rhs.x3,
            a21: self.x2 * rhs.x1,
            a22: self.x2 * rhs.x2,
            a23: self.x2 * rhs.x3,
            a31: self.x3 * rhs.x1,
            a32: self.x3 * rhs.x2,
            a33: self.x3 * rhs.x3,
        }
    }

    #[inline(always)]
    pub fn abs(self) -> f64 {
        (self.x1 * self.x1 + self.x2 * self.x2 + self.x3 * self.x3).sqrt()
    }

    #[inline(always)]
    pub fn norm(self) -> Self {
        self * (1.0 / self.abs())
    }
}
impl Mul<f64> for Vec3 {
    type Output = Self;
    fn mul(self, rhs: f64) -> Self {
        Vec3::new(self.x1 * rhs, self.x2 * rhs, self.x3 * rhs)
    }
}
impl Add for Vec3 {
    type Output = Self;
    #[inline]
    fn add(self, rhs: Self) -> Self {
        Vec3::new(self.x1 + rhs.x1, self.x2 + rhs.x2, self.x3 + rhs.x3)
    }
}
impl Sub for Vec3 {
    type Output = Self;
    #[inline]
    fn sub(self, rhs: Self) -> Self {
        Vec3::new(self.x1 - rhs.x1, self.x2 - rhs.x2, self.x3 - rhs.x3)
    }
}
impl Neg for Vec3 {
    type Output = Self;
    fn neg(self) -> Self {
        Vec3::new(-self.x1, -self.x2, -self.x3)
    }
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub struct Vec4 {
    pub x1: f64,
    pub x2: f64,
    pub x3: f64,
    pub x4: f64,
}
impl Vec4 {
    pub fn new(r: f64, g: f64, b: f64, a: f64) -> Vec4 {
        Vec4 {
            x1: r,
            x2: g,
            x3: b,
            x4: a,
        }
    }

    pub fn empty() -> Vec4 {
        Vec4 {
            x1: 0.0,
            x2: 0.0,
            x3: 0.0,
            x4: 0.0,
        }
    }

    pub fn from_vff(rg: Vec2, b: f64, a: f64) -> Vec4 {
        Vec4 {
            x1: rg.x1,
            x2: rg.x2,
            x3: b,
            x4: a,
        }
    }

    pub fn from_vf(rgb: Vec3, a: f64) -> Vec4 {
        Vec4 {
            x1: rgb.x1,
            x2: rgb.x2,
            x3: rgb.x3,
            x4: a,
        }
    }

    pub fn from_fvf(r: f64, gb: Vec2, a: f64) -> Vec4 {
        Vec4 {
            x1: r,
            x2: gb.x1,
            x3: gb.x2,
            x4: a,
        }
    }

    pub fn from_ffv(r: f64, g: f64, ba: Vec2) -> Vec4 {
        Vec4 {
            x1: r,
            x2: g,
            x3: ba.x1,
            x4: ba.x2,
        }
    }

    pub fn from_fv(r: f64, gba: Vec3) -> Vec4 {
        Vec4 {
            x1: r,
            x2: gba.x1,
            x3: gba.x2,
            x4: gba.x3,
        }
    }

    pub fn from_vv(rg: Vec2, ba: Vec2) -> Vec4 {
        Vec4 {
            x1: rg.x1,
            x2: rg.x2,
            x3: ba.x1,
            x4: ba.x2,
        }
    }

    pub fn dot(self, rhs: Self) -> f64 {
        self.x1 * rhs.x1 + self.x2 * rhs.x2 + self.x3 * rhs.x3 + self.x4 * rhs.x4
    }

    pub fn abs(self) -> f64 {
        (self.x1 * self.x1 + self.x2 * self.x2 + self.x3 * self.x3 + self.x4 * self.x4).sqrt()
    }

    pub fn norm(self) -> Self {
        self * (1.0 / self.abs())
    }
}
impl Mul<f64> for Vec4 {
    type Output = Self;
    fn mul(self, rhs: f64) -> Self {
        Vec4::new(self.x1 * rhs, self.x2 * rhs, self.x3 * rhs, self.x4 * rhs)
    }
}
impl Add for Vec4 {
    type Output = Self;
    fn add(self, rhs: Self) -> Self {
        Vec4::new(
            self.x1 + rhs.x1,
            self.x2 + rhs.x2,
            self.x3 + rhs.x3,
            self.x4 + rhs.x4,
        )
    }
}
impl Sub for Vec4 {
    type Output = Self;
    fn sub(self, rhs: Self) -> Self {
        Vec4::new(
            self.x1 - rhs.x1,
            self.x2 - rhs.x2,
            self.x3 - rhs.x3,
            self.x4 - rhs.x4,
        )
    }
}
impl Neg for Vec4 {
    type Output = Self;
    fn neg(self) -> Self {
        Vec4::new(-self.x1, -self.x2, -self.x3, -self.x4)
    }
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub struct Mat2 {
    a11: f64,
    a12: f64,
    a21: f64,
    a22: f64,
}
impl Mat2 {
    pub fn new(a11: f64, a12: f64, a21: f64, a22: f64) -> Mat2 {
        Mat2 {
            a11,
            a12,
            a21,
            a22
        }
    }

    pub fn from_cols(a1: Vec2, a2: Vec2) -> Mat2 {
        Mat2 {
            a11: a1.x1,
            a12: a2.x1,
            a21: a1.x2,
            a22: a2.x2,
        }
    }

    pub fn from_lins(a1: Vec2, a2: Vec2) -> Mat2 {
        Mat2 {
            a11: a1.x1,
            a12: a1.x2,
            a21: a2.x1,
            a22: a2.x2,
        }
    }

    pub fn from_slice(s: &[f64]) -> Option<Self> {
        Some(Self{
            a11: *s.get(0)?,
            a12: *s.get(1)?,
            a21: *s.get(2)?,
            a22: *s.get(3)?,
        })
    }

    pub fn rot(phi: f64) -> Mat2 {
        Mat2 {
            a11: phi.cos(),
            a12: -(phi.sin()),
            a21: phi.sin(),
            a22: phi.cos(),
        }
    }

    pub fn identity() -> Mat2 {
        Mat2 {
            a11: 1.0,
            a12: 0.0,
            a21: 0.0,
            a22: 1.0,
        }
    }

    #[inline(always)]
    pub fn transpose(self) -> Self {
        Mat2 {
            a11: self.a11,
            a12: self.a21,
            a21: self.a12,
            a22: self.a22,
        }
    }

    #[inline(always)]
    pub fn det(self) -> f64 {
        self.a11 * self.a22 - self.a12 * self.a21
    }

    #[inline(always)]
    pub fn adj(self) -> Self {
        Mat2 {
            a11: self.a22,
            a12: -self.a12,
            a21: -self.a21,
            a22: self.a11,
        }
    }

    pub fn inverse(self) -> Option<Self> {
        if self.det() == 0.0 {
            None
        } else {
            Some(self.adj() * (1.0 / self.det()))
        }
    }
}
impl Mul<f64> for Mat2 {
    type Output = Self;
    fn mul(self, rhs: f64) -> Self {
        Mat2 {
            a11: self.a11 * rhs,
            a12: self.a12 * rhs,
            a21: self.a21 * rhs,
            a22: self.a22 * rhs,
        }
    }
}
impl Mul<Vec2> for Mat2 {
    type Output = Vec2;
    #[inline(always)]
    fn mul(self, rhs: Vec2) -> Vec2 {
        Vec2 {
            x1: self.a11 * rhs.x1 + self.a12 * rhs.x2,
            x2: self.a21 * rhs.x1 + self.a22 * rhs.x2,
        }
    }
}
impl Mul<Mat2> for Mat2 {
    type Output = Self;
    #[inline(always)]
    fn mul(self, rhs: Self) -> Self {
        Mat2 {
            a11: self.a11 * rhs.a11 + self.a12 * rhs.a21,
            a12: self.a11 * rhs.a12 + self.a12 * rhs.a22,
            a21: self.a21 * rhs.a11 + self.a22 * rhs.a21,
            a22: self.a21 * rhs.a12 + self.a22 * rhs.a22,
        }
    }
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub struct Mat3 {
    a11: f64,
    a12: f64,
    a13: f64,
    a21: f64,
    a22: f64,
    a23: f64,
    a31: f64,
    a32: f64,
    a33: f64,
}
impl Mat3 {
    pub fn from_cols(a1: Vec3, a2: Vec3, a3: Vec3) -> Mat3 {
        Mat3 {
            a11: a1.x1,
            a12: a2.x1,
            a13: a3.x1,
            a21: a1.x2,
            a22: a2.x2,
            a23: a3.x2,
            a31: a1.x3,
            a32: a2.x3,
            a33: a3.x3,
        }
    }
    pub fn from_lins(a1: Vec3, a2: Vec3, a3: Vec3) -> Mat3 {
        Mat3 {
            a11: a1.x1,
            a12: a1.x2,
            a13: a1.x3,
            a21: a2.x1,
            a22: a2.x2,
            a23: a2.x3,
            a31: a3.x1,
            a32: a3.x2,
            a33: a3.x3,
        }
    }

    pub fn from_slice(s: &[f64]) -> Option<Self> {
        Some(Self{
            a11: *s.get(0)?,
            a12: *s.get(1)?,
            a13: *s.get(2)?,
            a21: *s.get(3)?,
            a22: *s.get(4)?,
            a23: *s.get(5)?,
            a31: *s.get(6)?,
            a32: *s.get(7)?,
            a33: *s.get(8)?,
        })
    }

    pub fn identity() -> Mat3 {
        Mat3 {
            a11: 1.0,
            a12: 0.0,
            a13: 0.0,
            a21: 0.0,
            a22: 1.0,
            a23: 0.0,
            a31: 0.0,
            a32: 0.0,
            a33: 1.0,
        }
    }

    pub fn rot(phi: f64, axis: Vec3) -> Mat3 {
        let s = phi.sin();
        let c = phi.cos();
        let c1 = 1.0 - c;
        let n = axis.norm();
        Mat3 {
            a11: n.x1 * n.x1 * c1 + c,
            a12: n.x1 * n.x2 * c1 - n.x3 * s,
            a13: n.x1 * n.x3 * c1 + n.x2 * s,
            a21: n.x2 * n.x1 * c1 + n.x3 * s,
            a22: n.x2 * n.x2 * c1 + c,
            a23: n.x2 * n.x3 * c1 - n.x1 * s,
            a31: n.x3 * n.x1 * c1 - n.x2 * s,
            a32: n.x3 * n.x2 * c1 + n.x1 * s,
            a33: n.x3 * n.x3 * c1 + c,
        }
    }

    pub fn translate(self, offset: Vec3) -> Mat4 {
        Mat4 {
            a11: self.a11,
            a12: self.a12,
            a13: self.a13,
            a14: offset.x1,
            a21: self.a21,
            a22: self.a22,
            a23: self.a23,
            a24: offset.x2,
            a31: self.a31,
            a32: self.a32,
            a33: self.a33,
            a34: offset.x3,
            a41: 0.0,
            a42: 0.0,
            a43: 0.0,
            a44: 1.0,
        }
    }

    pub fn extend(self) -> Mat4 {
        Mat4 {
            a11: self.a11,
            a12: self.a12,
            a13: self.a13,
            a14: 0.0,
            a21: self.a21,
            a22: self.a22,
            a23: self.a23,
            a24: 0.0,
            a31: self.a31,
            a32: self.a32,
            a33: self.a33,
            a34: 0.0,
            a41: 0.0,
            a42: 0.0,
            a43: 0.0,
            a44: 1.0,
        }
    }

    #[inline(always)]
    pub fn transpose(self) -> Self {
        Mat3 {
            a11: self.a11,
            a12: self.a21,
            a13: self.a31,
            a21: self.a12,
            a22: self.a22,
            a23: self.a32,
            a31: self.a13,
            a32: self.a23,
            a33: self.a33,
        }
    }

    #[inline(always)]
    pub fn det(self) -> f64 {
        self.a11
            * Mat2 {
                a11: self.a22,
                a12: self.a23,
                a21: self.a32,
                a22: self.a33,
            }
            .det()
            - self.a21
                * Mat2 {
                    a11: self.a12,
                    a12: self.a13,
                    a21: self.a32,
                    a22: self.a33,
                }
                .det()
            + self.a31
                * Mat2 {
                    a11: self.a12,
                    a12: self.a13,
                    a21: self.a22,
                    a22: self.a23,
                }
                .det()
    }

    #[inline(always)]
    pub fn adj(self) -> Self {
        Mat3 {
            a11: Mat2 {
                a11: self.a22,
                a12: self.a23,
                a21: self.a32,
                a22: self.a33,
            }
            .det(),
            a12: -Mat2 {
                a11: self.a21,
                a12: self.a23,
                a21: self.a31,
                a22: self.a33,
            }
            .det(),
            a13: Mat2 {
                a11: self.a21,
                a12: self.a22,
                a21: self.a31,
                a22: self.a32,
            }
            .det(),
            a21: -Mat2 {
                a11: self.a12,
                a12: self.a13,
                a21: self.a32,
                a22: self.a33,
            }
            .det(),
            a22: Mat2 {
                a11: self.a11,
                a12: self.a13,
                a21: self.a31,
                a22: self.a33,
            }
            .det(),
            a23: -Mat2 {
                a11: self.a11,
                a12: self.a12,
                a21: self.a31,
                a22: self.a32,
            }
            .det(),
            a31: Mat2 {
                a11: self.a12,
                a12: self.a13,
                a21: self.a22,
                a22: self.a23,
            }
            .det(),
            a32: -Mat2 {
                a11: self.a11,
                a12: self.a13,
                a21: self.a21,
                a22: self.a23,
            }
            .det(),
            a33: Mat2 {
                a11: self.a11,
                a12: self.a12,
                a21: self.a21,
                a22: self.a22,
            }
            .det(),
        }
        .transpose()
    }

    pub fn inverse(self) -> Option<Self> {
        if self.det() == 0.0 {
            None
        } else {
            Some(self.adj() * (1.0 / self.det()))
        }
    }
}
impl Mul<f64> for Mat3 {
    type Output = Self;
    #[inline(always)]
    fn mul(self, rhs: f64) -> Self {
        Mat3 {
            a11: self.a11 * rhs,
            a12: self.a12 * rhs,
            a13: self.a13 * rhs,
            a21: self.a21 * rhs,
            a22: self.a22 * rhs,
            a23: self.a23 * rhs,
            a31: self.a31 * rhs,
            a32: self.a32 * rhs,
            a33: self.a33 * rhs,
        }
    }
}
impl Mul<Vec3> for Mat3 {
    type Output = Vec3;
    #[inline(always)]
    fn mul(self, rhs: Vec3) -> Vec3 {
        Vec3 {
            x1: self.a11 * rhs.x1 + self.a12 * rhs.x2 + self.a13 * rhs.x3,
            x2: self.a21 * rhs.x1 + self.a22 * rhs.x2 + self.a23 * rhs.x3,
            x3: self.a31 * rhs.x1 + self.a32 * rhs.x2 + self.a33 * rhs.x3,
        }
    }
}
impl Mul<Mat3> for Mat3 {
    type Output = Self;
    #[inline(always)]
    fn mul(self, rhs: Self) -> Self {
        Mat3 {
            a11: self.a11 * rhs.a11 + self.a12 * rhs.a21 + self.a13 * rhs.a31,
            a12: self.a11 * rhs.a12 + self.a12 * rhs.a22 + self.a13 * rhs.a32,
            a13: self.a11 * rhs.a13 + self.a12 * rhs.a23 + self.a13 * rhs.a33,
            a21: self.a21 * rhs.a11 + self.a22 * rhs.a21 + self.a23 * rhs.a31,
            a22: self.a21 * rhs.a12 + self.a22 * rhs.a22 + self.a23 * rhs.a32,
            a23: self.a21 * rhs.a13 + self.a22 * rhs.a23 + self.a23 * rhs.a33,
            a31: self.a31 * rhs.a11 + self.a32 * rhs.a21 + self.a33 * rhs.a31,
            a32: self.a31 * rhs.a12 + self.a32 * rhs.a22 + self.a33 * rhs.a32,
            a33: self.a31 * rhs.a13 + self.a32 * rhs.a23 + self.a33 * rhs.a33,
        }
    }
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub struct Mat4 {
    a11: f64,
    a12: f64,
    a13: f64,
    a14: f64,
    a21: f64,
    a22: f64,
    a23: f64,
    a24: f64,
    a31: f64,
    a32: f64,
    a33: f64,
    a34: f64,
    a41: f64,
    a42: f64,
    a43: f64,
    a44: f64,
}
impl Mat4 {
    pub fn from_cols(a1: Vec4, a2: Vec4, a3: Vec4, a4: Vec4) -> Self {
        Self {
            a11: a1.x1,
            a12: a2.x1,
            a13: a3.x1,
            a14: a4.x1,
            a21: a1.x2,
            a22: a2.x2,
            a23: a3.x2,
            a24: a4.x2,
            a31: a1.x3,
            a32: a2.x3,
            a33: a3.x3,
            a34: a4.x3,
            a41: a1.x4,
            a42: a2.x4,
            a43: a3.x4,
            a44: a4.x4,
        }
    }

    pub fn from_lins(a1: Vec4, a2: Vec4, a3: Vec4, a4: Vec4) -> Self {
        Self {
            a11: a1.x1,
            a12: a1.x2,
            a13: a1.x3,
            a14: a1.x4,
            a21: a2.x1,
            a22: a2.x2,
            a23: a2.x3,
            a24: a2.x4,
            a31: a3.x1,
            a32: a3.x2,
            a33: a3.x3,
            a34: a3.x4,
            a41: a4.x1,
            a42: a4.x2,
            a43: a4.x3,
            a44: a4.x4,
        }
    }

    pub fn from_slice(s: &[f64]) -> Option<Self> {
        Some(Self{
            a11: *s.get(0)?,
            a12: *s.get(1)?,
            a13: *s.get(2)?,
            a14: *s.get(3)?,
            a21: *s.get(4)?,
            a22: *s.get(5)?,
            a23: *s.get(6)?,
            a24: *s.get(7)?,
            a31: *s.get(8)?,
            a32: *s.get(9)?,
            a33: *s.get(10)?,
            a34: *s.get(11)?,
            a41: *s.get(12)?,
            a42: *s.get(13)?,
            a43: *s.get(14)?,
            a44: *s.get(15)?
        })
    }

    pub fn identity() -> Mat4 {
        Mat4 {
            a11: 1.0,
            a12: 0.0,
            a13: 0.0,
            a14: 0.0,
            a21: 0.0,
            a22: 1.0,
            a23: 0.0,
            a24: 0.0,
            a31: 0.0,
            a32: 0.0,
            a33: 1.0,
            a34: 0.0,
            a41: 0.0,
            a42: 0.0,
            a43: 0.0,
            a44: 1.0,
        }
    }

    #[inline(always)]
    pub fn transpose(self) -> Self {
        Mat4 {
            a11: self.a11,
            a12: self.a21,
            a13: self.a31,
            a14: self.a41,
            a21: self.a12,
            a22: self.a22,
            a23: self.a32,
            a24: self.a42,
            a31: self.a13,
            a32: self.a23,
            a33: self.a33,
            a34: self.a43,
            a41: self.a14,
            a42: self.a24,
            a43: self.a34,
            a44: self.a44,
        }
    }

    #[inline(always)]
    fn det(self) -> f64 {
        self.a11
            * Mat3 {
                a11: self.a22,
                a12: self.a23,
                a13: self.a24,
                a21: self.a32,
                a22: self.a33,
                a23: self.a34,
                a31: self.a42,
                a32: self.a43,
                a33: self.a44,
            }
            .det()
            - self.a21
                * Mat3 {
                    a11: self.a12,
                    a12: self.a13,
                    a13: self.a14,
                    a21: self.a32,
                    a22: self.a33,
                    a23: self.a34,
                    a31: self.a42,
                    a32: self.a43,
                    a33: self.a44,
                }
                .det()
            + self.a31
                * Mat3 {
                    a11: self.a12,
                    a12: self.a13,
                    a13: self.a14,
                    a21: self.a22,
                    a22: self.a23,
                    a23: self.a24,
                    a31: self.a42,
                    a32: self.a43,
                    a33: self.a44,
                }
                .det()
            - self.a41
                * Mat3 {
                    a11: self.a12,
                    a12: self.a13,
                    a13: self.a14,
                    a21: self.a22,
                    a22: self.a23,
                    a23: self.a24,
                    a31: self.a32,
                    a32: self.a33,
                    a33: self.a34,
                }
                .det()
    }

    #[inline(always)]
    pub fn adj(self) -> Self {
        Mat4 {
            a11: Mat3 {
                a11: self.a22,
                a12: self.a23,
                a13: self.a24,
                a21: self.a32,
                a22: self.a33,
                a23: self.a34,
                a31: self.a42,
                a32: self.a43,
                a33: self.a44,
            }
            .det(),
            a12: -Mat3 {
                a11: self.a21,
                a12: self.a23,
                a13: self.a24,
                a21: self.a31,
                a22: self.a33,
                a23: self.a34,
                a31: self.a41,
                a32: self.a43,
                a33: self.a44,
            }
            .det(),
            a13: Mat3 {
                a11: self.a21,
                a12: self.a22,
                a13: self.a24,
                a21: self.a31,
                a22: self.a32,
                a23: self.a34,
                a31: self.a41,
                a32: self.a42,
                a33: self.a44,
            }
            .det(),
            a14: -Mat3 {
                a11: self.a21,
                a12: self.a22,
                a13: self.a23,
                a21: self.a31,
                a22: self.a32,
                a23: self.a33,
                a31: self.a41,
                a32: self.a42,
                a33: self.a43,
            }
            .det(),
            a21: -Mat3 {
                a11: self.a12,
                a12: self.a13,
                a13: self.a14,
                a21: self.a32,
                a22: self.a33,
                a23: self.a34,
                a31: self.a42,
                a32: self.a43,
                a33: self.a44,
            }
            .det(),
            a22: Mat3 {
                a11: self.a11,
                a12: self.a13,
                a13: self.a14,
                a21: self.a31,
                a22: self.a33,
                a23: self.a34,
                a31: self.a41,
                a32: self.a43,
                a33: self.a44,
            }
            .det(),
            a23: -Mat3 {
                a11: self.a11,
                a12: self.a12,
                a13: self.a14,
                a21: self.a31,
                a22: self.a32,
                a23: self.a34,
                a31: self.a41,
                a32: self.a42,
                a33: self.a44,
            }
            .det(),
            a24: Mat3 {
                a11: self.a11,
                a12: self.a12,
                a13: self.a13,
                a21: self.a31,
                a22: self.a32,
                a23: self.a33,
                a31: self.a41,
                a32: self.a42,
                a33: self.a43,
            }
            .det(),
            a31: Mat3 {
                a11: self.a12,
                a12: self.a13,
                a13: self.a14,
                a21: self.a22,
                a22: self.a23,
                a23: self.a24,
                a31: self.a42,
                a32: self.a43,
                a33: self.a44,
            }
            .det(),
            a32: -Mat3 {
                a11: self.a11,
                a12: self.a13,
                a13: self.a14,
                a21: self.a21,
                a22: self.a23,
                a23: self.a24,
                a31: self.a41,
                a32: self.a43,
                a33: self.a44,
            }
            .det(),
            a33: Mat3 {
                a11: self.a11,
                a12: self.a12,
                a13: self.a14,
                a21: self.a21,
                a22: self.a22,
                a23: self.a24,
                a31: self.a41,
                a32: self.a42,
                a33: self.a44,
            }
            .det(),
            a34: -Mat3 {
                a11: self.a11,
                a12: self.a12,
                a13: self.a13,
                a21: self.a21,
                a22: self.a22,
                a23: self.a23,
                a31: self.a41,
                a32: self.a42,
                a33: self.a43,
            }
            .det(),
            a41: -Mat3 {
                a11: self.a12,
                a12: self.a13,
                a13: self.a14,
                a21: self.a22,
                a22: self.a23,
                a23: self.a24,
                a31: self.a32,
                a32: self.a33,
                a33: self.a34,
            }
            .det(),
            a42: Mat3 {
                a11: self.a11,
                a12: self.a13,
                a13: self.a14,
                a21: self.a21,
                a22: self.a23,
                a23: self.a24,
                a31: self.a31,
                a32: self.a33,
                a33: self.a34,
            }
            .det(),
            a43: -Mat3 {
                a11: self.a11,
                a12: self.a12,
                a13: self.a14,
                a21: self.a21,
                a22: self.a22,
                a23: self.a24,
                a31: self.a31,
                a32: self.a32,
                a33: self.a34,
            }
            .det(),
            a44: Mat3 {
                a11: self.a11,
                a12: self.a12,
                a13: self.a13,
                a21: self.a21,
                a22: self.a22,
                a23: self.a23,
                a31: self.a31,
                a32: self.a32,
                a33: self.a33,
            }
            .det(),
        }
        .transpose()
    }

    pub fn inverse(self) -> Option<Self> {
        if self.det() == 0.0 {
            None
        } else {
            Some(self.adj() * (1.0 / self.det()))
        }
    }
}
impl Mul<f64> for Mat4 {
    type Output = Self;
    fn mul(self, rhs: f64) -> Self {
        Mat4 {
            a11: self.a11 * rhs,
            a12: self.a12 * rhs,
            a13: self.a13 * rhs,
            a14: self.a14 * rhs,
            a21: self.a21 * rhs,
            a22: self.a22 * rhs,
            a23: self.a23 * rhs,
            a24: self.a24 * rhs,
            a31: self.a31 * rhs,
            a32: self.a32 * rhs,
            a33: self.a33 * rhs,
            a34: self.a34 * rhs,
            a41: self.a41 * rhs,
            a42: self.a42 * rhs,
            a43: self.a43 * rhs,
            a44: self.a44 * rhs,
        }
    }
}
impl Mul<Vec4> for Mat4 {
    type Output = Vec4;
    fn mul(self, rhs: Vec4) -> Vec4 {
        Vec4 {
            x1: self.a11 * rhs.x1 + self.a12 * rhs.x2 + self.a13 * rhs.x3 + self.a14 * rhs.x4,
            x2: self.a21 * rhs.x1 + self.a22 * rhs.x2 + self.a23 * rhs.x3 + self.a24 * rhs.x4,
            x3: self.a31 * rhs.x1 + self.a32 * rhs.x2 + self.a33 * rhs.x3 + self.a34 * rhs.x4,
            x4: self.a41 * rhs.x1 + self.a42 * rhs.x2 + self.a43 * rhs.x3 + self.a44 * rhs.x4,
        }
    }
}
impl Mul<Mat4> for Mat4 {
    type Output = Self;
    fn mul(self, rhs: Self) -> Self {
        Mat4 {
            a11: self.a11 * rhs.a11 + self.a12 * rhs.a21 + self.a13 * rhs.a31 + self.a14 * rhs.a41,
            a12: self.a11 * rhs.a12 + self.a12 * rhs.a22 + self.a13 * rhs.a32 + self.a14 * rhs.a42,
            a13: self.a11 * rhs.a13 + self.a12 * rhs.a23 + self.a13 * rhs.a33 + self.a14 * rhs.a43,
            a14: self.a11 * rhs.a14 + self.a12 * rhs.a24 + self.a13 * rhs.a34 + self.a14 * rhs.a44,
            a21: self.a21 * rhs.a11 + self.a22 * rhs.a21 + self.a23 * rhs.a31 + self.a24 * rhs.a41,
            a22: self.a21 * rhs.a12 + self.a22 * rhs.a22 + self.a23 * rhs.a32 + self.a24 * rhs.a42,
            a23: self.a21 * rhs.a13 + self.a22 * rhs.a23 + self.a23 * rhs.a33 + self.a24 * rhs.a43,
            a24: self.a21 * rhs.a14 + self.a22 * rhs.a24 + self.a23 * rhs.a34 + self.a24 * rhs.a44,
            a31: self.a31 * rhs.a11 + self.a32 * rhs.a21 + self.a33 * rhs.a31 + self.a34 * rhs.a41,
            a32: self.a31 * rhs.a12 + self.a32 * rhs.a22 + self.a33 * rhs.a32 + self.a34 * rhs.a42,
            a33: self.a31 * rhs.a13 + self.a32 * rhs.a23 + self.a33 * rhs.a33 + self.a34 * rhs.a43,
            a34: self.a31 * rhs.a14 + self.a32 * rhs.a24 + self.a33 * rhs.a34 + self.a34 * rhs.a44,
            a41: self.a41 * rhs.a11 + self.a42 * rhs.a21 + self.a43 * rhs.a31 + self.a44 * rhs.a41,
            a42: self.a41 * rhs.a12 + self.a42 * rhs.a22 + self.a43 * rhs.a32 + self.a44 * rhs.a42,
            a43: self.a41 * rhs.a13 + self.a42 * rhs.a23 + self.a43 * rhs.a33 + self.a44 * rhs.a43,
            a44: self.a41 * rhs.a14 + self.a42 * rhs.a24 + self.a43 * rhs.a34 + self.a44 * rhs.a44,
        }
    }
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub struct AaBb3 {
    max: Vec3,
    min: Vec3,
}
impl AaBb3 {
    pub fn from_point(p: Vec3) -> AaBb3 {
        AaBb3 { max: p, min: p }
    }

    pub fn fold_initial() -> Self {
        Self {
            min: Vec3::new(f64::INFINITY, f64::INFINITY, f64::INFINITY),
            max: Vec3::new(f64::NEG_INFINITY, f64::NEG_INFINITY, f64::NEG_INFINITY)
        }
    }

    #[inline(always)]
    fn get_corner(self, x1: bool, x2: bool, x3: bool) -> Vec3 {
        Vec3 {
            x1: {
                if x1 {
                    self.max.x1
                } else {
                    self.min.x1
                }
            },
            x2: {
                if x2 {
                    self.max.x2
                } else {
                    self.min.x2
                }
            },
            x3: {
                if x3 {
                    self.max.x3
                } else {
                    self.min.x3
                }
            },
        }
    }

    pub fn max(&self) -> Vec3 {
        self.max
    }

    pub fn min(&self) -> Vec3 {
        self.min
    }

    fn corners(self) -> [Vec3; 8] {
        [
            self.get_corner(false, false, false),
            self.get_corner(false, false, true),
            self.get_corner(false, true, false),
            self.get_corner(false, true, true),
            self.get_corner(true, false, false),
            self.get_corner(true, false, true),
            self.get_corner(true, true, false),
            self.get_corner(true, true, true),
        ]
    }
}
impl Add<Vec3> for AaBb3 {
    type Output = AaBb3;
    fn add(self, rhs: Vec3) -> AaBb3 {
        AaBb3 {
            max: Vec3 {
                x1: self.max.x1.max(rhs.x1),
                x2: self.max.x2.max(rhs.x2),
                x3: self.max.x3.max(rhs.x3),
            },
            min: Vec3 {
                x1: self.min.x1.min(rhs.x1),
                x2: self.min.x2.min(rhs.x2),
                x3: self.min.x3.min(rhs.x3),
            },
        }
    }
}
impl Add<AaBb3> for AaBb3 {
    type Output = AaBb3;
    fn add(self, rhs: AaBb3) -> AaBb3 {
        AaBb3 {
            max: Vec3 {
                x1: self.max.x1.max(rhs.max.x1),
                x2: self.max.x2.max(rhs.max.x2),
                x3: self.max.x3.max(rhs.max.x3),
            },
            min: Vec3 {
                x1: self.min.x1.min(rhs.min.x1),
                x2: self.min.x2.min(rhs.min.x2),
                x3: self.min.x3.min(rhs.min.x3),
            },
        }
    }
}

#[inline(always)]
fn intersect_planes(d1: f64, n1: Vec3, d2: f64, n2: Vec3, d3: f64, n3: Vec3) -> Vec3 {
    (n2.cross(n3) * d1 + n3.cross(n1) * d2 + n1.cross(n2) * d3) * (1.0 / n1.dot(n2.cross(n3)))
}

#[cfg(test)]
mod tests {
    use super::*;
    //These don't seem to work logic-wise. Since a broken pipeline on master means no executables, I've just disabled it.
    /*
    #[test]
    pub fn __tests() {
        let tm2: Mat2 = Mat2 {
            a11: 2.0,
            a12: 3.5,
            a21: 3.0,
            a22: 1.5,
        };
        assert_eq!(tm2 * tm2.inverse().unwrap(), Mat2::identity());
        let tm3: Mat3 = Mat3 {
            a11: 2.0,
            a12: 1.0,
            a13: 4.0,
            a21: 1.0,
            a22: 2.0,
            a23: 3.0,
            a31: 1.0,
            a32: 3.0,
            a33: 3.0,
        };
        assert_eq!(tm3.det(), -2.0);
        assert_eq!(tm3 * tm3.inverse().unwrap(), Mat3::identity());
        let tm4: Mat4 = Mat4 {
            a11: 2.0,
            a12: 1.5,
            a13: 4.0,
            a14: 2.0,
            a21: 1.0,
            a22: 2.0,
            a23: 1.5,
            a24: 1.0,
            a31: 1.0,
            a32: 2.5,
            a33: 2.5,
            a34: 3.0,
            a41: 2.0,
            a42: 1.0,
            a43: 2.0,
            a44: 1.0,
        };
        assert_eq!(tm4.det(), 8.0);
        assert_eq!(tm4 * tm4.inverse().unwrap(), Mat4::identity());
    }
    */
}