//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use std::path::PathBuf;

use serde::{Serialize, de::DeserializeOwned};

use crate::*;


pub trait ParameterType: Sized + Send + Sync + Clone + Serialize + DeserializeOwned{
    fn type_name() -> &'static str;
}

impl ParameterType for String{
    fn type_name() -> &'static str {"String"}
}

impl ParameterType for bool{
    fn type_name() -> &'static str {"Boolean"}
}

impl ParameterType for i64{
    fn type_name() -> &'static str {"Integer"}
}

impl ParameterType for f64{
    fn type_name() -> &'static str {"Float"}
}

impl ParameterType for PathBuf{
    fn type_name() -> &'static str {"File"}
}

impl ParameterType for Vec<PathBuf>{
    fn type_name() -> &'static str {"Files"}
}

impl ParameterType for UEC{
    fn type_name() -> &'static str {"Position"}
}

impl ParameterType for Vector2D{
    fn type_name() -> &'static str {"Vector2D"}
} 

impl ParameterType for Vector3D{
    fn type_name() -> &'static str {"Vector3D"}
} 

impl ParameterType for Color{
    fn type_name() -> &'static str {"Color"}
} 

pub struct SourceParameter<T> where T: ParameterType{
    pub name: String,
    pub value: Option<T>,
    pub default: Option<T>,
    pub necessary: bool
}