//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

pub mod datatypes;
mod source;
mod tile;
mod position;
mod queries;
pub mod constants;
pub mod prelude;

pub mod task_deduplicator;

pub use datatypes::*;
pub use position::*;
pub use tile::*;
pub use source::*;
pub use source::Metadata;
pub use queries::*;

mod parameters;
pub use parameters::*;

pub use chrono;
pub use rayon;
pub use log;
pub use log::*;

pub use tokio;

pub use smallvec;

mod sources;
pub use sources::simple_scalar_point_source::SimpleScalarPointSourceBuilder;
pub use sources::multi_column_scalar_point_source::MultiColumnPointSourceBuilder;
pub use sources::simple_vector2d_point_source::SimpleVector2DPointSourceBuilder;
pub use sources::multi_source::MultiSource;
pub use sources::sampling;

pub use epsg_geodetic_parameters::{ellipsoids, projections, CoordTransform};

mod time;
pub use time::TimeParser;
pub use time::TimeParsingMode;

mod dataprovider;
pub use dataprovider::*;

mod vecmat;
pub use vecmat::*;

use std::fmt::{Display, Formatter};
#[derive(Debug)]
pub struct GenericError(pub String);
impl Display for GenericError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::result::Result<(), std::fmt::Error> { 
        write!(f, "GenericError: {}", self.0)
    }
}
impl std::error::Error for GenericError {}
unsafe impl Send for GenericError {}
unsafe impl Sync for GenericError {}

pub type SyncError = Box<dyn std::error::Error + Send + Sync>;

pub async fn block<F,R>(f: F) -> R where F: FnOnce() -> R + Send + 'static, R: Send + 'static{
    //tokio::task::block_in_place(f)
    tokio::task::spawn_blocking(f).await.unwrap()
}
