//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use std::error::Error;
use once_cell::sync::OnceCell;
use crate::*;
use lazy_static::lazy_static;

#[async_trait]
pub trait DataProvider: Sync + 'static{
    async fn get_source_info(&self, instance: String) -> Result<Option<SourceInfo>, Box<dyn Error + Send + Sync>>;
    async fn get_source_status(&self, instance: String) -> Result<Option<SourceStatus>, Box<dyn Error + Send + Sync>>;
    async fn get_geodata(&self, sample_query: SampleQuery, return_type: GeoDataType) -> Result<Option<GeoData>, Box<dyn Error + Send + Sync>>;

    async fn wait_for_source_info(&self, instance: String) ->Result<SourceInfo, Box<dyn Error + Send + Sync>>{
        //Wait until source available
        loop{
            let status = self.get_source_status(instance.clone()).await?;

            if let Some(status) = status{
                match status{
                    SourceStatus::Loading => {
                        tokio::time::sleep(tokio::time::Duration::from_millis(1000)).await;
                        continue;
                    }
                    SourceStatus::Loaded => {
                        break;
                    }
                    SourceStatus::Error(e) => {
                        return Err(format!("Required source {} could not be initialized due to error {}", instance, e).into());
                    }
                }
            }else{
                tokio::time::sleep(tokio::time::Duration::from_millis(250)).await;
                continue;
            }   
        }

        //Get sourceinfo
        let r_source_info = DATAPROVIDER.get().ok_or_else(|| "Could not access dataprovider")?
            .get_source_info(instance.to_string()).await?
            .ok_or_else(|| format!("Source info of required source {} not received", instance))?;

        Ok(r_source_info)
    }
}


lazy_static!{
    pub static ref DATAPROVIDER: OnceCell<Box<dyn DataProvider + Send + Sync>> = OnceCell::new();
}