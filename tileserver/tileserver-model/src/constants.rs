//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

///The size of a raster tile in pixels, also used for finding out how small features in vector tiles can be
pub const TILESIZE: (usize,usize) = (512,512);
///The aspect ratio after which tiles should not be subdivided widthwise any further
pub const ASPECT_RATIO_CUTOFF: f64 = 0.5;
///The number of requests that can be put on hold before the requests themselves start to get put on hold
pub const WAITLIST_SIZE: usize = 100;
///The number of tiles that should be kept in memory
pub const IN_MEMORY_CACHE_SIZE_BYTES: usize = 100_000_000;
///The path where the cache resides
pub const CACHEPATH: &str = "cache/cache";
///The path of the server configuration file
pub const INITIAL_SOURCES_EXT: &str = "sources.toml";
///The circumference of the Earth in Metres.
pub const CIRCUMFERENCE_EQUATOR_METRES: f64 = 40_075_017f64;
///Compression level for compressed tiledata
pub const ZSTD_LEVEL: i32 = 3;
///Recommended size for point arrays (in points)
pub const RECOMMENDED_PAGE_SIZE: usize = (1024 * 1024) / (5 * 4);
///User agent string the tileserver presents to http services
pub const TILESERVER_USER_AGENT: &str = "DIGITAL_EARTH_VIEWER_V0.1";
///Number of threads per core used by the sample request dispatch
pub const THREADS_PER_CPU_CORE: usize = 1;
