//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use crate::ellipsoid::Ellipsoid;

include!(concat!(env!("OUT_DIR"), "/ellipsoid_constructors.rs"));