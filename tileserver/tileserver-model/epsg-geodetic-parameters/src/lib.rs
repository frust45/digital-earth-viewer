//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

#![feature(const_option)]
#![feature(const_fn_floating_point_arithmetic)]
#![feature(const_float_classify)]
#![feature(const_float_bits_conv)]

pub const fn to_radians_ext(deg: f64) -> f64 {
    deg / 180.0 * std::f64::consts::PI
}
pub const fn to_degrees_ext(rad: f64) -> f64 {
    rad * 180.0 / std::f64::consts::PI
}

pub mod transverse_mercator;
pub mod stereographic;
pub mod lambert_azimuthal_equal_area;
pub mod zero_transformation;

pub mod ellipsoid;

pub mod ellipsoid_constructor;
pub mod projection_constructor;

pub use ellipsoid_constructor as ellipsoids;
pub use projection_constructor as projections;

pub mod traits;
pub use traits::CoordTransform;