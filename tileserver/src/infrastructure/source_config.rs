//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use serde::{Serialize, Deserialize};
use sha2::{Digest, Sha512};
use std::collections::HashMap;
use std::error::Error;
use std::path::{PathBuf, Path};
use toml::Value;

#[derive(Serialize, Deserialize, Debug)]
pub struct SourceConfiguration{
    pub source: String,
    #[serde(default)]
    pub files: Vec<PathBuf>,
    #[serde(default = "default_attribution")]
    pub attribution: String,
    #[serde(default = "default_category")]
    pub category: String,
    #[serde(default)]
    pub override_name: bool,
    #[serde(flatten)]
    pub toml_settings: HashMap<String, Value>,
    #[serde(skip)]
    pub settings: HashMap<String, String>
}

fn default_attribution() -> String{
    "All right reserved.".to_string()
}

fn default_category() -> String{
    "Other".to_string()
}

pub struct ConfigFile{
    pub config_name: String,
    pub sources: HashMap<String, SourceConfiguration>,
    pub hash: String
}

fn toml_value_to_string(v: &Value) -> String{
    match v {
        Value::String(s) => s.clone(),
        Value::Integer(i) => format!("{}", i),
        Value::Float(f) => format!("{}", f),
        Value::Boolean(b) => format!("{}", b),
        Value::Datetime(d) => format!("{}", d),
        _ => unimplemented!(),
    }
}


impl ConfigFile{
    pub fn from_str(name: &str, config: &str) -> Result<ConfigFile, Box<dyn Error + Send + Sync>>{
        let mut sources: HashMap<String, SourceConfiguration> = toml::from_str(config)?;
        for source in sources.values_mut() {
            for (key, value) in &source.toml_settings {
                source.settings.insert(key.to_string(), toml_value_to_string(value));
            }
        }

        //Hash configs
        let mut hasher = Sha512::new();
        let mut keys = sources.keys().collect::<Vec<_>>();
        keys.sort();
        for key in keys{
            hasher.update(key);
            let source = &sources[key];
            hasher.update(&source.attribution);
            hasher.update(&source.category);
            hasher.update(&source.source);
            let mut sorted_files = source.files.iter().map(|pb| pb.to_string_lossy().to_string()).collect::<Vec<_>>();
            sorted_files.sort();
            for file in sorted_files{
                hasher.update(file);
            }
            let mut sorted_settings_keys = source.settings.keys().collect::<Vec<_>>();
            sorted_settings_keys.sort();
            for settings_key in sorted_settings_keys{
                hasher.update(settings_key);
                hasher.update(&source.settings[settings_key]);
            }
        }
        let hash= hasher.finalize().as_slice().iter().map(|b| format!("{:02x}", b)).collect::<Vec<_>>().join("");

        Ok(
            ConfigFile{
                config_name: String::from(name),
                sources,
                hash
            }
        )
    }

    pub fn from_path(path: &Path) -> Result<ConfigFile, Box<dyn Error + Send + Sync>>{
        let name = path.file_name().map_or_else(|| "Unknown file".to_string(), |name| name.to_string_lossy().to_string());
        let content: String = std::fs::read_to_string(path).map_err(|e| format!("Error reading file {}: {:?}", name, e))?;
        ConfigFile::from_str(&name, &content)
    }
}