//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use std::{
    error::Error,
    marker::PhantomData,
    ops::Range,
    sync::Arc,
};

use crate::{
    infrastructure::{
        Cache,
        Scenelinks,
    },
    SOURCELIST,
};

use tileserver_model::{
    *,
    task_deduplicator::TaskDeduplicator
};

use bytes::Bytes;
use futures::{
    FutureExt, 
    stream::FuturesUnordered,
    TryStreamExt, 
};
use lazy_static::lazy_static;
use rand::Rng;

const MAX_TILES_FOR_SAMPLING: usize = 8;

lazy_static! {
    pub static ref SAMPLER: Sampler = Sampler::new();
}

pub struct Sampler {
    cache: Arc<Cache>,
    tile_dedup: TaskDeduplicator<SampleQuery, Result<Option<Bytes>, String>>,
    array_dedup: TaskDeduplicator<SampleQuery, Result<Option<Bytes>, String>>
}

impl Sampler {
    pub fn new() -> Sampler {
        Sampler {
            cache: Arc::new(Cache::start()),
            tile_dedup: TaskDeduplicator::new(),
            array_dedup: TaskDeduplicator::new()
            }
    }

    pub fn get_cache(&self) -> Arc<Cache>{
        self.cache.clone()
    }

    pub fn register_callbacks(&self) {
        struct ProviderStruct{
            pd: PhantomData<()>
        }
        #[async_trait]
        impl DataProvider for ProviderStruct{
            async fn get_source_info(&self, instance: String) -> Result<Option<SourceInfo>, Box<dyn Error + Send + Sync>> {
                SAMPLER.get_source_info(&instance).await
            }
            async fn get_source_status(&self, instance: String) -> Result<Option<SourceStatus>, Box<dyn Error + Send + Sync>> {
                SAMPLER.get_source_status(&instance).await
            }

            async fn get_geodata(&self, sample_query: SampleQuery, return_type: GeoDataType) -> Result<Option<GeoData>, Box<dyn Error + Send + Sync>> {
                let data;
                if return_type.is_point_type() {
                    data = SAMPLER.get_array(&sample_query.sourcename, &sample_query.layername, sample_query.tpage).await?;
                }else{
                    data = SAMPLER.get_tile(&sample_query.sourcename, &sample_query.layername, &sample_query.tile.path, sample_query.tpage, sample_query.zpage).await?;
                }

                Ok(data.map(|bytes| return_type.rehydrate(&bytes[..])))
            }
        }
        let pvs = ProviderStruct{
            pd: PhantomData
        };
        DATAPROVIDER.set(Box::new(pvs)).unwrap_or(());
    }

    pub async fn get_tile(
        &self,
        instance: &str,
        layer: &str,
        tilepath: &str,
        tpage: u32,
        zpage: u32,
    ) -> Result<Option<Bytes>, Box<dyn Error + Send + Sync>> {
        let tile = Tile::from_tilepath(tilepath).map_err(|e| format!("Error creating tile: {}", e))?;
        let query = SampleQuery::new(instance.to_string(),layer.to_string(),tile,tpage, zpage);
        self.tile_dedup.run_or_wait(query, async move |query| {
            let cache_for_cache_check = self.cache.clone();
            let query_for_cache_check = query.clone();
            match tokio::spawn(async move {
                cache_for_cache_check.get_tile(&query_for_cache_check).await
            }).await {
                Err(e) => Err(e.into()),
                Ok(Err(e)) => Err(e),
                Ok(Ok(Some(geodata))) => Ok(Some(Bytes::from(geodata))),
                Ok(Ok(None)) => { 
                    let source = SOURCELIST.get_source(instance).clone();
                    if let Some(source) = source {
                        let query_for_source_sampling = query.clone();
                        match tokio::spawn( async move {source.sample(&query_for_source_sampling).await}).await {
                            Err(e) => Err(e.into()),
                            Ok(Err(e)) => Err(e),
                            Ok(Ok(geodata_option)) => {
                                let cache_for_cache_insert = self.cache.clone();
                                let data_option = geodata_option.map(|geodata| geodata.into());
                                let data_option_cloned = data_option.clone();
                                tokio::spawn(async move {cache_for_cache_insert.insert_tile(&query, data_option_cloned).await});
                                Ok(data_option)
                            }
                        }
                    }else{
                        Err(format!("No source called {} found!", query.sourcename).into())
                    }            
                }
            }.map_err(|e| e.to_string())
        }).await.map_err(|e| e.into())
    }

    pub async fn get_array(
        &self,
        instance: &str,
        layer: &str,
        tpage: u32,
    ) -> Result<Option<Bytes>, Box<dyn Error + Send + Sync>> {
        let tile = Tile::from_tilepath("W").unwrap();
        let query = SampleQuery::new(instance.to_string(),layer.to_string(),tile,tpage, 0);
        self.array_dedup.run_or_wait(query, async move |query| {
            let cache_for_cache_check = self.cache.clone();
            let query_for_cache_check = query.clone();
            match tokio::spawn(async move {
                cache_for_cache_check.get_array_for_source_layer_tpage(&query_for_cache_check.sourcename, &query_for_cache_check.layername, query_for_cache_check.tpage).await
            }).await {
                Err(e) => Err(e.into()),
                Ok(Err(e)) => Err(e),
                Ok(Ok(Some(geodata))) => Ok(Some(Bytes::from(geodata))),
                Ok(Ok(None)) => {
                    let source = SOURCELIST.get_source(instance).clone();
                    if let Some(source) = source {
                        let query_for_source_sampling = query.clone();
                        match tokio::spawn(async move {source.sample(&query_for_source_sampling).await}).await {
                            Err(e) => Err(e.into()),
                            Ok(Err(e)) => Err(e),
                            Ok(Ok(geodata_option)) => {
                                let cache_for_cache_insert = self.cache.clone();
                                let data_option = geodata_option.map(|geodata| geodata.into());
                                let data_option_cloned = data_option.clone();
                                tokio::spawn(async move {cache_for_cache_insert.insert_array(&query.sourcename, &query.layername, query.tpage, data_option_cloned).await});
                                Ok(data_option)
                            }
                        }
                    } else {
                        Err(format!("No source called {} found!", query.sourcename).into())
                    }
                }
            }.map_err(|e| e.to_string())
        }).await.map_err(|e| e.into())
    }

    pub async fn create_scene(&self, slug: &str, content: &str) -> Result<String, Box<dyn Error + Send + Sync>>{
        if Scenelinks::scene_description_string_matches_schema(content){
            let mut ctr = 0;
            const LIMIT: u32 = 1000;
            const UID_LIMIT: u32 = 1000000;
            loop{
                ctr += 1;
                if ctr > LIMIT{
                    break Err("Too many unsucessful attempts, try again".into());
                }
                let random: u32 = rand::thread_rng().gen_range(0 .. UID_LIMIT);
                let name_replaced: String = slug.chars().filter(|c| c.is_ascii_alphanumeric() || *c == '_' || *c == '-').collect();
                let slug = format!("{}-{}", name_replaced, random);
                let cache_for_cache_check = self.cache.clone();
                let slug_for_cache_check = slug.clone();
                match tokio::spawn(async move {
                    cache_for_cache_check.get_link(&slug_for_cache_check).await
                }).await {
                    Err(e) => break Err(e.into()),
                    Ok(Ok(Some(_link))) => continue, //Link slug already exists, generate next one
                    Ok(Err(e)) =>  break Err(e), //Error retrieving the link, this is probably DB corruption
                    Ok(Ok(None)) => break {
                        //Insert content
                        let scene = content.to_string();
                        let cache_for_cache_insert = self.cache.clone();
                        let slug_for_insert = slug.clone();
                        match tokio::spawn(async move {
                            cache_for_cache_insert.insert_link(&slug_for_insert, &scene).await
                        }).await {
                            Ok(_) => Ok(slug),
                            Err(e) => Err(e.into())
                        }
                    }
                };
            }
        }else{
            Err("Scene description does not match schema!".into())
        }
    }

    pub async fn retrieve_scene(&self, slug: &str) -> Result<Option<String>, Box<dyn Error + Send + Sync>>{
        let cache_for_query = self.cache.clone();
        let slug_for_query = slug.to_string();
        match tokio::spawn(async move {cache_for_query.get_link(&slug_for_query).await}).await {
            Ok(r) => r,
            Err(e) => Err(e.into())
        }
    }

    async fn sample_points_array(
        &self,
        sourceinfo: &Arc<SourceInfo>,
        layerinfo: &LayerInfo,
        pos: UECArea,
        zrange: Range<f32>,
        timerange: Range<Timestamp>,
        timepage_range: Range<u32>
    ) -> Result<Bytes, Box<dyn Error + Send + Sync>>{
        let fut_unord: FuturesUnordered<_> =  timepage_range.map(|t| {
            self.get_array(&sourceinfo.instance_name, &layerinfo.name, t)
                .map(|f| {
                    f.map(|o| o.and_then(|d| match layerinfo.layer_type.rehydrate(&d[..]) {
                        GeoData::ColorPoints(d) => Some(d.into()),
                        GeoData::ScalarPoints(d) => Some(d.into()),
                        GeoData::Vector2DPoints(d) => Some(d.into()),
                        GeoData::Vector3DPoints(d) => Some(d),
                        _ => None
                    }))
                })
            })
            .collect();
        let hs = zrange.start;
        let he = zrange.end;
        let ts = timerange.start;
        let te = timerange.end;
        fut_unord.try_fold(Vec::new(), |p, c| async move {
            match c {
                Some(d) => {
                    let mut tmp = p;
                    tmp.extend(d.points.iter().filter(|p| {
                        p.pos.to_uec().in_area(pos)
                        && p.height >= hs
                        && p.height <= he
                        && p.timestamp >= ts
                        && p.timestamp <= te
                    }));
                    Ok(tmp)
                },
                None => Ok(p)
            }
        })
            .await
            .map(|v| PointsData::new(v).into())
    }

    async fn sample_points_tile(
        &self,
        sourceinfo: &Arc<SourceInfo>,
        layerinfo: &LayerInfo,
        pos: UECArea,
        zpage_range: Range<u32>,
        timepage_range: Range<u32>
    ) -> Result<Bytes, Box<dyn Error + Send + Sync>>{
        let tiles = Tile::covering_area(pos, MAX_TILES_FOR_SAMPLING, layerinfo.max_zoom_level);
        let fut_unord: FuturesUnordered<_> = tiles.iter()
            .flat_map(|tile| zpage_range.clone().map(move |h| (tile, h)))
            .flat_map(|(tile, h)| timepage_range.clone().map(move |t| (tile, h, t)))
            .map(|(tile, h, t)| {
                self.get_tile(
                    &sourceinfo.instance_name, 
                    &layerinfo.name, 
                    &tile.path, 
                    t, 
                    h
                    )
                    .map(|f| {
                        f.map(|o| o.and_then(|d| match layerinfo.layer_type.rehydrate(&d[..]) {
                            GeoData::ScalarTiles(mut d) => {
                                d.tile = tile.clone();
                                let ps: PointsData<Scalar> = d.into();
                                Some(ps.into())
                            },
                            GeoData::ColorTiles(mut d) => {
                                d.tile = tile.clone();
                                let ps: PointsData<Color> = d.into();
                                Some(ps.into())
                            },
                            _ => None
                        }))
                    })
                })
            .collect();
        fut_unord.try_fold( Vec::new(), |p, c: Option<PointsData<Vector3D>> | async move {
            match c {
                Some(d) => {
                    let mut tmp = p;
                    tmp.extend(d.points.iter().filter(|p|
                        p.pos.to_uec().in_area(pos)
                    ));
                    Ok(tmp)
                },
                None => Ok(p)
                
            }
        })
            .await
            .map(|v| PointsData::new(v).into())
    }

    pub async fn sample_points(
        &self,
        instance: &str,
        layer: &str,
        pos_min: UEC,
        pos_max: UEC,
        height_min: f32,
        height_max: f32,
        time_min: Timestamp,
        time_max: Timestamp,
    ) -> Result<Bytes, Box<dyn Error + Send + Sync>> {
        if let Some(sourceinfo) = SOURCELIST.get_info(&instance) {
            if let Some(layerinfo) = sourceinfo.layers.iter().find(|l| l.name == layer) {
                let timepage_range: Range<u32> = layerinfo.timesteps.as_ref().and_then(|ts| {
                    layerinfo.timerange.and_then(|(_tstart, tend)| {
                        let mut rangei = ts.iter()
                            .zip(ts.iter()
                                .skip(1)
                                .chain(std::iter::once(&tend)))
                            .enumerate()
                            .filter_map(|(pageindex, (&pagestart, &pageend))| {
                                if pagestart < time_max && time_min < pageend {
                                    Some(pageindex)
                                } else {
                                    None
                                }
                            });
                        rangei.next().map(|rangestart| {
                            rangestart as u32 .. (rangei.last().unwrap_or(rangestart) + 1) as u32
                        }) 
                })}).unwrap_or(0 .. 1);
                let zpage_range: Range<u32> = layerinfo.zsteps.as_ref().and_then(|zs| {
                    layerinfo.zrange.and_then(|(_zmin, zmax)| {
                        let mut rangei = zs.iter()
                            .zip(zs.iter()
                                .skip(1)
                                .chain(std::iter::once(&zmax)))
                            .enumerate()
                            .filter_map(|(pageindex, (&pagemin, &pagemax))| {
                                if pagemin < height_max && height_min < pagemax {
                                    Some(pageindex)
                                } else {
                                    None
                                }
                            });
                        rangei.next().map(|rangestart| {
                            rangestart as u32 .. (rangei.last().unwrap_or(rangestart) + 1) as u32
                        }) 
                })}).unwrap_or(0 .. 1);

                if layerinfo.layer_type.is_point_type() {
                    self.sample_points_array(
                        &sourceinfo,
                        layerinfo,
                        UECArea::area_from_two_points(pos_min, pos_max), 
                        height_min..height_max,
                        time_min..time_max,
                        timepage_range
                    ).await
                } else {
                    self.sample_points_tile(
                        &sourceinfo, 
                        layerinfo, 
                        UECArea::area_from_two_points(pos_min, pos_max), 
                        zpage_range, 
                        timepage_range
                    ).await
                }
            }else{
                Err(format!("Error getting layer info for instance {} layer {}", instance, layer).into())    
            }
        }else{
            Err(format!("Error getting source info for instance {}", instance).into())
        }
    }

    pub async fn get_source_status(&self, instance: &str) -> Result<Option<SourceStatus>, Box<dyn Error + Send + Sync>>{
        Ok(SOURCELIST.get_status(instance))
    }

    pub async fn get_source_info(&self, instance: &str) -> Result<Option<SourceInfo>, Box<dyn Error + Send + Sync>>{
        Ok(SOURCELIST.get_info(&instance).map(|i| (*i).clone()))
    }

    pub async fn get_metadata(&self, instance: &str, metadata: &str) -> Result<Vec<u8>, SyncError>{
        if let Some(source_instance) = SOURCELIST.get_source(instance) {
            let movable_metadata = metadata.to_string();
            match tokio::spawn(async move {
                source_instance.get_metadata(&movable_metadata).await
            }).await {
                Ok(r) => r,
                Err(e) => Err(e.into())
            }
        }else{
            Err(format!("No source instance with name {}", instance).into())
        }
    }
}
