//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

mod sourcelist;
pub use sourcelist::{SourceList, SOURCELIST};

mod cache;
pub use cache::Cache;

mod sampler;
pub use sampler::SAMPLER;

mod registry;
pub use registry::REGISTRY as Registry;

//Really, really broken.
//If you wanted to resurrect this, you should probably add a prerender task to the Sampler and call it regularly, advancing some kind of state every time.
//If you ran that at the lowest priority, it would not interfere with normal operations
//mod prerenderer;
//pub use prerenderer::Prerenderer;

//mod dynamicsources;
//pub use dynamicsources::DYNAMICSOURCES;

mod sources;
pub use sources::initialize_source_constructors;

pub mod source_config;

mod scenelinks;
pub use scenelinks::Scenelinks;
