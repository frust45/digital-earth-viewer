//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use futures::Future;
use lazy_static::lazy_static;
use std::pin::Pin;
use std::sync::{Arc, RwLock};
use tileserver_model::{SourceConfig, SourceInfo, SourceInitResult, SourceStatus, warn};
use crate::infrastructure::SOURCELIST;

lazy_static! {
    pub static ref REGISTRY: Registry = Registry::new();
}

type AcceptFn = fn(&std::path::Path) -> bool;
type InitFn = fn(
    SourceConfig,
    SourceInfo,
) -> Pin<Box<dyn Future<Output = SourceInitResult> + 'static + Send>>;

//type IF = dyn Future<Output = SourceInitResult>;

struct RegisteredSource
{
    pub name: String,
    //pub accept_fn: fn(&std::path::Path) -> bool,
    pub init_fn: Box<InitFn>
}

pub struct Registry {
    sources: RwLock<Vec<Arc<RegisteredSource>>>,
}

impl Registry {
    pub fn new() -> Registry {
        Registry {
            sources: RwLock::new(Vec::new()),
        }
    }

    pub fn add_source(&self, name: String, _acceptor: Box<AcceptFn>, initializer: Box<InitFn>) {
        self.sources.write().unwrap().push(Arc::new(RegisteredSource{
            name,
            //accept_fn: *acceptor,
            init_fn: initializer
        }));
        
    }

    pub async fn create_source(
        &self,
        sourcename: String,
        instance: String,
        config: SourceConfig,
        info: SourceInfo,
    ){
       
        let source_option = {
            let sources = self.sources.read().unwrap();
            sources.iter().find(|s| s.name == sourcename).map(|source_arc| (*source_arc).clone())
        };
        SOURCELIST.register_source(instance.clone());
        if let Some(source) = source_option {
            let info_clone = info.clone();
            let source_init_result = (source.init_fn)(config, info_clone).await;
                match source_init_result {
                    Ok((source_info, source)) => {
                        SOURCELIST.add_source_instance(&instance, source_info, source);
                    },
                    Err(e) => {
                        warn!("Initializing source {} failed: {:?}", &instance, e);
                        SOURCELIST.set_status(&instance, SourceStatus::Error(e.to_string()));
                    }
                }
        } else {
            SOURCELIST.set_status(&instance, SourceStatus::Error(format!("No source with name {}", sourcename)));
        }
    }

    pub fn get_source_names(&self) -> Vec<String> {
        self.sources.read().unwrap().iter().map(|s| s.name.clone()).collect()
    }
}
