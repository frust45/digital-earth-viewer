//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use flate2::write::ZlibDecoder;
use flate2::write::ZlibEncoder;
use flate2::Compression;
use log::*;
use std::{collections::HashMap, error::Error};
use std::io::prelude::*;
use tileserver_model::*;
use std::ops::Deref;

use rusqlite::OptionalExtension;
use rusqlite::{params, Connection, Result};
use tokio::sync::Mutex;

use super::source_config::ConfigFile;
pub struct CacheInternal<T> where T: CacheCompressorDecompressor {
    compressor: std::marker::PhantomData<T>,
    connection: Mutex<Connection>, //TODO: It might improve performance to add a queue or something in which new tiles are inserted and a long running task which checks this queue and if any tiles are present, inserts them into the database with long-ish sleep times inbetween
}

pub trait CacheCompressorDecompressor {
    fn compress(uncompressed: &[u8]) -> std::io::Result<Vec<u8>>;
    fn decompress(compressed: &[u8]) -> std::io::Result<Vec<u8>>;
}

pub struct ZlibCompressorDecompressor {}
impl CacheCompressorDecompressor for ZlibCompressorDecompressor {
    fn compress(uncompressed: &[u8]) -> std::io::Result<Vec<u8>> {
        let mut compressor = ZlibEncoder::new(Vec::new(), Compression::default());
        compressor.write_all(uncompressed).and_then(|_| compressor.finish())
    }
    fn decompress(compressed: &[u8]) -> std::io::Result<Vec<u8>> {
        let mut decompressor = ZlibDecoder::new(Vec::new());
        decompressor.write_all(compressed).and_then(|_| decompressor.finish())
    }
}

pub struct PassThroughCompressorDecompressor {}
impl CacheCompressorDecompressor for PassThroughCompressorDecompressor {
    fn compress(uncompressed: &[u8]) -> std::io::Result<Vec<u8>> {
        Ok(uncompressed.to_vec())
    }
    fn decompress(compressed: &[u8]) -> std::io::Result<Vec<u8>> {
        Ok(compressed.to_vec())
    }
}

///Set the cache compression strategy here:
pub type Cache = CacheInternal<ZlibCompressorDecompressor>;

impl Cache {
    pub fn start() -> Cache {
        debug!("Starting cache");
        let connection: Connection;
        if cfg!(feature = "local") {
            info!("Creating in-memory db");
            connection = Connection::open_in_memory().expect("Could not create in-memory database");
        } else if cfg!(test) {
            //connection = Connection::open("test_cache.db").expect("Could not create test database file");
            connection =
                Connection::open_in_memory().expect("Could not create in-memory test database for testing");
        } else {
            connection =
                Connection::open("cache.db").expect("Could not open database file cache.db");
        }
        //Table for the geodata tile cache
        connection.execute("CREATE TABLE IF NOT EXISTS tiles (source TEXT, layer TEXT, time TEXT, tile TEXT, tpage INTEGER, zpage INTEGER, data BLOB, UNIQUE(source, layer, tile, tpage, zpage))", params![]).expect("Could not create table tiles");
        connection.execute("CREATE INDEX IF NOT EXISTS tiles_index on tiles(source, layer, tile, tpage, zpage)", params![]).expect("Could not create index for table tiles");

        //Table for the geodata array cache
        connection.execute("CREATE TABLE IF NOT EXISTS arrays (source TEXT, layer TEXT, time TEXT, tpage INTEGER, data BLOB, UNIQUE(source, layer, tpage))", params![]).expect("Could not create table arrays");
        connection
            .execute(
                "CREATE INDEX IF NOT EXISTS arrays_index on arrays(source, layer, tpage)",
                params![],
            )
            .expect("Could not create index for table arrays");

        //Table for the stored scenes
        connection.execute("CREATE TABLE IF NOT EXISTS links (slug TEXT, time TEXT, scene BLOB, UNIQUE(slug))", params![]).expect("Could not create table links");

        //Table for metadata
        connection.execute("CREATE TABLE IF NOT EXISTS metadata (key TEXT, value TEXT, UNIQUE(key))", params![]).expect("Could not create table metadata");

        Cache {
            compressor: std::marker::PhantomData,
            connection: Mutex::new(connection),
        }
    }
}

impl<T: CacheCompressorDecompressor> CacheInternal<T> {
    pub async fn get_tile(
        &self,
        query: &SampleQuery,
    ) -> Result<Option<Vec<u8>>, Box<dyn Error + Send + Sync>> {
        let connection = self.connection.lock().await;
        let row = connection.query_row(
            "SELECT data FROM tiles WHERE source = ? AND layer = ? AND tile = ? AND tpage = ? AND zpage = ?", 
            params![
                &query.sourcename,
                &query.layername,
                &query.tile.path,
                query.tpage as i64,
                query.zpage as i64,
            ],
            |row| row.get::<_, Option<Vec<u8>>>(0)
        ).optional().map(|option| option.flatten());

        match row {
            Ok(Some(data)) => {
                match T::decompress(&data) {
                    Ok(bytes) => Ok(Some(bytes)),
                    Err(e) => {
                        error!("Error finishing decompression: {:?}, dropping tile", e);
                        std::mem::drop(connection);
                        self.drop_tile(query).await;
                        Err(e.into())
                    }
                }
            }
            Ok(None) => {
                trace!(
                    "Not found in cache: source:{:?} layer:{:?} tile:{:?} t:{:?} z:{:?}",
                    &query.sourcename,
                    &query.layername,
                    &query.tile.path,
                    query.tpage,
                    query.zpage
                );
                Ok(None)
            }
            Err(err) => {
                error!("Error in cache (get_tile): {:?}", err);
                Err(err.into())
            }
        }
    }

    pub async fn get_array_for_source_layer_tpage(
        &self,
        source: &str,
        layer: &str,
        tpage: u32,
    ) -> Result<Option<Vec<u8>>, Box<dyn Error + Send + Sync>> {
        let connection = self.connection.lock().await;
        let row = connection
            .query_row(
                "SELECT data FROM arrays WHERE source = ? AND layer = ? AND tpage = ?",
                params![source, layer, tpage as i64,],
                |row| row.get::<_, Option<Vec<u8>>>(0),
            )
            .optional()
            .map(|option| option.flatten());

        match row {
            Ok(Some(data)) => {
                match T::decompress(&data) {
                    Ok(bytes) => Ok(Some(bytes)),
                    Err(e) => {
                        error!("Error finishing decompression: {:?}, dropping array", e);
                        std::mem::drop(connection);
                        self.drop_array(source, layer, tpage).await;
                        Err(e.into())
                    }
                }
            }
            Ok(None) => {
                trace!(
                    "Not found in cache: source:{:?} layer:{:?} t:{:?}",
                    source,
                    layer,
                    tpage
                );
                Ok(None)
            }
            Err(err) => {
                error!(
                    "Error in cache (get_array_for_source_layer_tpage): {:?}",
                    err
                );
                Err(err.into())
            }
        }
    }

    pub async fn get_link(
        &self,
        slug: &str,
    ) -> Result<Option<String>, Box<dyn Error + Send + Sync>> {
        let connection = self.connection.lock().await;
        let row = connection.query_row(
            "SELECT scene FROM links WHERE slug = ?", 
            params![
                &slug
            ],
            |row| row.get::<_, Option<Vec<u8>>>(0)
        ).optional().map(|option| option.flatten());

        match row {
            Ok(Some(data)) => {
                match T::decompress(&data) {
                    Ok(bytes) => {
                        match String::from_utf8(bytes){
                            Ok(content) => Ok(Some(content)),
                            Err(e) => {
                                error!("Error checking string UTF-8 integrity: {:?}, dropping link", e);
                                std::mem::drop(connection);
                                self.drop_link(slug).await;
                                Err(e.into())
                            }
                        }
                    },
                    Err(e) => {
                        error!("Error finishing decompression: {:?}, dropping link", e);
                        std::mem::drop(connection);
                        self.drop_link(slug).await;
                        Err(e.into())
                    }
                }
            }
            Ok(None) => {
                trace!("No link found in cache for slug {}", slug);
                Ok(None)
            }
            Err(err) => {
                error!("Error in cache (get_link): {:?}", err);
                Err(err.into())
            }
        }
    }

    pub async fn insert_array<A>(
        &self,
        source: &str,
        layer: &str,
        tpage: u32,
        arraydata: Option<A>
    )
        where A: Deref<Target = [u8]>
    {
        match arraydata.as_ref().map(|d| T::compress(d)).unwrap_or_else(|| T::compress(&[0u8;0])) {
            Ok(compressed) => {
                let connection = self.connection.lock().await;
                let time = chrono::Utc::now();
                let rows_affected = connection.execute(
                    "INSERT OR REPLACE INTO arrays VALUES (?, ?, ?, ?, ?)",
                    params![source, layer, time, tpage as i64, &compressed],
                );

                match rows_affected {
                    Ok(number) => debug!("Inserted {} rows into table arrays", number),
                    Err(e) => error!("Error inserting rows into table arrays: {:?}", e),
                };
            }
            Err(e) => {
                error!("Error compressing: {:?}", e);
            }
        }
    }

    pub async fn insert_tile<A>(
        &self,
        query: &SampleQuery,
        tiledata: Option<A>
    )
    where A: Deref<Target = [u8]> 
    {
        match tiledata.as_ref().map(|d| T::compress(d)).unwrap_or_else(|| T::compress(&[0u8;0])) {
            Ok(compressed) => {
                let connection = self.connection.lock().await;
                let time = chrono::Utc::now();
                let rows_affected = connection.execute(
                    "INSERT OR REPLACE INTO tiles VALUES (?, ?, ?, ?, ?, ?, ?)",
                    params![
                        &query.sourcename,
                        &query.layername,
                        time,
                        &query.tile.path,
                        query.tpage as i64,
                        query.zpage as i64,
                        &compressed
                    ],
                );

                match rows_affected {
                    Ok(number) => debug!("Inserted {} rows into table tiles", number),
                    Err(e) => error!("Error inserting rows into table tiles: {:?}", e),
                };
            }
            Err(e) => {
                error!("Error compressing: {:?}", e);
            }
        }
    }

    pub async fn insert_link(&self, slug: &str, content: &str) {
        let contentbytes = content.as_bytes();
        match T::compress(contentbytes) {
            Ok(compressed) => {
                let connection = self.connection.lock().await;
                let time = chrono::Utc::now();
                let rows_affected = connection.execute(
                    "INSERT OR REPLACE INTO links VALUES (?, ?, ?)",
                    params![
                        &slug,
                        time,
                        &compressed
                    ],
                );

                match rows_affected {
                    Ok(number) => debug!("Inserted {} rows into table links", number),
                    Err(e) => error!("Error inserting rows into table links: {:?}", e),
                };
            }
            Err(e) => {
                error!("Error compressing: {:?}", e);
            }
        }
    }

    pub async fn drop_tile(&self, query: &SampleQuery) {
        let connection = self.connection.lock().await;
        let _rows_affected = connection.execute(
            "DELETE FROM tiles WHERE source = ? AND layer = ? AND tile = ? AND tpage = ? AND zpage = ?",
            params![
                &query.sourcename,
                &query.layername,
                &query.tile.path,
                query.tpage as i64,
                query.zpage as i64,
            ]).expect("Error dropping tile in cache!");
    }

    pub async fn drop_array(&self, source: &str, layer: &str, tpage: u32) {
        let connection = self.connection.lock().await;
        let _rows_affected = connection
            .execute(
                "DELETE FROM arrays WHERE source = ? AND layer = ? AND tpage = ?",
                params![source, layer, tpage as i64],
            )
            .expect("Error dropping array in cache");
    }

    pub async fn drop_link(&self, slug: &str) {
        let connection = self.connection.lock().await;
        let _rows_affected = connection
            .execute(
                "DELETE FROM links WHERE slug = ?",
                params![slug],
            )
            .expect("Error dropping link in cache");
    }

    pub async fn check_git_hash(&self) -> Result<(), Box<dyn Error + Send + Sync>>{
        let connection = self.connection.lock().await;

        const GIT_VERSION_KEY: &str = "gitversion";

        let cache_git_version: Option<String> = connection.query_row("SELECT value FROM metadata WHERE key = ?", params![GIT_VERSION_KEY], |row| {
            row.get(0)
        }).ok();

        use git_version::git_version;

        let current_git_version: &str = git_version!();

        if let Some(cache_git_version) = cache_git_version{
            if current_git_version != cache_git_version || current_git_version.contains("modified"){
                info!("Dropping cache created by version {}, current version is {}", cache_git_version, current_git_version);
                connection.execute("DELETE FROM tiles", params![])?;
                connection.execute("DELETE FROM arrays", params![])?;
                connection.execute("INSERT OR REPLACE INTO metadata VALUES (?, ?)", params![GIT_VERSION_KEY, current_git_version])?;
                info!("Updated cache version to {}", current_git_version);
            }else{
                info!("Git version: {} (equal to cache)", current_git_version);
            }
        }else{
        connection.execute("INSERT INTO metadata VALUES (?, ?)", params![GIT_VERSION_KEY, current_git_version])?;
           info!("Set cache version to {}", current_git_version);
        }

        Ok(())
    }

    pub async fn check_config_file_version(&self, configfile: &ConfigFile) -> Result<(), Box<dyn Error + Send + Sync>>{
        let connection = self.connection.lock().await;


        let configfile_version_key: String = format!("configfile_{}", configfile.config_name);

        let cache_configfile_version: Option<String> = connection.query_row("SELECT value FROM metadata WHERE key = ?", params![&configfile_version_key], |row| {
            row.get(0)
        }).ok();


        let current_configfile_version: &str = &configfile.hash;

        if let Some(cache_configfile_version) = cache_configfile_version{
            if cache_configfile_version != current_configfile_version{
                info!("Dropping sources created by configfile {} version {}, current version is {}", configfile.config_name, cache_configfile_version, current_configfile_version);
                for source in configfile.sources.keys(){
                    info!("Dropping arrays and tiles of {} -> {}", configfile.config_name, source);
                    connection.execute("DELETE FROM tiles WHERE source = ?", params![source])?;
                    connection.execute("DELETE FROM arrays WHERE source = ?", params![source])?;
                }
                connection.execute("INSERT OR REPLACE INTO metadata VALUES (?, ?)", params![configfile_version_key, current_configfile_version])?;
                info!("Updated configfile {} version to {}", configfile.config_name, current_configfile_version);
            }
        }else{
            connection.execute("INSERT INTO metadata VALUES (?, ?)", params![configfile_version_key, current_configfile_version])?;
           info!("Set configfile {} version to {}", configfile.config_name, current_configfile_version);
        }

        Ok(())
    }

    pub async fn get_versions(&self) -> Result<HashMap<String, String>, Box<dyn Error + Send + Sync>>{
        let connection = self.connection.lock().await;
        let mut versions = HashMap::new();
        let mut stmt = connection.prepare(r#"SELECT key, value FROM metadata WHERE key LIKE "configfile%" OR key == "gitversion""#)?;
        stmt.query_map(params![], |row| {
            let key = row.get("key")?;
            let value = row.get("value")?;
            versions.insert(key, value);
            Ok(())
        })?.collect::<Result<(),_>>()?;

        Ok(versions)
    }

    #[allow(dead_code)]
    pub async fn get_statistics(&self) {
        let connection = self.connection.lock().await;

        let tiles = connection
            .query_row("SELECT COUNT(_ROWID_) FROM tiles", params![], |row| {
                row.get(0)
            })
            .unwrap_or(0);
        let arrays = connection
            .query_row("SELECT COUNT(_ROWID_) FROM arrays", params![], |row| {
                row.get(0)
            })
            .unwrap_or(0);

        let stat = connection.query_row("SELECT sum(pgsize) as size from dbstat", params![], |row| {
            let size: u32 = row.get("size").unwrap();
            Ok(size)
        }).unwrap_or(0) / 1024 / 1024;

        info!("Database contains {} tiles and {} arrays, size {}MB", tiles, arrays, stat);
    }
}
/*
#[cfg(test)]
mod tests {
    use super::*;

    ///Sets up logging
    fn setup() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    ///Deletes potential files created during testing
    fn teardown() {
        let path = std::path::PathBuf::from("test_cache.db");
        if path.exists() {
            //trace!("Deleting file");
            std::fs::remove_file(path).unwrap_or(());
        }
    }

    ///Test that we can actually create a database
    #[tokio::test(flavor = "multi_thread")]
    async fn test_cache_is_created() {
        setup();
        //Create cache
        let cache = Cache::start();
        //Run a query
        cache.get_statistics().await;
        teardown();
    }

    ///Test that we can insert a tile and retrieve it and get informed about whether it is in the cache at all
    #[tokio::test(flavor = "multi_thread")]
    async fn test_cache_can_store_and_retrieve_tiles() {
        setup();

        //Create cache
        let cache = Cache::start();

        //Check that the tile does not exist before we create it
        assert_eq!(
            cache
                .tile_exists_for_source_layer_tilepath_tpage_zpage("test", "test", "W", 0, 0)
                .await,
            false
        );

        //Assemble a query and some data
        let query = SampleQuery::new(
            "test".to_string(),
            "test".to_string(),
            Tile::from_tilepath("W").unwrap(),
            0,
            0,
        );

        let data = vec![127u8; constants::TILESIZE.0 * constants::TILESIZE.1 * 4];

        //Insert the data as the tile given by the query
        cache.insert_tile(&query, Some(data.clone())).await;

        //Check that it is in there now...
        assert_eq!(
            cache
                .tile_exists_for_source_layer_tilepath_tpage_zpage("test", "test", "W", 0, 0)
                .await,
            true
        );

        //... and that the data is alright
        assert!(
            cache
                .get_tile(&query)
                .await
                .unwrap()
                .unwrap()
                == data[..]
        );

        teardown();
    }

    //Test that we can insert an array and retrieve it and get informed about whether it is in the cache at all
    #[tokio::test(flavor = "multi_thread")]
    async fn test_cache_can_store_and_retrieve_arrays() {
        setup();

        //Create cache
        let cache = Cache::start();

        //Check array is not in there already
        assert_eq!(
            cache
                .array_exists_for_source_layer_tpage("test", "test", 0)
                .await,
            false
        );

        //Create data
        let data = vec![127u8; constants::RECOMMENDED_PAGE_SIZE * 20];

        //and insert it
        cache.insert_array("test", "test", 0, Some(data.clone())).await;

        //Check the data is in there now
        assert_eq!(
            cache
                .array_exists_for_source_layer_tpage("test", "test", 0)
                .await,
            true
        );

        //And that we can retrieve it
        assert!(
            cache
                .get_array_for_source_layer_tpage("test", "test", 0)
                .await
                .unwrap()
                .unwrap()
                == data[..]
        );

        teardown();
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn test_cache_handles_broken_arrays_gracefully() {
        setup();

        //Create cache
        let cache = Cache::start();

        //Check our tile is not in there already
        assert_eq!(
            cache
                .array_exists_for_source_layer_tpage("test", "test", 0)
                .await,
            false
        );

        //Create data
        let data = vec![127u8; 20];

        //And insert it
        cache.insert_array("test", "test", 0, Some(data.clone())).await;

        //Break inserted tile
        let connection = cache.connection.lock().await;
        connection
            .execute(
                "UPDATE arrays SET data = ? WHERE source = ? AND layer = ? AND tpage = ?",
                params![&data[..], "test", "test", 0],
            )
            .unwrap();

        std::mem::drop(connection); //Drop connection handle so the other methods can use it

        //Check the data is still there
        assert_eq!(
            cache
                .array_exists_for_source_layer_tpage("test", "test", 0)
                .await,
            true
        );

        //Getting it should return an error
        assert!(cache
            .get_array_for_source_layer_tpage("test", "test", 0)
            .await
            .is_err());

        //and the tile should be deleted
        assert_eq!(
            cache
                .array_exists_for_source_layer_tpage("test", "test", 0)
                .await,
            false
        );

        teardown();
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn test_cache_handles_broken_tiles_gracefully() {
        setup();

        //Create cache
        let cache = Cache::start();

        //Create tile info
        let tile = Tile::from_tilepath("W").unwrap();

        //Check that the tile is not in the db already
        assert_eq!(
            cache
                .tile_exists_for_source_layer_tilepath_tpage_zpage("test", "test", &tile.path, 0u32, 0u32)
                .await,
            false
        );

        //Create data
        let data = vec![127u8; 20];

        //Insert it
        let query = SampleQuery::new("test".to_string(), "test".to_string(), tile.clone(), 0u32, 0u32);

        cache.insert_tile(&query, Some(data.clone())).await;

        //Break inserted tile

        let connection = cache.connection.lock().await;

        connection
            .execute(
                "UPDATE tiles SET data = ? WHERE source = ? AND layer = ? AND tpage = ?",
                params![&data[..], "test", "test", 0],
            )
            .unwrap();

        std::mem::drop(connection); //Drop connection handle so the other functions can use it

        //Check it's still there
        assert_eq!(
            cache
                .tile_exists_for_source_layer_tilepath_tpage_zpage("test", "test", &tile.path, 0, 0)
                .await,
            true
        );

        //Getting it should return an error
        assert!(cache
            .get_tile(&query)
            .await
            .is_err());

        //and the tile should be deleted
        assert_eq!(
            cache
                .tile_exists_for_source_layer_tilepath_tpage_zpage("test", "test", &tile.path, 0, 0)
                .await,
            false
        );

        teardown();
    }
}
*/