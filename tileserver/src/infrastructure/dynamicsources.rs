//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use dashmap::DashMap;
use lazy_static::lazy_static;
use std::sync::atomic::AtomicU64;
use std::sync::atomic::Ordering;
use std::sync::Arc;

lazy_static! {
    pub static ref DYNAMICSOURCES: DynamicSources = DynamicSources::new();
}

pub enum DynamicSourceStatus {
    WaitingForFiles,
    //Initializing,
    Error(String),
    //Cancelled, //Currently unused, this whole file needs to be reworked
    //Finished,
}

impl DynamicSourceStatus {
    pub fn to_string(&self) -> String {
        match &self {
            DynamicSourceStatus::WaitingForFiles => "Waiting for file selection".to_string(),
            //DynamicSourceStatus::Initializing => "Initializing source".to_string(),
            DynamicSourceStatus::Error(e) => format!("Error initializing source: {}", e),
            //DynamicSourceStatus::Cancelled => "Cancelled by user".to_string(),
            //DynamicSourceStatus::Finished => "Finished source initialization".to_string(),
        }
    }
}

pub struct DynamicSources {
    pub status: Arc<DashMap<u64, DynamicSourceStatus>>,
    pub id: AtomicU64,
}

impl DynamicSources {
    pub fn new() -> DynamicSources {
        DynamicSources {
            status: Arc::new(DashMap::new()),
            id: AtomicU64::new(0),
        }
    }

    pub fn get_status_message(&self, id: u64) -> Option<String> {
        self.status.get(&id).map(|s| s.to_string())
    }

    pub fn create_new_source(&self, source_name: String) -> u64 {
        if ! cfg!(feature = "dynamic_sources"){
            self.status.insert(0, DynamicSourceStatus::Error("Dynamic source feature not available".to_string()));
            return 0;
        }
        let id = self.id.fetch_add(1, Ordering::SeqCst);
        self.status.insert(id, DynamicSourceStatus::WaitingForFiles);
        let status = self.status.clone();
        /*tokio::spawn(async move {
            let dialog = native_dialog::FileDialog::new();
            let mut config = SourceConfig::default();

            match dialog.show_open_multiple_file() {
                Ok(files) => {
                    files
                        .iter()
                        .for_each(|f| config.input_files.push(f.clone()));
                    let mut sourceinfo = SourceInfo::default();
                    sourceinfo.category = "User".to_string();
                    status.insert(id, DynamicSourceStatus::Initializing);
                    crate::infrastructure::Registry
                        .write()
                        .unwrap()
                        .create_source(source_name, sourceinfo.instance_name.clone(),  config, sourceinfo).await;
                }
                Err(e) => {
                    //status.insert(id, DynamicSourceStatus::Cancelled);
                    status.insert(id, DynamicSourceStatus::Error(format!("{:?}", e)));
                }
            }
        });*/
        unimplemented!();
        id
    }
}
