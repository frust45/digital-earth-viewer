//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use crate::infrastructure::SAMPLER;
use crate::infrastructure::SOURCELIST;
use log::*;
use std::sync::Arc;
use std::time::Duration;
use tileserver_model::*;
use tokio::time::delay_for;
pub struct Prerenderer;

impl Prerenderer {
    pub async fn prerender(
        wait_time_ms_between_tiles: u64,
        max_prerender_level: u8,
    ) {
        info!("Prerender process started. Waiting 3 seconds before starting prerender process");
        delay_for(Duration::from_millis(3000)).await;
        debug!("Prerender process now active");
        let source_infos = SOURCELIST.get_source_infos();

        let maximum_time_step_count = source_infos
            .iter()
            .map(|s| s.layers.iter())
            .flatten()
            .map(|layer| layer.timesteps.as_ref().map(|t| t.len()).unwrap_or(1))
            .max()
            .unwrap_or(1);

        let maximum_zoom_level = source_infos
            .iter()
            .map(|s| s.layers.iter())
            .flatten()
            .map(|layer| layer.max_zoom_level)
            .max()
            .unwrap_or(1)
            .min(max_prerender_level);

        for tile in TileIterator::new(maximum_zoom_level) {
            for time_index in 0..maximum_time_step_count {
                for s in &source_infos {
                    for layer in &s.layers {
                        for height in 0..layer.zsteps.as_ref().map(|z| z.len()).unwrap_or(1) {
                            //Check the zoom level is still in bounds
                            if (layer.max_zoom_level as usize) < tile.path.len() {
                                continue;
                            }
                            //Check the timestep is still in bounds
                            if layer.timesteps.as_ref().map(|t| t.len()).unwrap_or(1) <= time_index
                            {
                                continue;
                            }
                            //Get the time
                            let time: u32 = time_index as u32;
                            //Check if we are idle
                            while SAMPLER.get_working_threads_number() > 1 {
                                debug!(
                                    "SAMPLER is currently busy with {} threads, waiting 2s",
                                    SAMPLER.get_working_threads_number()
                                );
                                delay_for(Duration::from_millis(2000)).await;
                            }
                            //Check if the tile already exists
                            if ((layer.layer_type == GeoDataType::ScalarTiles
                                || layer.layer_type == GeoDataType::ColorTiles)
                                && cache
                                    .tile_exists_for_source_layer_tilepath_tpage_zpage(
                                        &s.instance_name,
                                        &layer.name,
                                        &tile.path,
                                        time as u32,
                                        height as u32,
                                    )
                                    .await)
                                || cache
                                    .array_exists_for_source_layer_tpage(
                                        &s.instance_name,
                                        &layer.name,
                                        time,
                                    )
                                    .await
                            {
                                continue; //Skip this one
                            }

                            //Sample
                            if layer.layer_type == GeoDataType::ScalarTiles
                                || layer.layer_type == GeoDataType::ColorTiles
                            {
                                debug!(
                                    "Prerendering tile {} for {} {} @ {}, h={}",
                                    tile.path, s.instance_name, layer.name, time, height
                                );
                                if let Err(e) = SAMPLER
                                    .get_tile(
                                        cache.clone(),
                                        &s.instance_name,
                                        &layer.name,
                                        &tile.path,
                                        time,
                                        height as u32,
                                    )
                                    .await
                                {
                                    warn!(
                                        "Error sampling tile {} for {} {} @ {}, h={}: {:?}",
                                        tile.path, s.instance_name, layer.name, time, height, e
                                    );
                                }
                            } else {
                                debug!(
                                    "Prerendering array for {} {} @ tile {}",
                                    s.instance_name, layer.name, tile.path
                                );
                                if let Err(e) = SAMPLER
                                    .get_array(cache.clone(), &s.instance_name, &layer.name, time)
                                    .await
                                {
                                    warn!(
                                        "Error sampling tile for {} {} @ tile {}: {:?}",
                                        s.instance_name, layer.name, tile.path, e
                                    );
                                }
                            }
                            //3. Wait the alloted time
                            delay_for(Duration::from_millis(wait_time_ms_between_tiles)).await;
                        }
                    }
                }
            }
        }
    }
}

pub struct TileIterator {
    current_zoom_level: u8,
    max_zoom_level: u8,
    current_tiles: Vec<Tile>,
    current_tile_index: usize,
}

impl TileIterator {
    pub fn new(max_zoom_level: u8) -> TileIterator {
        TileIterator {
            current_zoom_level: 1,
            max_zoom_level,
            current_tiles: vec![
                Tile::from_tilepath("W").unwrap(),
                Tile::from_tilepath("E").unwrap(),
            ],
            current_tile_index: 0,
        }
    }

    pub fn increase_zoom_level(&mut self) {
        self.current_tile_index = 0;
        self.current_tiles = self
            .current_tiles
            .iter()
            .map(|t| t.split().into_iter())
            .flatten()
            .collect();
        self.current_zoom_level += 1;
    }
}

impl Iterator for TileIterator {
    type Item = Tile;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            if self.current_zoom_level > self.max_zoom_level {
                return None;
            }

            if self.current_tile_index >= self.current_tiles.len() {
                self.increase_zoom_level();
                continue;
            }

            let temp_index = self.current_tile_index;
            self.current_tile_index += 1;
            return Some(self.current_tiles[temp_index].clone());
        }
    }
}
