//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

pub struct Scenelinks;
use log::debug;

impl Scenelinks{
    pub fn scene_description_string_matches_schema(scene: &str) -> bool{
        serde_json::from_str::<scene_description::Scenelink>(scene).map_err(|e| debug!("Scene description does not match schema! {:?}", e)).map(|_v| true).unwrap_or(false)
    }
}

mod scene_description{
    use serde::{Serialize, Deserialize};
    use serde_json::{Value};
    #[allow(non_snake_case)]
    #[derive(Serialize, Deserialize)]
    pub struct Scenelink{
        pub cameraPosition: CameraPosition,
        pub layers: Vec<Layer>,
        pub selectedLayerName: String,
        pub timerange: (f64,f64)
    }
    #[allow(non_snake_case)]
    #[derive(Serialize, Deserialize)]
    pub struct CameraPosition{
        pub Latitude: f64,
        pub Longitude: f64,
        pub Distance: f64,
        pub Elevation: f64,
        pub Azimuth: f64,
        pub VerticalPosition: f64
    }

    #[derive(Serialize, Deserialize)]
    pub struct Layer{
        pub slots: Vec<Slot>,
        pub source_params: Vec<Param>,
        pub filter_pipeline_params: Vec<FilterPipelineParam>,
        pub composition_parameters: Vec<Param>,
        pub info: SceneLayerInfo
    }

    #[derive(Serialize, Deserialize)]
    pub struct Slot{
        pub slot: String,
        pub path: Option<String>
    }

    #[derive(Serialize, Deserialize)]
    pub struct Param{
        pub parameter: String,
        pub value: Value
    }

    #[derive(Serialize, Deserialize)]
    pub struct FilterPipelineParam{
        pub filter: String,
        pub parameter: String,
        pub value: Value
    }

    #[allow(non_snake_case)]
    #[derive(Serialize, Deserialize)]
    pub struct SceneLayerInfo{
        pub visible: bool,
        pub name: String,
        pub typeHint: String
    }

}