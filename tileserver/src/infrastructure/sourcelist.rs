//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use lazy_static::lazy_static;
//use std::collections::HashMap;
use dashmap::DashMap;
use std::sync::Arc;
use tileserver_model::*;

lazy_static! {
    pub static ref SOURCELIST: SourceList = SourceList::new();
}

pub struct SourceListEntry{
    pub name: String,
    pub status: SourceStatus,
    pub info: Option<Arc<SourceInfo>>,
    pub source: Option<Arc<Box<dyn Source>>>
}

impl SourceListEntry{
    pub fn loading(name: String) -> SourceListEntry{
        SourceListEntry{
            name,
            status: SourceStatus::Loading,
            info: None,
            source: None
        }
    }

    pub fn status(&self) -> SourceStatus{
        self.status.clone()
    }

    pub fn info(&self) -> Option<Arc<SourceInfo>>{
        self.info.clone()
    }

    pub fn source(&self) -> Option<Arc<Box<dyn Source>>>{
        self.source.clone()
    }
}

pub struct SourceList {
    entries: DashMap<String, SourceListEntry>
}

impl SourceList {
    pub fn new() -> SourceList {
        SourceList {
            entries: DashMap::new()
        }
    }

    pub fn register_source(&self, instance: String){
        self.entries.insert(instance.clone(), SourceListEntry::loading(instance));
    }

    pub fn add_source_instance(&self, instance: &str, mut info: SourceInfo, source: Box<dyn Source>) {
        if ! self.entries.contains_key(instance){
            self.register_source(instance.to_string());
        }

        if let Some(mut entry ) = self.entries.get_mut(instance){
            entry.source = Some(Arc::new(source));
            info.instance_name = instance.to_string();
            entry.info = Some(Arc::new(info));
            entry.status = SourceStatus::Loaded;
        }else{
            error!("Sourcelist: there should be a source here, map failed");
        }
    }

    pub fn get_source(&self, name: &str) -> Option<Arc<Box<dyn Source>>> {
        self.entries.get(name).and_then(|s| s.source())
    }

    pub fn get_info(&self, name: &str) -> Option<Arc<SourceInfo>> {
        self.entries.get(name).and_then(|s| s.info())
    }

    pub fn get_status(&self, name: &str) -> Option<SourceStatus>{
        self.entries.get(name).map(|s| s.status())
    }

    pub fn set_status(&self, name: &str, status: SourceStatus){
        if let Some(mut entry) = self.entries.get_mut(name){
            entry.status = status;
        }else{
            warn!("Tried to set status on non-existant source {} to {:?}", name, status);
        }
    }

    pub fn get_status_list(&self) -> Vec<(String, SourceStatus)>{
        self.entries.iter().map(|v| (v.key().clone(), v.value().status())).collect()
    }

    #[allow(dead_code)]
    pub fn does_source_need_caching(&self, sourcename: &str) -> bool {
        self.get_info(sourcename)
            .map(|i| i.cacheable)
            .unwrap_or(true)
    }
}
