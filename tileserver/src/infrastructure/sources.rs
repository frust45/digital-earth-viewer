//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

//Registry
use crate::infrastructure::Registry;
//Sourcelist
use crate::infrastructure::SOURCELIST;
//Traits
use tileserver_model::*;


macro_rules! make_init_fn {
    ($init:expr) => (
        {Box::new(|source_config, source_info| Box::pin(async move {$init(source_config, source_info).await}))}
    );
}

#[allow(clippy::clippy::too_many_lines)]
pub async fn initialize_source_constructors() {
    #[cfg(feature = "source_basic_earth")]
    Registry
        .add_source(
            "basic_earth".to_string(),
            Box::new(basic_earth_source::BasicEarthSource::accepts_file),
            make_init_fn!(basic_earth_source::BasicEarthSource::initialize)
            //Box::new(|source_config, source_info| Box::pin(async move {basic_earth_source::BasicEarthSource::initialize(source_config, source_info).await})),
        );
    
    #[cfg(feature = "source_basic_earth")]
    start_basic_earth_source().await;

    #[cfg(feature = "source_bluemarble")]
    Registry
        .add_source(
            "bluemarble".to_string(),
            Box::new(bluemarble_source::BlueMarbleSource::accepts_file),
            Box::new(bluemarble_source::BlueMarbleSource::initialize),
        );

    #[cfg(feature = "source_netcdf")]
    Registry
        .add_source(
            "netcdf".to_string(),
            Box::new(netcdf_source::NetCDFSource::accepts_file),
            make_init_fn!(netcdf_source::NetCDFSource::initialize)
        );

    #[cfg(feature = "source_esri_ascii")]
    Registry
        .add_source(
            "esri_ascii_grid".to_string(),
            Box::new(esri_ascii_grid_source::EsriAsciiGridSource::accepts_file),
            make_init_fn!(esri_ascii_grid_source::EsriAsciiGridSource::initialize),
        );

    #[cfg(feature = "source_odv")]
    Registry
        .add_source(
            "odv".to_string(),
            Box::new(odv_source::ODVSource::accepts_file),
            make_init_fn!(odv_source::ODVSource::initialize),
        );

    #[cfg(feature = "source_csv")]
    Registry
        .add_source(
            "csv".to_string(),
            Box::new(csv_source::CSVSource::accepts_file),
            make_init_fn!(csv_source::CSVSource::initialize),
        );

    #[cfg(feature = "source_gpx")]
    Registry
        .add_source(
            "gpx".to_string(),
            Box::new(gpx_source::GPXSource::accepts_file),
            make_init_fn!(gpx_source::GPXSource::initialize),
        );

    #[cfg(feature = "source_currents")]
    Registry
        .add_source(
            "currents".to_string(),
            Box::new(currents_source::CurrentsSource::accepts_file),
            make_init_fn!(currents_source::CurrentsSource::initialize),
        );

    #[cfg(feature = "source_turbidity")]
    Registry
        .add_source(
            "turbidity".to_string(),
            Box::new(turbidity_source::TurbiditySource::accepts_file),
            make_init_fn!(turbidity_source::TurbiditySource::initialize),
        );

    #[cfg(feature = "source_maptiles")]
    Registry
        .add_source(
            "maptiles".to_string(),
            Box::new(maptiles_source::MapTilesSource::accepts_file),
            make_init_fn!(maptiles_source::MapTilesSource::initialize),
        );

    #[cfg(feature = "source_test")]
    Registry
        .add_source(
            "test".to_string(),
            Box::new(test_source::TestSource::accepts_file),
            make_init_fn!(test_source::TestSource::initialize),
        );

    #[cfg(feature = "source_wms")]
    Registry
        .add_source(
            "wms".to_string(),
            Box::new(wms_source::WMSSource::accepts_file),
            make_init_fn!(wms_source::WMSSource::initialize),
        );

    #[cfg(feature = "source_geoimage")]
    Registry
        .add_source(
            "geoimage".to_string(),
            Box::new(geoimage_source::GeoImageSource::accepts_file),
            make_init_fn!(geoimage_source::GeoImageSource::initialize),
        );

    #[cfg(feature = "source_geotiff")]
    Registry
        .add_source(
            "geotiff".to_string(),
            Box::new(geotiff_source::GeoTiffSource::accepts_file),
            make_init_fn!(geotiff_source::GeoTiffSource::initialize),
        );

    #[cfg(feature = "source_sqlite")]
    Registry
        .add_source(
            "sqlite".to_string(),
            Box::new(sqlite_source::SQLiteSource::accepts_file),
            make_init_fn!(sqlite_source::SQLiteSource::initialize),
        );

    #[cfg(feature = "source_remote")]
    Registry
        .add_source(
            "remote".to_string(),
            Box::new(remote_source::RemoteSource::accepts_file),
            make_init_fn!(remote_source::RemoteSource::initialize),
        );

    #[cfg(feature = "source_k_nearest")]
    Registry
        .add_source(
            "k_nearest".to_string(),
            Box::new(k_nearest_source::KNearestSource::accepts_file),
            make_init_fn!(k_nearest_source::KNearestSource::initialize),
        );

    #[cfg(feature = "source_shapefile")]
    Registry
        .add_source(
            "shapefile".to_string(),
            Box::new(shapefile_source::ShapeFileSource::accepts_file),
            make_init_fn!(shapefile_source::ShapeFileSource::initialize),
        );

    #[cfg(feature = "source_onnx")]
    Registry
        .add_source(
            "onnx".to_string(),
            Box::new(onnx_source::OnnxSource::accepts_file),
            make_init_fn!(onnx_source::OnnxSource::initialize),
        );

    #[cfg(feature = "source_filterkernel")]
    Registry
        .add_source(
            "filterkernel".to_string(),
            Box::new(filterkernel_source::FilterKernelSource::accepts_file),
            make_init_fn!(filterkernel_source::FilterKernelSource::initialize),
        );

    #[cfg(feature = "source_time_merging")]
    Registry
        .add_source(
            "time_merging".to_string(),
            Box::new(time_merging_source::TimeMergingSource::accepts_file),
            make_init_fn!(time_merging_source::TimeMergingSource::initialize),
        );

    #[cfg(feature = "source_hierarchical")]
    Registry
        .add_source(
            "hierarchical".to_string(),
            Box::new(hierarchical_source::HierarchicalSource::accepts_file),
            make_init_fn!(hierarchical_source::HierarchicalSource::initialize),
        );

    #[cfg(feature = "source_mipmap")]
    Registry
        .add_source(
            "mipmap".to_string(),
            Box::new(mipmap_source::MipmapSource::accepts_file),
            make_init_fn!(mipmap_source::MipmapSource::initialize),
        );
}

#[cfg(feature = "source_basic_earth")]
async fn start_basic_earth_source() {
    let basic_info = SourceInfo::default();
    let (basic_info, basic) =
        basic_earth_source::BasicEarthSource::initialize(SourceConfig::default(), basic_info).await.unwrap();
    SOURCELIST.add_source_instance("Basic Earth Source", basic_info, basic);



}
