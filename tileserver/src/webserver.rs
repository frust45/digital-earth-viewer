//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

use bytes::Bytes;
use percent_encoding::percent_decode_str;
use tileserver_model::UEC;
use warp::{Reply, filters::BoxedFilter, hyper::{Response, StatusCode}, ws::{Message, WebSocket}};
use warp::Filter;
use crate::infrastructure::{self, SOURCELIST};
use crate::infrastructure::SAMPLER;
use std::{collections::HashMap, sync::{Arc, atomic::{AtomicUsize, Ordering}}};
use futures::{FutureExt, StreamExt};
use tokio::sync::{mpsc, RwLock};
use tokio_stream::wrappers::UnboundedReceiverStream;
use lazy_static::lazy_static;
use log::*;


fn statuslist() -> BoxedFilter<(impl Reply, )>{
    warp::path!("statuslist").map(|| warp::reply::json(&SOURCELIST.get_status_list())).boxed()
}

fn sourcestatus() -> BoxedFilter<(impl Reply, )>{
    warp::path!("source_info" / String)
    .map(|instance_name: String| {
        let instance_name = percent_decode_str(&instance_name).decode_utf8_lossy().to_string();
        match SOURCELIST.get_info(&instance_name){
            Some(info) => {
                warp::reply::json(&*info).into_response()
            }
            None => {
                warp::reply::with_status(format!("No source instance with name {}", instance_name),StatusCode::NOT_FOUND).into_response()
            }
        }
    }).boxed()
}

fn versions() -> BoxedFilter<(impl Reply, )>{
    warp::path("versions")
    .and_then( || async {
        match SAMPLER.get_cache().get_versions().await{
            Ok(versions) => {
                Response::builder()
                        .status(StatusCode::OK)
                        .header("content-type", "application/json")
                        .body(serde_json::to_string_pretty(&versions).expect("No idea how you managed to make a serialization of String->String fail"))
                        .map_err(|_e| warp::reject::reject())
            },
            Err(e) => {
                Response::builder()
                        .status(StatusCode::INTERNAL_SERVER_ERROR)
                        .body(format!("Error retrieving versions {:?}", e))
                        .map_err(|_e| warp::reject::reject())
            }
        }
    }).boxed()
}
/*fn sourcelist() -> BoxedFilter<(impl Reply,)>{
    warp::path!("sourcelist").map(|| warp::reply::json(&SOURCELIST.get_source_infos())).boxed()
}*/

fn tilesize() -> BoxedFilter<(impl Reply,)>{
    warp::path!("tilesize").map(|| warp::reply::json(&tileserver_model::constants::TILESIZE)).boxed()
}
 
fn initial_layers() -> BoxedFilter<(impl Reply,)>{
    warp::path!("initial_layers").and(warp::fs::file("initial_layers.json")).boxed()
}
        
fn tiles() -> BoxedFilter<(impl Reply,)>{
    warp::path!("tiles" / String / String / String / u32 / u32)
    .and_then(
        async move |instance: String, layer: String, tilepath: String, tpage: u32, zpage: u32| {
            let instance = percent_decode_str(&instance).decode_utf8_lossy().to_string();
            let layer = percent_decode_str(&layer).decode_utf8_lossy().to_string();
            match SAMPLER
                .get_tile( &instance, &layer, &tilepath, tpage, zpage)
                .await
            {
                Ok(Some(data)) => {
                    Response::builder()
                        .status(StatusCode::OK)
                        .body(data)
                        .map_err(|_e| warp::reject::reject())
                }
                Ok(None) => {
                    Response::builder()
                        .status(StatusCode::NO_CONTENT)
                        .body(Bytes::from("Empty Tile."))
                        .map_err(|_e| warp::reject::reject())
                }
                Err(e) => {
                    Response::builder()
                        .status(StatusCode::INTERNAL_SERVER_ERROR)
                        .body(Bytes::from(format!("{:?}", e)))
                        .map_err(|_e| warp::reject::reject())
                }
            }
        },
    ).boxed()
}

fn arrays() -> BoxedFilter<(impl Reply,)>{
    warp::path!("array" / String / String / u32)
    .and_then(
        async move |instance: String, layer: String, tpage: u32| {
            let instance = percent_decode_str(&instance).decode_utf8_lossy().to_string();
            let layer = percent_decode_str(&layer).decode_utf8_lossy().to_string();
            match SAMPLER.get_array(&instance, &layer, tpage).await {
                Ok(Some(data)) => {
                    Response::builder()
                        .status(StatusCode::OK)
                        .body(data)
                        .map_err(|_e| warp::reject::reject())
                }
                Ok(None) => {
                    Response::builder()
                        .status(StatusCode::NO_CONTENT)
                        .body(Bytes::from("Empty Array"))
                        .map_err(|_e| warp::reject::reject())
                }
                Err(e) => {
                    Response::builder()
                        .status(StatusCode::INTERNAL_SERVER_ERROR)
                        .body(Bytes::from(format!("{:?}", e).into_bytes()))
                        .map_err(|_e| warp::reject::reject())
                }
            }
        },
    ).boxed()
}

fn sample_points() -> BoxedFilter<(impl Reply,)>{
    warp::path!(
        "sample_points" / String / String / f32 / f32 / f32 / f32 / f32 / f32 / i64 / i64
    )
    .and_then(
        async move |instance: String,
        layer: String,
        x_min: f32,
        y_min: f32,
        z_min: f32,
        x_max: f32,
        y_max: f32,
        z_max: f32,
        tmin: i64,
        tmax: i64|{
            let instance = percent_decode_str(&instance).decode_utf8_lossy().to_string();
            let layer = percent_decode_str(&layer).decode_utf8_lossy().to_string();
            let pos_min = UEC::xy(f64::from(x_min), f64::from(y_min));
            let pos_max = UEC::xy(f64::from(x_max), f64::from(y_max));
            match SAMPLER
                .sample_points(
                     &instance, &layer, pos_min, pos_max, z_min, z_max, tmin, tmax,
                )
                .await
            {
                Ok(data) => {
                    Response::builder()
                        .status(StatusCode::OK)
                        .body(data)
                        .map_err(|_e| warp::reject::reject())
                },
                Err(e) => {
                    Response::builder()
                        .status(StatusCode::INTERNAL_SERVER_ERROR)
                        .body(format!("{:?}", e).into_bytes().into())
                        .map_err(|_e| warp::reject::reject())
                }
            }
        },
    ).boxed()
}

fn get_scene_content() -> BoxedFilter<(impl Reply,)>{
    warp::path!(
        "scene" / String
    )
    .and_then(
        async move |slug: String| {
            let slug = percent_decode_str(&slug)
            .decode_utf8_lossy()
            .to_string();
            match SAMPLER.retrieve_scene(&slug).await{
                Ok(Some(data)) => {
                    Response::builder()
                        .status(StatusCode::OK)
                        .body(data.into_bytes())
                        .map_err(|_e| warp::reject::reject())
                }
                Ok(None) => {
                    Response::builder()
                        .status(StatusCode::NOT_FOUND)
                        .body("No scene here".to_string().into_bytes())
                        .map_err(|_e| warp::reject::reject())
                }
                Err(e) => {
                    Response::builder()
                        .status(StatusCode::INTERNAL_SERVER_ERROR)
                        .body(format!("{:?}", e).into_bytes())
                        .map_err(|_e| warp::reject::reject())
                }
            }
        }
    ).boxed()
}

fn get_metadata() -> BoxedFilter<(impl Reply,)>{
    warp::path!(
        "metadata" / String / String
    ).and_then(async move |source: String, metadata: String| {
        match SAMPLER.get_metadata(&source, &metadata).await{
            Ok(metadata) => {
                Response::builder()
                    .status(StatusCode::OK)
                    .body(metadata)
                    .map_err(|_e| warp::reject())
            },
            Err(e) => {
                Response::builder()
                    .status(StatusCode::INTERNAL_SERVER_ERROR)
                    .body(format!("{:?}", e).into_bytes())
                    .map_err(|_e| warp::reject::reject())
            }
        }
    }).boxed()
}

fn insert_scene_content() -> BoxedFilter<(impl Reply,)>{
    warp::path!(
        "create_scene" / String
    ).and(warp::body::content_length_limit(1024 * 256))
    .and(warp::body::bytes())
    .and_then(
        async move |slug: String, bytes: warp::hyper::body::Bytes| {
            let slug = percent_decode_str(&slug)
            .decode_utf8_lossy()
            .to_string();
            let content = bytes.to_vec();
            match String::from_utf8(content).map_err(|e| format!("Could not decode string: {:?}", e)){
                Ok(content_str) => {
                    match SAMPLER.create_scene(&slug, &content_str).await{
                        Ok(slug) => {
                            Response::builder()
                                .status(StatusCode::OK)
                                .body(slug.into_bytes())
                                .map_err(|_e| warp::reject::reject())
                        },
                        Err(e) => {
                            Response::builder()
                                .status(StatusCode::INTERNAL_SERVER_ERROR)
                                .body(format!("{:?}", e).into_bytes())
                                .map_err(|_e| warp::reject::reject())
                        }
                    }
                },
                Err(e) => {
                    Response::builder()
                        .status(StatusCode::INTERNAL_SERVER_ERROR)
                        .body(format!("{:?}", e).into_bytes())
                        .map_err(|_e| warp::reject::reject())
                }
            }            
        }
    ).boxed()
}

fn registrylist() -> BoxedFilter<(impl Reply,)>{
    warp::path("registry")
    .map(|| warp::reply::json(&infrastructure::Registry.get_source_names())).boxed()
}

lazy_static!{
    static ref ROOMS: Arc<std::sync::RwLock<HashMap<String, 
    Arc<RwLock<HashMap<usize, mpsc::UnboundedSender<Result<Message, warp::Error>>>>>
>>> = Arc::new(std::sync::RwLock::new(HashMap::new())); 
}

static CLIENT_ID: AtomicUsize = AtomicUsize::new(1);

fn syncrooms() -> BoxedFilter<(impl Reply,)>{
    warp::path!("sync" / String)
    .and(warp::ws())
    .map(|roomname: String, ws: warp::ws::Ws| {
        let room = {
            let mut rooms_write =  ROOMS.write().unwrap();
            let r = rooms_write.entry(roomname.clone()).or_insert_with(|| Arc::new(RwLock::new(HashMap::new())));
            r.clone()
        };

        let roomname = Arc::new(roomname);

        ws.on_upgrade(move |socket: WebSocket| async move{
            //Generate a new id
            let id = Arc::new(CLIENT_ID.fetch_add(1, Ordering::Relaxed));
            info!("Client {} connected to room {}", id, &roomname);
            //Split the socket into sender and receiver
            let (user_tx, mut user_rx) = socket.split();

            let (tx, rx) = mpsc::unbounded_channel();
            let rx = UnboundedReceiverStream::new(rx);

            //Build a send queue and forward all messages sent to it to the websocket (log on error)
            let idclone = id.clone();
            tokio::task::spawn(rx.forward(user_tx).map(move |result| {
                if let Err(e) = result {
                    warn!("Websocket (ID={}) send error: {}", idclone, e);
                }
            }));

            //Insert client into room list
            {
                room.write().await.insert(*id, tx);
            }

            //Handle received messages
            while let Some(message) = user_rx.next().await{
                match message{
                    Err(e) => {
                        warn!("Websocket (ID={}) receive error: {}", id, e);
                        break;
                    }
                    Ok(message) => {
                        //Check that messages are string and not binary
                        if let Ok(message) = message.to_str(){
                            for (&uid, tx) in room.read().await.iter(){
                                if *id != uid{ //Send to everyone but ourselves
                                    let _user_disconnected_error = tx.send(Ok(Message::text(message))); //Disconnected users get handled later
                                }
                            }
                        }
                    }
                }
            }

            //Handle disconnection (user_rx stream will have next until user disconnects)
            {
                room.write().await.remove(&*id);

            }

        })
    }).boxed()
}











#[cfg(feature = "bundled")]
fn static_files() -> BoxedFilter<(impl Reply, )>{
    warp::path::full().and_then(async move |full_path: warp::path::FullPath| {
        let full_path_str = full_path.as_str();
        let path = if full_path_str.len() > 1 {
            &full_path_str[1..]
        } else {
            "index.html"
        };

        let content_type = mime_guess::from_path(path).first_or_text_plain();

        match tileclient::bundled_file(path) {
            Some(f) => {
                return Response::builder()
                    .status(StatusCode::OK)
                    .header("Content-Type", content_type.essence_str())
                    .header("Content-Length", f.len())
                    .body(Vec::from(f))
                    .map_err(|_e| warp::reject::reject());
            }
            None => {
                return Response::builder()
                    .status(StatusCode::NOT_FOUND)
                    .body(
                        format!("File `{}` not found.", full_path.as_str())
                            .into_bytes(),
                    )
                    .map_err(|_e| warp::reject::reject());
            }
        }
    }).boxed()
}

#[cfg(not(feature = "bundled"))]
fn static_files() -> BoxedFilter<(impl Reply,)>{
    warp::fs::dir("dist").boxed()
}

pub fn routes() -> BoxedFilter<(impl Reply, )>{
    warp::any().and(
        warp::get()
            .and(initial_layers()
                .or(syncrooms())
                .or(statuslist())
                .or(sourcestatus())
                .or(versions())
                .or(tilesize())
                .or(tiles())
                .or(arrays())
                .or(sample_points())
                .or(registrylist())
                //.or(dyn_source_status())
                .or(get_scene_content())
                .or(get_metadata())
                .or(static_files()))
    ).or(
        warp::post()
            .and(/*dyn_source_add()
                .or(*/insert_scene_content())//)
    ).boxed()
}