//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

#![windows_subsystem = "console"]
#![feature(trait_alias)]
#![feature(async_closure)]
#![feature(panic_info_message)]
#![warn(clippy::pedantic)]

use futures::stream::{StreamExt, FuturesUnordered};
use log::*;
use simplelog::*;
use tokio::runtime::Builder;
//Infrastructure
mod infrastructure;
use crate::infrastructure::SAMPLER;

//Configuration
use crate::infrastructure::source_config::ConfigFile;

//Web
mod webserver;

//Sourcelist
use crate::infrastructure::SOURCELIST;

use tileserver_model::{SourceConfig, SourceInfo, log, tokio};

use dashmap::DashMap;

use std::default::Default;

use lazy_static::lazy_static;

mod launcher;

lazy_static! {
    static ref COMPILED_PROGRAMS: DashMap<String, String> = DashMap::new();
}

async fn initialize_configurations() {
    //Check git version
    SAMPLER.get_cache().check_git_hash().await.expect("Something went wrong checking for the current git version");

    //Find files in directory that might match
    let configfiles = std::fs::read_dir(".")
        .expect("Could not read files from directory")
        .filter_map(Result::ok)
        .filter(|f| 
            f.file_type().map_or(false, |t| t.is_file()) && 
            f.path().file_name().map_or(false, |name| name.to_string_lossy().ends_with(tileserver_model::constants::INITIAL_SOURCES_EXT))
        ).map(|f| f.path())
        .map(|f|ConfigFile::from_path(&f)
            .unwrap_or_else(|e| panic!("Error parsing the '{}' configuration file: {:?}", f.to_string_lossy(), e))
        ).collect::<Vec<_>>();

        if configfiles.is_empty(){
            warn!("No files ending in sources.toml found. Add more sources by creating this configuration file.");
        }

        for configfile in &configfiles {
            SAMPLER.get_cache().check_config_file_version(&configfile).await.unwrap_or_else(|_| panic!("Something went wrong checking for the correct config file version for config file {}", configfile.config_name));
        }


    configfiles.into_iter().flat_map(|config_file | {
        info!("Adding sources from config file {}", config_file.config_name);
        config_file.sources
    }).map(|(name, source)| {
        async move {
            //Create and prefill source config & source info
            let mut s = SourceConfig::default();
            let mut i = SourceInfo::default();
            i.instance_name = name.to_string();
            i.category = source.category.clone();
            i.attribution = source.attribution.clone();
            //Get PathBufs
            s.input_files = source.files.clone();
            s.settings = source.settings.clone();

            // if source there, if so, initialize source with 'create_source()'
            let source_clone = source.source.clone();
            let name_clone = name.clone();
            info!("Initializing source {} as new {} source\n\t{:?}",  name, source.source, s);
            tokio::spawn(async {
                infrastructure::Registry.create_source(source_clone, name_clone, s, i).await;
            }).await.expect("Really bad error happened during source init");
        }
    }).collect::<FuturesUnordered<_>>().collect::<()>().await;

    info!("All config files read!");

}
/// main function. Initializes all source types first. Then loads server configuration and intitializes each source.
fn main() {
    //Set up panic handling
    if !cfg!(debug_assertions) {
        std::panic::set_hook(Box::new(|pi: &std::panic::PanicInfo| {
            if let Some(args) = pi.message() {
                let message = format!("{:?}", args);
                println!("{}", if message.len() < 1000 { message } else {message[0..1000].to_string()});
                std::process::exit(0);
            }
        }));
    }
    //Init logging
    CombinedLogger::init(vec![
        TermLogger::new(
            //Currently the only way to detect debug builds, but may be overridden in Cargo.toml.
            //TODO: replace once dedicated cfg option becomes available.
            if cfg!(debug_assertions) {
                LevelFilter::max()
            } else {
                LevelFilter::Warn
            },
            ConfigBuilder::new()
                .add_filter_ignore_str("hyper")
                .add_filter_ignore_str("warp")
                .add_filter_ignore_str("mio")
                .add_filter_ignore_str("want")
                .add_filter_ignore_str("tracing::span::active")
                .add_filter_ignore_str("tracing::span")
                .add_filter_ignore_str("reqwest::blocking::wait")
                .add_filter_ignore_str("tract_core")
                .add_filter_ignore_str("tract_hir")
                .add_filter_ignore_str("tract_onnx")
                .add_filter_ignore_str("tokio_tungstenite")
                .add_filter_ignore_str("tungstenite")
                .add_filter_ignore_str("reqwest::blocking::client")
                .build(),
            TerminalMode::Mixed,
            ColorChoice::Auto
        ),
        //.expect("Could not create terminal logger"),
        WriteLogger::new(
            LevelFilter::max(),
            ConfigBuilder::new()
                .add_filter_ignore_str("hyper")
                .add_filter_ignore_str("warp")
                .add_filter_ignore_str("mio")
                .add_filter_ignore_str("want")
                .add_filter_ignore_str("tracing::span::active")
                .add_filter_ignore_str("tracing::span")
                .add_filter_ignore_str("reqwest::blocking::wait")
                .add_filter_ignore_str("tract_core")
                .add_filter_ignore_str("tract_hir")
                .add_filter_ignore_str("tract_onnx")
                .add_filter_ignore_str("tokio_tungstenite")
                .add_filter_ignore_str("tungstenite")
                .add_filter_ignore_str("reqwest::blocking::client")
                .build(),
            std::fs::File::create("tileserver.log")
                .expect("Could not create file logger into 'tileserver.log'"),
        ),
    ])
    .expect("Could not create combined logger");


    //Initialize tokio runtime
    let runtime = Builder::new_multi_thread().thread_stack_size(64 * 1024 * 1024).enable_all().build().unwrap();

    runtime.block_on(async move {

        //Initialize dataprovider
        SAMPLER.register_callbacks();

        infrastructure::initialize_source_constructors().await;

        //Initialize webserver
        let port = std::env::var("PORT").ok()
            .and_then(|v| v.parse::<u16>().ok())
            .unwrap_or_else(|| {warn!("PORT not set, defaulting to 8080."); 8080});
        let addr = std::env::var("ADDRESS").ok()
            .map(|v| v.split('.').filter_map(|p| p.parse::<u8>().ok()).collect::<Vec<_>>())
            .and_then(|v| if v.len() == 4 {Some([v[0],v[1],v[2],v[3]])}else{None})
            .unwrap_or_else(|| {warn!("IP not set, defaulting to 0.0.0.0"); [0,0,0,0]});

        let addr = (addr, port);
        let r = warp::serve(webserver::routes()).run(addr);

        //Initialize source
        
        let webserver_task = tokio::spawn(r);
        info!("Starting server on {}.{}.{}.{}:{}", addr.0[0], addr.0[1], addr.0[2], addr.0[3], addr.1);


        if cfg!(feature = "open") {
            #[cfg(feature = "open")]
            launcher::open_in_browser(addr)
        }

        if cfg!(feature = "gui") {
            #[cfg(feature = "gui")]
            launcher::open_in_webview_gui(addr);
        }

        initialize_configurations().await;

        webserver_task.await.unwrap();

    });

}
