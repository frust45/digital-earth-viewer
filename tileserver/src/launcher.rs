//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

#[cfg(feature = "open")]
pub fn open_in_browser(addr: ([u8; 4], u16)) {
    use log::*;
    use std::process::{Command, Stdio};
    let addr = if addr.0 == [0,0,0,0] {([127,0,0,1], addr.1)} else {addr};
    let _p;
    if cfg!(target_os = "windows") {
        _p = Command::new("cmd")
            .arg("/C")
            .arg(format!(
                "start http://{}.{}.{}.{}:{}",
                addr.0[0], addr.0[1], addr.0[2], addr.0[3], addr.1
            ))
            .stdin(Stdio::null())
            .stderr(Stdio::null())
            .stdout(Stdio::null())
            .spawn()
            .map_err(|e| {
                error!(
                    "Could not open in browser with \">cmd /C start ...\": {:?}",
                    e
                );
            });
    } else if cfg!(target_os = "linux") {
        //TODO: Test this on linux
        _p = Command::new("xdg-open")
            .arg(format!(
                "http://{}.{}.{}.{}:{}",
                addr.0[0], addr.0[1], addr.0[2], addr.0[3], addr.1
            ))
            .stdin(Stdio::null())
            .stderr(Stdio::null())
            .stdout(Stdio::null())
            .spawn()
            .map_err(|e| {
                error!("Could not open in browser with \"$ xdg-open ...\": {:?}", e);
            });
    } else if cfg!(target_os = "macos") {
        _p = Command::new("open")
            .arg(format!(
                "http://{}.{}.{}.{}:{}",
                addr.0[0], addr.0[1], addr.0[2], addr.0[3], addr.1
            ))
            .stdin(Stdio::null())
            .stderr(Stdio::null())
            .stdout(Stdio::null())
            .spawn()
            .map_err(|e| {
                error!("Could not open in browser with \"$ open ...\": {:?}", e);
            });
    } else {
        warn!("No known way to open a web browser on this platform. If you know how to do it, submit a patch to the project!");
    }
}


#[cfg(feature = "gui")]
pub fn open_in_webview_gui(addr: ([u8; 4], u16)){
    use log::*;
    std::thread::spawn(move || {
        let data_dir_path = std::path::Path::new("./userdata");
        let result: Result<alcro::UI, String> = (||{
            if !data_dir_path.exists(){
                std::fs::create_dir(data_dir_path).map_err(|e| format!("No write access to current directory. Unable to store user settings: {:?}", e))?;
            }
            let addr = if addr.0 == [0,0,0,0] {([127,0,0,1], addr.1)} else {addr};
            let ui = alcro::UIBuilder::new()
            .content(alcro::Content::Url(&format!(
                "http://{}.{}.{}.{}:{}/",
                addr.0[0], addr.0[1], addr.0[2], addr.0[3], addr.1)))
            .size(1280, 720)
            .user_data_dir(&std::path::Path::new("./userdata").canonicalize().map_err(|e| format!("Error finding user data dir {:?}", e))?)
            .run().map_err(|e| format!("GUI crashed: {:?}", e))?;
            Ok(ui)
        })();

        match result {
            Err(err) => {
                error!("Error starting GUI: {}", err);
            },
            Ok(ui) => {
            let result: Result<(), String> = (||{
                ui.wait_finish();

                let cache_dir_path = data_dir_path.join("Default").join("Cache");
                if cache_dir_path.exists(){
                    std::fs::remove_dir_all(cache_dir_path).map_err(|e| format!("Could not clean up residue: {:?}", e))?;
                }
            Ok(())
            })();

            if let Err(e) = result{
                error!("Error in GUI shutdown: {}", e);
            }

            std::process::exit(0);
        }
        };
    });
}