# Digital Earth Viewer
4D viewer for georeferenced data. Development is part of [Digital Earth](https://www.digitalearth-hgf.de). The Digital Earth Viewer was [presented at EuroVis 2021](https://youtu.be/iIRBpXOWuPg?t=123).

[Here](https://diglib.eg.org/bitstream/handle/10.2312/envirvis20211081/033-037.pdf) we present the software architecture and [here](https://diglib.eg.org/bitstream/handle/10.2312/envirvis20211082/039-042.pdf) we demonstrate a use case.

## License

Copyright Jens Greinert, Valentin Buck, Flemming Stäbler, Everardo Gonzalez, Jochen Mohrmann at the GEOMAR Helmholtz-Centre 2021.

The Digital Earth Viewer is licensed under the EUPL v.1.2.

This means for you:
- by downloading, using or modifying this software, you choose to accept this license ([section 10](LICENSE.md#10-Acceptance-of-the-Licence))
- you are free to use, reproduce and modify the work ([section 2](LICENSE.md#2-Scope-of-the-rights-granted-by-the-Licence))
- you need to keep intact all license, copyright and other attribution notices ([section 5](LICENSE.md#5-Obligations-of-the-Licensee))
- if you modify the software, you need to prominently mark it as such and provide the modified source code under this or a later version of this license ([section 5](LICENSE.md#5-Obligations-of-the-Licensee))
- you accept that we do not offer warranty for this work ([section 7](LICENSE.md#7-Disclaimer-of-Warranty)) and are not liable for damages caused by it ([section 8](LICENSE.md#8-Disclaimer-of-Liability))

A full license text can be found in the file [LICENSE.md](LICENSE.md).
Translations are available at https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12 

## Release and Download

The gitlab builds new versions of the Digital Earth Viewer daily.

Click on your operating system icon to download a bundled desktop executable.

[<img width="120px" src="documentation/icons/Logo-ubuntu_no_r_-black_orange-hex.svg.png">](https://git.geomar.de/digital-earth/digital-earth-viewer/-/jobs/artifacts/master/raw/tileserver/target/release/digitalearthviewer?job=build_bundled_linux)

[<img width="120px" src="documentation/icons/320px-Windows_darkblue_2012.svg.png">](https://git.geomar.de/digital-earth/digital-earth-viewer/-/jobs/artifacts/master/raw/tileserver/target/release/digitalearthviewer.exe?job=build_bundled_windows)

There are also [releases](https://git.geomar.de/digital-earth/digital-earth-viewer/-/releases) prepared every few months which contain other platform builds as well.

## Manuals

[<img width="120px" src="documentation/icons/DEV_Handbook_Icon.png">](documentation/pdf_manuals/UserManual.pdf)
[User Manual](documentation/pdf_manuals/UserManual.pdf)

[<img width="120px" src="documentation/icons/DEV_Difference_Layer_Icon.png">](documentation/pdf_manuals/ColorScalarLayers.pdf)
[Layer Recipe Cookbook](documentation/pdf_manuals/ColorScalarLayers.pdf)

[<img width="120px" src="documentation/icons/DEV_Video_Icon.png">](https://git.geomar.de/digital-earth/digital-earth-viewer/-/wikis/One-Minute-Tutorials)
[Video Tutorials](https://git.geomar.de/digital-earth/digital-earth-viewer/-/wikis/One-Minute-Tutorials)


## Running A Prebuilt Binary Locally
The Server hosts the website and the tiles on http://127.0.0.1:8080. It should automatically open in your default browser.
You can change the port using the `PORT` environment variable.

## Building The Tileserver

Building the tileserver takes the same steps on Windows, Linux and MacOS:
* [Download and install Rust](https://rustup.rs).
    * On some systems you might have to log out and log in again for the `PATH` environment variable to update after installation. You may even have to set it manually.
* Switch to the `nightly` branch. Note that some components of the rust compiler may not be available with every nightly build.
    * `tileserver` needs to be built with `nightly` because it uses async closures, which aren't supported in `stable` yet.
    * Switch using `$ rustup default nightly`.
* Install a C++ compiler, e.g. `msvc` on Windows or `gcc` on Linux.
    * Be careful to have an up-to-date version of the `msvc`. It is usually installed with the [Visual Studio Community Edition](https://visualstudio.microsoft.com/download) or the [Microsoft Build Tools for C++](https://visualstudio.microsoft.com/visual-cpp-build-tools/).
* Install [cmake](https://cmake.org/).
* Install [GNU M4](https://www.gnu.org/software/m4/m4.html). On Windows, it is available through [`mingw-get`](http://www.mingw.org/wiki/Getting_Started).
    * This is required to build `hdf5` for static linking.
* Build using `$ cargo build` or `$ cargo build --release` from the tileserver directory. A first-time build may take 15 minutes or more. Later builds will be faster.
    * Check out `Cargo.toml` which lists in the `[features]` section optional features that do not get compiled into the binary by default. For example, you can bundle the client into the server binary. 
    * Be sure to have sufficient disk space to store the build artifacts. Building both `dev` and `release` will result in several GB of build artifacts for incremental builds, even though the release binary is only 8.5 MB. Use a filesystem that supports symbolic links (not FAT32).
* Build with `--features bundled` to include the tileclient into the server binary. Otherwise you'll have to build the tileclient for development.

## Building The Tileclient For Development

* Install [`node`](https://nodejs.org/).
* Run `$ npm install` in the tileclient directory.

Run `$ npm run dev` in the tileclient directory to get live updates of the client during development.

This will use Vue.js development mode and allow "hot module reload". Note that HMR is unlikely to work for graphics-related code since reloading usually leads to creation of a new WebGLRenderingContext, invalidating existing textures, programs and buffers.
Changes to any of the data models may require deleting cache databases.
tileclient development files are served by the tileserver from `tileserver/dist/`, which is managed by parcel. Shaders are not managed by parcel directly, to rebuild them exit from the parcel process and restart it.

### Reference Documents

* [WebGL Fundamentals](https://git.geomar.de/snippets/119#webgl)
* [Renderer Implementation Details](https://git.geomar.de/snippets/120#webgl-rendering-of-4d-geodata)
