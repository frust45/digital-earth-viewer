# Digital Earth Viewer - Developer Handbook

<style>
.infobox{
    border: 0.4mm dashed #00adef;
    border-radius: 0.2cm;
    border-bottom-left-radius: 0;
    /*margin-left: 1cm;*/
    padding: 0.25cm;
    display: inline-block;
}
</style>

## Table of Contents

- [Introduction](#introduction)
- [Data Model](#data-model)
    - [SourceInfo](#source-info)
    - [DataArray types](#dataarray-types)
    - [TileData and ArrayData](#tiledata-and-arraydata)
- [Server Infrastructure](#server-infrastructure)
    - [Source Lifecycle](#source-lifecycle)
    - [Source Sampling](#source-sampling)
    - [Source Caching](#source-caching)
- [Client Infrastructure](#client-infrastructure)
    - [Layer Subsystem](#layer-subsystem)
    - [Rendering Subsytem](#rendering-subsystem)
    - [Decorator Subsystem](#decorator-subsystem)
    - [Overlay Subsystem](#overlay-subsystem)

## Introduction

The Digital Earth Viewer is a server-client-application meant to visualize data from heterogenous sources in 3D over time.

The server component is an asynchronous web server that uses file-type-specific plugins to extract data, then stores it in a cache and delivers it to the client.
This server component is written in Rust, uses an SQLite database through SQLx and a Warp HTTP Server running on the Tokio asynchronous runtime. Plugins are written as autonomous lib-type subcrates.

![Overview Back End Architechture](images/Introduction/OverviewBackend.png)

The client application presents the user with an interactive interface and handles communication with the client. This client engine is written in typescript as a set of VueJS components and uses IndexedDB to cache data.

![Overview Front End Architechture](images/Introduction/OverviewFrontend.png)

## Data Model

The communication between server and client is handled through HTTP requests.

This communication is based on common types found in the [tileserver-model](https://git.geomar.de/valentin-buck/digital-globe/-/tree/master/tileserver/tileserver-model/src)-directory of the tileserver project.

### Source Info

For communicating about data sources available from the server, the client requests a *SourceList*, which internally is represented as a list of `SourceInfo`

([SourceInfo](https://git.geomar.de/valentin-buck/digital-globe/-/blob/master/tileserver/tileserver-model/src/source.rs#L10))
```rust
///Key parameters of a source. Its JSON representation is digested by the client.
#[derive(Debug, Serialize, Deserialize, Clone, Default)]
pub struct SourceInfo{
    ///Reasonably short name of the dataset. Defaults to the source filename unless overridden in the server configuration
    pub instance_name: String,
    ///The name of the implementation providing the data. Same as in the server configuration.
    pub source_name: String,
    ///Information on the source's available layers.
    pub layers: Vec<LayerInfo>,
    //Whether this source is cacheable or not
    pub cacheable: bool,
    ///The category this source falls into
    pub category: String,
    //The attribution data for this source
    pub attribution: String
}
```

As can be seen from the description, the `SourceInfo` struct contains naming information, a category and attribution, an attribute to decide whether the source should be cached or not and a vector of `LayerInfo`.

One source file can contain one or more data layers, which itself are described through the `LayerInfo`-struct:

([LayerInfo](https://git.geomar.de/valentin-buck/digital-globe/-/blob/master/tileserver/tileserver-model/src/source.rs#L27))
```rust
///Serialized to inform the client about a layer's metadata.
#[derive(Debug, Serialize, Deserialize, Clone, Default)]
pub struct LayerInfo{
    ///Internal name, as used to query data from the layer.
    pub name: String,
    ///Display name, should not contain excessive explanation.
    pub long_name: Option<String>,
    ///Format this reasonably, they are displayed as plain text by the client.
    pub unit: Option<String>,
    ///Hint on how the data that can be requested from this layer is to be interpreted.
    pub layer_type: String,
    ///Not to be used on point data.
    pub timesteps: Option<Vec<i64>>,
    //No to be used on point data
    pub heightsteps: Option<Vec<f32>>,
    ///The lowest and the highest value. Not applicable to color layers. Used for colormap ranging in the client.
    pub datarange: Option<(f64, f64)>,
    ///The lowest and the highest sample point (geometrically).
    pub zrange: Option<(f64, f64)>,
    ///Measure this at the equator, anything else doesn't really make a lot of sense.
    pub max_zoom_level: u8
}
```

Each layer has a descriptor through which it can be identified (`name`) as well as a human-readable `long_name`. For example, a layer may have the `name` *U(a2m)* and the `long_name` *Zonal wind speed at 2 meters*. Additional to this, a `unit` can be specified which will be rendered when the layer is queried by the user or in text fields.

### DataArray types

The `layer_type` attribute selects a suitable method of rendering in the client.
Currently, three values for `layer_type` are in use, though these should probably be replaced/extended:
- `vector/points/scalar`: Point values with a scalar value
- `raster/layer/scalar`: A 2D floating-point raster of scalar values
- `raster/layer/color`: A 2d 4x8-bit color image raster

These attributes conform roughly to the datatypes found in the [Datatypes](https://git.geomar.de/valentin-buck/digital-globe/-/blob/master/tileserver/tileserver-model/src/datatypes.rs#L6) module in that each query on a layer should produce an instance of [Geodata](https://git.geomar.de/valentin-buck/digital-globe/-/blob/master/tileserver/tileserver-model/src/datatypes.rs#L231)

```rust
///Union type for handling data abstractly.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum GeoData{
    TileData(DataArray),
    ArrayData(DataArray)
}
```

with a distinction between map tiles (`GeoData::TileData`) and arrays (`GeoData::ArrayData`). Both datatypes are instances of [DataArray](https://git.geomar.de/valentin-buck/digital-globe/-/blob/master/tileserver/tileserver-model/src/datatypes.rs#L237)

```rust
#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum DataArray{
    ScalarData(Vec<Scalar>),
    ColorData(Vec<Color>),
    BoolData(Vec<bool>)
}
```

with the internal datatypes [Scalar](https://git.geomar.de/valentin-buck/digital-globe/-/blob/master/tileserver/tileserver-model/src/datatypes.rs#L6), [Color](https://git.geomar.de/valentin-buck/digital-globe/-/blob/master/tileserver/tileserver-model/src/datatypes.rs#L15) or bool (currently unused).

```rust
///Alias to `f32`.
pub type Scalar = f32;
///RGBA byte representation of a color.
pub type Color = [u8;4];
```

Both of these implement the [Nodata](https://git.geomar.de/valentin-buck/digital-globe/-/blob/master/tileserver/tileserver-model/src/datatypes.rs#L18)-Trait which specifies a value to be used if a location in a raster or an array contains no data. It defaults to `NaN` for `Scalar` and transparent black (`[0u8,0u8,0u8,0u8]`) for `Color`.

```rust
///Guarantees an unambiguous "No Data" value, which can be used to signify elements that do not contain (valid) data.
pub trait Nodata{
    ///Returns the type's "No Data" value.
    fn nodata() -> Self;
}

impl Nodata for Scalar{
    ///Uses NaN to signify absence of valid data.
    fn nodata() -> Scalar{
        std::f32::NAN
    }
}

impl Nodata for Color{
    ///A black, transparent pixel is easily identified as missing data.
    fn nodata() -> Color{
        [0u8,0u8,0u8,0u8]
    }
}
```

Further facilities already exist for `Vector2` and `Vector3` but are currently unused.

### TileData and ArrayData

A difference is made between array-form-data and tile-form-data.

Arrays are used for sparse data and contain a flattened list of five-element-tuples (all elements as `f32`):

1. Longitude in `UEC`
2. Latitude in `UEC`
3. Height as meters above standard zero vertical datum
4. Timestamp as milliseconds (signed) after 1970-01-01 00:00:00 GMT
5. Value

For positioning points, the `UEC` coordinate system is used. It is specified in the [UEC](https://git.geomar.de/valentin-buck/digital-globe/-/blob/master/tileserver/tileserver-model/src/uec.rs) module in the tileserver-model.
It represents a homogenous sphere-coordinate system where longitudes from 0° (datum line) to 360° are represented as the "`x`"-part of a struct with a value going from 0 to 1 and the "`y`"-part representing latitudes from 0° (north pole) to 180° (south pole) with another value going from 0 to 1.

```rust
///Unified Earth Coordinates.
///The entire latitude and longitude ranges are remapped to [0, 1].
///UEC0/0 is equivalent to 180°W, 90°N, UEC1/1 is equivalent to 180°E, 90°S.
///0°N 0°E is represented by UEC0.5/0.5.
#[derive(Debug, Clone, Copy, Serialize, Deserialize, Default)]
pub struct UEC{
    ///The normalized longitude.
    pub x: f64,
    ///The normalized latitude.
    pub y: f64
}
```

This coodinate system is useful, since it allows for very simple arithmetic when determining where to render data onto a rotated and zoomed-in sphere.

An area can be represented by giving a tuple of `UEC`-coordinates with the first element determining the position of the northwestern corner and the second element specifying the size as an offset to the first element.

```rust
///A UEC area is given by its northwestern corner and its dimensions.
#[allow(non_camel_case_types)]
pub type UEC_Area = (UEC, UEC);
```

For tile-form-data, the world is broken up in regularly sized tiles as described by the [Tile](https://git.geomar.de/valentin-buck/digital-globe/-/blob/master/tileserver/tileserver-model/src/tile.rs#L12) module in the tileserver-model.

```rust
///The basic concept of digital globe's data model is the tile.
///Tiles are given regions of the earth's surface.
///They are recursively defined and can be generated to arbitrary precision levels.
///Each `tile` has bounds and an unambiguous path which can be used to determine the bounds.
#[derive(Debug, Clone, Serialize, Default, Deserialize)]
pub struct Tile{
    pub position: UEC,
    pub size: UEC,
    pub path: String
}

```

The tiling algorithm at first divides earth into two square areas called `W`est and `E`ast

![West and East](images/DataModel/WestAndEast.png)

These square regions are then subdivided into the `zoom_levels` called `A`, `B`, `C` and `D`.

![Subdivisions](images/DataModel/Subdivisions.png)

Each letter for each subdivision is concatenated into a `path`.

If the aspect ratio of a tile drops below 0.75 (very tall and not very wide), the subdivision is performed differently. Tiles below this aspect ratio are divided into an `U`pper and a `L`ower region where `U` combines `A` and `B` while `L` encompasses `C` and `D`.

Tile-form-data now consists of a regularly sampled grid of data represented as a flattened array of rows of data.

While the tile size is theoretically configurable, it should remain fixed to 512 by 512 samples.

A data array for tile-form-data thus consists of 262.144 `f32` or `[u8;4]`-values reaching exactly one mebibyte (1.048.576 bytes) in size.

Time is again represented as signed milliseconds since the unix datum.

## Server Infrastructure

The tile server consists of a main process that starts several subcomponents.

![Tile Server Infrastructure](images/ServerInfrastructure/TileServerInfrastructure.png)

The main process starts both the warp webserver, which drives control flow after the initial startup has been completed, as well as calls the `initialize_sources`-function, which initializes the registry, a map of source names to their respective constructor functions.
This registry is then used to instantiate sources according to the configuration file. The source instances are stored in a second map, the `SourceList`.
The SourceList provides a list of `SourceInfo`, as well as a way to access the source instances by name.

When a request is received by the webserver, either the list of `SourceInfo` is returned or in the case of a sample request, the sampler module is queried.
This sampler module consults the cache to see if the request was fulfilled before or uses the source list to get a source instance to fulfill the request.

### Source Lifecycle

Each source plugin in the server implements two traits: `Source` and `SourceInit`. 

`SourceInit` provides a function that can be called with a `SourceConfig` and a pre-filled `SourceInfo`.

```rust
///Allows unified behavior of all sources at initialization. 
pub trait SourceInit: Send + Sync + Sized{
    ///Initializes the source with the options given in the server configuration file.
    fn initialize(settings: &SourceConfig, source_info: &mut SourceInfo) -> Result<Box<dyn Source>, Box<dyn std::error::Error>>;
}
```


The [SourceConfig](https://git.geomar.de/valentin-buck/digital-globe/-/blob/master/tileserver/tileserver-model/src/source.rs#L50)-Object contains a lists of paths to input files as well as a HashMap of additional settings.

```rust
///As loaded from the server configuration.
#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct SourceConfig{
    ///Input paths.
    pub input_files: Vec<PathBuf>,
    ///Additional options by name, serialized to `String`.
    pub settings: HashMap<String, Value>
}
```

`SourceInfo` was described in the previous chapter [SourceInfo](#source-info). The `initialize_sources`-function calls the `initialize`-function of each `SourceInit`-implementing plugin with a pre-filled instance, so that the source only has to add `LayerInfos` and modify the existing information.

This initialization process is handled through a threadpool and in theory all source instances can be created in parallel. Usually, locking mechanisms in the file loading process of individual sources prohibits this behaviour.

The created source instance implements the [Source](https://git.geomar.de/valentin-buck/digital-globe/-/blob/master/tileserver/tileserver-model/src/source.rs#L66)-Trait with a method for sampling from the source as well as a destructor for manual cleanup of sources. This destructor is usually left empty and never called.

```rust
///Generalized functionality of data sources.
#[async_trait]
pub trait Source: Send + Sync{
    ///Returns the queried tile data, or whatever error occurs during sampling.
    async fn sample(&self, query: &SampleQuery) -> Result<Option<GeoData>, Box<dyn std::error::Error>>;
    ///Releases aquired ressources. The instance becomes permanently unavailable after this gets called.
    fn dispose(self);
}
```

### Source Sampling

Sampling from sources happens through the sampler component.

When a request is received in the webserver through either the `/tiles` or the `/array`paths (see [main.rs:180](https://git.geomar.de/valentin-buck/digital-globe/-/blob/master/tileserver/src/main.rs#L180)), the respective method in the `Sampler` instance is queried.

For tile queries, this method is [get_tile](https://git.geomar.de/valentin-buck/digital-globe/-/blob/master/tileserver/src/infrastructure/sampler.rs#L26)

```rust
pub async fn get_tile(&self, cache: Arc<Cache>, instance: &str, layer: &str, tilepath: &str, timestamp: i64, height: f32) -> Result<Option<GeoData>, Box<dyn Error>>{
        let cached = cache.get_tile_for_source_layer_tilepath_timestamp(instance, layer, tilepath, timestamp, height);
        if let Some(data) = cached.await{
            return Ok(data);
        }else{
            let source = SOURCELIST.get_source(instance);
            if let Some(s) = source{
                let sq = SampleQuery::new(instance.to_string(),layer.to_string(),Tile::from_tilepath(tilepath).unwrap(),height,timestamp);
                self.working.fetch_add(1, Ordering::Relaxed);
                let tiledata = s.sample(&sq).await;
                self.working.fetch_sub(1, Ordering::Relaxed);
                match tiledata{
                    Ok(data) =>{
                        //Spawn insertion thread
                        let d2 = data.clone();
                        tokio::task::spawn(async move {
                            cache.insert_tile_borrowed(&sq, &d2.as_ref()).await;
                        });
                        //Return tile
                        return Ok(data);
                    },
                    Err(e) => {
                        return Err(format!("Error sampling tile source {} layer {} tilepath {} timestamp {}: {:?}", instance, layer, tilepath,timestamp, e).into());
                    }
                }
            }else{
                return Err(format!("No tile source with name {}", instance).into());
            }
        }
    }
```

The `get_tile`-method first checks whether this request is already cached, and if not, accesses the source instance through the `SourceList` and pass on the request. Once a result is received, it is stored asynchronously in the cache and returned to the webserver.

### Source Caching

The process of computing new tiles or arrays for previously not requested data areas can take quite a while, so a server-side caching mechanism exists.

The cache is built as a WAL-SQLite database which contains a table for tiles, one for arrays as well as two unused ones for storing `SourceInfos` or providing a cache for computation the sources do during initialization.

An index is built for each table which takes into account the usual combination of queried data, for example
([cache.rs]())
```sql
CREATE TABLE IF NOT EXISTS tiles (source TEXT, layer TEXT, tile TEXT, time INTEGER, height REAL, data BLOB);
CREATE INDEX IF NOT EXISTS tiles_index on tiles(source, layer, tile, time, height);
```

Methods exist to insert new data or retrieve it.

Data gets zlib-compressed on insertion and decompressed on retrieval.

## Client Infrastructure

The digital earth viewer client is built as a VueJS-application supported by a service pattern.

![Client Infrastructure](images/ClientInfrastructure/ColoredDigitalEarthViewerClient.png)

While this overview might look complex, the client consists of several subsystems:

- <span style="background-color: #b9e0a5">The rendering subsystem</span>
- <span style="background-color: #dae8fc">The layer subsystem</span> containing
    - The layer stack and settings
    - The popup dialogues
- <span style="background-color: #fff2cc">The decoration subsystem</span> with
    - Compass, Scale and Loading Indicator
- <span style="background-color: #e1d5e7">The overlay subsystem</span>

The typical application lifecycle is as follows:

The set of loaded sources in the server is fetched as a list of `SourceLayerInfo`s.

A stack of `RenderLayer`s is constructed and assigned a `RenderSource` and `RenderFilter`s with `Parameter`s. These objects are displayed in the *Layer Settings* sidebar and can be modified from there.

The `ThreeDee` component creates a `WebGLContext` which is distributed through the `GLService` and used by the `RenderService` to create a data texture using tiles or arrays from the `TileCacheService` and the layers `RenderSource`. 

This data texture is then shaded into visible colors using the `RenderFilter`s and potentially the `ColormapService`.

Decoration elements such as the *Compass*, the *TimeSlider* or the *Scale* display and manipulate visualization state information such as the current display rotation, the displayed time or the data range of the visualization.

Using the `Overlay` subsystem, the user can interrogate the visualization and interactively query data.

All subsystems use services that are initialized on startup by the `Services` superclass.

### Layer Subsystem

When the `SourceInfoService` is started, it loads the list of `SourceInfo`s from the server. For the `SourceInfo` and `LayerInfo` structs in the server, equivalent typescript classes are created:

([SourceInfoService.ts](https://git.geomar.de/valentin-buck/digital-globe/-/blob/renderer-rewrite/tileclient/services/SourceInfoService.ts#L4))
```typescript
export class Source {
    public instance_name: string;
    public source_name: string;
    public layers: SourceLayer[];
    public category: string;
    public attribution: string;
}

export class SourceLayer{
    public name: string;
    public long_name: string;
    public unit: string;
    public layer_type: string;
    public timesteps: number[];
    public heightsteps: number[];
    public datarange: number[];
    public zrange: number[];
    public max_zoom_level: number;
    get_time_limits(): number[] {
        if (this.timesteps) return [this.timesteps[0], this.timesteps[this.timesteps.length - 1]];
        return null;
    }
}
```

These classes are packaged into a convenience class, the `SourceLayerInfo` which contains the included classes as well as a few quick access shortcuts. It is also assigned a path, which is used as an ID for a source and layer combination throughout the program.

([SourceLayerInfo](https://git.geomar.de/valentin-buck/digital-globe/-/blob/renderer-rewrite/tileclient/services/SourceInfoService.ts#L29))
```typescript
export class SourceLayerInfo{
    public source_name: string;
    public instance_name: string;
    public layer_name: string;
    public layer: SourceLayer;
    public source: Source;

    constructor(source: Source, layer: SourceLayer){
        this.source_name = source.source_name;
        this.instance_name = source.instance_name;
        this.layer_name = layer.name;
        this.layer = layer;
        this.source = source;
    }

    public getPath(){
        return this.source_name + "/" + this.instance_name + "/" + this.layer_name;
    }
}
```

The `SourceLayerInfo` is purely a metadata container. To visualize it, a `RenderLayer` needs to be created.

([RenderLayer](https://git.geomar.de/valentin-buck/digital-globe/-/blob/renderer-rewrite/tileclient/modules/RenderLayer.ts#L11))
```typescript
export class RenderLayer{
    source: RenderSource;
    filterPipeline: LayerFilter[] = [];
    visible: boolean;
    name: string;
    id: number;
    compositionFilter: CompositionFilter;
}
```

A `RenderLayer` contains a `RenderSource`, usually either a `TileRenderSource` or a `PointRenderSource` which takes references to up to five source layers.

`RenderLayer`s can be quickly created by using the `RenderLayerFactory`-class in the `RenderLayer`-module.

