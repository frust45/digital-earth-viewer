# Digital Earth Viewer - User Handbook

## Table of contents

- [Installation](#installation)
- [User Interface Basics](#user-interface-basics)
- [Tile Dialogues](#tile-dialogues)


## Installation

A test version of the viewer is accessible at https://digitalearth-webapps.geomar.de

A custom version can be installed by downloading the appropriate executable from https://git.geomar.de/valentin-buck/digital-globe/-/releases

The release version viewer only consists of one executable file and needs a configuration file called `initial_sources.toml` (example configuration file included in abovementioned release).

### Configuration file format

This configuration file is read when the executable is started and defines the data sources for the viewer.

Each data source is defined as a section, like this

```toml
["Source Name"]
```

The source name should be enclosed in quotemarks, but can in some cases be written without them (for example if it does not contain spaces). Users should always consider using quotemarks.

Under this section, source attributes select the *source plugin* and configure it.

Attributes are added by creating a new line with the following information under the section:

```toml
attribute = value
```

Values can take the form of text (enclosed in quotemarks, like `"this"`), truth values (`true` or `false`) or lists (enclosed in square brackets with individual elements seperated by commata and enclosed in quotemarks, `["like", "this"]`).

For each source, the following attributes are *mandatory*:

### Source Attribute

`source` selects the *source plugin* to be used. This selects an appropriate reader for the file format. Valid values as of the time of writing are:
- `csv` Reader for comma seperated values
- `ctd` Reader for tab seperated files in the CTD standard text format
- `esri_ascii_grid` Reader for ESCII ASCII Grid Raster files (text format)
- `gpx` Reader for GPX files
- `netcdf` Reader for NetCDF-files (in HDF5 internal format)
- `openstreetmap` Reader that connects to an openstreetmap server

An example attribute selecting a netCDF source can be written as follows:

```toml
source = "netcdf"
```

### Files Attribute

`files` defines which input files are passed to the reader.
Since some sources can take multiple input files (the CTD source for example can read and aggregate files from one directory), the files list is given as a list.
File paths should be given as absolute paths.

An example attribute specifying a netCDF file for use with the netCDF source can be written as:

```toml
files = ["G:\\GEBCO\\GEBCO_2019.nc"]
```

Some sources can take multiple files, in which cas, the individual files are given as multiple items in the list

```toml
files = ["C:\\file1.txt", "C:\\file2.txt"]
```

A few sources like the openstreetmap source do not need local files to work, but since the `files` attribute is mandatory, need to be passed an empty list

```toml
files = []
```

### Optional attributes

`override_name`: If set to `true`, it overwrites the instance name the source gives itself (almost always derived from the file name) with the source name defined in the section.

Example:

```toml
["GEBCO Grid"]
source = "netcdf"
files = ["G:\\GEBCO\\GEBCO_2019.nc"]
```

creates a source named *GEBCO_2019.nc* while

```toml
["GEBCO Grid"]
source = "netcdf"
files = ["G:\\GEBCO\\GEBCO_2019.nc"]
override_name = true
```

creates a source named *GEBCO Grid*

<br>
<br>

`category`: Sources are sorted into categories to make it easier for the user to find them. This attribute takes a text value. If not set, the category of the source defaults to "*Other*".

Example: 
```toml
category = "Oceanography"
```

<br>
<br>

`attribution`: Since a good scientist always cites their sources, this is the place to do it. Text passed to this attribute can be formatted with [Markdown](https://daringfireball.net/projects/markdown/basics) markup. Also, DOIs are automatically linked to their content. To make it easier to write longer attributions, this field supports multi-line text using triple quotemarks.

Example:

```toml
attribution = """GEBCO Compilation Group (2020) 
[GEBCO 2020 Grid](https://www.gebco.net/data_and_products/gridded_bathymetry_data) 
(doi:10.5285/a29c5465-b138-234d-e053-6c86abc040b9)"

```

is rendered as 

![GEBCO Compilation Group (2020) GEBCO 2020 Grid (doi:10.5285/a29c5465-b138-234d-e053-6c86abc040b9)](images/installation/Attribution.png)

with both the link to the data products site as well as the DOI.

### Source-Specific Attributes

The only source taking source-specific attributes at the moment is the `openstreetmap` source.

It takes the following additional attributes:

`base_url` selects the server from which to sample. It defaults to the standard openstreetmap.org server. In base_url, placeholders for *x*, *y* and *zoom* can be used.

Example which selects the openseamap tile server: 

```toml
base_url= "http://tiles.openseamap.org/seamark/zoom/x/y.png"
```

<br>
<br>

`x_pattern`, `y_pattern` and `z_pattern` specify the placeholders used in the `base_url`-attribute

`x_pattern` defaults to `"${x}"`, `y_pattern` to `"${y}"` and `z_pattern` to `"${z}"`

Example:

```toml
x_pattern = "x"
```

<br>
<br>

`instance_name` can be used to set the layer name for the source, for example

```toml
instance_name = "tiles.openseamap.org"
```

<br>
<br>

`max_zoom_level` is used to control the maximum zoom level and defaults to *18* for most sources. It should not be enclosed in quotemarks.

Example:

```toml
max_zoom_level = 18
```

<br>
<br>

`max_tiles_cached` selects how many PNG tiles should be kept in RAM to reduce the number of downloads from the OpenStreetMap tile server. Most tile servers have a very conservative rate limit, but since the PNG tiles they deliver are usually only a couple kilobytes in size, it poses little problem to keep a large number of them in cache. This value should not be enclosed in quotemarks and defaults to *1000*.

### Starting the server

The server can be started from a terminal by issuing `./tileserver`.

It then creates a log file called `tileserver.log` in which status messages are written. This file is overwritten on consecutive launches.

If the launch is successful, a cache is also created as `cache.db`.

The tileserver then proceeds to read the sources from the `initial_sources.toml` file and after finishing starts the internal web server at http://localhost:8000

Loading netCDF files or large CSV-files can take a few minutes. Large cache-files also slow down the startup and may be deleted before launch. Do not delete the cache file while the server is running.

**Common error messages**

*Editors note: These error messages will be more descriptive in the next release*

`thread 'main' panicked at 'called `Result::unwrap()` on an `Err` value: Os { code: 2, kind: NotFound, message: "Das System kann die angegebene Datei nicht finden." }', src\main.rs:78:5`

In this case, no `initial_sources.toml` file could be found.

<br><br>

`thread 'main' panicked at 'called `Result::unwrap()` on an `Err` value: Error { inner: ErrorInner { kind: Wanted { expected: "a right bracket", found: "an identifier" }, line: Some(16), col: 10, at: Some(433), message: "", key: [] } }', src\main.rs:79:26`

In this case, the `initial_sources.toml` contains faulty formatting.


### Stopping the server

The server can be stopped by issuing `Ctrl+C` into the terminal from which it was launched or just closing that terminal.

### Housekeeping

In some cases, the `cache.db`-file gets excessively large (several dozen gigabytes). In this case, it contains a lot of precomputed data tiles. After shutting down the server, this file, as well as the temporary files `cache.db-shm` and `cache.db-wal` can be deleted. Deleting the cache file whill cause delays when revisiting previously cached locations in the viewer.

In a very few cases, the `tileserver.log` file grows very large. It can also be deleted after stopping the server.

## User Interface Basics

When launched, the viewer screen will look something like this

![User-Interface](images/user-interface-basics/User-Interface.png)

This screen consists of an interactive 3D view overlaid with a few controls.

The 3D view can be moved by holding down the left mouse button and dragging.

It can also be rotated by holding down the middle mouse button or the left control key while dragging.

![User-Interface controls](images/user-interface-basics/User-Interface-Annotated.png)

The controls have the following functions:

**Layers**

The layers tab offers information and control over the data being displayed.

It can be hidden by clicking the "Layers" print at the top right corner of the tab, at which point it is replaced by an icon:

![Layers-Icon](images/user-interface-basics/Layers-Icon.png)

The layers tab can be restored again by clicking this icon.


New layers can be added using the *Add Layer* Button

![Add Layer Button](images/user-interface-basics/Add-Layer-Button.png)

For each layer, a bar with some controls is displayed

![Layer Bar](images/user-interface-basics/Layer-Bar.png)

From left to right, this bar contains the following elements:

- Eye-Symbol 👁: Click to hide / show layer. If hidden, the eye is striked through

- Layer name *GEBCO Grid (elevation)*: The name of the layer. Can also be edited by the user. Clicking the name selects the layer and displays it's options below. The selected layers name will be highlighted in blue.

- Layer up button ˄: Moves the layer up one level

- Layer down button ˅: Moves the layer down one level

- Layer delete button X: Removes the current layer. Does not work if the current layer is the last remaining layer. Add another layer first, then remove the current one.

<br><br>

**Layer-Settings**

For each layer, a set of settings is displayed.

This starts with the source matrix.

![Source-Matrix](images/user-interface-basics/Source-Matrix.png)

Each source can have up to five slots of data that influence what is displayed.

For example, a "Scalar Layer" has a slot for a parameter that is displayed through a colormap (Parameter) and a slot for a displacement, that bends the map up or down (Displacement). The other slots are unused.

Below the source matrix is a set of options.

![Settings-Matrix](images/user-interface-basics/Settings-Matrix.png)

- Displacement Strength controls how strongly the terrain effects are applied to the map

- Displacement offset can be used to move layers vertically, i.e. to move an atmospheric layer to a certain height above ground

- Colormap control which colormap is used to color the data

- The buttons *Symm. (min)*, *Symm. (max)* and *Reset* control the data range which is displayed. For each layer, a minimum and maximum value are computed. *Symm. (min)* sets the range to [min, -min], *Symm. (max)* to [-max, max] and *Reset* to [min, max].

- The fields *min* and *max* can be used to individually adjust the range.

The settings look slightly different for other layers. For example, point layers have additional controls which can be used to control the point size and the time range which is displayed.

<br><br>

**Attribution**

In this field, attribution information from the configuration file is displayed.

<br><br>

The **Timeslider** is used to control the time step that is currently investigated.

Click and drag to the left or right on the black and blue bars to change time.

![Timeslider-Handle](images/user-interface-basics/Timeslider-Handle.png)

Changing time is only possible when the layer supports multiple time steps.

The blue bars represent the time are of the layers currently loaded, with the order of layers being the same as in the layers tab.

<br><br>

The **Compass** control displays the current heading in which the view is rotated. The heading can be reset to 0° (or north) by clicking the compass.

![Compass](images/user-interface-basics/Compass.gif)

<br><br>

To the left of the screen, the **Scale** is shown for Scalar Layers and Point Layers.

![Scale](images/user-interface-basics/Scale.png)

The scale shows both the current colormap as well as four graduations at 95%, 65%, 35% and 5%.

<br><br>

In the top right corner exists the *Settings* button 

![Settings-Button](images/user-interface-basics/Settings-Button.png)

Clicking it reveals the global settings menu

![Settings-Menu](images/user-interface-basics/Settings-Menu.png)

The currently availabe settings are

- Enable LOD (always enabled): Switch between a level-of-detail-based tile selection algorithm and a zoom-based tile selection algorithm. This function is currently locked since the zoom-based algorithm is very unstable

- Resolution: Here, a render resolution between 100% and 25% can be selected. If a lower resolution is selected, the 3D view becomes slightly more blurry, but uses a lot less computing power. This option is especially helpful for laptops.

- Always Rerender: The application uses heuristics to establish whether anything in the current view has changed and can pause the renderer to save power. This option disables the heuristic, which can be important for debugging. Laptop users should leave this option unchecked.

- Clear Cache: The application uses a client-side cache in the users browser to buffer tiles and points that are loaded from the server. This cache can grow outdated or very large (although most browsers already manage cache sizes based on a least-recently-used policy), so this button can be used to clear it.
Clearing the cache reloads the page.

<br><br>

**Debug overlay**

The debug overlay is positioned in the top right corner and can be opened by clicking the *[+]*-icon.

![Debug-Overlay](images/user-interface-basics/Debug-Overlay.png)

The overlay displays the current position, the time needed for the cyclic refresh operation as well as frame rate and renderer hibernation status.

## Tile Dialogues

A new layer can be added using the *Add Layer*-button in the Layers-tab.

![Add Layer Button](images/user-interface-basics/Add-Layer-Button.png)

Clicking this button opens a layertype selection dialogue.

![Layertype Selection Dialogue](images/dialogues/LayerTypeDialogue.png)

In this dialogue, the following layer types can be selected:

**Color Layer**

A layer where tiles display precomputed color data. This is usually used for existing maps, satellite imagery or other such sources.

**Scalar Layer**

A layer where tiles display data through a colormap. The data layer should have only one (scalar) parameter distributed on a raster. Usually used for climate simulations, terrain data, ...

**Points Layer**

Sparse scalar data can be displayed as points of a certain size. This is the standard layer for i.e. temperature probes, salinity measurements or other measurements taken at specific, non-gridded locations.

**Vector Tracer Layer**

Gridded dense vector data can be displayed as an animation. This is usually used for current or wind layers from simulations.

<br>

Selecting on of these layer types will open the source selection dialogue.

![Source Selection Dialogue](images/dialogues/SourceSelectionDialogue.png)

Here, available layers for the selected type are first grouped by category and then by source entry from the configuration file.

Other groups can be expanded by clicking them.

Once a source is selected, it is displayed in the layer settings as a badge.

![Source Badge](images/dialogues/SourceBadge.png)

Clicking the badge will reopen the source selection dialogue.

