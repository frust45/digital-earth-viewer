# Open Issues in the Digital Earth Viewer

## ZIP is not adequate for large source sets

When precaching sources (i.e. using *PrebuildTiles*), saving them to a ZIP archive seems like a good idea for the following reasons:

- All data is included in one file
- Index at the start of the file can be read to memory and allows for really fast seeking
- compression is a good idea anyways and GZIP is so fast you probably don't even notice

In practice, saving even moderately large (in terms of zoom level and timesteps) sources to a ZIP fails (and you only notice this when reading), because files in a ZIP are indexed with a 16-bit integer. Since the structure chosen by *PrebuildTiles* is to make a directory for each tile and a file for each timestep, this limit is reached faster than initially though.
In some cases, more advanced readers like *7-zip* are able to recover the correct file structure, this is basically why the directory source exists.

In any case, storing files as pure directories is extremely inefficient and will take up a ton of space.

## Cache Database needs to be reworked

The DB currently used is *Sled*, a more-or-less experimental Nosql-database, chosen because the API looked quite simple and in the beginning, caching was only supposed to last for one session anyways.

Sled is fast, sled stores the data well, sled works across multiple threads.

But I'm not so sure it actually stores everything. In a lot of cases, retrieving the same tiles across two restarts takes a lot more time than in the same run.
This might have something to do with RAM caching, which leads to another problem: even though it should be limited to 100MB RAM cache globally, the cache size explodes and causes the server to crash.

I would suggest we fix this by moving the cache AND the precache to using one SQLite database per source. This would have a lot of advantages, since it would allow for multiple access across different sources, has a proven architecture and stores everything in one file per source. `TileData` could be stored as a *gzip* or *zstd*-compressed blob, and there could even be tables for `SourceInfo`, `LayerInfo` and `Timesteps`.

## Interpolation and Mipmapping

Currently, `RasterTileData` doesn't get interpolated, really. Sampling a larger tile from fine-resolution data happens by dropping a number of samples inbetween points. This is arguably the fastest approach possible, especially, since the `E` and `W`-tiles get requested first and mipmapping would mean rendering out the whole source for this timestep anyways, but it is probably scientifically nonsensical.

A good solution for this would need to be found.

## Cleanup data structures for TileData

Currently, the only data structures really used are `RasterScalar`, `RasterImage` and `PointsScalar`. These are named a lot of different things in the program.

There even exist data types for things like precomputed geometry or voxel data, but are these actually useful?

This line of thinking would need to be extended to things like vector layers: currently 2D vector fields are represented as two scalar fields (U and V). Is this a good idea? Some sources implement it this way, but it might be possible to extract it into one unified layer (as opposed to doing this in the shader). On the other hand, having the two layers seperate lowers the complexity of the server.

Another issue is, that these datastructures don't really get transmitted to the client. The client always receives a big heap of `u8` which it then pushes sight-unseen onto the GPU. This is a fast and efficient way of handling the data transfer, but leads to the string-tagging of the data type per layer that is currently implemented.

## Better data transmission infrastructure

Currently, tile requests are implemented as a sort-of-semi-async process. For each tile for which no data already exists on the GPU during rendering, a new request is made to the client `TileCache`, which then makes up to 8 requests concurrently to the server. In the server, these tiles are then sampled, except if the tile is currently already sampling, in which case the request is aborted with a *"try-again"*-message.

This leads to a whole slew of weird bugs, such as long load times if a number of people simulaneously try to access the same source, massive memory usage if a lot of layers are concurrently loaded, higher computational load on the first zoom-in (2 tiles -> ~40 tiles) and so on.

Each http request also carries quite a lot of overhead, since they carry header information and get individually compressed using gzip or brotli. 

I would suggest rewriting this infrastructure to use websockets for the data exchange. That would enable a client to send a list of requested tiles all at once and the server to send over tile data as soon as it becomes ready. This list of requests could then be held globally and be processed by a pool of worker threads, so memory usage stays about the same.

Each request could then be also compressed *once* with a good compression algorithm and be unpacked in a `WebWorker` on the client side.

One of the downsides of this method would be, that the server would possibly have to handle a large number of concurrent connections (and client-connection-instances in RAM) when a lot of clients connect, but for lower numbers of clients, this method would in the end save RAM.

## Unified request structure

Requests contain a lot of information that is not necessarily needed for each source. Some layers don't have z-information, others will need a zmin and zmax, so sending it every time does not make a lot of sense. For example, tiles don't really make sense even for large point sources. Maybe a flexible infrastructure like with source initialization through the TOML could be adapted to work with the requests?

## Client-side caching

Using the webapp takes multiple hundred megabytes of data transmission. This might not be advantageous in situation where bandwidth is limited (mobile internet, conference wifi, ...).
A lot of tiles get loaded each time the site is started and are almost never removed (for example bluemarble and gebco), so they could be stored in something like `localStorage`. This would need to be limited to a couple hundred megabytes to avoid flooding the user with too much data, but even if only the first two zoom layers get cached for each source, it would speed up the experience a lot since there will be always something shown on the display.

## Time - Sliding and Ranging

The time slider is currently more of a time selector since you will have to wait about ten seconds after each adjustment for the tiles to load. Either we adapt the slider to preload all E and W tiles for the layer that is currently being slid through, or we take client-side-caching seriously.

For viewing ranges, this raises the question on how to amalgamate the multiple time steps per tile into one displayable result.
Out of compute power concerns, this should probably happen on the client. Maybe a mipmap-style data structure could be used to pre-average the whole time range, both halves of the time range, ... down to a certain time zoom level at which only singular time steps will be displayed.

## Rendering abstractions

The rendering currently done is monolithic and even worse, it is duplicated in the render_gl and sample_gl functions.

Ideally, this could be abstracted through an inversion-of-control mechanism which would call a render-function in each source for each tile.