# Source Options
This document will document the differen configuration options for the sources.
All options will be needed to be written into the corresponding source block in the initial_sources.toml.
When adding a source to the configuration use the **Name** specified in each block here.
The block will look something like this
```toml
["My Human Readable Source Name"]
source="Name"
files=["path/to/my/file"]
option="value"
option2="value"
override_name=true
attribution="Read my paper at doi:number!"
```

Most of the options come from the source options, but two:

- `override_name=true` tells the system not to use the filename provided to list the source in the user interface, but the name from the section, in this case "My Human Readable Source Name".
- `attribution` contains attribution information. This information can be markdown content, which will get converted to HTML and is displayed below the source settings. Normal web links work and DOI references get automatically converted to links.

All sources require a `files` list, even if that list is empty (`files=[]`).

Most sources are *default* sources, meaning they are included in every build.

Some sources are *feature* sources, meaning they have to be enabled specifically by passing `--features "source_feature"` when building.

A few sources are *unstable* sources, meaning there is already a feature tag reserved for them, but they cannot be currently built.



## Basic Earth Source

**Function**: Delivers raster tiles from a precomputed dataset, so the earth can be shown when opening the program for the first time.

**Source status**: Default

**Feature**: `source_basic_earth`

**Name**: - (cannot be added)

**Comment**: No options. As a default source, this source will always be available and is always created.

## Bluemarble Source

**Function**: Reads PNG images with equirectangluar views of the whole earth as exported by NASAs BlueMarble project.

**Source status**: Feature

**Feature**: `source_bluemarble`

**Name**: `bluemarble`

**Comment**: No options. Very inefficient source and prone to crashes. Obscure file naming convention. Don't try at home.

## Corona Source

**Function**: Reads the Github Repository of corona case numbers by country and exports raster tiles.

**Source status**: Unstable

**Feature**: None reserved yet

**Name**: None reserved yet

**Comment**: No options. Started on a whim while listening to a lecture, never got very far

## CSV Source

**Function**: Reads CSVs and creates a point for each line and column

**Source status**: Default

**Feature**: `source_csv`

**Name**: `csv`

**Comment**: Only understands numerical values. Missing or string values get parsed as NaN. Parsing happens in parallel, so this source might consume a lot of CPU juice on startup

Option       | Default                 | Description
-------------|-------------------------|------------
lat_name     |latitude                 | The name of the column that contains the latitude of the points position
lon_name     |longitude                | The name of the column that contains the longitude of the points position
time_mode    |iso                      |Determines how time is parsed. Can be either "iso", "odv" or "seperate". Iso tries to parse the date as an ISO8601-compliant string (does not need to contain all parts, also can use spaces instead of T and Z). ODV means the time format of the text export from ODV (yyyy-mm-ddThh:mm:ss.sss). Seperate means time is split over multiple columns, named "year", "month", "day", "hour", "minute", "second", "millisecond". All of these are optional and get read as zero if left out.
time_name    |time                     | The name of the column containing the time value (irrelevant if using time_mode="seperate")
height_name  | height                  | The name of the column containing the height or depth of each point
height_positive|true                   | If true, the value of the height column is read as positive above datum, negative below. If for example your column contains depth values, it should be set to false
seperator    | ,                       | The character or string which seperates columns in the file. Can also be something like a tab character or any UTF-8 emoji.
nodata_value | NaN                     | All values in column that fit this description get converted to NaN (not-a-number). Useful if your table contains a lot of -9999 values for where no measurements were taken.

## Currents Source

**Function**: Reads GEOMAR ADCP specification compliant files

**Source status**: Default

**Feature**: `source_currents`

**Name**: `currents`

**Comment**: Since the file header contains all information used to parse it, no options but the file are required.

## ESRI ASCII Grid Source

**Function**: Reads ESRI ASCII grid files (.asc)

**Source status**: Default

**Feature**: `source_esri_ascii_grid`

**Name**: `esri_ascii_grid`

**Comment**: Since the file header contains all information used to parse it, no options but the file are required.

## GPX Source

**Function**: Reads GPX files as a series of points

**Source status**: Unstable

**Feature**: `source_gpx`

**Name**: `gpx`

**Comment**: Originally this source was only added as an example of how to create a point source. It has gone stale while other features were developed, but bringing it back should be fairly easy.

## Maptiles source

**Function**: Connects to an openstreetmap or openstreetmap-like server and downloads webmercator maptiles which are then reprojected to normal raster tiles.

**Source status**: Default

**Feature**: `source_maptiles`

**Name**: `maptiles`

**Comment**: Downloading is limited to a single connection, so we don't get banned. This source uses a local cache, so it doesn't ask for the same tile too often. Careful with the zoomlevel option, as it means digital-earth-viewer-zoomlevels, not openstreetmap zoomlevels.

Option       | Default                 | Description
-------------|------------------------ |------------
base_url     |`http://b.tile.openstreetmap.org/${z}/${x}/${y}.png`| The URL of the server that handles the requests. It should contain the patterns for x, y and z.
instance_name|tile.openstreetmap.org   |The name of the layer this source creates in the digital earth viewer UI
x_pattern    |${x}                     | The part of the URL that should be replaced with the x-value of the tile that is requested
y_pattern    |${y}                     | The part of the URL that should be replaced with the y-value of the tile that is requested
z_pattern    |${z}                     | The part of the URL that should be replaced with the z-value of the tile that is requested
max_zoom_level|18                      | The maximum zoom level in the digital earth viewer for that new tiles are created (if zoomed in further than this, no further detail is added)
max_tiles_cached|1000                  | How many tiles from the maptiles server should be kept in RAM while the source is active. This saves us from requesting the same tile over and over again, since many digital-earth-viewer tiles are created from four or more maptiles-tiles. A maptiles-tile is on the order of 30kb, so this value can be fairly large on most machines.

## NetCDF source

**Function**: Reads rasters from NetCDF files following the CDF specification

**Source status**: Default

**Feature**: `source_netcdf`

**Name**: `netcdf`

**Comment**: NetCDF files are read through the netcdf-c library, which can be unperformant at times. It is also really sensitive to small deviations from the norm. Most of the configuration happens in the netcdf file itself.

Option       | Default                 | Description
-------------|------------------------ |------------
lat_name     |lat                      | The name of the latitude dimension
lon_name     |lon                      | The name of the longitude dimension
time_name    |time                     | The name of the time dimension
height       |height                   | The name of the z-dimension (positive above datum)


## ODV Source

**Function**: Reads text exports from ODV

**Source status**: Default

**Feature**: `source_odv`

**Name**: `odv`

**Comment**: The odv text export always follows are relatively strict pattern, so no further configuration is required.

## Test Source

**Function**: Creates layers that can be used to graphically debug several functions of the viewer, such as the tiling algorithm

**Source status**: Default

**Feature**: `source_test`

**Name**: `test`

**Comment**: This source is probably not very useful to normal users

## Turbidity Source

**Function**: Reads information from turbidity sensors following the GEOMAR format

**Source status**: Default

**Feature**: `source_turbidity`

**Name**: `turbidity`

**Comment**: The GEOMAR turbidity convention is strictly defined, so no options are needed.

## WMS source

**Function**: Connects to a WMS server and downloads rasters

**Source status**: Default

**Feature**: `source_wms`

**Name**: `wms`

**Comment**: Currently only servers using WGS84 as their CRS work. The server also needs to export at least one style for each layer (even if it is empty) and support version 1.3.0 queries resulting in PNG images.

Option       | Default                 | Description
-------------|------------------------ |------------
endpoint_url |(no default value)       | The URL of the WMS server, usually ending in /wms, but not containing the ?-character or anything behind it