# Adding a server source to the tileserver

In this document, the addition of a new source for tile data from GPX files to the tile server will be described.

## Prerequisites

In order to build the tileserver application, several software packages will need to be present. 

First of all, rust will need to be installed from https://rustup.rs. Follow the instructions given by the installer. On some systems, the package will need the build-essential package or another C++-compiler like Microsoft Build Tools.

Second, the netcdf-development libraries will need to be installed. On the Ubuntu machine I'm writing this on, the package is named `libnetcdf-dev`. For the windows version, a netcdf dll is already included in the build process (folder deps), but sometimes the build process will inform you about necessary environment variables to set.

As a development environement, I am using Visual Studio Code with the *Rust (rls)* plugin by *rust-lang* enabled.

With all of this present, we can now start.

## Adding a new source file

Sources in the tileserver project live in the folder `src/sources`. To this folder we add a new empty file named `gpx_source.rs`.

Every source in the tile server is implemented as a struct. We add a struct stub to the gpx source code unit:

```rust
pub struct GPXSource{
}
```

As you may have noticed, RLS has not started recompiling the project yet. This is because the tileserver does not yet link to the `gpx_source.rs` module.

We can amend this by going into `src/sources/mod.rs` and adding the following two lines at the bottom of the file

```rust
mod gpx_source;
pub use gpx_source::GPXSource;
```

Let's review what we have done so far:
- We have added a new, publicly visible struct in a new code unit
- We have linked to that code unit (`mod`-statement)
- We have exported this code unit to the rest of the program (`pub use`-statement)

## Adding a crate to read GPX files
Oftentimes, specific readers for file formats do not need to be implemented from scratch. In the case of GPX, we can go to https://crates.io and search for a crate referring to GPX.

![Search results for "GPX" on crates.io](CratesIO.png)

As we can see, there are (as of the time of writing this) two results for "GPX". One is concerned with guitars, the other with GPS, to the `gpx`-Crate it is.

![Description crate of "gpx" Crate](GPXCrate.png)

Going to the description page of this crate reveals a version information and even a simple example.

As described in the upper section of the page, lets now add the line

```toml
gpx = "0.8.0"
```

to the file `Cargo.toml` in the `tileserver` directory root.

For other filetypes, the process works similarly.

If no crate for reading a specific filetype exists yet, oftentimes preexisting C or python bindings can be easily adapted to work with Rust, but this is a topic for another time.


## Implementing the bare minimum source traits

Each source in the tileserver needs to implement two traits found in `src/model/source.rs`

- `SourceInit` providing a factory method for initializing a source
- `Source` providing information and eventually tile data

To access the documentation for these two traits, let us build the project documentation by opening a terminal in the root `tileserver` directory and running the command

```bash
cargo doc --open
```

![Building documentation](BuildingDoc.png)

After a bit of computation, we will be greeted by a browser window displaying our (completely offline) documentation.

Navigating to the source module, we can view the traits section

![Traits section](DocsTraits.png)

Lets take a look at the `SourceInit` section

![SourceInit section](DocsSourceInit.png)

We can see the trait requires a single method named `initialize` and is implemented by every source in the tile server.

This document is of course generated from the source code, in this case from `src/model/source.rs`.

Taking a look at the original trait code, we see this method represented as 

```rust
///Allows unified behavior of all sources at initialization. 
pub trait SourceInit: Send + Sync + Sized{
    ///Initializes the source with the options given in the server configuration file.
    fn initialize(settings: &SourceConfig) -> Result<Box<dyn Source>, Box<dyn std::error::Error>>;
}
```

`SourceInit` requires a method called `initialize` which takes a reference to a `SourceConfig` and returns either something that implements `Source` or an Error.

Moving the cursor to `SourceConfig` and hitting F12 reveals the definition

```rust
///As loaded from the server configuration.
#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct SourceConfig{
    ///Input paths.
    pub input_files: Vec<PathBuf>,
    ///Additional options by name, serialized to `String`.
    pub settings: HashMap<String, String>
}
```

As can be seen, `SourceConfig` contains a list of input files and a HashMap of additional settings.

Back to our stub!

We can extend our code

```rust
pub struct GPXSource{
}
```
with an implementation of the `SourceInit`-trait

First we import everything from the `model`-module

```rust
use crate::model::*;
```

Then we can define the `SourceInit`-trait

```rust
impl SourceInit for GPXSource{
        ///Initializes the source with the options given in the server configuration file.
    fn initialize(settings: &SourceConfig) -> Result<Box<dyn Source>, Box<dyn std::error::Error>>{
        let source = GPXSource{};
        Ok(Box::new(source))
    }
}
```
In this code, we first instantiate the `GPXSource` and then return the `Ok`-Branch of the error with the source moved to a new `Box` (relocated to the heap).

Unfortunately we get an error message in RLS

```
the trait bound `sources::gpx_source::GPXSource: model::source::Source` is not satisfied

the trait `model::source::Source` is not implemented for `sources::gpx_source::GPXSource`

note: required for the cast to the object type `dyn model::source::Source`rustc(E0277)
```
This message basically only says that we also need to implement the `Source`-Trait, so lets do that.

The `Source`-Trait is also defined in `src/model/source.rs` and looks like this

```rust
///Generalized functionality of data sources.
pub trait Source: Send + Sync{
    ///Returns the source's key parameters.
    fn get_info(&self) -> SourceInfo;
    ///Returns the source's timesteps or an empty `Vec` if the use of timesteps is inappropriate.
    fn get_timesteps(&self) -> Vec<Timestamp>;
    ///Returns the queried tile data, or whatever error occurs during sampling.
    fn sample(&self,query: &SampleQuery) -> Result<Option<TileData>, Box<dyn std::error::Error>>;
    ///Marker indicating whether caching this source is reasonable or not, depends on the backing storage.
    fn is_cacheable(&self) -> bool;
    ///Releases aquired ressources. The instance becomes permanently unavailable after this gets called.
    fn dispose(self);
}
```

As you can see, all the interaction with the source is handled through this trait:

- `get_info` returns a `SourceInfo` object that describes what datatype the source is, what its name is, etc
- `get_timesteps` is mostly deprecated at this point
- `sample` returns data for a given tile and timestamp
- `is_cacheable` marks this source as either cacheable or not (useful for sources like the *zip*-Source which run from pre-cached content)
- `dispose` consumes self and deallocates the source (also not used at this point)

Lets add an impmenentation of this to `GPXSource`

```rust
impl Source for GPXSource{
    ///Returns the source's key parameters.
    fn get_info(&self) -> SourceInfo{
        todo!();
    }

    ///Returns the source's timesteps or an empty `Vec` if the use of timesteps is inappropriate.
    fn get_timesteps(&self) -> Vec<Timestamp>{
        todo!();
    }

    ///Returns the queried tile data, or whatever error occurs during sampling.
    fn sample(&self, query: &SampleQuery) -> Result<Option<TileData>, Box<dyn std::error::Error>>{
        todo!();
    }

    ///Marker indicating whether caching this source is reasonable or not, depends on the backing storage.
    fn is_cacheable(&self) -> bool{true}

    ///Releases aquired ressources. The instance becomes permanently unavailable after this gets called.
    fn dispose(self){}
}
```

We've now marked all methods except for two as todo. `dispose` is not marked, since we do not need to actually do anything yet to clean the source up. We can also already fill in `is_cacheable`, since we are not dealing with pre-cached data.

The macro invocation `todo!();` is used for the other methods, since it will stop server execution when the method is reached, but otherwise allows the method to exist without a return value.

Our source now implements all the necessary traits to be registered in the program.

## Registering the source

For a source to become usable, it is not sufficient for it to just exist, but it needs to be registered and configured.

Source registration happens in `src/main.rs` at the beginning of the `main`-method:

```rust
/// actix main function. Initializes all source types first. Then loads server configuration and intitializes each source.
#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_web=info");
    //Initialize source registry
    infrastructure::Registry.write().unwrap().add_source("bluemarble".to_string(), Box::new(BlueMarbleSource::initialize));
    infrastructure::Registry.write().unwrap().add_source("netcdf".to_string(), Box::new(NetCDFSource::initialize));
    infrastructure::Registry.write().unwrap().add_source("esri_ascii_grid".to_string(), Box::new(EsriAsciiGridSource::initialize));
    infrastructure::Registry.write().unwrap().add_source("ctd".to_string(), Box::new(CTDSource::initialize));
    infrastructure::Registry.write().unwrap().add_source("csv".to_string(), Box::new(CSVSource::initialize));
    infrastructure::Registry.write().unwrap().add_source("zip".to_string(), Box::new(ZipSource::initialize));
    infrastructure::Registry.write().unwrap().add_source("directory".to_string(), Box::new(DirectorySource::initialize));
```

As can be seen, for each source, the configuration name of the source (i.e. "ctd" for a `CTDSource` is added to the source registry together with a Box containing the initialization method from `SourceInit`).

We will now add our Source likewise

```rust
    infrastructure::Registry.write().unwrap().add_source("gpx".to_string(), Box::new(GPXSource::initialize));
```

But if we now try running the server by executing `cargo run --release` in the `tileserver` directory, we get an error message:

 ```
Running `target/release/tileserver`
thread 'main' panicked at 'called `Result::unwrap()` on an `Err` value: Os { code: 2, kind: NotFound, message: "No such file or directory" }', src/main.rs:168:5
note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace
Panic in Arbiter thread.
 ```

This message means to tell us that there is no configuration file present!

So let us create one.

To provide some demo data, we can download a precached source from https://cloud.geomar.de/s/ex7ixd3ibR8M8W2

Let's download the *bluemarble*-source from that folder (https://cloud.geomar.de/s/b2JAjzTSwSFfdA4)

With this file downloaded, we can now create the file `initial_sources.toml` in the root directory of `tileserver` and add this source as a pre-cached zip source.

```toml
["Blue Marble"]
source = "zip"
files = ["G:\\bluemarble.zip"]
```

If we start the program now

```
cargo run --release
Running `target\release\tileserver.exe`
Initializing source Blue Marble as new zip source
        SourceConfig { input_files: ["G:\\bluemarble.zip"], settings: {} }
Source added as bluemarble
Server started
```
it starts up fine, but without our source. So let's add it to the `initial_sources.toml`.

```toml
[GPX]
source = "gpx"
files = ["G:\\garmin-activity.gpx"]
```

But now, the program won't start
```
cargo run --release
Running `target\release\tileserver.exe`
Initializing source Blue Marble as new zip source
        SourceConfig { input_files: ["G:\\bluemarble.zip"], settings: {} }
Source added as bluemarble
Initializing source GPX as new gpx source
        SourceConfig { input_files: ["G:\\garmin-activity.gpx"], settings: {} }
thread 'main' panicked at 'not yet implemented', src\sources\gpx_source.rs:63:9
note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace
Panic in Arbiter thread.
error: process didn't exit successfully: `target\release\tileserver.exe` (exit code: 101)
```

Looking up the line referenced in the output, we see that we need to fill in `get_info`.

Back in `gpx_source.rs`, we first add the necessary imports:

```rust
use gpx;
use gpx::{Gpx, Track, TrackSegment};
use std::fs::File;
use std::io::BufReader;
use geomorph::coord::Coord;
```

We will now also add a few instance variable fields to the `GPXSource`-struct

```rust
pub struct GPXSource{
    info: SourceInfo, //Cache the source info so we only need to copy it to send it out
    timestamps: Vec<Timestamp>, //Same for the timestamps
    gpx: Gpx //We only want to read the Gpx file once
}
```

Our `initialize`-routine now cannot instantiate `GPXSource` since we have now added these fields. Let us extend it.

First, we need to check if we actually have a file in our `SourceConfig` or need to terminate early.

```rust
if settings.input_files.len() < 1{
    return Err("No input file given".into());
}
```

Next up, we can open the file for reading through a buffer.

```rust
let mut reader = BufReader::new(File::open(&settings.input_files[0])?);
```

This is best read inside-out:

- `File::open(&settings.input_files[0])` opens the file at position 0 in the `input_files` list for reading.
- `File::open(&settings.input_files[0])?`: since opening the file returns a `Result`, we use the `?`-operator to return early if it is `Err`.
- `BufReader::new(File::open(&settings.input_files[0])?)` creates a new `BufReader` using the `Reader` object obtained from `File::open`.
- `let mut reader = BufReader::new(File::open(&settings.input_files[0])?);` we need the `mut`-attribute so we can modify the stream position (by reading) later on.

With the reader in hand, we can now create the `Gpx` struct.

```rust
let gpx = gpx::read(&mut reader)?;
```

To create the `SourceInfo`, we need a `LayerInfo` for each of the tracks in the GPX file.

Let's use an iterator to run over each track and create a `LayerInfo` from it

```rust
let layerinfo: Vec<LayerInfo> = gpx.tracks
            .iter() //Iterate all tracks
            .enumerate() //Count each element we come across (used later)
            .map(|(i, t)| { //Transform each element (tuple of number, track, since we used enumerate())
                LayerInfo{ //Create and return new LayerInfo-struct
                    name: t.name.clone().unwrap_or_else(|| format!("track {}",i + 1)), //Either the track has a name in the file, or we use the number we created through enumerate to create a new name for it
                    long_name: t.description.clone(), //Copy the description over
                    max_zoom_level: 1, //Points don't have zoom levels, for a tile source we would need further computation
                    layer_type: "vector/points/scalar".to_string(), //This is the correct source type for a point source that has scalar float information
                    unit: None, //The points in our example gpx file don't really have data, so we don't report a unit, datarange or zrange
                    datarange: None,
                    zrange: None,
                    timesteps: None //Time in point sources works differently and does not use the timesteps field
                }
            }).collect();
```

Using this `LayerInfo` we can also create the `SourceInfo`

```rust
let source_info = SourceInfo{
            source_name: "GPXSource".to_string(), //Usually the name of the struct, as a string. Pretty much deprecated at this point
            instance_name: gpx.metadata.clone()
                .map(|m| m.name) //If we have metadata in the gpx, we might have a name in it
                .flatten() //But the name is an option as well
                .or_else(|| settings.input_files[0].file_name().map(|f| f.to_string_lossy().to_string())) //If we don't have a name, try getting the file name
                .unwrap_or(settings.input_files[0].to_string_lossy().to_string()), //And if we can't get the file name, just return the whole path
            layers: layerinfo
        };
```

and instantiate the source and return it

```rust
let source = GPXSource{
    gpx,
    info: source_info,
    timestamps: Vec::new() //Again, no timesteps
};

Ok(Box::new(source))
```

Since the `GPXSource`-instance now contains everything to fill out the `get_info` and `get_timesteps`-methods.

```rust
///Returns the source's key parameters.
fn get_info(&self) -> SourceInfo{
    self.info.clone()
}
```

```rust
///Returns the source's timesteps or an empty `Vec` if the use of timesteps is inappropriate.
fn get_timesteps(&self) -> Vec<Timestamp>{
    self.timestamps.clone()
}
```

With all of this implemented, the `tileserver` will now start up.

```
cargo run --release
Running `target\release\tileserver.exe`
Initializing source Blue Marble as new zip source
        SourceConfig { input_files: ["G:\\bluemarble.zip"], settings: {} }
Source added as bluemarble
Initializing source GPX as new gpx source
        SourceConfig { input_files: ["G:\\garmin-activity.gpx"], settings: {} }
Source added as garmin-activity.gpx
Server started
```

## Implementing Sampling

Our server starts up, but it can't sample from the source yet.

Let's fix that.

First, we will need to find the correct track using the information supplied in `query`.

```rust
///Returns the queried tile data, or whatever error occurs during sampling.
fn sample(&self, query: &SampleQuery) -> Result<Option<TileData>, Box<dyn std::error::Error>>{
    let track = self.gpx.tracks.iter().find(|t| t.name == Some(query.layername.clone()));
```

There might not actually be a track with the name given (since the layer name is essentially sanitized user data), so we need to check the `Option` returned by `find`. We will do this using a destructuring `if let`.

```rust
if let Some(track) = track{
    //Track found
}else{
    return Err(format!("No track with name {}", query.layername).into());
}
```

If the correct track is found, we can now transform the points for this track into the format required for the `tileclient`.
First, we need to check if the point belongs into the tile required. For that, we retrieve an iterator over the points for this track (flattened from the segments) and get UEC x and y.

```rust
track.segments
    .iter() //Iterate track segments
    .map(|s| s.points.iter()) //For each track segment, iterate the points
    .flatten() //Concatenate these iterators
    .filter_map(|p| { //Filter_map to simulataneously convert the points into the correct format and filter out points that are not in the area
        let point = p.point(); //Retrieve the lat/lon-coordinates from the waypoint
        let pos: UEC = Coord::new(point.y(), point.x()).into(); //Convert into UEC
        if pos.in_area(query.tile.into_uec_tuple()){ //Check if the point is inside the tile

        }else{
            None
        }
    })
```

Finally, we need to transform the point data into a five-element-float32-array of the form `[x,y,height,value,timestamp]` and create a contiguous array from these.

As a value, we will use 0 for this tutorial, but in a serious source, this would contain the application payload value.

We will also need to create the correct enum variants.

This is the finished code

```rust
///Returns the queried tile data, or whatever error occurs during sampling.
fn sample(&self, query: &SampleQuery) -> Result<Option<TileData>, Box<dyn std::error::Error>>{
    let track = self.gpx.tracks.iter().find(|t| t.name == Some(query.layername.clone()));
    if let Some(track) = track{
        //Track found
        Ok( //Need to return a result
            Some( //And we have a result!
            TileData::Vector( //Points are vector data
                VectorTileData::DataPoints( //we want to store points with scalar data, this is the datatype for this
                track.segments
                .iter() //Iterate track segments
                .map(|s| s.points.iter()) //For each track segment, iterate the points
                .flatten() //Concatenate these iterators
                .filter_map(|p| { //Filter_map to simulataneously convert the points into the correct format and filter out points that are not in the area
                    let point = p.point(); //Retrieve the lat/lon-coordinates from the waypoint
                    let pos: UEC = Coord::new(point.y(), point.x()).into(); //Convert into UEC
                    if pos.in_area(query.tile.into_uec_tuple()){ //Check if the point is inside the tile
                        let time = p.time.map(|t| t.timestamp_millis()).unwrap_or(0) as f32; //Compute time if we have a timestamp, otherwise return the NODATA-time-value 0, convert to f32
                        let height = p.elevation.unwrap_or(0f64) as f32; //Convert elevation to f32 or return 0 if there's no height
                        Some(vec![pos.x as f32, pos.y as f32, height, 0f32, time]) // return array
                    }else{
                        None
                    }
                })
                .flatten() //Concatenate all these 5-element-arrays
                .collect()) //And store them in a vector
            , query.timestamp)
            )
        )
    }else{
        return Err(format!("No track with name {}", query.layername).into());
    }
}
```

## Testing the source

Running the server is successful now

```
cargo run --release
Running `target\release\tileserver.exe`
Initializing source Blue Marble as new zip source
        SourceConfig { input_files: ["G:\\bluemarble.zip"], settings: {} }
Source added as bluemarble
Initializing source GPX as new gpx source
        SourceConfig { input_files: ["G:\\garmin-activity.gpx"], settings: {} }
Source added as garmin-activity.gpx
Server started
```

If we open the client application in the browser (https://localhost:8080) and scroll over to northern California, we can see the track.

![Track shown in web client](ClientTrack.png)

