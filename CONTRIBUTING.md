Refer to the [WebGL Introduction](https://git.geomar.de/snippets/119#webgl) for
an overview of the technology.

More reference material will be added eventually.

Server internals are available as rustdoc. Run `cargo doc` in the `tileserver`
directory for interface descriptions, or read the code comments.