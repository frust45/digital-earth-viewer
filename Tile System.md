# Explanation of the tile system

Earth is divided in two regions: east (`E`) and west (`W`), with the dividing line being the prime meridian.
Each region is then divided into four tiles:

```
A   B
C   D
```

From this, a tilepath is derived like

`EADDCB`

In the extreme north and south, tiles often reach extreme aspect ratios.
If a tile reaches an aspect ratio of less than 0.5, it is not divided width-wise, and the subtiles are called `U` for upper and `L` for lower, so tilepaths end up looking like `WBBLBB`

