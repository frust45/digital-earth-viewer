# The Digital Earth Viewer

# Introduction

Copyright Jens Greinert, Valentin Buck, Flemming Stäbler, Everardo Gonzalez, Jochen Mohrmann at the GEOMAR Helmholtz-Centre 2021.

The Digital Earth Viewer is licensed under the European Union Public License version 1.2.

This means for you:
- by downloading, using or modifying this software, you choose to accept this license (as per section 10)
- you are free to use, reproduce and modify the work (as per section 2)
- you need to keep intact all license, copyright and other attribution notices (as per section 5)
- if you modify the software, you need to prominently mark it as such and provide the modified source code under this or a later version of this license (as per section 5)
- you accept that we do not offer warranty for this work (as per section 7a) and are not liable for damages caused by it (as per section 8)

# License Text

## EUROPEAN UNION PUBLIC LICENCE v. 1.2
*EUPL © the European Union 2007, 2016*

This European Union Public Licence (the ‘EUPL’) applies to the Work (as defined
below) which is provided under the terms of this Licence. Any use of the Work,
other than as authorised under this Licence is prohibited (to the extent such
use is covered by a right of the copyright holder of the Work).

The Work is provided under the terms of this Licence when the Licensor (as
defined below) has placed the following notice immediately following the
copyright notice for the Work:

> Licensed under the EUPL

or has expressed by any other means his willingness to license under the EUPL.

### 1. Definitions

In this Licence, the following terms have the following meaning:

- ‘The Licence’: this Licence.

- ‘The Original Work’: the work or software distributed or communicated by the
  Licensor under this Licence, available as Source Code and also as Executable
  Code as the case may be.

- ‘Derivative Works’: the works or software that could be created by the
  Licensee, based upon the Original Work or modifications thereof. This Licence
  does not define the extent of modification or dependence on the Original Work
  required in order to classify a work as a Derivative Work; this extent is
  determined by copyright law applicable in the country mentioned in Article 15.

- ‘The Work’: the Original Work or its Derivative Works.

- ‘The Source Code’: the human-readable form of the Work which is the most
  convenient for people to study and modify.

- ‘The Executable Code’: any code which has generally been compiled and which is
  meant to be interpreted by a computer as a program.

- ‘The Licensor’: the natural or legal person that distributes or communicates
  the Work under the Licence.

- ‘Contributor(s)’: any natural or legal person who modifies the Work under the
  Licence, or otherwise contributes to the creation of a Derivative Work.

- ‘The Licensee’ or ‘You’: any natural or legal person who makes any usage of
  the Work under the terms of the Licence.

- ‘Distribution’ or ‘Communication’: any act of selling, giving, lending,
  renting, distributing, communicating, transmitting, or otherwise making
  available, online or offline, copies of the Work or providing access to its
  essential functionalities at the disposal of any other natural or legal
  person.

### 2. Scope of the rights granted by the Licence

The Licensor hereby grants You a worldwide, royalty-free, non-exclusive,
sublicensable licence to do the following, for the duration of copyright vested
in the Original Work:

- use the Work in any circumstance and for all usage,
- reproduce the Work,
- modify the Work, and make Derivative Works based upon the Work,
- communicate to the public, including the right to make available or display
  the Work or copies thereof to the public and perform publicly, as the case may
  be, the Work,
- distribute the Work or copies thereof,
- lend and rent the Work or copies thereof,
- sublicense rights in the Work or copies thereof.

Those rights can be exercised on any media, supports and formats, whether now
known or later invented, as far as the applicable law permits so.

In the countries where moral rights apply, the Licensor waives his right to
exercise his moral right to the extent allowed by law in order to make effective
the licence of the economic rights here above listed.

The Licensor grants to the Licensee royalty-free, non-exclusive usage rights to
any patents held by the Licensor, to the extent necessary to make use of the
rights granted on the Work under this Licence.

### 3. Communication of the Source Code

The Licensor may provide the Work either in its Source Code form, or as
Executable Code. If the Work is provided as Executable Code, the Licensor
provides in addition a machine-readable copy of the Source Code of the Work
along with each copy of the Work that the Licensor distributes or indicates, in
a notice following the copyright notice attached to the Work, a repository where
the Source Code is easily and freely accessible for as long as the Licensor
continues to distribute or communicate the Work.

### 4. Limitations on copyright

Nothing in this Licence is intended to deprive the Licensee of the benefits from
any exception or limitation to the exclusive rights of the rights owners in the
Work, of the exhaustion of those rights or of other applicable limitations
thereto.

### 5. Obligations of the Licensee

The grant of the rights mentioned above is subject to some restrictions and
obligations imposed on the Licensee. Those obligations are the following:

Attribution right: The Licensee shall keep intact all copyright, patent or
trademarks notices and all notices that refer to the Licence and to the
disclaimer of warranties. The Licensee must include a copy of such notices and a
copy of the Licence with every copy of the Work he/she distributes or
communicates. The Licensee must cause any Derivative Work to carry prominent
notices stating that the Work has been modified and the date of modification.

Copyleft clause: If the Licensee distributes or communicates copies of the
Original Works or Derivative Works, this Distribution or Communication will be
done under the terms of this Licence or of a later version of this Licence
unless the Original Work is expressly distributed only under this version of the
Licence — for example by communicating ‘EUPL v. 1.2 only’. The Licensee
(becoming Licensor) cannot offer or impose any additional terms or conditions on
the Work or Derivative Work that alter or restrict the terms of the Licence.

Compatibility clause: If the Licensee Distributes or Communicates Derivative
Works or copies thereof based upon both the Work and another work licensed under
a Compatible Licence, this Distribution or Communication can be done under the
terms of this Compatible Licence. For the sake of this clause, ‘Compatible
Licence’ refers to the licences listed in the appendix attached to this Licence.
Should the Licensee's obligations under the Compatible Licence conflict with
his/her obligations under this Licence, the obligations of the Compatible
Licence shall prevail.

Provision of Source Code: When distributing or communicating copies of the Work,
the Licensee will provide a machine-readable copy of the Source Code or indicate
a repository where this Source will be easily and freely available for as long
as the Licensee continues to distribute or communicate the Work.

Legal Protection: This Licence does not grant permission to use the trade names,
trademarks, service marks, or names of the Licensor, except as required for
reasonable and customary use in describing the origin of the Work and
reproducing the content of the copyright notice.

### 6. Chain of Authorship

The original Licensor warrants that the copyright in the Original Work granted
hereunder is owned by him/her or licensed to him/her and that he/she has the
power and authority to grant the Licence.

Each Contributor warrants that the copyright in the modifications he/she brings
to the Work are owned by him/her or licensed to him/her and that he/she has the
power and authority to grant the Licence.

Each time You accept the Licence, the original Licensor and subsequent
Contributors grant You a licence to their contributions to the Work, under the
terms of this Licence.

### 7. Disclaimer of Warranty

The Work is a work in progress, which is continuously improved by numerous
Contributors. It is not a finished work and may therefore contain defects or
‘bugs’ inherent to this type of development.

For the above reason, the Work is provided under the Licence on an ‘as is’ basis
and without warranties of any kind concerning the Work, including without
limitation merchantability, fitness for a particular purpose, absence of defects
or errors, accuracy, non-infringement of intellectual property rights other than
copyright as stated in Article 6 of this Licence.

This disclaimer of warranty is an essential part of the Licence and a condition
for the grant of any rights to the Work.

### 8. Disclaimer of Liability

Except in the cases of wilful misconduct or damages directly caused to natural
persons, the Licensor will in no event be liable for any direct or indirect,
material or moral, damages of any kind, arising out of the Licence or of the use
of the Work, including without limitation, damages for loss of goodwill, work
stoppage, computer failure or malfunction, loss of data or any commercial
damage, even if the Licensor has been advised of the possibility of such damage.
However, the Licensor will be liable under statutory product liability laws as
far such laws apply to the Work.

### 9. Additional agreements

While distributing the Work, You may choose to conclude an additional agreement,
defining obligations or services consistent with this Licence. However, if
accepting obligations, You may act only on your own behalf and on your sole
responsibility, not on behalf of the original Licensor or any other Contributor,
and only if You agree to indemnify, defend, and hold each Contributor harmless
for any liability incurred by, or claims asserted against such Contributor by
the fact You have accepted any warranty or additional liability.

### 10. Acceptance of the Licence

The provisions of this Licence can be accepted by clicking on an icon ‘I agree’
placed under the bottom of a window displaying the text of this Licence or by
affirming consent in any other similar way, in accordance with the rules of
applicable law. Clicking on that icon indicates your clear and irrevocable
acceptance of this Licence and all of its terms and conditions.

Similarly, you irrevocably accept this Licence and all of its terms and
conditions by exercising any rights granted to You by Article 2 of this Licence,
such as the use of the Work, the creation by You of a Derivative Work or the
Distribution or Communication by You of the Work or copies thereof.

### 11. Information to the public

In case of any Distribution or Communication of the Work by means of electronic
communication by You (for example, by offering to download the Work from a
remote location) the distribution channel or media (for example, a website) must
at least provide to the public the information requested by the applicable law
regarding the Licensor, the Licence and the way it may be accessible, concluded,
stored and reproduced by the Licensee.

### 12. Termination of the Licence

The Licence and the rights granted hereunder will terminate automatically upon
any breach by the Licensee of the terms of the Licence.

Such a termination will not terminate the licences of any person who has
received the Work from the Licensee under the Licence, provided such persons
remain in full compliance with the Licence.

### 13. Miscellaneous

Without prejudice of Article 9 above, the Licence represents the complete
agreement between the Parties as to the Work.

If any provision of the Licence is invalid or unenforceable under applicable
law, this will not affect the validity or enforceability of the Licence as a
whole. Such provision will be construed or reformed so as necessary to make it
valid and enforceable.

The European Commission may publish other linguistic versions or new versions of
this Licence or updated versions of the Appendix, so far this is required and
reasonable, without reducing the scope of the rights granted by the Licence. New
versions of the Licence will be published with a unique version number.

All linguistic versions of this Licence, approved by the European Commission,
have identical value. Parties can take advantage of the linguistic version of
their choice.

### 14. Jurisdiction

Without prejudice to specific agreement between parties,

- any litigation resulting from the interpretation of this License, arising
  between the European Union institutions, bodies, offices or agencies, as a
  Licensor, and any Licensee, will be subject to the jurisdiction of the Court
  of Justice of the European Union, as laid down in article 272 of the Treaty on
  the Functioning of the European Union,

- any litigation arising between other parties and resulting from the
  interpretation of this License, will be subject to the exclusive jurisdiction
  of the competent court where the Licensor resides or conducts its primary
  business.

### 15. Applicable Law

Without prejudice to specific agreement between parties,

- this Licence shall be governed by the law of the European Union Member State
  where the Licensor has his seat, resides or has his registered office,

- this licence shall be governed by Belgian law if the Licensor has no seat,
  residence or registered office inside a European Union Member State.

### Appendix

‘Compatible Licences’ according to Article 5 EUPL are:

- GNU General Public License (GPL) v. 2, v. 3
- GNU Affero General Public License (AGPL) v. 3
- Open Software License (OSL) v. 2.1, v. 3.0
- Eclipse Public License (EPL) v. 1.0
- CeCILL v. 2.0, v. 2.1
- Mozilla Public Licence (MPL) v. 2
- GNU Lesser General Public Licence (LGPL) v. 2.1, v. 3
- Creative Commons Attribution-ShareAlike v. 3.0 Unported (CC BY-SA 3.0) for
  works other than software
- European Union Public Licence (EUPL) v. 1.1, v. 1.2
- Québec Free and Open-Source Licence — Reciprocity (LiLiQ-R) or Strong
  Reciprocity (LiLiQ-R+).

The European Commission may update this Appendix to later versions of the above
licences without producing a new version of the EUPL, as long as they provide
the rights granted in Article 2 of this Licence and protect the covered Source
Code from exclusive appropriation.

All other changes or additions to this Appendix require the production of a new
EUPL version.


# Third Party Components

The Digital Earth Viewer uses a set of third party components to achieve its functionality. These third party components are licensed under a set of diverse, but compatible open source licenses and listed below by area of interest.

### Client side libraries
These libraries are used for the client-side application
|Library | Author | License | Repository|
|--------|--------|---------|-----------|
|Vue | Evan You & Contributors | [MIT](https://github.com/vuejs/vue/blob/dev/LICENSE) | https://github.com/vuejs/vue|
|lru_map | Rasmus Andersson | [MIT](https://github.com/rsms/js-lru/blob/master/LICENSE.txt) | https://github.com/rsms/js-lru|
|marked | Tony Brix & Contributors | [MIT](https://github.com/markedjs/marked/blob/master/LICENSE.md) | https://github.com/markedjs/marked|
|core-js | Denis Pushkarev & Contributors | [MIT](https://github.com/zloirock/core-js/blob/master/LICENSE) | https://github.com/zloirock/core-js|

### Server Base Libraries
These libaries power the webserver, plugin loader and scheduler
|Library | Author | License | Repository|
|--------|--------|---------|-----------|
|alcro | Srinivasa Mahesh | [MIT](https://choosealicense.com/licenses/mit) or [Apache](https://choosealicense.com/licenses/apache-2.0) | https://github.com/Srinivasa314/alcro|
|tokio | Carl Lerche & Contributors | [MIT](https://github.com/tokio-rs/tokio/blob/master/LICENSE) | https://github.com/tokio-rs/tokio|
|futures | Alex Crichton & Contributors | [MIT](https://github.com/rust-lang/futures-rs/blob/master/LICENSE-MIT) or [Apache](https://github.com/rust-lang/futures-rs/blob/master/LICENSE-APACHE) | https://github.com/rust-lang/futures-rs|
|warp | Sean McArthur & Contributors | [MIT](https://github.com/seanmonstar/warp/blob/master/LICENSE) | https://github.com/seanmonstar/warp|
|percent-encoding | Simon Sapin & Contributors | [MIT](https://github.com/servo/rust-url/blob/master/LICENSE-MIT) or [Apache](https://github.com/servo/rust-url/blob/master/LICENSE-APACHE) | https://github.com/servo/rust-url/|
|bytes | Carl Lerche & Contributors | [MIT](https://github.com/tokio-rs/bytes/blob/master/LICENSE) | https://github.com/tokio-rs/bytes|
|log | Steven Fackler & Contributors | [MIT](https://github.com/rust-lang/log/blob/master/LICENSE-MIT) or [Apache](https://github.com/rust-lang/log/blob/master/LICENSE-APACHE) | https://github.com/rust-lang/log|
|simplelog | Victor Brekenfeld | [MIT](https://github.com/Drakulix/simplelog.rs/blob/master/LICENSE.MIT) or [Apache2](https://github.com/Drakulix/simplelog.rs/blob/master/LICENSE.APACHE2) | https://github.com/drakulix/simplelog.rs|
|serde, serde_json | David Tolnay | [MIT](https://github.com/serde-rs/serde/blob/master/LICENSE-MIT) or [Apache](https://github.com/serde-rs/serde/blob/master/LICENSE-APACHE) | https://github.com/serde-rs/serde|
|chrono | Brandon W. Maister & Contributors | [MIT and Apache2.0](https://github.com/chronotope/chrono/blob/main/LICENSE.txt) | https://github.com/chronotope/chrono|
|git-version | Mara Bos & Contributors | [BSD 2-Clause-License](https://github.com/fusion-engineering/rust-git-version/blob/master/COPYING) | https://github.com/fusion-engineering/rust-git-version |
|sha2 | Artyom Pavlov & Contributors | [Apache 2](http://www.apache.org/licenses/LICENSE-2.0) or [MIT](http://opensource.org/licenses/MIT) | https://github.com/RustCrypto/hashes|
|lazy-static | Marvin Löbel | [Apache 2](https://github.com/rust-lang-nursery/lazy-static.rs/blob/master/LICENSE-APACHE) | https://github.com/rust-lang-nursery/lazy-static.rs|
|lru | Jerome Froelich | [MIT](https://github.com/jeromefroe/lru-rs) | https://github.com/jeromefroe/lru-rs|
|rand | Diggory Hardy & Contributors | [MIT or Apache 2](https://github.com/rust-random/rand/blob/master/COPYRIGHT) | https://github.com/rust-random/rand|
|flate2 | Alex Crichton & Contributors | [MIT](https://github.com/rust-lang/flate2-rs/blob/master/LICENSE-MIT) or [Apache 2](https://github.com/rust-lang/flate2-rs/blob/master/LICENSE-APACHE) | https://github.com/rust-lang/flate2-rs|
|regex | Andrew Gallant & Contributors | [Apache](https://github.com/rust-lang/regex/blob/master/LICENSE-APACHE) or [MIT](https://github.com/rust-lang/regex/blob/master/LICENSE-MIT) | https://github.com/rust-lang/regex|
|toml-rs | Alex Crichton & Contributors | [MIT](https://github.com/alexcrichton/toml-rs/blob/master/LICENSE-MIT) or [Apache](https://github.com/alexcrichton/toml-rs/blob/master/LICENSE-APACHE) | https://github.com/alexcrichton/toml-rs|
|num_cpus | Sean McArthur & Contributors | [Apache 2](https://github.com/seanmonstar/num_cpus/blob/master/LICENSE-APACHE) or [MIT](https://github.com/seanmonstar/num_cpus/blob/master/LICENSE-MIT) | https://github.com/seanmonstar/num_cpus|
|dashmap | Joel Wejdenstal & Contributors | [MIT](https://github.com/xacrimon/dashmap/blob/master/LICENSE) | https://github.com/xacrimon/dashmap|
|crc32fast | Sam Rijs & Contributors | [MIT](https://github.com/srijs/rust-crc32fast/blob/master/LICENSE-MIT) or [Apache](https://github.com/srijs/rust-crc32fast/blob/master/LICENSE-APACHE) | https://github.com/srijs/rust-crc32fast|
|rusqlite | John Gallagher & Contributors | [MIT](https://github.com/rusqlite/rusqlite/blob/master/LICENSE) | https://github.com/rusqlite/rusqlite
|mime-guess | Austin Bonander & Contributors | [MIT](https://github.com/abonander/mime_guess/blob/master/LICENSE) | https://github.com/abonander/mime_guess|
|stopwatch | Chucky Ellison & Contributors | [MIT](https://github.com/ellisonch/rust-stopwatch/blob/master/LICENSE.txt) | https://github.com/ellisonch/rust-stopwatch|
|rayon | Josh Stone & Contributors | [MIT](https://github.com/rayon-rs/rayon/blob/master/LICENSE-MIT) or [Apache](https://github.com/rayon-rs/rayon/blob/master/LICENSE-APACHE) | https://github.com/rayon-rs/rayon|
|async-trait | David Tolnay & Contributors | [MIT](https://github.com/dtolnay/async-trait/blob/master/LICENSE-MIT) or [Apache](https://github.com/dtolnay/async-trait/blob/master/LICENSE-APACHE) | https://github.com/dtolnay/async-trait|
|env-logger | Ashley Mannix & Contributors | [MIT](https://github.com/env-logger-rs/env_logger/blob/main/LICENSE-MIT) or [Apache](https://github.com/env-logger-rs/env_logger/blob/main/LICENSE-APACHE) | https://github.com/env-logger-rs/env_logger|
|smallvec | Matt Brubeck & Contributors | [MIT](https://github.com/servo/rust-smallvec/blob/master/LICENSE-MIT) or [Apache](https://github.com/servo/rust-smallvec/blob/master/LICENSE-APACHE) | https://github.com/servo/rust-smallvec|
|once_cell | Aleksey Kladov & Contributors | [MIT](https://github.com/matklad/once_cell/blob/master/LICENSE-MIT) or [Apache](https://github.com/matklad/once_cell/blob/master/LICENSE-APACHE) | https://github.com/matklad/once_cell|

### Server Plugin Libraries
These libaries power the plugins that are included with the Digital Earth Viewer by default. Only libraries not included in the base server application are listed here.
|Library | Author | License | Repository|
|--------|--------|---------|-----------|
|image | Andreas Molzer & Contributors | [MIT](https://github.com/image-rs/image/blob/master/LICENSE) | https://github.com/image-rs/image|
|rstar | Stephan Hügel & Contributors | [Apache 2](https://github.com/georust/rstar/blob/master/LICENSE-APACHE) | https://github.com/georust/rstar|
|gpx | Corey Farwell & Contributors | [MIT](https://github.com/georust/gpx/blob/master/LICENSE) | https://github.com/georust/gpx|
|reqwest | Sean McArthur & Contributors | [Apache 2](https://github.com/seanmonstar/reqwest/blob/master/LICENSE-APACHE) | https://github.com/seanmonstar/reqwest|
|bincode | Ty Overby & Contributors | [MIT](https://github.com/bincode-org/bincode/blob/trunk/LICENSE.md) | https://github.com/bincode-org/bincode|
|netcdf | Magnus Ulimoen & Contributors | [MIT](https://github.com/georust/netcdf/blob/master/LICENSE-MIT) or [Apache 2](https://github.com/georust/netcdf/blob/master/LICENSE-APACHE) | https://github.com/georust/netcdf|
|shapefile-rs | Roger Filmyer & Contributors | [MIT](https://github.com/tmontaigu/shapefile-rs/blob/master/LICENSE.md) | https://github.com/tmontaigu/shapefile-rs|
|geo | Stephan Hügel & Contributors | [MIT](https://github.com/georust/geo/blob/master/LICENSE-MIT) or [Apache](https://github.com/georust/geo/blob/master/LICENSE-APACHE) | https://github.com/georust/geo|
|rusttype | Alex Butler & Contributors | [MIT](https://gitlab.redox-os.org/redox-os/rusttype/-/blob/master/LICENSE-MIT) or [Apache 2](https://gitlab.redox-os.org/redox-os/rusttype/-/blob/master/LICENSE-APACHE) | https://gitlab.redox-os.org/redox-os/rusttype|
|palette | Erik Hedvall & Contributors | [MIT](https://github.com/Ogeon/palette/blob/master/LICENSE-MIT) or [Apache 2](https://github.com/Ogeon/palette/blob/master/LICENSE-APACHE) | https://github.com/Ogeon/palette|
|imageproc | Sven Nilsen & Contributors | [MIT](https://github.com/image-rs/imageproc/blob/master/LICENSE) | https://github.com/image-rs/imageproc|
|quick-xml | Johann Tuffe & Contributors | [MIT](https://github.com/tafia/quick-xml/blob/master/LICENSE-MIT.md) | https://github.com/tafia/quick-xml|
