//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset'
  ]
}
