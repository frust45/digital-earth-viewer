//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

bool isnan(float val)
{
  return (val < 0.0 || 0.0 < val || val == 0.0) ? false : true;
}
