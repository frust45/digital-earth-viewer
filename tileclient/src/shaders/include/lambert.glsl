//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

float lambert_shading(vec3 normal, vec3 light) {
    return clamp(dot(normal, light), 0.0, 1.0);
}