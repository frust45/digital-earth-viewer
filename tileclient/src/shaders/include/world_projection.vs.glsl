//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

vec3 world_normal(vec2 xy){
    vec2 p2 = vec2(
        (xy.x + 0.5) * radians(360.0),
        -(xy.y - 0.5) * radians(180.0)
    );
    float cy = cos(p2.y);
    return vec3(
        cos(p2.x) * cy,
        sin(p2.x) * cy,
        sin(p2.y)
    );
}

#if defined SPHERE
vec4 world_projection(vec3 pos, mat4 world_mat, mat4 proj_mat){
    vec3 rpos = (1.0 + pos.z) * world_normal(pos.xy);
    return proj_mat * world_mat * vec4(rpos, 1.0);
}
#elif defined EQUIRECT
vec4 world_projection(vec3 pos, mat4 world_mat, mat4 proj_mat){
    return proj_mat * world_mat * vec4(pos, 1.0);
}
#elif defined POLAR
//TODO
vec4 world_projection(vec3 pos, mat4 world_mat, mat4 proj_mat){
    return vec4(0.0, 0.0, 0.0, 1.0);
}
#endif

vec3 world_tangent(vec2 xy){
    vec2 p2 = vec2(
        (xy.x + 0.5) * radians(360.0),
        -(xy.y - 0.5) * radians(180.0)
    );
    float sy = sin(p2.y);
    return vec3(
        -cos(p2.x) * sy,
        -sin(p2.x) * sy,
        cos(p2.y)
    );
}

vec3 world_cotangent(vec2 xy){
    vec2 p2 = vec2(
        (xy.x + 0.5) * radians(360.0),
        -(xy.y - 0.5) * radians(180.0)
    );
    return vec3(
        -sin(p2.x),
        cos(p2.x),
        0.0
    );
}