float depth_linear(float depth_raw, float near_ratio){
    float a = near_ratio / (near_ratio - 1.0);
    float b = 1.0 - a;
    return a / (depth_raw - b);
}
