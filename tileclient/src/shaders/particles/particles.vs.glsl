//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

precision highp float;

attribute vec2 position;
uniform float source_offset;
uniform float target_offset;

varying vec2 screen_coordinate;

void main() {
    vec2 position_ = position;
    position_ = (position + 1.0) / 2.0;
    screen_coordinate = vec2((position_.x + source_offset) / 8.0, position_.y);
    position_.x = (position_.x + target_offset) / 8.0;
    gl_Position = vec4(position_ * 2.0 - 1.0, 0.0, 1.0);
}