//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

precision highp float;

varying vec2 screen_coordinate;

uniform sampler2D particles;
uniform sampler2D vector_field;

uniform vec2 vector_field_coord_offset;
uniform vec2 vector_field_coord_scale;

uniform float particle_speed;
uniform float particle_lifetime;

uniform vec2 frame_random;
uniform sampler2D global_random;

#include "../include/isnan.fs.glsl"
#include "../include/world_projection.vs.glsl"
#include "../include/constants.glsl"

void main() {

    vec3 particle_data = texture2D(particles, screen_coordinate).rgb;
    vec2 particle_pos = particle_data.rg;
    float particle_age = particle_data.b;

    vec2 vector_field_coord = (particle_pos - vector_field_coord_offset) / vector_field_coord_scale;
    vec2 direction = texture2D(vector_field, vector_field_coord).rg;

    if( isnan(direction.x) ||
        isnan(direction.y) ||
        length(direction) == 0.0 ||
        particle_pos.x < vector_field_coord_offset.x ||
        particle_pos.y < vector_field_coord_offset.y ||
        particle_pos.x > vector_field_coord_offset.x + vector_field_coord_scale.x ||
        particle_pos.y > vector_field_coord_offset.y + vector_field_coord_scale.y ||
        particle_age < 0.0 ||
        isnan(particle_pos.x) ||
        isnan(particle_pos.y) ||
        isnan(particle_age))
    {
        vec4 random = texture2D(global_random, screen_coordinate + frame_random);
        vec2 new_pos = vector_field_coord + random.xy * 1.241352;
        new_pos = mod(new_pos, 1.0);
        float npx = new_pos.x * vector_field_coord_scale.x + vector_field_coord_offset.x;
        
        //Correct density for smaller differential area near the poles
        float ymin_tr = 0.5 - cos(radians(180.0) * vector_field_coord_offset.y) / 2.0;
        float ymax_tr = 0.5 - cos(radians(180.0) * (vector_field_coord_offset.y + vector_field_coord_scale.y)) / 2.0;
        float npy = new_pos.y * (ymax_tr - ymin_tr) + ymin_tr;
        npy = 1.0 - acos(2.0 * npy - 1.0) / radians(180.0);

        gl_FragColor = vec4(npx, npy, particle_lifetime * (random.z + 0.5), 1.0);
    } else {
        //vec2 new_pos = particle_pos + direction * (particle_speed / EARTH_RADIUS);
        
        vec2 speed = direction * particle_speed / EARTH_RADIUS;

        vec3 world_pos = world_normal(particle_pos);

        vec3 world_u = world_cotangent(particle_pos);
        vec3 world_v = world_tangent(particle_pos);

        /*
        float cos_a = cos(speed);
        float sin_a = sin(speed);
        float cos_1 = 1.0 - cos(speed);

        vec2 norm_dir = normalize(direction);
        vec3 rot_axis = norm_dir.x * world_u + norm_dir.y * world_v;

        mat3 rot_mat = mat3(
            rot_axis.x * rot_axis.x * cos_1 + cos_a,
            rot_axis.y * rot_axis.x * cos_1 + rot_axis.z * sin_a,
            rot_axis.z * rot_axis.x * cos_1 - rot_axis.y * sin_a,
            rot_axis.x * rot_axis.y * cos_1 - rot_axis.z * sin_a,
            rot_axis.y * rot_axis.y * cos_1 + cos_a,
            rot_axis.z * rot_axis.y * cos_1 + rot_axis.x * sin_a,
            rot_axis.x * rot_axis.z * cos_1 + rot_axis.y * sin_a,
            rot_axis.y * rot_axis.z * cos_1 - rot_axis.x * sin_a,
            rot_axis.z * rot_axis.z * cos_1 + cos_a
        );
        */
        vec3 new_world_pos = normalize(world_pos + world_u * speed.x + world_v * speed.y);

        float npx = atan(new_world_pos.y, new_world_pos.x) / radians(360.0) + 0.5;
        float npy = 0.5 - asin(new_world_pos.z) / radians(180.0);

        gl_FragColor = vec4(npx, npy, particle_age - 1.0, 1.0);
    }
}