//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

var fs = require('fs');
var path = require('path');

let shaders_dir = __dirname;

let directories = fs.readdirSync(shaders_dir, {withFileTypes: true}).filter(d => d.isDirectory()).map(d => d.name);


let allowed_extensions = ["prog"];

let modules = {};

let resolve_includes_recursive = (p) => {
    let fi = fs.readFileSync(p,{encoding: "utf-8"}).toString();
    let matches = [...fi.matchAll(/#include\s+\"(.*)\".*/gm)];
    if(matches) matches.forEach(m => {
        let p_include = m[1];
        if(!path.isAbsolute(p_include)){
            p_include = path.join(path.dirname(p), p_include);
        }
        let included_file = resolve_includes_recursive(p_include);
        fi = fi.replace(m[0], included_file + "\n");
    });
    return fi;
}

for(var dir of directories){
    let program_filenames = fs.readdirSync(path.join(shaders_dir, dir), {withFileTypes: true}).filter(f => f.isFile()).map(f => f.name).filter(f => allowed_extensions.some(ext => f.endsWith(ext)));
    
    let programs = {};


    for(var pf of program_filenames){
        pf = path.join(shaders_dir, dir, pf);
        console.info("Loading program " + pf);
        let lines = fs.readFileSync(pf, {encoding: "utf-8"}).toString().split("\n").map(l => l.trim());
        let cwd = path.dirname(pf);
        let vertex = lines.filter(f => !f.startsWith("//") && f.endsWith("vs.glsl")).map(fn => `//@import ${fn}\n` + resolve_includes_recursive(path.join(cwd, fn))).join("\n\n");
        let fragment = lines.filter(f => !f.startsWith("//") && f.endsWith("fs.glsl")).map(fn => `//@import ${fn}\n` + resolve_includes_recursive(path.join(cwd, fn))).join("\n\n");
        programs[path.basename(pf, path.extname(pf))] = {vertex, fragment};
    }

    modules[dir]  = programs;

}

let header = "//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer\n\nexport const Shaders: {[module: string]: {[program_name: string]: {vertex: string, fragment: string}}} = ";

let body = JSON.stringify(modules,null, 3);

let footer = ";";

fs.writeFileSync(path.join(shaders_dir, "Shaders.ts"), header + body + footer,{encoding: "utf-8"});