//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

#extension GL_EXT_frag_depth : require
precision highp float;

varying vec2 screen_coordinate;

uniform sampler2D color_map; //data that has already been composited
uniform sampler2D depth_map;
uniform sampler2D normal_map;
uniform sampler2D layer_value_map; //selection bounding box as rendered to screen space
uniform sampler2D layer_depth_map;
uniform sampler2D layer_normal_map;

#include "../include/isnan.fs.glsl"

float sigmoid_4(float x){
    return (x / sqrt(1.0 + x * x)) / 4.0 + 0.25;
}

void main(){
    float ld = texture2D(layer_depth_map, screen_coordinate).x;
    float d = texture2D(depth_map, screen_coordinate).x;
    float depth_difference = d - ld;
    vec4 lvm_color = texture2D(layer_value_map, screen_coordinate);
    vec4 cm_color = texture2D(color_map, screen_coordinate);
    bool lvm_isnan = isnan(lvm_color.r + lvm_color.g + lvm_color.b + lvm_color.a);
    if(lvm_isnan){
        gl_FragColor = cm_color;
        gl_FragDepthEXT = d;
        return;
    }
    float blendfac = sigmoid_4(depth_difference * 100000.0) * lvm_color.a;
    if(blendfac < 1.0/255.0){
        gl_FragColor = cm_color;
        gl_FragDepthEXT = d;
        return;
    }
    gl_FragDepthEXT = min(ld, d);
    gl_FragColor = blendfac * lvm_color + (1.0 - blendfac) * cm_color;
}