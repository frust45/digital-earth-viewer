//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

precision highp float;

attribute vec2 position;
uniform float source_offset;

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;

varying vec2 screen_coordinate;

uniform sampler2D stitched_displacement_map;
uniform vec2 stitched_displacement_coord_scale;
uniform vec2 stitched_displacement_coord_offset;

uniform sampler2D stitched_vector_map;
uniform vec2 stitched_vector_coord_scale;
uniform vec2 stitched_vector_coord_offset;

uniform sampler2D particle_pos;

varying float velocity;

varying float displaced_geometry_height;
uniform float displacement_scale;
uniform float displacement_offset;
uniform bool displacement_active;

uniform float point_size;

varying vec3 var_position;

varying float var_value;

#include "../include/world_projection.vs.glsl"
#include "../include/constants.glsl"

void main() {
    vec2 position_ = position / 2.0 + 0.5;

    position_ = texture2D(particle_pos, vec2((position_.x + source_offset) / 8.0, position_.y)).xy;

    screen_coordinate = position_;

    var_position = vec3(position, 0.0);

    vec2 stitched_displacement_coord = (position_ - stitched_displacement_coord_offset) / stitched_displacement_coord_scale;
    vec2 stitched_vector_coord = (position_ - stitched_vector_coord_offset) / stitched_vector_coord_scale;

    vec3 euclidian_position = world_normal(position_);

    displaced_geometry_height = texture2D(stitched_displacement_map, stitched_displacement_coord).r;
    velocity = length(texture2D(stitched_vector_map, stitched_vector_coord).rg);

    var_value = velocity;

    float displacement_height = (displaced_geometry_height + displacement_offset) * displacement_scale / EARTH_RADIUS;
    //gl_Position = vec4(position, 0.0, 1.0);
    gl_Position = world_projection(vec3(position_.xy, displacement_height), viewMatrix, projectionMatrix);
    gl_PointSize = max(1.0, point_size);

}