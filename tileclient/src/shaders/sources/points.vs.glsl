//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

precision highp float;

attribute vec3 position;
attribute float value;
attribute float time;

uniform vec2 reference_position;


uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;

uniform float displacement_scale;
uniform float displacement_offset;

uniform float time_min;
uniform float time_max;

uniform bool value_sizing_enabled;
uniform float value_sizing_zero;
uniform float value_sizing_one;
uniform float value_sizing_power;

varying vec3 var_position;
varying float displaced_point_height;
varying float var_value;
varying float var_time;

uniform float point_size;

#include "../include/world_projection.vs.glsl"
#include "../include/isnan.fs.glsl"
#include "../include/constants.glsl"

void main(){
    vec2 position_;
    #if defined SPHERE
        position_ = position.xy + reference_position;
        var_position.xy = position_;
        var_position.z = position.z;
    #elif defined EQUIRECT
        position_ = position.xy;
        var_position.xy = position.xy + reference_position;
        var_position.z = position.z;
    #elif defined POLAR
        position_ = position.xy;
        var_position.xy = position.xy + reference_position;
        var_position.z = position.z;
    #endif
    var_value = value;
    var_time = time;
    if(isnan(value) || time < time_min || time > time_max){
        gl_Position = vec4(0.0, 0.0, 2.0, 1.0);
        gl_PointSize = 0.0;
    }else{
        displaced_point_height = (isnan(position.z) ? displacement_offset : ((position.z + displacement_offset) * displacement_scale)) / EARTH_RADIUS;
        gl_Position = world_projection(vec3(position_, displaced_point_height), viewMatrix, projectionMatrix);
        if(value_sizing_enabled){
            gl_PointSize = point_size * pow(
                (value - value_sizing_zero) / (value_sizing_one - value_sizing_zero),
                value_sizing_power
            );
        }else{   
            gl_PointSize = max(1.0, point_size);
        }
    }
}