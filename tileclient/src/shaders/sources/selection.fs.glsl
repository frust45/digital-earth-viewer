//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

#extension GL_EXT_draw_buffers : require 
precision highp float;

uniform vec4 selection_color;

void main() {
    gl_FragData[0] = selection_color;;
    gl_FragData[1] = vec4(0.0);
    gl_FragData[2] = vec4(0.0);
}