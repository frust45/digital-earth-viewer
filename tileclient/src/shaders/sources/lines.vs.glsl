//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

precision highp float;

attribute vec3 geometry_position;

attribute vec3 position;
attribute float value;
attribute float time;

attribute vec3 position2;
attribute float value2;
attribute float time2;

uniform vec4 value_mixing;

uniform vec2 reference_position;

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;

uniform float displacement_scale;
uniform float displacement_offset;

varying vec3 var_position;
varying float displaced_point_height;
varying float var_value;
varying float var_time;

uniform vec2 point_size;

#include "../include/world_projection.vs.glsl"
#include "../include/isnan.fs.glsl"
#include "../include/constants.glsl"

void main(){
    vec2 rel_position = geometry_position.xy;
    vec3 interpolated_position = geometry_position.z * position + (1.0 - geometry_position.z) * position2;
    float interpolated_value = geometry_position.z * value + (1.0 - geometry_position.z) * value2;
    float interpolated_time = geometry_position.z * time + (1.0 - geometry_position.z) * time2;

    vec2 position_;
    #if defined SPHERE
        position_ = interpolated_position.xy + reference_position;
        var_position.xy = position_;
        var_position.z = interpolated_position.z;
    #elif defined EQUIRECT
        position_ = interpolated_position.xy;
        var_position.xy = interpolated_position.xy + reference_position;
        var_position.z = interpolated_position.z;
    #elif defined POLAR
        position_ = interpolated_position.xy;
        var_position.xy = interpolated_position.xy + reference_position;
        var_position.z = interpolated_position.z;
    #endif
    var_value = interpolated_value;
    var_time = interpolated_time;
    displaced_point_height = (isnan(interpolated_position.z) ? displacement_offset : ((interpolated_position.z + displacement_offset) * displacement_scale)) / EARTH_RADIUS;
    vec4 out_pos = world_projection(vec3(position_, displaced_point_height), viewMatrix, projectionMatrix);
    gl_Position = out_pos + vec4(rel_position * point_size * out_pos.w, 0.0, 0.0);
}