//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

precision highp float;

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;

uniform vec2 selection_coord;
uniform vec2 selection_size;

uniform float selection_zmin;
uniform float selection_height;

attribute vec3 position;
//varying vec2 world_position;

//varying vec3 undisplaced_geo_normal;
//"sine" direction, V
//varying vec3 undisplaced_geo_tangent;
//"cosine" direction, U
//varying vec3 undisplaced_geo_cotangent;

#include "../include/world_projection.vs.glsl"
#include "../include/constants.glsl"

void main(){
    vec2 position_ = position.xy / 2.0 + 0.5;
    vec2 world_position;
#if defined SPHERE
    position_ = position_ * selection_size + selection_coord;
    world_position = position_;
#elif defined EQUIRECT
    world_position = position_ * selection_size + selection_coord;
#elif defined POLAR
    position_ = position_ * selection_size + selection_coord;
    world_position = position_;
#endif

    vec3 euclidian_position = world_normal(world_position);

    gl_Position = world_projection(vec3(position_.xy, selection_zmin + position.z * selection_height - 1.0), viewMatrix, projectionMatrix);
}