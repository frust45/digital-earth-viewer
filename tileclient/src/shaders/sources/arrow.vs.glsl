//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

precision highp float;

attribute vec3 geometry_position;

attribute float time;
attribute vec3 position;
attribute vec2 value;

uniform vec4 value_mixing;

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;

uniform float displacement_scale;
uniform float displacement_offset;

uniform float time_min;
uniform float time_max;

uniform bool value_sizing_enabled;
uniform float value_sizing_zero;
uniform float value_sizing_one;
uniform float value_sizing_power;

varying vec3 var_position;
varying float displaced_point_height;
varying float var_value;
varying vec3 var_normal;

uniform float point_size;

#include "../include/world_projection.vs.glsl"
#include "../include/isnan.fs.glsl"
#include "../include/constants.glsl"

void main(){
    var_position = position;
    float value_abs = length(value);
    var_value = value_abs;
    vec2 value_dir = normalize(value);
    if(isnan(value.x) || isnan(value.y) || time < time_min || time > time_max){
        gl_Position = vec4(0.0, 0.0, 2.0, 1.0);
    }else{
        displaced_point_height = (isnan(position.z) ? displacement_offset : ((position.z + displacement_offset) * displacement_scale)) / EARTH_RADIUS;
        vec3 normal = world_normal(position.xy);
        vec3 euclidian_position = (displaced_point_height + 1.0) * normal;
        float arrowscale;
        if(value_sizing_enabled){
            arrowscale = min(point_size, point_size * pow(
                (value_abs - value_sizing_zero) / (value_sizing_one - value_sizing_zero),
                value_sizing_power
            ));
        }else{   
            arrowscale = max(1.0, point_size);
        }
        vec3 tangent = world_tangent(position.xy);
        vec3 cotangent = world_cotangent(position.xy);
        vec3 fz = tangent * value_dir.y + cotangent * value_dir.x;
        vec3 fx = tangent * value_dir.x - cotangent * value_dir.y;
        vec3 fy = normal;
        vec3 transformed_geometry_position = geometry_position.x * fx + geometry_position.y * fy + geometry_position.z * fz;
        transformed_geometry_position /= EARTH_RADIUS / arrowscale;
        gl_Position = projectionMatrix * viewMatrix * vec4(transformed_geometry_position + euclidian_position, 1.0);// + euclidian_position * arrowscale, 1.0);
    }
}