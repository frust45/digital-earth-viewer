//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

#extension GL_EXT_draw_buffers : require 
precision highp float;

varying vec3 var_position;
varying float displaced_point_height;
varying float var_value;

#include "../include/isnan.fs.glsl"

void main() {

    gl_FragData[0] = vec4(var_value, 0.0, 0.0, 0.0);
    gl_FragData[1] = vec4(normalize(vec3(1.0)), 1.0);
    gl_FragData[2] = vec4(var_position, displaced_point_height);
}