//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

precision highp float;

attribute vec3 geometry_position;

attribute vec2 position;

uniform vec4 value_mixing;

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;

uniform float displacement_scale;
uniform float displacement_offset;

uniform bool value_sizing_enabled;
uniform float value_sizing_zero;
uniform float value_sizing_one;
uniform float value_sizing_power;

varying vec3 var_position;
varying float displaced_point_height;
varying float var_value;
varying vec3 var_normal;

uniform float point_size;

uniform sampler2D stitched_displacement_map;
uniform vec2 stitched_displacement_coord_scale;
uniform vec2 stitched_displacement_coord_offset;

uniform sampler2D stitched_vector_map;
uniform vec2 stitched_vector_coord_scale;
uniform vec2 stitched_vector_coord_offset;

uniform sampler2D particle_pos;

uniform float source_offset;

#include "../include/world_projection.vs.glsl"
#include "../include/isnan.fs.glsl"
#include "../include/constants.glsl"

void main(){
    vec2 position_ = position / 2.0 + 0.5;

    position_ = texture2D(particle_pos, vec2((position_.x + source_offset) / 8.0, position_.y)).xy;

    var_position = vec3(position, 0.0);

    vec2 stitched_displacement_coord = (position_ - stitched_displacement_coord_offset) / stitched_displacement_coord_scale;
    vec2 stitched_vector_coord = (position_ - stitched_vector_coord_offset) / stitched_vector_coord_scale;

    displaced_point_height = texture2D(stitched_displacement_map, stitched_displacement_coord).r;

    vec2 value = texture2D(stitched_vector_map, stitched_vector_coord).rg;

    float value_abs = length(value);
    var_value = value_abs;
    vec2 value_dir = normalize(value);
    if(isnan(value.x) || isnan(value.y)){
        gl_Position = vec4(0.0, 0.0, 2.0, 1.0);
    }else{
        displaced_point_height = (isnan(displaced_point_height) ? displacement_offset : ((displaced_point_height + displacement_offset) * displacement_scale)) / EARTH_RADIUS;
        vec3 normal = world_normal(position_);
        vec3 euclidian_position = (displaced_point_height + 1.0) * normal;
        float arrowscale;
        if(value_sizing_enabled){
            arrowscale = min(point_size, point_size * pow(
                (value_abs - value_sizing_zero) / (value_sizing_one - value_sizing_zero),
                value_sizing_power
            ));
        }else{   
            arrowscale = max(1.0, point_size);
        }
        vec3 tangent = world_tangent(position_.xy);
        vec3 cotangent = world_cotangent(position_.xy);
        vec3 fz = tangent * value_dir.y + cotangent * value_dir.x;
        vec3 fx = tangent * value_dir.x - cotangent * value_dir.y;
        vec3 fy = normal;
        vec3 transformed_geometry_position = geometry_position.x * fx + geometry_position.y * fy + geometry_position.z * fz;
        transformed_geometry_position /= EARTH_RADIUS / arrowscale;
        gl_Position = projectionMatrix * viewMatrix * vec4(transformed_geometry_position + euclidian_position, 1.0);// + euclidian_position * arrowscale, 1.0);
    }
}