//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

precision highp float;

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;

uniform vec2 tile_coord;
uniform vec2 tile_size;

varying vec2 data0_coord;
uniform vec2 data0_coord_offset;
uniform vec2 data0_coord_scale;

varying vec2 data1_coord;
uniform vec2 data1_coord_offset;
uniform vec2 data1_coord_scale;

varying vec2 data2_coord;
uniform vec2 data2_coord_offset;
uniform vec2 data2_coord_scale;

varying vec2 data3_coord;
uniform vec2 data3_coord_offset;
uniform vec2 data3_coord_scale;

varying vec2 stitched_displacement_coord;
uniform sampler2D stitched_displacement_map;
uniform vec2 stitched_displacement_coord_scale;
uniform vec2 stitched_displacement_coord_offset;

varying vec2 displacement_coord;
uniform vec2 displacement_coord_offset;
uniform vec2 displacement_coord_scale;

varying float displaced_geometry_height;
uniform float displacement_scale;
uniform float displacement_offset;
uniform bool displacement_active;

attribute vec2 position;
varying vec2 world_position;

varying vec3 undisplaced_geo_normal;
//"sine" direction, V
varying vec3 undisplaced_geo_tangent;
//"cosine" direction, U
varying vec3 undisplaced_geo_cotangent;

#include "../include/world_projection.vs.glsl"
#include "../include/constants.glsl"

void main(){
    vec2 position_ = position / 2.0 + 0.5;
    
    data0_coord = position_ * data0_coord_scale + data0_coord_offset;
    data1_coord = position_ * data1_coord_scale + data1_coord_offset; 
    data2_coord = position_ * data2_coord_scale + data2_coord_offset; 
    data3_coord = position_ * data3_coord_scale + data3_coord_offset; 
    displacement_coord = position_ * displacement_coord_scale + displacement_coord_offset; 
    stitched_displacement_coord = position_ * stitched_displacement_coord_scale + stitched_displacement_coord_offset;
#if defined SPHERE
    position_ = position_ * tile_size + tile_coord;
    world_position = position_;
#elif defined EQUIRECT
    world_position = position_ * tile_size + tile_coord;
#elif defined POLAR
    position_ = position_ * tile_size + tile_coord;
    world_position = position_;
#endif

    if(displacement_active)
        displaced_geometry_height = texture2D(stitched_displacement_map, stitched_displacement_coord).r;
    else
        displaced_geometry_height = 0.0;
    float displacement_height = (displaced_geometry_height + displacement_offset) * displacement_scale / EARTH_RADIUS;

    vec3 euclidian_position = world_normal(world_position);
    undisplaced_geo_normal = euclidian_position;
    undisplaced_geo_tangent = world_tangent(world_position);
    undisplaced_geo_cotangent = world_cotangent(world_position);
    gl_Position = world_projection(vec3(position_.xy, displacement_height), viewMatrix, projectionMatrix);
}