//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

#extension GL_EXT_draw_buffers : require 
precision highp float;

varying vec2 data0_coord;
uniform sampler2D data0_map;
uniform bool data0_active;
uniform mat4 data0_mixing;

varying vec2 data1_coord;
uniform sampler2D data1_map;
uniform bool data1_active;
uniform mat4 data1_mixing;

varying vec2 data2_coord;
uniform sampler2D data2_map;
uniform bool data2_active;
uniform mat4 data2_mixing;

varying vec2 data3_coord;
uniform sampler2D data3_map;
uniform bool data3_active;
uniform mat4 data3_mixing;

varying vec2 displacement_coord;
uniform sampler2D displacement_map;
uniform float displacement_scale;
uniform float displacement_offset;
uniform bool displacement_active;

varying vec2 stitched_displacement_coord;

varying float displaced_geometry_height;

uniform float out0_bias;
uniform float out1_bias;
uniform float out2_bias;
uniform float out3_bias;

uniform vec2 tile_size;
uniform vec2 displacement_coord_scale;

varying vec2 world_position;
varying vec3 undisplaced_geo_normal;
varying vec3 undisplaced_geo_tangent;
varying vec3 undisplaced_geo_cotangent;

//TODO FIXME magic number
const float BUMPMAPPING_DELTA = 1.0 / 512.0;

#include "../include/isnan.fs.glsl"
#include "../include/constants.glsl"

void main() {
    vec4 value = vec4(out0_bias, out1_bias, out2_bias, out3_bias);
    if(data0_active)
        value += data0_mixing * texture2D(data0_map, data0_coord);
    if(data1_active)
        value += data1_mixing * texture2D(data1_map, data1_coord);
    if(data2_active)
        value += data2_mixing * texture2D(data2_map, data2_coord);
    if(data3_active)
        value += data3_mixing * texture2D(data3_map, data3_coord);
    if(displacement_active && (stitched_displacement_coord.x > 1.0 || stitched_displacement_coord.x < 0.0 || stitched_displacement_coord.y < 0.0 || stitched_displacement_coord.y > 1.0))
        discard;
    if(isnan(value.r) || isnan(value.g) || isnan(value.b) || isnan(value.a))
        discard;
    
    vec3 normal = undisplaced_geo_normal;
    float heightmap_height = 0.0;
    if(displacement_active){
        vec2 displacement_coord_2 = displacement_coord * (1.0 - 2.0 * BUMPMAPPING_DELTA);
        float x0y0 = texture2D(displacement_map, displacement_coord_2 + BUMPMAPPING_DELTA * vec2(0.5, 0.5)).r;
        float x1y0 = texture2D(displacement_map, displacement_coord_2 + BUMPMAPPING_DELTA * vec2(1.5, 0.5)).r;
        float x0y1 = texture2D(displacement_map, displacement_coord_2 + BUMPMAPPING_DELTA * vec2(0.5, 1.5)).r;
        float x1y1 = texture2D(displacement_map, displacement_coord_2 + BUMPMAPPING_DELTA * vec2(1.5, 1.5)).r;

        float dz_dx = ((x1y0 + x1y1) - (x0y0 + x0y1)) / 2.0
            / (BUMPMAPPING_DELTA * radians(360.0) * sin(radians(180.0) * world_position.y))
            / tile_size.x * displacement_coord_scale.x;
        float dz_dy = ((x0y0 + x1y0) - (x0y1 + x1y1)) / 2.0 
            / (BUMPMAPPING_DELTA * radians(180.0))
            / tile_size.y * displacement_coord_scale.y;

        vec2 dz = vec2(dz_dx, dz_dy) * displacement_scale / EARTH_RADIUS;

        normal -= dz.x * undisplaced_geo_cotangent;
        normal -= dz.y * undisplaced_geo_tangent;
        heightmap_height = (x0y0 + x1y0 + x0y1 + x1y1) / 4.0;
    }

    gl_FragData[0] = value;
    gl_FragData[1] = vec4(normalize(normal), 1.0);
    gl_FragData[2] = vec4(world_position, heightmap_height, displaced_geometry_height);
}