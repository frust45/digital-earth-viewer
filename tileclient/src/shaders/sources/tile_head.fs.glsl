//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

#extension GL_EXT_draw_buffers : require 
precision highp float;

varying vec2 data0_coord;
uniform sampler2D data0_map;
uniform bool data0_active;
uniform mat4 data0_mixing;

varying vec2 data1_coord;
uniform sampler2D data1_map;
uniform bool data1_active;
uniform mat4 data1_mixing;

varying vec2 data2_coord;
uniform sampler2D data2_map;
uniform bool data2_active;
uniform mat4 data2_mixing;

varying vec2 data3_coord;
uniform sampler2D data3_map;
uniform bool data3_active;
uniform mat4 data3_mixing;

varying vec2 displacement_coord;
uniform sampler2D displacement_map;
uniform float displacement_scale;
uniform float displacement_offset;
uniform bool displacement_active;

varying float displaced_geometry_height;

uniform float out0_bias;
uniform float out1_bias;
uniform float out2_bias;
uniform float out3_bias;

uniform vec2 tile_size;
uniform vec2 displacement_coord_scale;

varying vec2 world_position;
varying vec3 undisplaced_geo_normal;
varying vec3 undisplaced_geo_tangent;
varying vec3 undisplaced_geo_cotangent;