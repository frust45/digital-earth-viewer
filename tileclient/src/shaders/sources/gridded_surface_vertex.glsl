//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

attribute vec2 position;

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;

uniform sampler2D height_map;
uniform vec2 latlon_corner;
uniform vec2 latlon_size;
uniform vec2 coord_offset_albedo;
uniform vec2 coord_scale_albedo;
uniform vec2 coord_offset_height;
uniform vec2 coord_scale_height;
uniform float height_scale;
uniform float offset;

varying vec2 var_uv_albedo;

bool isnan( float val )
{
  return ( val < 0.0 || 0.0 < val || val == 0.0 ) ? false : true;
  // important: some nVidias failed to cope with version below.
  // Probably wrong optimization.
  /*return ( val <= 0.0 || 0.0 <= val ) ? false : true;*/
}

void main() {
    //gl_Position = vec4(position, 0.0, 1.0);
    vec2 np = position / 2.0 + 0.5;
    var_uv_albedo = np * coord_scale_albedo + coord_offset_albedo;
    vec2 uv_height = np * coord_scale_height + coord_offset_height;
    vec2 position_global = np * latlon_size + latlon_corner;
    vec2 position_angle_global = vec2(
        (position_global.x + 0.5) * radians(360.0),
        -(position_global.y - 0.5) * radians(180.0)
    );
    float v = texture2D(height_map, uv_height).r;
    float h;
    if(!isnan(v)) h = 1.0 + (v + offset) * 2.0 / 12756320.0 * height_scale;
    else h = 1.0 + offset * 2.0 / 12756320.0 * height_scale;
    gl_Position = projectionMatrix * viewMatrix * vec4(
        cos(position_angle_global.x) * cos(position_angle_global.y) * h,
        sin(position_angle_global.x) * cos(position_angle_global.y) * h,
        sin(position_angle_global.y) * h,
        1.0
    );
}