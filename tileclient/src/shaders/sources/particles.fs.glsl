//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

#extension GL_EXT_draw_buffers : require 
precision highp float;

varying vec3 var_position;
varying float displaced_geometry_height;
varying float var_value;

#include "../include/isnan.fs.glsl"

void main() {
    vec2 coord = gl_PointCoord * 2.0 - 1.0;
    float r = length(coord);
    if(r > 1.0)discard;
    vec3 normal = vec3(coord, sqrt(1.0 - r * r));

    gl_FragData[0] = vec4(var_value, 1.0, 0.0, 0.0);
    gl_FragData[1] = vec4(normal, 1.0);
    gl_FragData[2] = vec4(var_position, displaced_geometry_height);
}