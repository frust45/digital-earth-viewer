//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

#extension GL_EXT_frag_depth : require
precision highp float;

varying vec2 screen_coordinate;

uniform vec3 light_direction;

uniform float shading_intensity;
uniform float shading_falloff;
uniform float brightness;

uniform sampler2D value_map;
uniform sampler2D depth_map;
uniform sampler2D normal_map;
uniform sampler2D worldPosition_map;

#include "../include/isnan.fs.glsl"
#include "../include/lambert.glsl"

void main() {
    vec4 value = texture2D(value_map, screen_coordinate);

    if(isnan(value.r) || isnan(value.g) || isnan(value.b) || isnan(value.a) || value.a == 0.0)
        discard;
    else{
        vec3 normal = texture2D(normal_map, screen_coordinate).xyz;
        float lambert = lambert_shading(normal, normalize(light_direction));
        float shading = 1.0 + (pow(lambert, pow(10.0, shading_falloff)) - 1.0) * shading_intensity;
        gl_FragColor = vec4(value.rgb * shading * brightness, value.a);
        gl_FragDepthEXT = texture2D(depth_map, screen_coordinate).r;
    }
}