//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

attribute vec2 position;

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;

uniform vec3 u_dir;
uniform vec3 v_dir;
uniform vec3 p0;
uniform float fac;

varying float z;

bool isnan( float val )
{
  return ( val < 0.0 || 0.0 < val || val == 0.0 ) ? false : true;
  // important: some nVidias failed to cope with version below.
  // Probably wrong optimization.
  /*return ( val <= 0.0 || 0.0 <= val ) ? false : true;*/
}

mat4 transpose(in highp mat4 inMatrix) {
    highp vec4 i0 = inMatrix[0];
    highp vec4 i1 = inMatrix[1];
    highp vec4 i2 = inMatrix[2];
    highp vec4 i3 = inMatrix[3];

    highp mat4 outMatrix = mat4(
                 vec4(i0.x, i1.x, i2.x, i3.x),
                 vec4(i0.y, i1.y, i2.y, i3.y),
                 vec4(i0.z, i1.z, i2.z, i3.z),
                 vec4(i0.w, i1.w, i2.w, i3.w)
                 );

    return outMatrix;
}


void main() {
    //gl_Position = vec4(position, 0.0, 1.0);
    vec2 np = -position / 2.0 + 0.5;
    vec2 position_angle_global = vec2(
        (np.x + 0.5) * radians(360.0),
        np.y
    );
    float h = (position_angle_global.y * 0.25 + 0.95) * fac;
    mat4 pvmatrix = projectionMatrix * viewMatrix;
    vec4 p = vec4(
        (u_dir * (cos(position_angle_global.x) * h)) +
        (v_dir * (sin(position_angle_global.x) * h)) + p0,
        1.0
    );
    z = 1.0 - position_angle_global.y;
    gl_Position = pvmatrix * p;
}