//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

#extension GL_EXT_frag_depth : require
precision highp float;

varying vec2 screen_coordinate;

uniform sampler2D colormap;
uniform float colormap_min;
uniform float colormap_max;

uniform sampler2D value_map;
uniform sampler2D depth_map;
uniform sampler2D normal_map;
//uniform sampler2D worldPosition_map;

#include "../include/isnan.fs.glsl"

void main() {
    vec4 value = texture2D(value_map, screen_coordinate);

    float cm_val = (value.r - colormap_min) / (colormap_max - colormap_min); 

    if(isnan(cm_val))
        discard;
    else{
        vec3 normal = texture2D(normal_map, screen_coordinate).xyz;
        vec4 color = texture2D(colormap, vec2(cm_val, 0.5));
        gl_FragColor = vec4(color.rgb, color.a);
        gl_FragDepthEXT = texture2D(depth_map, screen_coordinate).r;
    }
}