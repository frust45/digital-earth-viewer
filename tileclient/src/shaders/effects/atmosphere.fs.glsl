//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

precision highp float;

varying float z;

void main() {
    const vec3 c1 = vec3(0.0, 0.6, 1.0);
    const vec3 c2 = vec3(0.5, 0.9, 1.0);
    gl_FragColor = vec4(mix(c1, c2, max(2.0 * z - 1.0, 0.0)), z); 
}