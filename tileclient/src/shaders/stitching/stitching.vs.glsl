//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

precision highp float;

attribute vec3 position;

uniform vec2 tile_coord;
uniform vec2 tile_size;

uniform vec2 texture0_coord_offset;
uniform vec2 texture0_coord_scale;
uniform vec2 texture1_coord_offset;
uniform vec2 texture1_coord_scale;
uniform vec2 texture2_coord_offset;
uniform vec2 texture2_coord_scale;
uniform vec2 texture3_coord_offset;
uniform vec2 texture3_coord_scale;

varying vec2 texture0_coord;
varying vec2 texture1_coord;
varying vec2 texture2_coord;
varying vec2 texture3_coord;

void main() {
    vec2 norm_position = (position.xy + 1.0) / 2.0;
    texture0_coord = norm_position * texture0_coord_scale + texture0_coord_offset;
    texture1_coord = norm_position * texture1_coord_scale + texture1_coord_offset;
    texture2_coord = norm_position * texture2_coord_scale + texture2_coord_offset;
    texture3_coord = norm_position * texture3_coord_scale + texture3_coord_offset;
    gl_Position = vec4((norm_position * tile_size + tile_coord) * 2.0 - 1.0, position.z, 1.0);
}