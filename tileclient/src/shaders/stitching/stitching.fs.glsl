//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

precision highp float;

uniform bool use_color;

uniform sampler2D texture0;
uniform sampler2D texture1;
uniform sampler2D texture2;
uniform sampler2D texture3;

uniform bool texture0_active;
uniform bool texture1_active;
uniform bool texture2_active;
uniform bool texture3_active;

varying vec2 texture0_coord;
varying vec2 texture1_coord;
varying vec2 texture2_coord;
varying vec2 texture3_coord;

uniform mat4 texture0_mixing;
uniform mat4 texture1_mixing;
uniform mat4 texture2_mixing;
uniform mat4 texture3_mixing;


void main() {
    vec4 value = vec4(0.0);
    if(texture0_active)
        value += texture0_mixing * texture2D(texture0, texture0_coord);
    if(texture1_active)
        value += texture1_mixing * texture2D(texture1, texture1_coord);
    if(texture2_active)
        value += texture2_mixing * texture2D(texture2, texture2_coord);
    if(texture3_active)
        value += texture3_mixing * texture2D(texture3, texture3_coord);
    gl_FragColor = value;
}