//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

var fs = require('fs');
var path = require('path');

let shaders_dir = __dirname;

let entries = {
    programs: {},
    vertex_shaders: {},
    fragment_shaders: {},
    shader_includes: {}
};

let resolve_includes_recursive = (p) => {
    let fi = fs.readFileSync(p,{encoding: "utf-8"}).toString();
    let matches = [...fi.matchAll(/#include\s+\"(.*)\".*/gm)];
    if(matches) matches.forEach(m => {
        let p_include = m[1];
        if(!path.isAbsolute(p_include)){
            p_include = path.join(path.dirname(p), p_include);
        }
        resolve_includes_recursive(p_include);
        
    });
    return fi;
}

let list_dir_recursive = (p) => {
    let local_entries = fs.readdirSync(p, {withFileTypes: true}).forEach(e => {
        let local_path = path.join(p, e.name);
        if(e.isFile()){
            let name_comps = path.basename(local_path).split(".");
            let p = path.relative(shaders_dir, local_path);
            if(name_comps.length==1)return;
            if(name_comps[1]=="prog"){

            }else if(name_comps[name_comps.length - 1]=="glsl"){
                if(name_comps[name_comps.length - 2]=="vs"){

                }else if(name_comps[name_comps.length - 2]=="fs"){

                }else{

                }
            }
            
        }else if(e.isDirectory()){
            list_dir_recursive(local_path);
        }
    });
}
fs.readdirSync(
    shaders_dir,
    {withFileTypes: true})
.filter(d => d.isDirectory())
.forEach(d => list_dir_recursive(path.join(shaders_dir, d.name)));
/*
let directories = fs.readdirSync(shaders_dir, {withFileTypes: true}).filter(d => d.isDirectory()).map(d => d.name);


let allowed_extensions = ["prog"];

let modules = {};

let resolve_includes_recursive = (p) => {
    let fi = fs.readFileSync(p,{encoding: "utf-8"}).toString();*/
    //let matches = [...fi.matchAll(/#include\s+\"(.*)\".*/gm)];
    /*if(matches) matches.forEach(m => {
        let p_include = m[1];
        if(!path.isAbsolute(p_include)){
            p_include = path.join(path.dirname(p), p_include);
        }
        let included_file = resolve_includes_recursive(p_include);
        fi = fi.replace(m[0], included_file + "\n");
    });
    return fi;
}

for(var dir of directories){
    let program_filenames = fs.readdirSync(path.join(shaders_dir, dir), {withFileTypes: true}).filter(f => f.isFile()).map(f => f.name).filter(f => allowed_extensions.some(ext => f.endsWith(ext)));
    
    let programs = {};


    for(var pf of program_filenames){
        pf = path.join(shaders_dir, dir, pf);
        console.info("Loading program " + pf);
        let lines = fs.readFileSync(pf, {encoding: "utf-8"}).toString().split("\n").map(l => l.trim());
        let cwd = path.dirname(pf);
        let vertex = lines.filter(f => f.endsWith("vs.glsl")).map(fn => `//@import ${fn}\n` + resolve_includes_recursive(path.join(cwd, fn))).join("\n\n");
        let fragment = lines.filter(f => f.endsWith("fs.glsl")).map(fn => `//@import ${fn}\n` + resolve_includes_recursive(path.join(cwd, fn))).join("\n\n");
        programs[path.basename(pf, path.extname(pf))] = {vertex, fragment};
    }

    modules[dir]  = programs;

}
*/
let header = "export const Shaders: {[module: string]: {[program_name: string]: {vertex: string, fragment: string}}} = ";

let body = JSON.stringify(modules,null, 3);

let footer = ";";

fs.writeFileSync(path.join(shaders_dir, "Shaders.ts"), header + body + footer,{encoding: "utf-8"});