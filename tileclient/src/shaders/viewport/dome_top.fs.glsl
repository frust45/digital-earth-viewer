precision highp float;

uniform float near_ratio;

varying vec2 screen_coordinate;

uniform sampler2D color_map;
uniform sampler2D depth_map;

#include "../include/depth_conversion.glsl"

void main(){
    vec2 uv = screen_coordinate;
    vec2 xy = normalize(uv) * tan(length(uv));
    if(xy.x <= 1.0 && xy.y <= 1.0 && xy.x >= -1.0 && xy.y >= -1.0){
        float raw_depth = texture2D(depth_map, xy / 2.0 + 0.5).r;
        float depth = depth_linear(raw_depth, near_ratio);
        float hypothenuse = length(vec3(xy, 1.0));
        if(depth * hypothenuse > 1.0){
            discard;
        } else {
            gl_FragColor = texture2D(color_map, xy / 2.0 + 0.5);
        }
    } else {
        //gl_FragColor = vec4(0.2, 0.0, 0.0, 1.0);
        discard;
    }
    
}