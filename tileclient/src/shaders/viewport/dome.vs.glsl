precision highp float;

attribute vec2 position;

uniform vec2 section_size;
uniform vec2 section_position;
uniform vec2 section_scale;
uniform float central_scale;

varying vec2 screen_coordinate;

void main() {
    vec2 position_corrected = position * section_size;

    screen_coordinate = (position_corrected + section_position) * section_scale * central_scale;
    gl_Position = vec4(position_corrected * section_scale + section_position, 0.0, 1.0);
}