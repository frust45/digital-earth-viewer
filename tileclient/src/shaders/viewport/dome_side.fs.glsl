precision highp float;

uniform float near_ratio;

varying vec2 screen_coordinate;

uniform sampler2D color_map;
uniform sampler2D depth_map;

uniform vec2 azimuth_direction;

uniform bool overflow_texture;

#include "../include/depth_conversion.glsl"

void main(){
    mat2 azimuth_rot = mat2(
         azimuth_direction.x, azimuth_direction.y,
        -azimuth_direction.y, azimuth_direction.x
    );

    vec2 uv = screen_coordinate * azimuth_rot;

    vec3 xyz = vec3(
        normalize(uv) * tan(length(uv)),
        1.0
    );
    vec2 xy = xyz.yz / xyz.x;
    
    if((/*overflow_texture ||*/ (xy.x <= 1.0 && xy.y <= 1.0 && xy.x >= -1.0)) && xy.y >= 0.0){
        vec2 tc = vec2(
            xy.x / 2.0 + 0.5,
            xy.y);
        float raw_depth = texture2D(depth_map, tc).r;
        float depth = depth_linear(raw_depth, near_ratio);
        float hypothenuse = length(vec3(xy, 1.0));
        if(depth * hypothenuse > 1.0){
            discard;
        } else {
            gl_FragColor = texture2D(color_map, tc);
        }
    } else {
        //gl_FragColor = vec4(0.0, 0.2, 0.0, 1.0);
        discard;
    }
    
}