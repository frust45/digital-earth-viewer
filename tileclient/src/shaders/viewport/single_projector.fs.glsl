precision highp float;

//tan of frustum side angles
uniform vec2 frustum_up_left;
uniform vec2 frustum_down_right;
//near/far
uniform float near_ratio;

//uniform vec4 clear_color;

varying vec2 screen_coordinate;

uniform sampler2D color_map;
uniform sampler2D depth_map;

#include "../include/depth_conversion.glsl"

void main(){
    vec2 far_plane_coord = frustum_up_left + screen_coordinate * (frustum_down_right - frustum_up_left);
    float raw_depth = texture2D(depth_map, screen_coordinate).r;
    float depth = depth_linear(raw_depth, near_ratio);
    float hypothenuse = length(vec3(far_plane_coord, 1.0));
    if(depth * hypothenuse > 1.0){
        discard;
    } else {
        gl_FragColor = texture2D(color_map, screen_coordinate);
    }
}