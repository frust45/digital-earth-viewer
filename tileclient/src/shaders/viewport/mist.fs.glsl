//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

precision highp float;

varying vec2 screen_coordinate;

uniform sampler2D color_map;
uniform sampler2D depth_map;

uniform float power;

void main(){
    vec4 scolor = texture2D(color_map, screen_coordinate);
    float d = pow(texture2D(depth_map, screen_coordinate).r, 0.5);
    gl_FragColor = vec4(d, d, d, 1.0);
}