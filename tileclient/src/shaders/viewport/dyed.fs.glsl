precision highp float;

varying vec2 screen_coordinate;

uniform sampler2D color_map;
uniform sampler2D depth_map;

uniform vec3 dye_color;

void main(){
    vec4 color = texture2D(color_map, screen_coordinate);
    float luma = (color.r * 0.2126) + (color.g * 0.7152) + (color.b * 0.0722);
    gl_FragColor =  vec4(dye_color * luma * 0.2 + dye_color * color.rgb * 0.8, color.a);
}