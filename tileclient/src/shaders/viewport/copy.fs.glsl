//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

precision highp float;

varying vec2 screen_coordinate;

uniform sampler2D color_map;
uniform sampler2D depth_map;

void main(){
    gl_FragColor = texture2D(color_map, screen_coordinate);
}