//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

precision highp float;

attribute vec2 position;

varying vec2 screen_coordinate;

void main() {
    screen_coordinate = (position + 1.0) / 2.0;
    gl_Position = vec4(position, 0.0, 1.0);
}