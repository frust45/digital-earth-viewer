//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

/* eslint-disable */
declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}
