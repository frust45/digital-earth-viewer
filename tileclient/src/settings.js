//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

export const viewer_settings = {
    threedee: {
        "tile_base_res": 128,
        "tile_tex_res": 512,
        "fov": 78,
        "controls": {
            "filter": 0.1,
            "scroll_zoom_fac": 1.1,
            "drag_move_fac": [0.1]
        },
        "default_pos": {
            lat: 54.32753,
            lon: 10.18327,
        },
    }
}