//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer
export function AwaitableDelay(ms: number): Promise<void>{
    return new Promise((res, _rej) => setTimeout(res, ms));
}