//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer

var fs = require('fs');
var path = require('path');

ggt = (a, b) => {
    a = Math.abs(a);
    b = Math.abs(b);
    while(b > 0){
        let h = a % b;
        a = b;
        b = h;
    }
    return a;
};

kgv = (a, b) => {
    let g = ggt(a, b);
    return a * b / g;
};

parse2rational = (s) => {
    let parts = s.split("e")
    let exp = 0;
    //check for "e+5" and similar parts
    if(parts.length > 1)
        exp = parseInt(parts[1]);
    parts = parts[0].split(".");
    let decs = 0;
    let numerator;
    //check for decimal point
    if(parts.length > 1) {
        decs = parts[1].length;
        numerator = parseInt(parts[0] + parts[1]);
    }
    else {
        numerator = parseInt(parts[0])
    }
    let denominator = Math.pow(10, decs);
    //scale denominator or numerator with positive powers of 10 (to keep them both as integers)
    if(exp < 0){
        denominator *= Math.pow(10, -exp);
    } else {
        numerator *= Math.pow(10, exp);
    }
    //return smaller representation
    let div = ggt(numerator, denominator);
    return [
        numerator / div,
        denominator / div
    ];
};

//subtraction of rationals
sub_rational = (a, b) => {
    let base = a[1] * b[1];
    let en = a[0] * b[1] - b[0] * a[1];
    let div = ggt(en, base);
    return [en / div, base / div];
};

//generate a 1d texture buffer for a well-aligned discrete colormap to be displayed with nearest-neigbour filtering
gen_arraybuffer_discrete = (segments) => {
    let denominator = 1;
    let enumerator = segments[0].p0[0];
    segments.forEach(l => {
        enumerator = ggt(enumerator, ggt(l.p0[0], l.p1[0]));
        denominator = kgv(denominator, kgv(l.p0[1], l.p1[1]));
    });
    let result = "new Uint8Array([";
    segments.forEach(s => {
        colstr = s.c0[0] + "," + s.c0[1] + "," + s.c0[2] + ","
        let starti = denominator * s.p0[0] / s.p0[1] / enumerator;
        let endi = denominator * s.p1[0] / s.p1[1] / enumerator;
        result += colstr.repeat(endi - starti);
    });
    result = result.slice(0, -1);
    result += "])";
    return result;
};
//generate a 1d texture buffer for a well-aligned blended colormap to be displayed with linear filtering
gen_arraybuffer_linear = (segments) => {
    let denominator = 1;
    let enumerator = segments[0].p0[0];
    segments.forEach(l => {
        enumerator = ggt(enumerator, ggt(l.p0[0], l.p1[0]));
        denominator = kgv(denominator, kgv(l.p0[1], l.p1[1]));
    });
    let result = "new Uint8Array([";
    segments.forEach(s => {
        let starti = denominator * s.p0[0] / s.p0[1] / enumerator;
        let endi = denominator * s.p1[0] / s.p1[1] / enumerator;
        for(var i = 0; i < endi - starti; i++){
            let vf = i / (endi - starti);
            let vf1 = 1 - vf;
            result += 
                parseInt(vf * s.c1[0] + vf1 * s.c0[0])
                + "," +
                parseInt(vf * s.c1[1] + vf1 * s.c0[1])
                + "," +
                parseInt(vf * s.c1[2] + vf1 * s.c0[2])
                + ","
        }
    });
    let lastc = segments[segments.length - 1].c1;
    result += lastc[0] + "," + lastc[1] + "," + lastc[2];
    result += "])";
    return result;
};
//generate a 1d texture buffer for a misaligned colormap to be displayed with linear filtering
gen_arraybuffer_compromise = (segments, points) => {
    let result = "new Uint8Array([";
    //convert all segments to float positions
    segments = segments.map(s => {
        return {
            p0: s.p0[0] / s.p0[1],
            p1: s.p1[0] / s.p1[1],
            c0: [s.c0[0], s.c0[1], s.c0[2]],
            c1: [s.c1[0], s.c1[1], s.c1[2]]
        };
    });
    let length = segments[segments.length - 1].p1;
    //for every texel, check contribution of all segments
    for(var i = 0; i < points; i++){
        let r0 = i * length / points;
        let r1 = (i + 1) * length / points;
        let l = points / length;
        let cr = 0;
        let cg = 0;
        let cb = 0;
        segments.forEach(s => {
            if(s.p1 > r0 && s.p0 < r1){
                let sl = s.p1 - s.p0;
                let pl0 = Math.max(s.p0, r0);
                let pl1 = Math.min(s.p1, r1);
                let f = (pl0 + pl1) / 2 / sl;
                cr += (f * s.c1[0] + (1 - f) * s.c0[0]) * (pl1 - pl0) * l;
                cg += (f * s.c1[1] + (1 - f) * s.c0[1]) * (pl1 - pl0) * l;
                cb += (f * s.c1[2] + (1 - f) * s.c0[2]) * (pl1 - pl0) * l;
            }
        });
        result += parseInt(cr) + "," + parseInt(cg) + "," + parseInt(cb) + ",";
    }
    result = result.slice(0, -1);
    result += "])";
    return result;
};

let parse_cpt = (s) => {
    let lines = s.split("\n").filter(l => !l.startsWith("#")).filter(l => l.trim().split(/\s+/).length == 8);
    lines = lines.map(l => {
        let segs = l.trim().split(/\s+/);
        return {
            "p0": parse2rational(segs[0]),
            "c0": [parseInt(segs[1]), parseInt(segs[2]), parseInt(segs[3])],
            "p1": parse2rational(segs[4]),
            "c1": [parseInt(segs[5]), parseInt(segs[6]), parseInt(segs[7])]
        };
    });
    return lines;
};

let calc_cpt_range = (lines) => [lines[0].p0[0] / lines[0].p0[1], lines[lines.length - 1].p1[0] / lines[lines.length - 1].p1[1]];

let gen_cpt_arraybuffer = (lines) => {
    let range = calc_cpt_range(lines);
    let refpoint = lines[0].p0;
    lines = lines.map(l => {
        let p0 = sub_rational(l.p0, refpoint);
        let p1 = sub_rational(l.p1, refpoint);
        l.p0 = p0;
        l.p1 = p1;
        return l;
    });
    let denominator = 1;
    let enumerator = lines[0].p0[0];
    lines.forEach(l => {
        enumerator = ggt(enumerator, ggt(l.p0[0], l.p1[0]));
        denominator = kgv(denominator, kgv(l.p0[1], l.p1[1]));
    });
    let required_points = denominator * lines[lines.length - 1].p1[0] / lines[lines.length - 1].p1[1] / enumerator;
    let is_discrete = lines.every(l => l.c0[0] == l.c1[0] && l.c0[1] == l.c1[1] && l.c0[2] == l.c1[2]);
    let is_continuous = true;
    for(var i = 0; i < lines.length - 2; i++){
        is_continuous = is_continuous && lines[i].c1[0] == lines[i + 1].c0[0] && lines[i].c1[1] == lines[i + 1].c0[1] && lines[i].c1[2] == lines[i + 1].c0[2];
    }
    let res;
    if(is_continuous && required_points < 512) {
        res = [gen_arraybuffer_linear(lines), false];
    } else if (is_discrete && required_points < 512) {
        res = [gen_arraybuffer_discrete(lines), true];
    } else {
        res = [gen_arraybuffer_compromise(lines, 256), false];
    }
    return res;
};

let format_percent = (val) => ((val * 100) + "").slice(0, 5) + "%";

let gen_cpt_c3g_string = (lines) => {
    //if(lines.length > 128){
    //    let tmp = gen_arraybuffer_compromise(lines, 129);
    //}else{
        let range = calc_cpt_range(lines);
        let res = "\"";
        let offs = range[0];
        let scale = range[1] - range[0];
        for(var i = 0; i < lines.length - 1; i++){
            let c0i = lines[i].c0;
            let p0i = ((lines[i].p0[0] / lines[i].p0[1]) - offs) / scale;
            res += `rgb(${parseInt(c0i[0])},${parseInt(c0i[1])},${parseInt(c0i[2])}) ${format_percent(p0i)},`;
            c1i = lines[i].c1;
            if(c0i[0] == c1i[0] && c0i[1] == c1i[1] && c0i[2] == c1i[2]){
                let p1i = ((lines[i].p1[0] / lines[i].p1[1]) - offs) / scale;
                res += `rgb(${parseInt(c1i[0])},${parseInt(c1i[1])},${parseInt(c1i[2])}) ${format_percent(p1i)},`;
            }
        }
        let cpl = lines[lines.length - 1].c0;
        let ppl = ((lines[lines.length - 1].p0[0] / lines[lines.length - 1].p0[1]) - offs) / scale;
        res += `rgb(${parseInt(cpl[0])},${parseInt(cpl[1])},${parseInt(cpl[2])}) ${format_percent(ppl)},`;
        let cl = lines[lines.length - 1].c1;
        res += `rgb(${parseInt(cl[0])},${parseInt(cl[1])},${parseInt(cl[2])}) 100%\"`;
        return res;
    //}
};

let colormaps_dir = __dirname;

let cpts = [];
let licenses = [];
fs.readdirSync(colormaps_dir, {withFileTypes: true}).filter(f => f.isFile()).forEach(f => {
    if(f.name.endsWith(".cpt")){
        cpts.push(f);
    }else if(f.name.endsWith("license")){
        licenses.push(f);
    }}); 
colormaps = {};
let body = [];
cpts.filter(
    f => licenses.findIndex(b => f.name.split(".").slice(0, -1).join(".") + ".license" == b.name) > -1
).forEach(f => {
    let license_name = path.join(colormaps_dir, f.name.split(".").slice(0, -1).join(".") + ".license");
    let cpt_name = path.join(colormaps_dir, f.name);
    let license_data = JSON.parse(fs.readFileSync(license_name, {"encoding": "utf-8"}));
    let cpt_data = parse_cpt(fs.readFileSync(cpt_name, {"encoding": "utf-8"}));
    let range_actual = calc_cpt_range(cpt_data);
    let [arraybuffer_constructor, interpolation_style] = gen_cpt_arraybuffer(cpt_data);
    let c3g_string = gen_cpt_c3g_string(cpt_data);
    body.push(
`{
    name: ${JSON.stringify(license_data.Name)},
    author: {
        name: ${JSON.stringify(license_data.Author.Name)},
        url: ${license_data.Author.Link ? JSON.stringify(license_data.Author.Link) : "null"}
    },
    license: {
        name: ${JSON.stringify(license_data.License.Name)},
        url: ${license_data.License.Link ? JSON.stringify(license_data.License.Link) : "null"}
    },
    range: [${range_actual[0]}, ${range_actual[1]}],
    symmetric: ${license_data.Symmetric ? "true" : "false"},
    c3g_string: ${c3g_string},
    tex_buffer: ${arraybuffer_constructor},
    linear_interpolation: ${interpolation_style ? "false" : "true"}
}`
    );
    console.log(`Parsed Colormap "${license_data.Name}".`)
});
let header = "//This file is licensed under EUPL v1.2 as part of the Digital Earth Viewer\n\nexport const colormaps = [\n"
let footer = "\n];";
fs.writeFileSync(path.join(colormaps_dir, "colormap.js"), header + body.join(",\n") + footer,{encoding: "utf-8"});